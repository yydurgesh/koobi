
function ajax_fun()
   {
       $.ajax({

            url: '/deviceGraph',
            type: "GET", 
            data:{'gender':$('#gender').val(),'ageFilter':$('#dageFilter').val()} ,                 
            cache: false,
            success: function(data){

                    $("#dGRaph").val(data.dGRaph);
                    dGRaph = JSON.parse($('#dGRaph').val());
                      $("#deviceGraph").dxChart({
                          dataSource: dGRaph,
                           equalBarWidth:true,
                          palette: "soft",
                          commonSeriesSettings: {
                              argumentField: "state",
                              type: "bar"
                          },
                          series: [
                              { valueField: "Biz", name: "Biz Side", stack: "Biz Side", color:"#3c8dbc"  },
                              { valueField: "Social", name: "Social Side", stack: "Social Side",color:"#f83272" },
                              { valueField: "Total", name: "Total", stack: "Total", color : "#90A4AE"  }
                          ],
                          legend: {
                              verticalAlignment: "bottom",
                              horizontalAlignment: "center",
                              itemTextPosition: 'top'
                          },
                          valueAxis: {
                              title: {
                                  text: ""
                              },
                              position: "left"
                          },
                          "export": {
                              enabled: true
                          },
                          tooltip: {
                              enabled: true,
                              location: "edge",
                              customizeTooltip: function (arg) {
                                  return {
                                      text: arg.seriesName + " : " + arg.valueText
                                  };
                              }
                          }
                      });



            }

        });

       

    }ajax_fun();


    function gender_filter()
   {
       $.ajax({

            url: '/downloadGraph',
            type: "GET", 
            data:{'gender':$('#agender').val(),'ageFilter':$('#AageFilter').val()} ,                 
            cache: false,
            success: function(data){
                $("#aGRaph").val(data.aGRaph);
                 aGRaph = JSON.parse($('#aGRaph').val());
                $("#downloadGraph").dxChart({
                        dataSource: aGRaph,
                         equalBarWidth:true,
                        palette: "soft",
                        commonSeriesSettings: {
                            argumentField: "state",
                            type: "bar"
                        },
                        series: [
                             { valueField: "Ios", name: "Ios", stack: "Ios", color:"#3c8dbc"  },
                            { valueField: "Android", name: "Android", stack: "Android",color:"#f83272" },
                            { valueField: "Total", name: "Total", stack: "Total", color : "#90A4AE"  }
                        ],
                        legend: {
                            verticalAlignment: "bottom",
                            horizontalAlignment: "center",
                            itemTextPosition: 'top'
                        },
                        valueAxis: {
                            title: {
                                text: ""
                            },
                            position: "left"
                        },
                        "export": {
                            enabled: true
                        },
                        tooltip: {
                            enabled: true,
                            location: "edge",
                            customizeTooltip: function (arg) {
                                return {
                                    text: arg.seriesName + " : " + arg.valueText
                                };
                            }
                        }
                });



            }

        });

       

    }gender_filter();




    function service_gender_filter()
   {
       $.ajax({

            url: '/artistGraph',
            type: "GET", 
            data:{'gender':$('#agender').val(),'ageFilter':$('#ageFilter').val()} ,                 
            cache: false,
            success: function(data){

                $("#sGRaph").val(data.sGRaph);
                sGRaph = JSON.parse($('#sGRaph').val());

                $("#artistchart").dxChart({
                    dataSource: sGRaph,
                     equalBarWidth:true,
                    palette: "soft",
                    commonSeriesSettings: {
                        argumentField: "state",
                        type: "bar"
                    },
                    series: [
                         { valueField: "Independent", name: "Independent", stack: "Independent", color:"#3c8dbc"  },
                        { valueField: "Business", name: "Business", stack: "Business",color:"#f83272" },
                        { valueField: "tservice", name: "Total service provider", stack: "Total service provider", color : "#90A4AE"  }
                    ],
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: 'top'
                    },
                    valueAxis: {
                        title: {
                            text: ""
                        },
                        position: "left"
                    },
                    "export": {
                        enabled: true
                    },
                    tooltip: {
                        enabled: true,
                        location: "edge",
                        customizeTooltip: function (arg) {
                            return {
                                text: arg.seriesName + " : " + arg.valueText
                            };
                        }
                    }
                });



            }

        });

       

    }service_gender_filter();


     function booking_gender_filter()
   {
       $.ajax({

            url: '/bookingGraph',
            type: "GET", 
            data:{'gender':$('#bgender').val(),'serviceFilter':$('#serviceFilter').val()} ,                 
            cache: false,
            success: function(data){

                $("#bGRaph").val(data.bGRaph);
                bGRaph = JSON.parse($('#bGRaph').val());
                
                $("#booking").dxChart({
                    dataSource: bGRaph,
                     equalBarWidth:true,
                    palette: "soft",
                    commonSeriesSettings: {
                        argumentField: "state",
                        type: "bar"
                    },
                    series: [
                         { valueField: "Confirm", name: "Confirm Booking", stack: "Confirm Booking", color:"#00a65a"  },
                        { valueField: "Complete", name: "Complete Booking", stack: "Complete Booking",color:"#3c8dbc" },
                        { valueField: "Total", name: "Total Booking", stack: "Total Booking", color : "#f39c12"  }
                    ],
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center",
                        itemTextPosition: 'top'
                    },
                    valueAxis: {
                        title: {
                            text: ""
                        },
                        position: "left"
                    },
                    "export": {
                        enabled: true
                    },
                    tooltip: {
                        enabled: true,
                        location: "edge",
                        customizeTooltip: function (arg) {
                            return {
                                text: arg.seriesName + " : " + arg.valueText
                            };
                        }
                    }
                });



            }

        });

       

    }booking_gender_filter();

      function user_gender_filter()
   {
       $.ajax({

            url: '/userGraph',
            type: "GET", 
            data:{'ageFilter':$('#uageFilter').val()} ,                 
            cache: false,
            success: function(data){

                $("#ggg").val(data.uGRaph);
                uGRaph = JSON.parse($('#ggg').val());
                $("#chart").dxChart({
                  dataSource: uGRaph,
                   equalBarWidth:true,
                  palette: "soft",
                  commonSeriesSettings: {
                      argumentField: "state",
                      type: "bar"
                  },
                  series: [
                       { valueField: "male", name: "Male", stack: "male", color:"#3c8dbc"  },
                      { valueField: "female", name: "Female", stack: "female",color:"#f83272" },
                      { valueField: "tUser", name: "Total User", stack: "Total User", color : "#90A4AE"  }
                  ],
                  legend: {
                      verticalAlignment: "bottom",
                      horizontalAlignment: "center",
                      itemTextPosition: 'top'
                  },
                  valueAxis: {
                      title: {
                          text: ""
                      },
                      position: "left"
                  },
                  "export": {
                      enabled: true
                  },
                  tooltip: {
                      enabled: true,
                      location: "edge",
                      customizeTooltip: function (arg) {
                          return {
                              text: arg.seriesName + " : " + arg.valueText
                          };
                      }
                  }
              });



            }

        });

       

    }user_gender_filter();

