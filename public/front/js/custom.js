// $(window).on('load', function() {
//     // PRELOADER
//     $(".preloader").fadeOut(500);

// });
$(document).ready(function(){
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $(".navbar").addClass("navbarFixed");
    } else {
        $(".navbar").removeClass("navbarFixed");
    }
});


$('.owl-carouselNew').owlCarousel({
    loop: true,
    responsiveClass: true,
    nav: true,
    dots: false,
    margin: 5,
    autoplay: false,
    autoplayTimeout: 4000,
    smartSpeed: 500,
    center: true,
    navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
    responsive: {
        0: {
            items: 1,
        },
        640: {
            items: 2,
        },
        767: {
            items: 2
        },
        1200: {
            items: 4
        }
    }
});


    $('.single_screen_slide').owlCarousel({
        loop: true,
        margin: 0,
        dots: false,
        autoplay: false,
        autoplayTimeout: 4000,
        smartSpeed: 1000,
        mouseDrag: true,
        touchDrag: true,
        center: true,
        items: 1,
    });


// $('.single_screen_slide').on('translate.owl.carousel', function(property) {
//     $('.home_screen_slide_main .owl-dot:eq(' + property.page.index + ')').click();
// });


$('.mainMenu li a[href^="#"]').on('click',function (e) {
    // e.preventDefault();
    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top - 60
    }, 900, 'swing', function () {
     window.location.hash = target;
    });
});

});


$(document).ready(function(){
    if (screen.width < 767){
        $(".collapse .hideMenuSM").css({"display": "none"});
        $(".navbar .mobileMenuView").css({"display": "block"});
        $(".navbar-brand").addClass("mobileLogo");
    }
});
$(document).ready(function(){
  $(".menuOpenBtn").click(function(){
    $("#mobileMenuShow").slideToggle();
  });
});


// $(document).ready(function() {
//     var showChar = 250;  // How many characters are shown by default
//     var ellipsestext = "...";
//     var moretext = "Show more";
//     var lesstext = "Show less";

//     $('.more').each(function() {
//         var content = $(this).html();
//         if(content.length > showChar) {
 
//             var c = content.substr(0, showChar);
//             var h = content.substr(showChar, content.length - showChar);
 
//             var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
//             $(this).html(html);
//         }
 
//     });
 
//     $(".morelink").click(function(){
//         if($(this).hasClass("less")) {
//             $(this).removeClass("less");
//             $(this).html(moretext);
//         } else {
//             $(this).addClass("less");
//             $(this).html(lesstext);
//         }
//         $(this).parent().prev().toggle();
//         $(this).prev().toggle();
//         return false;
//     });
// });