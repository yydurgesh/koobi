var User = require('../app/models/front/home.js');
var appUser = require('../app/controllers/api/user.js');
var firebase = require("firebase");
const FCM = require('fcm-node');
var constanPath = require('../config/envConstants.js');
const fcm = new FCM(constanPath.FIREBASE_SERVER_KEY);
 var config = {
 apiKey: constanPath.API_KEY,
 authDomain: constanPath.AUTH_DOMAIN,
 databaseURL:constanPath.DATABASE_URL,
 projectId: constanPath.PROJECT_ID,
 storageBucket:constanPath.STORAGE_BUCKET,
 messagingSenderId:constanPath.MESSAGE_SENDER_ID,
 gcm_sender_id :constanPath.GCM_SENDER_ID
 }; // Live

/*var config = {
    apiKey: "AIzaSyD_uX3hbydbD0BEhcaMqKlOs0xQH7jf1FM",
    authDomain: "koobdevelopment.firebaseapp.com",
    databaseURL: "https://koobdevelopment.firebaseio.com",
    projectId: "koobdevelopment",
    storageBucket: "",
    messagingSenderId: "696055690698",
    gcm_sender_id : "103953800507"
};*///Development

/*var config = {
    apiKey: "AIzaSyDhPcvN0liBX24VqG0tQ38WjODxELozeQQ",
    authDomain: "mualablocal.firebaseapp.com",
    databaseURL: "https://mualablocal.firebaseio.com",
    projectId: "mualablocal",
    storageBucket: "mualablocal.appspot.com",
    messagingSenderId: "714386763021"
  };*///local

notifications = 'webnotification';
users = 'users';

firebase.initializeApp(config);
 
exports.sendNotification = function(token, notification, data,receiverId,type="socialBookingBadgeCount"){


    firebase.database().ref().child(type).child(receiverId).once('value',function(rdata){

        var badge = (rdata.val()) ? Number(rdata.val().totalCount)+Number(1) : 1;
        notification.badge =  badge.toString();
        data.badge =  badge.toString();
        var message = {
        	      
            to: token,
            collapse_key: 'your_collapse_key',
            delay_while_idle : false,
            priority : "high", 
            content_available: true,
            notification: notification,		        
            data: data,
            mutable_content: true,
            category : '',
            badge : badge.toString()
        };
        console.log(message);

        fcm.send(message, function(err, response){
           
           console.log(err);
           console.log(response);
            if (err) 
            	return err;           
            else 
               return response;
            

        });
    });
}

exports.sendWebNotification = function(userId, data){


 newPostKey = firebase.database().ref().child(notifications).child(userId).push().key;
 if(newPostKey ){
 
 var updates = {};
 updates[newPostKey] = data;
 firebase.database().ref().child(notifications).child(userId).update(updates);

 }

 return true;

}
/*this function use for get sender and receiver info. */
exports.notificationUser = function(senderId,receiverId,type,notifyId,notifyType,serviceName='',paymentType='',startDate='',endDate=''){
    
   
    userArr   =  [];
    userArr   =  [senderId,receiverId];
    User.find({'_id':{ $in:userArr}},{'_id':1,'userName':1,'businessName':1,'userType':1,'firebaseToken':1,'deviceType':1,'profileImage':1,'appType':1}).exec(function(err,userData){
        if(userData){
          
            appUser.sendNotification(senderId,receiverId,type,userData,notifyId,notifyType,serviceName,paymentType,startDate,endDate);
        }
         
        
    });    
}

/*code for send multiple notification*/

exports.sendNotificationMultiple = function(token, notification, data){
   var message = {
              
                registration_ids: token,
                collapse_key: 'your_collapse_key',
                delay_while_idle : false,
                priority : "high", 
                content_available: true,
                notification: notification,             
                data: data,
                category : '',
                mutable_content: true,
                badge : 1
    };
   
    fcm.send(message, function(err, response){
       
     console.log(err);  
     console.log("///////////////////////");  
     console.log(response);  
        if (err) 
            return err;           
        else 
           return response;
           


    });
}


exports.bedgeCount = function(receiverId,notifyType,type,notificationType='0'){

      newData=[];

    if(notificationType=='17' && notificationType!='0'){

        firebase.database().ref().child('socialBookingBadgeCount').child(receiverId).once('value',function(rdata){

            if(rdata.val()){

              var businessInvitationCount = rdata.val().businessInvitationCount ? Number(rdata.val().businessInvitationCount)+Number(1) : 1;
              firebase.database().ref('socialBookingBadgeCount').child(receiverId).child('businessInvitationCount').set(businessInvitationCount);  

            }else{

               firebase.database().ref('socialBookingBadgeCount').child(receiverId).child('businessInvitationCount').set(1);  
            }
        });
    }

    if(notifyType=='booking'){
         firebase.database().ref().child(type).child(receiverId).once('value',function(rdata){
           if(rdata.val()){

              var incbook             = rdata.val().bookingCount ? Number(rdata.val().bookingCount)+Number(1) : 1;
              var socialCount         = rdata.val().socialCount ? rdata.val().socialCount : 0;
              var businessInvitationCount = rdata.val().businessInvitationCount ? rdata.val().businessInvitationCount : 0;
              var chatCount           = rdata.val().chatCount ? rdata.val().chatCount : 0;
              var commissionCount     = rdata.val().commissionCount ? rdata.val().commissionCount : 0;

              newData.socialCount     = socialCount;
              newData.chatCount       = chatCount;
              newData.bookingCount    = incbook ;
              newData.commissionCount    = commissionCount ;
              newData.businessInvitationCount    = businessInvitationCount ;
              newData.totalCount      = rdata.val().totalCount ? Number(rdata.val().totalCount)+Number(1) : 1;;

              firebase.database().ref(type).child(receiverId).set(newData);  
              return newData.totalCount;

           }else{

              newData.socialCount  = 0;
              newData.bookingCount = 1 ; 
              newData.chatCount           = 0 ;
              newData.businessInvitationCount = 0 ; 
              newData.commissionCount = 0 ; 
              newData.totalCount   = 1 ; 
              firebase.database().ref(type).child(receiverId).set(newData); 
              return newData.totalCount;
           }
        });

    }

    if(notifyType=='commissionCount'){

        firebase.database().ref().child(type).child(receiverId).once('value',function(rdata){
           if(rdata.val()){

              var commissionCount = rdata.val().commissionCount ? Number(rdata.val().commissionCount)+Number(1) : 1;
              var bookingCount = rdata.val().bookingCount ? rdata.val().bookingCount : 0;
              var incsocal = rdata.val().incsocal ? rdata.val().incsocal : 0;
              var businessInvitationCount = rdata.val().businessInvitationCount ? rdata.val().businessInvitationCount : 0;
              var chatCount = rdata.val().chatCount ? rdata.val().chatCount : 0;


              newData.commissionCount         = commissionCount;
              newData.socialCount         = incsocal;
              newData.bookingCount        = bookingCount;
              newData.chatCount           = chatCount;
              newData.businessInvitationCount           = businessInvitationCount;
              newData.totalCount          = rdata.val().totalCount ? Number(rdata.val().totalCount)+Number(1) : 1;

              firebase.database().ref(type).child(receiverId).set(newData);  
              return newData.totalCount;

           }else{

              newData.commissionCount     = 1 ;
              newData.socialCount         = 0 ;
              newData.businessInvitationCount         = 0 ;
              newData.chatCount           = 0 ;
              newData.bookingCount        = 0 ;
              newData.totalCount          = 1 ;
              firebase.database().ref(type).child(receiverId).set(newData); 
              return newData.totalCount;

           }
        });

    }

    if(notifyType=='social'){

        firebase.database().ref().child(type).child(receiverId).once('value',function(rdata){
           if(rdata.val()){

              var incsocal = rdata.val().socialCount ? Number(rdata.val().socialCount)+Number(1) : 1;
              var bookingCount = rdata.val().bookingCount ? rdata.val().bookingCount : 0;
              var commissionCount = rdata.val().commissionCount ? rdata.val().commissionCount : 0;
              var businessInvitationCount = rdata.val().bookingCount ? rdata.val().businessInvitationCount : 0;
              var chatCount = rdata.val().chatCount ? rdata.val().chatCount : 0;


              newData.socialCount         = incsocal;
              newData.bookingCount        = bookingCount;
              newData.chatCount           = chatCount;
              newData.businessInvitationCount           = businessInvitationCount;
              newData.commissionCount     = commissionCount;
              newData.totalCount          = rdata.val().totalCount ? Number(rdata.val().totalCount)+Number(1) : 1;

              firebase.database().ref(type).child(receiverId).set(newData);  
              return newData.totalCount;

           }else{

              newData.socialCount         = 1 ;
              newData.chatCount           = 0 ;
              newData.bookingCount        = 0 ;
              newData.businessInvitationCount        = 0 ;
              newData.commissionCount        = 0 ;
              newData.totalCount          = 1 ;
              firebase.database().ref(type).child(receiverId).set(newData); 
              return newData.totalCount;

           }
        });

    }

}
/* login and register firebase*/


exports.register = function(userId, data){

  data.uId = Number(userId);
    firebase.database().ref().child('users').child(userId).once('value',function(rdata){

            if(rdata.val()){
                
                banAdmin = rdata.val().banAdmin ? rdata.val().banAdmin : 0;
            
            }else{

                banAdmin = 0;
            }

            data.banAdmin = banAdmin;    
            firebase.database().ref('users').child(userId).set(data);
            firebase.database().ref().child('myGroup').child(userId).on('child_added',function(ddata){

                var s = ddata.val();
                firebase.database().ref('group').child(s).child('member').child(userId).child('firebaseToken').set(data.firebaseToken);
                firebase.database().ref('group').child(s).child('member').child(userId).child('profilePic').set(data.profilePic);
                firebase.database().ref('group').child(s).child('member').child(userId).child('userName').set(data.userName);

                return true;

            });
        
    });
}


exports.followunFollow = function(data){
    
    
    firebase.database().ref('follow').child(data.userId).child(data.id).set(data);   
    return true;
}

exports.unFollow = function(data){
    
    
    firebase.database().ref('follow').child(data.userId).child(data.id).set(null);   
    return true;
}

exports.timeConvert =  function(otime){
  
      var ohours = Number(otime.match(/^(\d+)/)[1]);
      var ominutes = Number(otime.match(/:(\d+)/)[1]);
      var AMPM = otime.match(/\s(.*)$/)[1];
      if(AMPM == "PM" && ohours<12) ohours = ohours+12;
      if(AMPM == "AM" && ohours==12) ohours = ohours-12;
      var osHours = ohours.toString();
      var osMinutes = ominutes.toString();
      if(ohours<10) osHours = "0" + osHours;
      if(ominutes<10) osMinutes = "0" + osMinutes;
     return osHours + ":" + osMinutes;
}
exports.socialBookingBadgeCount = function(data){
     newData=[];
     //newData.bookingCount = 2;
     //newData.socialCount =5;
     firebase.database().ref().child('socialBookingBadgeCount').child(7).once('value',function(rdata){
       if(rdata.val()){
         console.log(rdata.val().bookingCount);
         var incbook = rdata.val().bookingCount+1;
         newData.bookingCount = incbook;
          newData.socialCount = 10;
          firebase.database().ref('socialBookingBadgeCount').child(7).set(newData);  
          return true;
       }
    });
}

exports.blockUserGet = function(req, res,next){

    userId = req.body.userId ?  authData._id : req.body.userId;
    data = [];
    bdata = [];
    firebase.database().ref().child('blockUserArtistWeb').child(userId).once('value',function(rdata){
        if(rdata.val()){

          sdata = rdata.val();
            rdata.forEach(function(item) {

          //    data.push(Number(item.val()));
              bdata.push(Number(item.val()));

            });

        }
        next();
    });
}





