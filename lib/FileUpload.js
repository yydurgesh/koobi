'use strict';
const appRoot = require('app-root-path');
const fs      = require('fs');
const uniqid  = require('uniqid');
const commonConstants = require('../config/commanConstants');
const S3 = require("./aws-s3");

/**
 * File Upload Library
 */
module.exports = class FileUpload {
    /**
     * Upload File.
     *
     * @param  {Object} fileObject
     * @param  {String} folder
     * @returns {Boolean}
     */
    uploadFile(file, uploadFolder, newName, thumboptions={}) {
        let classRef = this;
        let localUploadPath = commonConstants.STORAGE_PATH+uploadFolder+newName;
        let s3UploadPath = uploadFolder+newName;
        if(Object.keys(thumboptions).length){
            thumboptions.thumbFolder = thumboptions.thumbFolder+newName;
        }
        return new Promise ((resolve, reject) => {
            fs.readFile(file.path , function(err,data) {
                if (err) {
                    reject(err);
                }
                fs.writeFile(localUploadPath, data, function(err) {
                    if (err) {
                        reject(err);
                    }

                    S3.upload(localUploadPath, s3UploadPath, thumboptions)
                    .then(r => {
                      classRef.unlinkFile(localUploadPath)
                      resolve(true);
                    })
                    .catch(e => {
                      console.error(e);
                      reject(e);
                    });                    
                })
            })
        });
    }

     /**
     * File unlink.
     *
     * @param  {Object} fileObject
     * @param  {String} folder
     * @returns {Boolean}
     */
    unlinkFile(flocalUploadPath) {
        fs.unlink(flocalUploadPath, (err) => {
          if (err) {
            console.error(err)
            return
          }
        })
    }

}
