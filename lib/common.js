var sizeOf          = require('image-size');
exports.parseTime = function(s) {

 var c = s.split(':');
 return parseInt(c[0]) * 60 + parseInt(c[1]);

 }
exports.timeConvert = function(otime){
  
      var ohours = Number(otime.match(/^(\d+)/)[1]);
      var ominutes = Number(otime.match(/:(\d+)/)[1]);
      var AMPM = otime.match(/\s(.*)$/)[1];
      if(AMPM == "PM" && ohours<12) ohours = ohours+12;
      if(AMPM == "AM" && ohours==12) ohours = ohours-12;
      var osHours = ohours.toString();
      var osMinutes = ominutes.toString();
      if(ohours<10) osHours = "0" + osHours;
      if(ominutes<10) osMinutes = "0" + osMinutes;
     return osHours + ":" + osMinutes;
}

exports.final_time_slot =  function(times_ara, serviceTime){

    if(serviceTime){
     serviceTime =    common.parseTimeService(serviceTime);
    }
    i=0;
    var s = serviceTime*10;

  
    Ntime_slots = Array();
    times_ara.forEach(function(rs) {

        var end = Number(i)+Number(serviceTime);
        
        var end_time = times_ara[end];
         
        var currentSlot = rs;
        var end = common.timeDiffrence(currentSlot,end_time); 
 
        if(s==end){
            Ntime_slots.push(rs);
        }

    i++;    
    });

    return Ntime_slots;

}

exports.timeDiffrence =  function(start,end){

    var day = '1 1 1970 '
    diff_in_min = ( Date.parse(day + end) - Date.parse(day + start) )/ 1000 / 60;
    return diff_in_min;


}

exports.calculate_time_slot =  function(start_time, end_time, interval = "10", day, bussy_slot, AbookingTime,curentTime,AbookingDate,newDate){

      var i, formatted_time;
      var a=0;
      var time_slots = new Array();
      start_time.forEach(function(rs) {

            for(var i=rs; i<=end_time[a]; i = i+interval){

                  formatted_time = common.convertHours(i);
                   
                  if(common.currentDay()==day){

                    
                      
                      if(curentTime<i){
                     
                        
                          if(AbookingTime){
                                 
                              if(AbookingTime<=i){

                                  time_slots.push(common.formatAMPM(formatted_time));
                              }

                          }else{
                                                    

                              time_slots.push(common.formatAMPM(formatted_time));
                          }



                      }


                  }else if(AbookingDate==newDate){

                      if(AbookingTime<=i){

                          time_slots.push(common.formatAMPM(formatted_time));
                      }


                  }else{

                      time_slots.push(common.formatAMPM(formatted_time));

                  }

              }

            a++;});

      time_slots = time_slots.filter(function(val) {
      return bussy_slot.indexOf(val) == -1;
      });
      return time_slots;

}



exports.currentDay = function() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    //return today = dd + '-' + mm + '-' + yyyy;
    return today = yyyy + '-' + mm + '-' + dd;

   
}

exports.bookingTime =  function(start_time,end_time,interval){

    var i, formatted_time;
    var a=0;
    var time_slots = new Array();
    start_time.forEach(function(rs) {


        for(var i=rs; i<end_time[a]; i = i+interval){

            formatted_time = common.convertHours(i);

            time_slots.push(common.formatAMPM(formatted_time));


        }

    a++;});
    return time_slots;


}
/*22-10-20*/

exports.bookingStartTime =  function(start_time){

    var i, formatted_time;
    var a=0;
    var time_slots = new Array();
    start_time.forEach(function(rs) {
      time_slots.push(common.formatAMPM(common.convertHours(rs)));
    a++;});
    return time_slots;


}
/*end*/

exports.convertHours =  function(mins){
    var hour = Math.floor(mins/60);
     var mins = mins%60; 
    var converted = common.pad(hour, 2)+':'+common.pad(mins, 2);
     return converted;

}

exports.pad =  function(str, max) {

      str = str.toString();
      return str.length < max ? common.pad("0" + str, max) : str;

}

exports.formatAMPM =  function(otime) {

    var ohours = Number(otime.match(/^(\d+)/)[1]);
    var ominutes = Number(otime.match(/:(\d+)/)[1])
    var hours   = ohours;
    var minutes = ominutes;
    var ampm    = hours >= 12 ? 'PM' : 'AM';
    hours       = hours % 12;
    hours       = hours ? hours : 12; // the hour '0' should be '12'
    minutes     = minutes < 10 ? '0'+minutes : minutes;
    hours       = hours<10 ? "0"+hours : hours;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;

}

exports.parseTimeService = function(s) {

  var c = s.split(':');
  return parseInt(c[0]) * 60/10 + parseInt(c[1])/10;

}


exports.jsUcfirst = function(string) {

    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

}


exports.titleCase = function(str) {
    
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}

exports.escapeRegExp = function(str) {
    
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");

}
/*
 * @Purpose: This function is responsible to calculate image height 
 *           to make thumb according to ratio
 * @Parmas: imagePath (Contains image path)
 *
*/
 exports.calculateFeedImageThunmbHeight = function(imagePath){

    let dimensions = sizeOf(imagePath);
    let height;
    if(dimensions.height > dimensions.width){
        height = 600;
    }else if(dimensions.height < dimensions.width){
        height = 360;
    }else{
        height = 480;
    }
    return height;
}


exports.milesToRadian = function(miles){
    var earthRadiusInMiles = 3959;
    return miles / earthRadiusInMiles;
};

exports.milesToMeters = function(miles){
    var earthMetersInMiles = 1609.344;
    return miles * earthMetersInMiles;
};


