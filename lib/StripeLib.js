const Stripe  = require("stripe");
var constanPath = require('../config/envConstants.js');


/**
 *  Stripe Payment Gateway Library
 */
class StripeLib {

    constructor() {
        this.stripe = new Stripe(constanPath.STRIPE_KEY,{
                          apiVersion: '2020-03-02',
                      });

      
      }
    createCustomer(customerEmailAddress) {
        return this.stripe.customers.create({
          email: customerEmailAddress,
        })
        .then(customer => {
            return customer.id;
        })
        .catch(error => {
           // logger.error( error );
            //throw Boom.badRequest( error );
            return error;
        });
    }

    makePayment(cardToken, price, currency, description, metadata){
        let payObject = {
            amount: parseFloat(price)*100,
            currency: currency,
            card: cardToken, 
            description: description,
            metadata: metadata,
            description: 'Software development services',
            shipping: {
                name: 'Jenny Rosen',
                address: {
                  line1: '510 Townsend St',
                  postal_code: '98140',
                  city: 'San Francisco',
                  state: 'CA',
                  country: 'US',
                }
          }
        }
        return this.stripe.charges.create(payObject)
        .then(charge => {
            let res = {
                "status":true,
                "data": charge
            }
            return res;
        })
        .catch(error => {
            console.log('error', error);
            let res = {
                "status":false,
                "data": error
            }
            return res;
        });
    }

    refundByChargeID(chargeID){
        let refundObject = {
            charge: chargeID
          }
        return this.stripe.refunds.create(refundObject)
        .then(charge => {
            let res = {
                "status":true,
                "data": charge
            }
            return res;
        })
        .catch(error => {
            let res = {
                "status":false,
                "data": error
            }
            return res;
        });
    }

    // create account 

    createAccount( firstName, lastName, customerEmailAddress ){

        const createObj = {
            "country": "GB",
            "type": "custom",
            "requested_capabilities": [ "card_payments", "transfers" ],
            "business_type": constanPath.BUSINESS_TYPE,
            "business_profile": {
                "mcc": constanPath.MCC,
                "product_description": constanPath.PRODUCT_DESCRIPTION
            },
            "individual": {
                "email": customerEmailAddress,
                "first_name": firstName,
                "last_name": lastName
            }
        };
            
        if( customerEmailAddress !== "" && customerEmailAddress != null ) {
            createObj.email = customerEmailAddress;
        }
        return this.stripe.accounts.create( createObj )
        .then(account => {
            return account.id;
        })
        .catch(error => {
       
            return error;
        });
   }
       // retrive  account 

    retrieveAccount(accountId){
        return this.stripe.accounts.retrieve(accountId)
        .then(account => {
            return account;
        })
        .catch(error => {
       
            return error;
        });
   }
    // create account link for connect account


    createAccountLink( account_id, type ) {
        return this.stripe.accountLinks.create({
          account: account_id,
          failure_url: 'https://dev.koobi.co.uk/fail',
          success_url: 'https://dev.koobi.co.uk/success',
          type: type,
          collect: "currently_due"
        })
        .then(accountLink => {
            
            return accountLink;
        })
        .catch(error => {
            return error;
          //  logger.error( error );
            //throw Boom.badRequest( error );
            //{external_account: "btok_gb"}

        });
    }
    updateAccount(account_id, account_number, routing_number, currency, country ){
        return this.stripe.accounts.update( account_id,
           { external_account: {
               object:'bank_account',
               country: country,
               currency: currency,
               routing_number: routing_number,
               account_number: account_number  
            }}
        )
        .then(bankAccount => {
            
            return bankAccount;
        })
        .catch(error => {
            return error;
          //  logger.error( error );
            //throw Boom.badRequest( error );
        });



    }
    createBankAccount(account_id, account_number, routing_number, currency, country ){
        return this.stripe.accounts.createExternalAccount( account_id,
        { external_account: {
               object:'bank_account',
               country: country,
               currency: currency,
               routing_number: routing_number,
               account_number: account_number  
            }}
         )
        .then(bankAccount => {
            
            return bankAccount;
        })
        .catch(error => {
            return error;
          //  logger.error( error );
            //throw Boom.badRequest( error );
        });
    }
}
module.exports = new StripeLib();
