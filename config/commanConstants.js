/**
 * Common Constants
 *
 * @package                Koobi
 * @subpackage             Common Constants
 * @category               Constants
 * @DateOfCreation         19 Sep 2019
 * @ShortDescription       This is responsible for common constants
 */
var appRoot = require('app-root-path');

module.exports = {
    STORAGE_PATH: `${appRoot}/public/uploads/`,
    FEED_MEDIA_UPLOAD_FOLDEFR: 'feeds/',
    PROFILE_MEDIA_UPLOAD_FOLDEFR: 'profile/',
    FEED_THUMB_FOLDER: 'thumbs/',
    THUMB_FOLDER: 'thumbs/',
    CERTIFICATE_MEDIA_UPLOAD_FOLDEFR: 'certificateImage/',
    COMMENT_MEDIA_UPLOAD_FOLDEFR: 'commentImage/',
    MYSTORY_MEDIA_UPLOAD_FOLDEFR: 'myStory/',
    SUBSERVICE_MEDIA_UPLOAD_FOLDEFR: 'subservice/',
    S3_BASE_PATH:process.env.S3_BASE_PATH
};
