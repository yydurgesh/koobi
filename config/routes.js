var adminHome = require('../app/controllers/admin/home');
var adminCategory = require('../app/controllers/admin/category');
var adminSubCategory = require('../app/controllers/admin/subcategory');
var adminuser = require('../app/controllers/admin/userList');
var adminReport = require('../app/controllers/admin/report_ctrl');


var frontHome = require('../app/controllers/front/home');
var User = require('../app/controllers/front/user');
var login = require('../app/controllers/front/login');
var booking = require('../app/controllers/front/booking');
var explore = require('../app/controllers/front/explore');
var payment = require('../app/controllers/front/payment');
var frontService = require('../app/controllers/front/service');
var fregister = require('../app/controllers/admin/firebaseregister');
var stripe = require('../app/controllers/front/stripe');
var staff = require('../app/controllers/front/staff');
var feed = require('../app/controllers/front/feed');
var bookingHistory = require('../app/controllers/front/bookingHistory');
var S3 = require("../lib/aws-s3");

//Api //

var artist          =   require('../app/controllers/api/artist');
var user            =   require('../app/controllers/api/user');
var appUser         =   require('../app/controllers/api/user');
var service         =   require('../app/controllers/api/service_controller');
var authtokenCheck  =   require('../app/controllers/api/validateRequest');
var folder          =   require('../app/controllers/api/folder_ctrl');
var comment         =   require('../app/controllers/api/comment_ctrl');
var feed_ctrl       =   require('../app/controllers/api/feed_ctrl');
var category_ctrl   =   require('../app/controllers/api/category_ctrl');
var staff_ctrl      =   require('../app/controllers/api/staff_ctrl');
var certificate_ctrl=   require('../app/controllers/api/certificate_ctrl');
var voucher_ctrl    =   require('../app/controllers/api/voucher_ctrl');
var booking_ctrl    =   require('../app/controllers/api/booking_ctrl');
var notify          =   require('../lib/notification.js');

//Api//
//you can include all your controllers
 /* Admin module start*/

module.exports = function (app, passport) {

    app.get('/admin/login', adminHome.login);
    app.get('/signup', adminHome.signup);
    app.get('/fregister', fregister.fregister);
    app.get('/logout', adminHome.logout);
   //comment 07-20web flow app.get('/',frontHome.loggedInuserData,login.profileCheck,booking.locationGet,booking.categoryGet,booking.searchbooking);    
    app.get('/feedDetail/:id',frontHome.loggedInuserData,login.profileCheck,booking.locationGet,booking.categoryGet,booking.searchbooking);    
    // comment web flow app.get('/home',frontHome.loggedInuserData,frontHome.loggedIn,login.profileCheck,booking.locationGet,booking.categoryGet,booking.searchbooking);
    app.get('/',frontHome.koobiBiz);
    app.get('/biz',frontHome.bizHome);
    
    
     /*code for voucher02-05-19*/
    app.get('/voucherDetail/:id',frontHome.loggedInuserData,login.profileCheck,booking.locationGet,booking.categoryGet,booking.searchbooking);

    app.get('/adminDashboard', adminHome.loggedIn, adminHome.categoryCount, adminHome.subcategoryCount, adminHome.userCount, adminHome.artistCount, adminHome.graphUsers, adminHome.graphartist, adminHome.graphBooking, adminHome.graphDownload, adminHome.bookedServices, adminHome.graphdDevice, adminHome.home);//home
    app.get('/adminProfile', adminHome.loggedIn, adminHome.profile);//admin profile
    app.get('/deviceGraph', adminHome.graphdDevice, adminHome.deviceVGraph);
    app.get('/downloadGraph', adminHome.graphDownload, adminHome.downloadGraph);
    app.get('/artistGraph', adminHome.graphartist, adminHome.artistGraph);
    app.get('/bookingGraph', adminHome.graphBooking, adminHome.bookingGraph);
    app.get('/userGraph', adminHome.graphUsers, adminHome.userGraph);

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/adminDashboard', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    // process the login form

    app.post('/admin/login', passport.authenticate('local-login', {
        
        successRedirect: '/adminDashboard', // redirect to the secure profile section
        failureRedirect: '/admin/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages

    }));

  
    /* Category module start*/

    app.get('/addCategory',adminCategory.loggedIn, adminCategory.addCategory);
    app.post('/createCategory', adminCategory.loggedIn , adminCategory.insertCategory);
    app.get('/listCategory',adminCategory.loggedIn, adminCategory.listCategory);
    app.get('/categoryList', adminCategory.categoryList);
    app.get('/categoryEdit/:categoryId',adminCategory.loggedIn, adminCategory.edit);
    app.get('/categoryStatus/:categoryId/:status',adminCategory.loggedIn, adminCategory.categoryStatus);
    app.post('/updateCategory', adminCategory.loggedIn , adminCategory.updateCategory);
    app.get('/categoryDelete/:categoryId',adminCategory.loggedIn, adminCategory.categoryDelete);
    app.get('/listUserCategory',adminCategory.loggedIn, adminCategory.listUserCategory);
    app.get('/userCategoryList', adminCategory.userCategoryList);
    app.get('/categoryApprove/:categoryId/:status',adminCategory.loggedIn, adminCategory.categoryApprove);
    /* Category module end*/


    /* Sub Category module start*/

    app.get('/addSubCategory',adminCategory.loggedIn, adminSubCategory.addSubCategory);
    app.post('/createSubCategory', adminCategory.loggedIn , adminSubCategory.insertSubCategory);
    app.get('/listSubCategory',adminCategory.loggedIn, adminSubCategory.listsubCategory);
    app.get('/subcategoryList', adminSubCategory.subcategoryList);
    app.get('/categorySubEdit/:categoryId',adminCategory.loggedIn, adminSubCategory.edit);
    app.get('/subServicesStatus/:categoryId/:status',adminCategory.loggedIn, adminSubCategory.subServicesStatus);
    app.post('/updateSubCategory', adminCategory.loggedIn , adminSubCategory.updateSubCategory);
    app.get('/categorySubDelete/:categoryId',adminCategory.loggedIn, adminSubCategory.categoryDelete);
    app.get('/listUserSubCategory',adminCategory.loggedIn, adminSubCategory.listUserSubCategory);
    app.get('/userSubcategoryList', adminSubCategory.userSubcategoryList);
    app.get('/subCategoryApprove/:categoryId/:businessTypeId',adminCategory.loggedIn, adminSubCategory.subCategoryApprove);
    /* Sub Category module end*/

      /* userList and artist module start*/

    app.get('/artist',adminCategory.loggedIn, adminuser.artistList);
    app.get('/listArtist', adminuser.listartist);
    app.get('/artistStatus/:categoryId/:status',adminCategory.loggedIn, adminuser.artistStatus);
    app.get('/artistview/:id',adminCategory.loggedIn, adminuser.artistview);
    app.get('/artistServicesList',adminuser.artistServicesList);

    app.get('/customer',adminCategory.loggedIn, adminuser.userList);
    app.get('/listcutomer', adminuser.listuser);
    app.get('/customerStatus/:categoryId/:status',adminCategory.loggedIn, adminuser.userStatus);
    app.get('/customerview/:id',adminCategory.loggedIn, adminuser.customerview);
    app.get('/certificateUpdate/:id/:status/:artistId',adminCategory.loggedIn, adminuser.certificateUpdate);
    app.get('/admin_staff_List',adminCategory.loggedIn,adminuser.staff_List_data,adminuser.company_List_data,adminuser.staff_List);
    app.get('/admin_booking_list',adminCategory.loggedIn,adminuser.booking_data,adminuser.booking_list);    
    app.get('/admin_followers_list',adminCategory.loggedIn,adminuser.followersData,adminuser.following_list);
    app.get('/admin_following_list',adminCategory.loggedIn,adminuser.followingData,adminuser.following_list);
    app.get('/bookingview/:id',adminCategory.loggedIn,booking.artistservicesList,booking.bookingInfoData,adminuser.admin_staff_List_data,adminReport.bookingReport,adminuser.bookingview);
    app.get('/invoice_print/:id',adminCategory.loggedIn,booking.artistservicesList,booking.bookingInfoData,adminuser.admin_staff_List_data,adminuser.invoice_print);
    app.get('/admin_payment_history',adminCategory.loggedIn, adminuser.payment_history);
    app.get('/payment_history_list',adminCategory.loggedIn,adminuser.payment_list);
    app.get('/total_services',adminCategory.loggedIn, adminSubCategory.total_services);
    app.get('/total_services_list',adminCategory.loggedIn,adminHome.bookedServices,adminSubCategory.total_services_list);
    app.get('/staffview/:id/:businessType',adminCategory.loggedIn, adminuser.staff_Info, adminuser.staffview);
    app.get('/staffServicesAdmin',adminuser.artistServicesListData,adminuser.staffServiceList);
    app.get('/admin_group_report',adminCategory.loggedIn,adminuser.admin_group_report);
    app.get('/admin_voucher_list',adminCategory.loggedIn,adminuser.admin_voucher_data,adminuser.admin_voucher_list);
    app.get('/voucherStatus/:categoryId/:status/:artistId',adminCategory.loggedIn, adminuser.voucherStatus);
    app.get('/admin_feed_report',adminCategory.loggedIn,adminuser.admin_feed_report);
    app.get('/admin_booking_report',adminCategory.loggedIn, adminReport.reportList);
    app.get('/listReport', adminReport.listReport);
    app.post('/createReport',adminCategory.loggedIn,adminReport.reportDetail,adminReport.createReport);
    app.post('/user_profile_update',adminuser.userProfileUpdate);
    app.post('/makePayment',adminuser.makePayment);
    app.get('/admin_booking_reminder',adminCategory.loggedIn, adminHome.admin_booking_reminder);
    app.get('/admin_user_report',adminCategory.loggedIn, adminReport.admin_user_report);
    app.get('/listUserReport', adminReport.listUserReport);
    app.post('/reminderUpdate',adminHome.reminderUpdate);
    /* userList and artist end*/

    /* profile update  and change password module start*/
    
    app.post('/admin_profile_update', adminCategory.loggedIn , adminHome.admin_profile_update);
    app.post('/admin_changepassword', adminCategory.loggedIn , adminHome.admin_changepassword);

    /* profile update  and change password module start*/




    /* Front module start*/
    // code for strip responce
     app.get('/success', login.stripSuccess);
     app.get('/fail', login.stripFail);
    // end code

    app.get('/frontHome',login.profileCheck, frontHome.home);
    app.get('/login', login.login);
    app.get('/successMsg', login.successMsg);
    app.get('/invalid_link', login.invalid_link);
    app.get('/businessLogin', login.businesslogin);
    app.get('/businessRegister', login.businessRegister);
    app.post('/phoneVerification', login.phoneVerification, login.sendSmsdata);
    app.get('/register', login.register);
    app.get('/userProfile',frontHome.loggedIn,login.profileCheck, frontHome.userProfile);
    app.get('/myProfile',frontHome.loggedIn,login.profileCheck,frontHome.certificateCount, frontHome.myProfile);
    app.get('/my_certificate',frontHome.loggedIn,login.profileCheck, frontHome.certificateCount, frontHome.artist_my_certificate);
    app.get('/emailCheck',login.emailCheck);
    app.get('/userLogout', frontHome.userLogout);
    app.post('/profileUpdate',frontHome.loggedIn, frontHome.userprofileUpdate);
    app.post('/certificate_upload',frontHome.certificate_upload);
    app.get('/removecertificate',frontHome.removecertificate);
    app.post('/socialRegister',login.socialRegister);
    app.post('/userRegister',login.UserSignup);
    app.post('/businessRegister',login.businessSignup);
    app.post('/userLogin',login.userLogin);
    app.post('/forgotPassword',login.forgotPassword,login.sendMail);
    app.get('/businessHours',frontHome.loggedIn , User.businessHours);
    app.get('/subCategoryAdd',frontHome.loggedIn,User.serviceCount,User.categorydata, User.subCategoryAdd);
    app.get('/addsubservices',frontHome.loggedIn,User.subservicesList,User.addsubservices);
    app.get('/updatesubservices',frontHome.loggedIn,User.updatesubservices);
    app.post('/update_workingTime',User.update_workingTime);
    app.post('/servicesAdd',User.serviceCount,User.AddServices);
    app.get('/registerStep3',frontHome.loggedIn, User.registerStep3);
    app.post('/certificateAdd',User.certificate_upload);
    app.post('/addBackAccount',User.addBackAccount);
    app.get('/skipstep3',frontHome.loggedIn,User.skipstep3);
    app.get('/artistDashboard',frontHome.loggedIn,login.profileCheck,frontHome.artistdashboard);
    app.get('/explore',frontHome.loggedIn,login.profileCheck,explore.explore);
    app.get('/bookinghistory',frontHome.loggedIn,login.profileCheck,staff.staff_List_data,staff.company_List_data,booking.bookinghistory);    
    app.post('/stripe',stripe.stripeaddAccount);
    app.get('/searchResult',booking.categoryGet,booking.searchResult);
    app.post('/search_artist',booking.search_artistdata,booking.search_artist);
    app.post('/get_sub_category',booking.get_sub_category);
    app.post('/home_search_artist',booking.home_search_artist,booking.home_search_artist_result);
    app.post('/get_sub_service',booking.get_sub_service);
    app.get('/booking_detial/:id/:distance',frontHome.loggedInuserData,booking.locationGet,booking.userDetail,booking.Artistcategorydata,booking.booking_detial);
    app.get('/booking/:id/:distance',frontHome.loggedInuserData,booking.loginBookingUpdate,booking.userDetail,booking.Artistcategorydata,booking.booking);
    app.get('/artistsubservices',booking.artistsubservicesList,booking.artistsubservices);
    app.get('/artistStaff',booking.artistData,booking.artistStaff);
    app.get('/artistslot',booking.artistslot);
    app.get('/ArtistServiceDetail',booking.ArtistServiceDetail);
    app.get('/bookingServiceDetail',booking.bookingServiceDetail);
    app.post('/serviceBookingAdd',booking.serviceBookingAdd);
    app.post('/finalBooking',booking.finalBooking);
    app.post('/bookingUpdate',booking.bookingInfoData,booking.bookingUpdate);
    app.get('/artistFreeSlot',booking.artistFreeSlot);
    app.post('/artistbookingHistory',booking.artistservicesList,staff.company_List_data,booking.pendingBooking,booking.completeBooking,staff.staff_List_data,staff.company_List_data,staff.independArtistListData,booking.artistbookingHistory);    
    app.get('/artistmainsubservices',booking.Artistcategorydata,booking.artistmainsubservices);
    app.get('/bookingRemove',booking.bookingRemove);
    app.get('/bookingInfo/:id',frontHome.loggedIn,booking.artistservicesList,booking.bookingInfoData,staff.staff_List_data,staff.independArtistListData,staff.company_List_data,booking.bookingInfo);   
    app.get('/staffManagement',frontHome.loggedIn,login.profileCheck,staff.staffManagement);    
    app.post('/staffArtistList',frontHome.loggedIn,staff.independArtistList,staff.staffArtistList);    
    app.get('/add_staff/:id',frontHome.loggedIn,login.profileCheck,booking.userDetail,staff.artistCategoryData,staff.bussinessHoursGet,staff.staff_Info,staff.add_staff);
    app.get('/get_artistservices',staff.get_artistservices);    
    app.get('/get_artistsubservices',staff.get_artistsubservices);    
    app.post('/staffServiceAdd',staff.staffServiceAdd); 
    app.get('/ArtistStaffServiceDetail',staff.staffServiceDetail,staff.ArtistServiceDetail);
    app.get('/staffserviceList',booking.artistservicesList,staff.artistCategoryData,staff.staffServiceList);
    app.get('/removestaffservice',staff.removestaffservice);
    app.post('/staff_add',staff.staff_add);
    app.post('/staff_List',booking.artistservicesList,staff.staff_List_data,staff.company_List_data,staff.staff_List);
    app.get('/staffdetail',booking.staffdetail);
    app.post('/staffUpdate',booking.staffUpdate);
    app.post('/changstaff',staff.staff_List_data_service, staff.changstaff);
    app.get('/delete_staff',frontHome.loggedIn,staff.delete_staff);
    app.get('/profile',frontHome.loggedInuserData,frontHome.certificateCount , frontHome.myProfile);
    app.get('/aboutUs',frontHome.loggedInuserData,frontHome.certificateCount,frontHome.aboutUs);
    app.get('/following',frontHome.loggedInuserData,frontHome.certificateCount,frontHome.following);
    app.get('/followers',frontHome.loggedInuserData,frontHome.certificateCount,frontHome.followers);
    app.get('/following_list',frontHome.certificateCount,frontHome.followrsCheckData,frontHome.followingData,frontHome.following_list);
    app.get('/followers_list',frontHome.certificateCount,frontHome.followrsCheckData,frontHome.followersData,frontHome.following_list);
    app.post('/followUnfollow',frontHome.loggedIn,frontHome.followUnfollow);
    app.post('/artistFavorite',frontHome.loggedIn,frontHome.artistFavorite);
    app.post('/faveroite_list',booking.faveroite_list,booking.faveroite_list_artist,booking.faveroite_list_result);
    app.get('/my_services',frontHome.loggedInuserData,frontHome.certificateCount,frontService.servicesdata,frontService.serviceManagement);
    app.get('/feed_List',frontHome.certificateCount,feed.isLikeCheck,feed.feedData,feed.feed);
    app.get('/feed_image_List',frontHome.certificateCount,feed.isLikeCheck,feed.feedData,feed.feed_image_List);
    app.get('/allBookinghistory',frontHome.loggedIn,login.profileCheck,bookingHistory.allBookinghistory);    
    app.post('/artistAllBookingHistory',frontHome.loggedIn,booking.artistservicesList,bookingHistory.futureBooking,bookingHistory.pastBooking,bookingHistory.artistAllBookingHistory);    
    app.get('/payment',frontHome.loggedIn,stripe.bookingInfoData,stripe.payment);
    app.get('/bankPayment',frontHome.loggedIn,stripe.bookingInfoData12,stripe.bookingArtistInfo,stripe.bankPayment);
    app.get('/cardPayment',frontHome.loggedIn,stripe.bookingInfoData12,stripe.bookingArtistInfo,stripe.cardPayment);
    app.get('/postRatingReview',booking.bookingInfoData,bookingHistory.postRatingReview);
    app.post('/authTokenCheck',frontHome.loggedIn,frontHome.authTokenCheck);
    app.get('/paymenthistory',frontHome.loggedIn,login.profileCheck,payment.paymenthistory);
    app.get('/payment_list',booking.artistservicesList,staff.independArtistListData,payment.paymentList_data,payment.payment_list);
    app.get('/chat/',frontHome.loggedIn,frontHome.loggedInuserData,frontHome.certificateCount,frontHome.chat);   
    app.post('/chatRequest_accept',frontHome.loggedIn,frontHome.chatRequest_accept);

    app.get('/resetPassword',login.resetpassword);
    app.post('/changePassword', login.changePassword);
 
    /* Front module end*/


    /* Service  module start*/
    app.post('/api/phonVerification',service.phonVerification);
    app.post('/api/checksocialLogin',service.checksocialLogin);
    app.post('/api/artistRegistration',service.artistRegistration);
    app.post('/api/userRegistration',service.userRegistration);
    app.post('/api/updateCustomerStrip',service.updateCustomerStrip);
    app.post('/api/forgotPassword',service.forgotPassword,service.sendMail);
    app.post('/api/userLogin',service.userLogin);
    app.get('/api/getBusinessProfile',authtokenCheck.checkaccessToken,artist.artistMainServiceCount,artist.artistInfo);
    app.post('/api/addBusinessHour',authtokenCheck.checkaccessToken,artist.businessHours);
    app.post('/api/addMyStory',authtokenCheck.checkaccessToken,appUser.addMyStoryV2);
    app.post('/api/myStory',authtokenCheck.checkaccessToken,appUser.deleteOldStory,appUser.getMyStory);
    app.post('/api/getMyStoryUser',authtokenCheck.checkaccessToken,appUser.deleteOldStory,feed_ctrl.followerStory,appUser.getMyStoryUser);
    app.post('/api/addFavorite',authtokenCheck.checkaccessToken,appUser.addFavorite);
    app.post('/api/artistSearch',authtokenCheck.checkaccessToken,certificate_ctrl.certificateFilter,appUser.artistSearch,appUser.finalData);
    app.post('/api/checkUser',service.checkUser);
    app.post('/api/updateRecord',authtokenCheck.checkaccessToken,appUser.updateRecord);
    app.get('/api/deleteRecord',artist.deleteRecord);//not in use
    app.post('/api/skipPage',authtokenCheck.checkaccessToken,artist.skipPage);
    app.post('/api/followFollowing',authtokenCheck.checkaccessToken,appUser.followFollowing);
    app.post('/api/followerList',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.followerList,appUser.followersData);
    app.post('/api/followingList',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.followingList,appUser.followingData);
    app.post('/api/exploreSearch',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.exploreSearch,appUser.exploreSearchFinal);
    app.post('/api/allArtist',authtokenCheck.checkaccessToken,artist.allArtist,artist.finalAllArtist);
    app.post('/api/deleteCompany',authtokenCheck.checkaccessToken,artist.deleteCompany);
    app.post('/api/companyInfo',authtokenCheck.checkaccessToken,artist.companyInfo,artist.staffService);
    app.post('/api/favoriteList',authtokenCheck.checkaccessToken,appUser.favoriteArtist,appUser.favoriteList);
    app.post('/api/getProfile',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.checkFavorite,appUser.invitationCheck,appUser.checkAdminCommission,appUser.getProfile);
    app.post('/api/getNotificationList',authtokenCheck.checkaccessToken,appUser.getNotificationList,appUser.getNotificationListFinal);
    app.post('/api/readNotification',authtokenCheck.checkaccessToken,appUser.readNotification);
    app.post('/api/paymentList',authtokenCheck.checkaccessToken,appUser.paymentList,appUser.paymentListInfo,appUser.paymentListFinal);
    app.post('/api/checkUserStatus',authtokenCheck.checkaccessToken,appUser.checkUserStatus);
    app.get('/api/sendNotificationOld',service.sendNotificationOld);
    app.post('/api/profileUpdate',authtokenCheck.checkaccessToken,appUser.profileUpdate);
    app.post('/api/getFolderFeed',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.checkLike,folder.fileGet,folder.getFolderData,folder.folderfeedList);
    app.get('/api/reportReason',authtokenCheck.checkaccessToken,appUser.reportReason);
    app.post('/api/profileByUserName',authtokenCheck.checkaccessToken,appUser.profileByUserName);
    app.post('/api/deleteStory',authtokenCheck.checkaccessToken,appUser.deleteStory);
    app.post('/api/userReport',authtokenCheck.checkaccessToken,appUser.reportSubmit);
    app.post('/api/changepassword',authtokenCheck.checkaccessToken,service.changepassword);
    app.get('/api/walkingList',authtokenCheck.checkaccessToken,artist.walkingList);
    app.post('/api/updateCustomer',authtokenCheck.checkaccessToken,artist.updateCustomer);



    app.get('/api/bankPayment',authtokenCheck.checkaccessToken,stripe.bookingInfoData12,stripe.bookingArtistInfo,stripe.bankPayment);
    app.post('/api/cardPayment',authtokenCheck.checkaccessToken,stripe.bookingInfoData12,stripe.bookingArtistInfo,stripe.paymentCard); 
    app.post('/api/bankAccountDetail',authtokenCheck.checkaccessToken,artist.userBankDetail,stripe.stripeagetAccount); 
    app.post('/api/addBankDetail',authtokenCheck.checkaccessToken,artist.addBankDetail);
    app.post('/api/artistPayment',authtokenCheck.checkaccessToken,stripe.bookingInfoData12,stripe.bookingArtistInfo,stripe.artistPayment); 
// api for stripe new flow on march
    app.post('/api/createAccount',authtokenCheck.checkaccessToken,artist.createAccount);
    app.post('/api/connectOnboarding',authtokenCheck.checkaccessToken,artist.connectOnboarding);


    app.post('/api/createFolder',authtokenCheck.checkaccessToken,folder.createFolder);
    app.get('/api/getFolder',authtokenCheck.checkaccessToken,folder.getFolder);
    app.post('/api/editFolder',authtokenCheck.checkaccessToken,folder.editFolder);
    app.post('/api/deleteFolder',authtokenCheck.checkaccessToken,folder.deleteFolder);
    app.post('/api/saveToFolder',authtokenCheck.checkaccessToken,folder.saveToFolder);
    app.post('/api/removeToFolder',authtokenCheck.checkaccessToken,folder.removeToFolder);


    app.post('/api/commentList',authtokenCheck.checkaccessToken,comment.commentList,comment.replayCommentList,comment.finalCommentList);
    app.post('/api/addComment',authtokenCheck.checkaccessToken,comment.addComment);
    app.post('/api/commentReplay',authtokenCheck.checkaccessToken,comment.replayComment);
    app.post('/api/commentLike',authtokenCheck.checkaccessToken,comment.commentLike);
    app.post('/api/deleteComment',authtokenCheck.checkaccessToken,comment.deleteComment);
    app.post('/api/editComment',authtokenCheck.checkaccessToken,comment.editComment);


    app.post('/api/profileFeed',authtokenCheck.checkaccessToken,appUser.followrsCheckData,appUser.checkLike,feed_ctrl.userFeedTagSearch,feed_ctrl.profileFeedV2,folder.getFolderData,feed_ctrl.finalProfileFeedV2);
    app.post('/api/getAllFeeds',authtokenCheck.checkaccessToken,notify.blockUserGet,feed_ctrl.followerFeed,feed_ctrl.userFeedTagSearch,feed_ctrl.getAllFeedsV2,folder.getFolderData,feed_ctrl.finalFeedV2);
    app.post('/api/feedDetails',authtokenCheck.checkaccessToken,notify.blockUserGet,folder.getFolderData,feed_ctrl.feedDetailsV2);
    app.post('/api/addFeed',authtokenCheck.checkaccessToken,feed_ctrl.addTag,feed_ctrl.followerFeedGet,feed_ctrl.addFeedv2);
    app.post('/api/editFeed',authtokenCheck.checkaccessToken,feed_ctrl.addTag,feed_ctrl.followerFeedGet,feed_ctrl.editFeed); 
    app.post('/api/like',authtokenCheck.checkaccessToken,feed_ctrl.like);
    app.post('/api/likeList',authtokenCheck.checkaccessToken,feed_ctrl.likeList,feed_ctrl.likeListFinal);
    app.post('/api/exploreFeeds',authtokenCheck.checkaccessToken,notify.blockUserGet,feed_ctrl.followerFeed,feed_ctrl.exploreAllFeedsV2,folder.getFolderData,feed_ctrl.exploreFinalFeedV2);

    app.post('/api/get-buisnes-type-by-miles',authtokenCheck.checkaccessToken,notify.blockUserGet,feed_ctrl.getBuisnesTypeAndCatBymiles);
    

    app.post('/api/exploreFeedsNew',authtokenCheck.checkaccessToken,notify.blockUserGet,feed_ctrl.followerFeed,feed_ctrl.exploreAllFeedsNew,folder.getFolderData,feed_ctrl.exploreFinalFeedNew);
    app.post('/api/deleteFeed',authtokenCheck.checkaccessToken,feed_ctrl.deleteFeed);

 
    app.post('/api/addBusinessType',authtokenCheck.checkaccessToken,category_ctrl.insertCategory);
    app.get('/api/allBusinessType',authtokenCheck.checkaccessToken,category_ctrl.allBusinessType);
    app.get('/api/myBusinessType',authtokenCheck.checkaccessToken,category_ctrl.myBusinessType);
    app.post('/api/addSubCategory',authtokenCheck.checkaccessToken,category_ctrl.insertSubCategory);
    app.get('/api/allSubcategory',authtokenCheck.checkaccessToken,category_ctrl.allSubcategory);
    app.get('/api/mysubCategory',authtokenCheck.checkaccessToken,category_ctrl.mysubCategory);
    app.post('/api/addArtistService',authtokenCheck.checkaccessToken,category_ctrl.addArtistService);
    app.post('/api/allService',authtokenCheck.checkaccessToken,category_ctrl.allUserCategory);
    app.post('/api/allCategory',authtokenCheck.checkaccessToken,category_ctrl.allCategory);
    app.post('/api/subService',authtokenCheck.checkaccessToken,category_ctrl.subService);
    app.post('/api/deleteBussinessType',authtokenCheck.checkaccessToken,category_ctrl.deleteBussinessType);
    app.post('/api/deleteCategory',authtokenCheck.checkaccessToken,category_ctrl.deleteCategory);
    app.post('/api/artistService',authtokenCheck.checkaccessToken,category_ctrl.artistAllService,category_ctrl.artistService);
    app.post('/api/addService',authtokenCheck.checkaccessToken,category_ctrl.addService);
    app.post('/api/deleteService',authtokenCheck.checkaccessToken,category_ctrl.deleteService);
    app.post('/api/getMostBookedService',authtokenCheck.checkaccessToken,category_ctrl.getMostBookedService);
    app.post('/api/artistServices',authtokenCheck.checkaccessToken,category_ctrl.artistServices);
    app.get('/api/allExploreCategory',authtokenCheck.checkaccessToken,category_ctrl.allExploreCategory);



    app.post('/api/tagSearch',authtokenCheck.checkaccessToken,feed_ctrl.tagSearch);
    app.post('/api/userFeed',authtokenCheck.checkaccessToken,notify.blockUserGet,appUser.userFeedByTag,appUser.userFeed,folder.getFolderData,appUser.finalUserFeed);


    app.post('/api/addStaff',authtokenCheck.checkaccessToken,staff_ctrl.addStaff);
    app.post('/api/addStaffService',authtokenCheck.checkaccessToken,staff_ctrl.addStaffService);
    app.post('/api/artistStaff',authtokenCheck.checkaccessToken,staff_ctrl.artistStaff);
    app.post('/api/staffInformation',authtokenCheck.checkaccessToken,staff_ctrl.staffInformation);
    app.post('/api/deleteStaffService',authtokenCheck.checkaccessToken,staff_ctrl.deleteStaffService);
    app.post('/api/deleteStaff',authtokenCheck.checkaccessToken,staff_ctrl.deleteStaff);
    app.post('/api/invitationUpdate',authtokenCheck.checkaccessToken,staff_ctrl.invitationUpdate);
    app.post('/api/staffServices',authtokenCheck.checkaccessToken,staff_ctrl.artistInfo,staff_ctrl.staffServicesDetails);
    app.post('/api/tagStaffServices',authtokenCheck.checkaccessToken,staff_ctrl.artistInfo,staff_ctrl.staffServices); 
    app.post('/api/serviceStaff',authtokenCheck.checkaccessToken,staff_ctrl.artistInfo,staff_ctrl.holidayStaff,staff_ctrl.serviceStaff);
    app.post('/api/addHoliday',authtokenCheck.checkaccessToken,staff_ctrl.addHoliday);
    app.post('/api/holidayList',authtokenCheck.checkaccessToken,staff_ctrl.holidayList);
    app.post('/api/serviceDetail',authtokenCheck.checkaccessToken,staff_ctrl.artistInfo,staff_ctrl.staffServicesDetails);
   

    app.post('/api/addArtistCertificate',authtokenCheck.checkaccessToken,certificate_ctrl.addArtistCertificate);
    app.post('/api/getAllCertificate',authtokenCheck.checkaccessToken,certificate_ctrl.getAllCertificate);
    app.post('/api/deleteCertificate',authtokenCheck.checkaccessToken,certificate_ctrl.deleteCertificate);
    app.get('/api/certificateList',authtokenCheck.checkaccessToken,certificate_ctrl.certificateTag);
    app.post('/api/editArtistCertificate',authtokenCheck.checkaccessToken,certificate_ctrl.editArtistCertificate);
    app.get('/api/addReportreson',certificate_ctrl.addReportreson);

    app.post('/api/addVoucher',authtokenCheck.checkaccessToken,voucher_ctrl.addVoucher);
    app.get('/api/allvoucherList',authtokenCheck.checkaccessToken,voucher_ctrl.allvoucherList);
    app.post('/api/deleteVoucher',authtokenCheck.checkaccessToken,voucher_ctrl.deleteVoucher);
    app.post('/api/voucherList',authtokenCheck.checkaccessToken,appUser.getCurrentTime,voucher_ctrl.voucherList);
    app.post('/api/applyVoucher',authtokenCheck.checkaccessToken,appUser.getCurrentTime,voucher_ctrl.applyVoucher);
    
    app.post('/api/getVoucherDetail',authtokenCheck.checkaccessToken,voucher_ctrl.getVoucher);

    app.post('/api/artistTimeSlotNew',authtokenCheck.checkaccessToken,appUser.getCurrentTime,booking_ctrl.bookingInfo,booking_ctrl.bookingTimeSlot);
    app.post('/api/bookArtist',authtokenCheck.checkaccessToken,booking_ctrl.checkBusySlot,booking_ctrl.bookArtist);
    app.post('/api/confirmBooking',authtokenCheck.checkaccessToken,appUser.getCurrentTime,booking_ctrl.confirmBooking);
    app.post('/api/artistBookingHistory',authtokenCheck.checkaccessToken,appUser.getCurrentTime,booking_ctrl.bookingCreateCheck,booking_ctrl.paymentCheck,booking_ctrl.checkBooking,booking_ctrl.checkStaff,booking_ctrl.bookingRequestCount,booking_ctrl.bookingHistory);
    app.post('/api/bookingDetail',authtokenCheck.checkaccessToken,booking_ctrl.bookingDetail);
    app.post('/api/getBookedServices',authtokenCheck.checkaccessToken,booking_ctrl.bookServices);
    app.post('/api/bookingAction',authtokenCheck.checkaccessToken,appUser.getCurrentTime,booking_ctrl.bookingAction);
    app.post('/api/deleteBookService',authtokenCheck.checkaccessToken,booking_ctrl.deleteBookService);
    app.post('/api/userBookingHistory',authtokenCheck.checkaccessToken,appUser.getCurrentTime,booking_ctrl.bookingCreateCheck,booking_ctrl.paymentCheck,booking_ctrl.bookingcheck,booking_ctrl.userBookingHistory);
    app.post('/api/deleteUserBookService',authtokenCheck.checkaccessToken,booking_ctrl.deleteUserBookService);
    app.post('/api/deleteClientBookService',authtokenCheck.checkaccessToken,booking_ctrl.deleteClientBookService);
    app.post('/api/bookingupdate',authtokenCheck.checkaccessToken,booking_ctrl.bookingupdate);
    app.post('/api/bookingReviewRating',authtokenCheck.checkaccessToken,booking_ctrl.bookingReviewRating);
    app.get('/api/getRatingReview',authtokenCheck.checkaccessToken,booking_ctrl.ratingReview);
    app.post('/api/bookingCheck',authtokenCheck.checkaccessToken,booking_ctrl.bookingCheck);
    app.post('/api/nearByNotification',authtokenCheck.checkaccessToken,booking_ctrl.bookingNotification);
    app.post('/api/checkTrackingBooking',authtokenCheck.checkaccessToken,booking_ctrl.checkTrackingBooking);
    app.post('/api/addCustomer',authtokenCheck.checkaccessToken,artist.addCustomer);
    app.get('/api/customerList',authtokenCheck.checkaccessToken,artist.userSearchList,artist.customerList);
    app.post('/api/bookingReport',authtokenCheck.checkaccessToken,booking_ctrl.reportSubmit);
    app.post('/api/bookingAnalytics',authtokenCheck.checkaccessToken,booking_ctrl.bookingAnalytics);
    app.post('/api/paymentHistoryDelete',authtokenCheck.checkaccessToken,booking_ctrl.paymentHistoryDelete);
    app.post('/api/adminCommision',authtokenCheck.checkaccessToken,booking_ctrl.adminCommision);
    app.post('/api/bookingCount',authtokenCheck.checkaccessToken,booking_ctrl.bookingCount);
    app.post('/api/bookingAdminCommsion',authtokenCheck.checkaccessToken,booking_ctrl.bookingAdminCommsion);
    app.post('/api/adminPayment',authtokenCheck.checkaccessToken,stripe.adminPayment);
    app.get('/api/adminReminder',authtokenCheck.checkaccessToken,booking_ctrl.adminReminder);
    app.post('/api/notificationClear',booking_ctrl.notificationClear);
    app.get('/api/allExploreBusiness',authtokenCheck.checkaccessToken,category_ctrl.allExploreBusiness);
    app.post('/api/notificationAdminCommsionCron',booking_ctrl.reminderCron);
    app.post('/api/bookingReminderCron',booking_ctrl.bookingReminderCron);
    app.post('/api/testCronPath',service.testCronPath);
    app.post('/api/logout_user',authtokenCheck.checkaccessToken,service.logout_user);
    app.post('/api/checkBusySlot',authtokenCheck.checkaccessToken,booking_ctrl.checkBusySlot);
    /*code for test lat long use*/
    app.post('/api/get-service-by-miles',authtokenCheck.checkaccessToken,appUser.getArtistByMiles,appUser.getServiceByMiles,appUser.getService);
 

    /* Service module end*/
}
