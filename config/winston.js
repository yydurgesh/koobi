var appRoot = require('app-root-path');
var winston = require('winston');
var moment  =   require('moment');
require('winston-daily-rotate-file');


var transport = new (winston.transports.DailyRotateFile)({
    filename: `${appRoot}/logs/application-%DATE%.log`,
    datePattern: 'YYYY-MM-DD-HH',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    timestamp: true
  });

// instantiate a new Winston Logger with the settings defined above
var logger = winston.createLogger({
  transports: [
    transport
  ],
  exitOnError: false, // do not exit on handled exceptions
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write: function(message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message);
  },
};

let appLog = function(level, message){
  logger.log('info', 'LOGTIME: '+moment().format());
  logger.log(level, message);
}

let loggerObj = {
  winston:logger,
  appLog: appLog

}

module.exports = loggerObj;