var userCategory        =   require('../../models/front/userCategory.js');
var Category            =   require('../../models/admin/category_model.js');
var subService          =   require('../../models/admin/sub_category_model.js');
var dateFormat          =   require('dateformat');
var artistMainService   =   require('../../models/front/artistMainService.js');
var artistSubService    =   require('../../models/front/artistSubService.js');
var artistservices      =   require('../../models/front/artistService.js');
var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var async               =   require('async');
var staff               =   require('../../models/front/staff_model.js');
var staffBussinesType   =   require('../../models/front/staffBussinesType_model.js');
var staffCategory       =   require('../../models/front/staffCategory_model.js');
var staffService        =   require('../../models/front/staffService.js');
var lodash              =   require('lodash');
var bookingService      =   require('../../models/front/bookingService.js');

var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
exports.insertCategory = function(req, res) {

    title = common.titleCase(req.body.title); 

    if(title==''){
        
        res.json({'status': "fail","message": commonLang.REQ_TITLE_FIELD});
        return;
    
    }
    
    Category.findOne({title:title,'deleteStatus':'1'}, function(err, category) {
        
        if(err){
           res.json({err});
           return;
        }
        if(category){

            artistMainService.findOne({'serviceName':title,'deleteStatus':'1','artistId':authData._id,'deleteStatus':1}).sort([['_id', 'descending']]).exec(function(err, aat){


                if(aat){

                    res.json({'status': "fail","message": commonLang.EXIST_BUSINESS_TYPE});
                    return;

                }
                
                userCategory.findOne({'categoryId':category._id,'userId':authData._id,'businessTypeId':0}, function(err, cat) {
                
                    
                    if(cat){

                        res.json({'status': "fail","message": commonLang.EXIST_BUSINESS_TYPE});
                        return;

                    }else{

                        userCategory.findOne().sort([['_id', 'descending']]).exec(function(err, dat){
                        
                            var data = {
                                'categoryId' : category._id,
                                'userId'     : authData._id,
                                _id          : (dat) ? Number(dat._id)+Number(1) : 1
                            };
                            userCategory.insertMany(data);

                            artistservices.deleteMany({'serviceId': category._id,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });
                            artistservices.deleteMany({'serviceId': category._id,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });

                            artistMainService.findOne().sort([['_id', 'descending']]).exec(function(err, aat){
                                var data1 = {
                                    'bookingCount'  : 0,
                                    'status'        : (category.type) ? 0 : 1,
                                    'deleteStatus'  : 1,
                                    'serviceId'     : category._id,
                                    'serviceName'   : category.title,
                                    'artistId'      : authData._id,
                                    '_id'           : (aat) ? Number(aat._id)+Number(1) : 1
                                };

                                artistMainService.insertMany(data1);
                        //code for add busuness as staff 
                            staff.findOne({'businessId':authData._id,'artistId':authData._id}, function(err, staff) {
                                staffBussinesType.findOne().sort([['_id', 'descending']]).exec(function(err, sbns){
                                    var staffBns = {
                                        '_id': (sbns) ? Number(sbns._id)+Number(1) : 1,
                                        'staffId'    : staff._id,
                                        'businessId' :authData._id,
                                        'artistId'   :authData._id,
                                        'serviceId'  :category._id,
                                        'serviceName':category.title

                                    }
                                  staffBussinesType.insertMany(staffBns);
                                })
                            })
                        //end code

                            });

                            res.json({'status': "success","message": commonLang.SUCCESS_ADD_BUSINESS_TYPE});
                            return;
                        });
                    }

                });    
            });

        } else {

            
            
            Category.findOne().sort([['_id', 'descending']]).exec(function(err, categorydata){
                
                userCategory.findOne().sort([['_id', 'descending']]).exec(function(err, dat){
                    
                    var categoryId = (categorydata) ? Number(categorydata._id)+Number(1) : 1;
                    var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");

                    var data = {
                        '_id'           : categoryId,
                        'title'         : title,
                        'type'          : 1,
                        'created_date'  : day,
                        'updated_date'  : day,
                        'status'        : 1,
                        'active_hash'   : authData.userName
                    };

                    Category.insertMany(data);

                    var data1 = {
                        'categoryId' : categoryId,
                        'userId'     : authData._id,
                        '_id'        : (dat) ? Number(dat._id)+Number(1) : 1,
                    };

                    userCategory.insertMany(data1);

                    artistservices.deleteMany({'serviceId': categoryId,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });
                    artistservices.deleteMany({'serviceId': categoryId,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });

                    artistMainService.findOne().sort([['_id', 'descending']]).exec(function(err, aat){
                        var data1 = {
                            'bookingCount'  : 0,
                            'status'        : 0,
                            'deleteStatus'  : 1,
                            'serviceId'     : categoryId,
                            'serviceName'   : title,
                            'artistId'      : authData._id,
                            '_id'           : (aat) ? Number(aat._id)+Number(1) : 1
                        };
                        artistMainService.insertMany(data1);

                        //code for add busuness as staff 
                            staff.findOne({'businessId':authData._id,'artistId':authData._id}, function(err, staff ) {
                                staffBussinesType.findOne().sort([['_id', 'descending']]).exec(function(err, sbns){
                                    var staffBns = {
                                        '_id': (sbns) ? Number(sbns._id)+Number(1) : 1,
                                        'staffId'    : staff._id,
                                        'businessId' :authData._id,
                                        'artistId'   :authData._id,
                                        'serviceId'  :categoryId,
                                        'serviceName':title

                                    }
                                    staffBussinesType.insertMany(staffBns);

                                })
                            })
                        //end code
                    });

                    res.json({'status': "success","message": commonLang.SUCCESS_ADD_BUSINESS_TYPE});
                    return;
                });
                
            });
        }
          
    });
}


exports.insertSubCategory = function(req, res) {

    var title = common.titleCase(req.body.title);
    var businessTypeId = req.body.categoryId;

    if(title==''){
        
        res.json({'status': "fail","message": commonLang.REQ_TITLE_FIELD});
        return;
    
    }

    if(businessTypeId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_CATEGORY_ID_FIELD});
        return;
    
    }

    subService.findOne({title:title,'deleteStatus':'1','serviceId':businessTypeId}, function(err, category) {
        
        if(err){
           res.json({err});
           return;
        }
        
        if(category){

            artistSubService.findOne({'subServiceName':title,'serviceId':businessTypeId,'artistId':authData._id,'deleteStatus':1}).sort([['_id', 'descending']]).exec(function(err, aats){
                if(aats){

                    res.json({'status': "fail","message": commonLang.EXIST_CATEGORY});
                    return;

                }
                
                userCategory.findOne({'categoryId':category._id,'userId':authData._id,'businessTypeId':businessTypeId}, function(err, cat) {
                    
                    if(cat){

                        res.json({'status': "fail","message": commonLang.EXIST_CATEGORY});
                        return;

                    }else{

                        userCategory.findOne().sort([['_id', 'descending']]).exec(function(err, dat){
                        
                            var data = {
                                'categoryId'      : category._id,
                                'userId'          : authData._id,
                                '_id'             : (dat) ? Number(dat._id)+Number(1) : 1,
                                'businessTypeId'  : businessTypeId
                            };

                            userCategory.insertMany(data);

                            artistservices.deleteMany({'subServiceId': category._id,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });
                            artistservices.deleteMany({'subServiceId': category._id,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });

                            artistSubService.findOne().sort([['_id', 'descending']]).exec(function(err, aat){
                                var data1 = {
                                    'status'            : (category.type) ? 0 : 1,
                                    'deleteStatus'      : 1,
                                    'serviceId'         : businessTypeId,
                                    'subServiceId'      : category._id,
                                    'subServiceName'    : category.title,
                                    'artistId'          : authData._id,
                                    '_id'               : (aat) ? Number(aat._id)+Number(1) : 1
                                };
                                artistSubService.insertMany(data1);
                        //code for add busuness as staff 
                            staff.findOne({'businessId':authData._id,'artistId':authData._id}, function(err, staff) {
                                staffCategory.findOne().sort([['_id', 'descending']]).exec(function(err, sbns){
                                    var staffBns = {
                                        '_id': (sbns) ? Number(sbns._id)+Number(1) : 1,
                                        'staffId'    : staff._id,
                                        'businessId' :authData._id,
                                        'artistId'   :authData._id,
                                        'serviceId'  : businessTypeId,
                                        'subServiceId'  :category._id,
                                        'subServiceName':category.title

                                    }
                                  staffCategory.insertMany(staffBns);
                                })
                            })
                        //end code
                            });

                            res.json({'status': "success","message": commonLang.SUCCESS_ADD_CATEGORY});
                            return;
                        });


                    }
                });
            });

        } else {
            
            
            subService.findOne().sort([['_id', 'descending']]).exec(function(err, categorydata){
                
                userCategory.findOne().sort([['_id', 'descending']]).exec(function(err, dat){
                    
                    var categoryId = (categorydata) ? Number(categorydata._id)+Number(1) : 1;
                    var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");

                    var data = {
                        '_id'           : categoryId,
                        'title'         : title,
                        'type'          : 1,
                        'created_date'  : day,
                        'updated_date'  : day,
                        'serviceId'     : businessTypeId,
                        'status'        : 1,
                        'active_hash'   : authData.userName
                    };

                    subService.insertMany(data);

                    var data1 = {

                        'categoryId'      : categoryId,
                        'userId'          : authData._id,
                        '_id'             : (dat) ? Number(dat._id)+Number(1) : 1,
                        'businessTypeId'  : businessTypeId

                    };
                    userCategory.insertMany(data1);

                    artistservices.deleteMany({'subServiceId': categoryId,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });
                    artistservices.deleteMany({'subServiceId': categoryId,'artistId':authData._id,'deleteStatus':0}, function(err, results){ });

                    artistSubService.findOne().sort([['_id', 'descending']]).exec(function(err, aat){
                        var data1 = {
                            'status'            : 0,
                            'deleteStatus'      : 1,
                            'serviceId'         : businessTypeId,
                            'subServiceId'      : categoryId,
                            'subServiceName'    : title,
                            'artistId'          : authData._id,
                            '_id'               : (aat) ? Number(aat._id)+Number(1) : 1
                        };
                        artistSubService.insertMany(data1);
                        //code for add busuness as staff 
                            staff.findOne({'businessId':authData._id,'artistId':authData._id}, function(err, staff) {
                                staffCategory.findOne().sort([['_id', 'descending']]).exec(function(err, sbns){
                                    var staffBns = {
                                        '_id': (sbns) ? Number(sbns._id)+Number(1) : 1,
                                        'staffId'    : staff._id,
                                        'businessId' :authData._id,
                                        'artistId'   :authData._id,
                                        'serviceId'  : businessTypeId,
                                        'subServiceId'  :categoryId,
                                        'subServiceName':title

                                    }
                                  staffCategory.insertMany(staffBns);
                                })
                            })
                        //end code
                    });
                    res.json({'status': "success","message": commonLang.SUCCESS_ADD_CATEGORY});
                    return;
                });
                
            });
        }
          
    });
}

exports.myBusinessType = function(req, res) {

    artistMainService.find({'artistId':authData._id,'deleteStatus':1}).sort([['_id', 'ascending']]).exec(function(err, ser) {

        if(ser.length){

            res.json({status: "success",message: 'ok',data: ser});
            return;
        
        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        
        }
    
    });

}


exports.allBusinessType = function(req, res) {

    let search = req.query.search ? common.jsUcfirst(req.query.search) : '';
 
    Category.find({"type" : 0,"title": {$regex:search,$options:'i'},'deleteStatus':1 }).sort([['title', 'ascending']]).exec(function(err, ser) {

        if(ser.length){

            res.json({status: "success",message: 'ok',data: ser});
            return;
        
        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        
        }
    
    });

}

exports.mysubCategory = function(req, res) {

    let businessTypeId = req.query.categoryId ? Number(req.query.categoryId) :'';

    artistSubService.find({'serviceId':businessTypeId,'artistId':authData._id,'deleteStatus':1}).sort([['_id', 'ascending']]).exec(function(err, ser) {

        if(ser.length){

            res.json({status: "success",message: 'ok',data: ser});
            return;
        
        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        
        }
    
    });

}


exports.allSubcategory = function(req, res) {

    let search = req.query.search ? common.jsUcfirst(req.query.search) : '';
    let businessTypeId = req.query.categoryId ? Number(req.query.categoryId) :'';

    if(businessTypeId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_CATEGORY_ID_FIELD});
        return;
    
    }
 
    subService.find({'serviceId':businessTypeId,"type" : 0,"title": {$regex:search,$options:'i'},'deleteStatus':1}).sort([['title', 'ascending']]).exec(function(err, ser) {
        
        if(ser.length){
            
            res.json({status: "success",message: 'ok',data: ser});
            return;

        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;

        }
    
    });

}


/*api for add artist service*/
exports.addArtistService = async function(req, res){

    if(req.body.artistService==''){
        
        res.json({'status': "fail","message": commonLang.REQ_ARTIST_SERVICE_FIELD});
        return;    
    }
        
    let  artService = JSON.parse(req.body.artistService);
    var jsArr = [];
    var staffSer =[];
    var staffServiceId =[];

    artistservices.deleteMany({'artistId':authData._id}, function(err, results){ 
        User.updateOne({_id:authData._id},{$set:{serviceCount:'0'}},function(err, docs) {});
        // code for business as staff
          staffService.deleteMany({'businessId':authData._id,'artistId':authData._id}, function(err, results){ });
        //end code
    });
    // business as staff
    var staffData    =  await getStaffInfo(authData._id,authData._id);
    var stfServiceId =  await getStaffServiceLatID();
        var stfId = 0;
        if(stfServiceId){
            stfId = stfServiceId._id; 
        }
    // end 
    let query = artistservices.findOne().sort([['_id', 'descending']]);
    query.exec(function(err, s) {

        let autoId = (s) ? Number(s._id) + Number(1) : 1;

        for (var i = 0; i < artService.length; i++) {
            inc = Number(autoId) + Number(i);
            stfId = stfId + 1;
            jsArr.push({
                _id: inc,
                artistId: Number(authData._id),
                serviceId: artService[i].serviceId,
                subserviceId: artService[i].subserviceId,
                title: common.titleCase(artService[i].title),
                description: artService[i].description,
                inCallPrice: artService[i].inCallPrice,
                outCallPrice: artService[i].outCallPrice,
                completionTime:artService[i].completionTime
            });

            staffSer.push({
                _id: stfId,
                staffId: staffData._id,
                artistId: Number(authData._id),
                businessId: Number(authData._id),
                serviceId: artService[i].serviceId,
                subserviceId: artService[i].subserviceId,
                artistServiceId:inc,
                inCallPrice: artService[i].inCallPrice,
                outCallPrice: artService[i].outCallPrice,
                completionTime:artService[i].completionTime
            });
            staffServiceId.push(String(inc)); 


        }
        
        artistservices.insertMany(jsArr,function(err,result){
            staffService.insertMany(staffSer);
             staff.updateOne({ _id:staffData._id},{$set:{staffServiceId:staffServiceId}},function(err, docs) {});
            artistservices.findOne({'artistId':authData._id}).count().exec(function(err,ct) {
                User.updateOne({ _id:authData._id},{$set:{serviceCount:ct,'isDocument':2}},function(err, docs) {});
            });

        });
        res.json({'status': "success","message": commonLang.SUCCESS_ARTIST_ADD_SERVICE,"artistServices": jsArr});
        return;
        
    });   

}
async function getStaffInfo(businessId,userId){
    return await staff.findOne({'businessId':businessId,'artistId':userId}).exec()
}
async function getStaffServiceLatID(){
    return await staffService.findOne().sort({ _id: -1 }).exec();
}
exports.allUserCategory = function(req, res) {
    var baseUrl = req.protocol + '://' + req.headers['host'];

    async.parallel([

            function(callback) {
                var query = Category.find({
                    'status': 1,
                    'deleteStatus': 1
                })
                query.exec(function(err, ser) {
                    if (err) {
                        callback(err);
                    }

                    callback(null, ser);
                });
            },
            function(callback) {
                var query = subService.find({
                    'status': 1,
                    'deleteStatus': 1
                })
                query.exec(function(err, sub) {
                    if (err) {
                        callback(err);
                    }

                    callback(null, sub);
                });
            }

        ],


        function(err, results) {
            if (results[0]) {
                serArr = [];

                for (var i = 0; i < results[0].length; i++) {
                    subArr = [];
                    for (var j = 0; j < results[1].length; j++) {
                        if (results[0][i]._id == results[1][j].serviceId) {

                            subArr.push({
                                _id: results[1][j]._id,
                                serviceId: results[1][j].serviceId,
                                image: commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.SUBSERVICE_MEDIA_UPLOAD_FOLDEFR + results[1][j].image,
                                title: results[1][j].title
                            });
                        }
                    }
                    serArr.push({
                        _id: results[0][i]._id,
                        title: results[0][i].title,
                        subService: subArr
                    });
                }


                artistservices.find({
                    'artistId': authData._id,
                    'status': 1,
                    'deleteStatus': 1
                }, {
                    "_id": 1,
                    "serviceId": 1,
                    "subserviceId": 1,
                    "completionTime": 1,
                    "outCallPrice": 1,
                    "inCallPrice": 1,
                    "description": 1,
                    "title": 1
                }, function(err, result) {
                    res.json({
                        status: "success",
                        message: 'ok',
                        serviceList: serArr,
                        artistService: result
                    });
                });


            } else {
                res.json({
                    status: "fail",
                    message: commonLang.RECORD_NOT_FOUND
                });
                return;
            }

        });

}


exports.allCategory = function(req, res) {


   baseUrl =  req.protocol + '://'+req.headers['host'];

    async.parallel([

       function(callback) {
            var query = Category.find({'status':1,'deleteStatus':1,'type':0}).sort([['title', 'ascending']])
            query.exec(function(err, ser) {
                if (err) {
                    callback(err);
                }
     
                callback(null, ser);
            });
        },
        function(callback) {
            var query = subService.find({'status':1,'deleteStatus':1,'type':0}).sort([['title', 'ascending']])
            query.exec(function(err, sub) {
                if (err) {
                    callback(err);
                }
     
                callback(null, sub);
            });
        }
      
    ],
 
    //Compute all results
    function(err, results) {
      if(results[0]){
         serArr =[];
        
        for(var i = 0; i < results[0].length; i++) {
            subArr =[];
            for(var j=0;j<results[1].length;j++){
               if(results[0][i]._id == results[1][j].serviceId){
          
                    subArr.push({
                                _id: results[1][j]._id,
                                 serviceId: results[1][j].serviceId,
                                 image: commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.SUBSERVICE_MEDIA_UPLOAD_FOLDEFR+results[1][j].image,
                                 title: results[1][j].title
                            });
                }
           }
           serArr.push({
                _id:results[0][i]._id,
                title:results[0][i].title,
                subService:subArr
            }); 
        }
          
       
          
        res.json({status:"success",message:'ok',serviceList:serArr});
       // return res.send(200,  serArr);
    }else{
         res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
           return;
    }
        
    });

}


exports.subService = function(req, res) {
    
    subService.find({'serviceId':req.body.serviceId,'status':1,'deleteStatus':1}, function(err, data)
     {
        if (data.length==0){
            res.json({status:"success",message: commonLang.RECORD_NOT_FOUND});
        }else{ 
            res.json({status:"success",message:'ok',subServices:data});
        }
    });

}


exports.deleteBussinessType = function(req, res) {

    let id = Number(req.body.id);

    if(id == ''){

        res.json({status:"fail",message: commonLang.REQ_BUSINESS_TYPE_ID});
        return;
    }

    artistMainService.updateMany({'serviceId':id,'artistId': authData._id},  {$set: {'deleteStatus':0}}, function(err, docs){


        userCategory.deleteMany({'categoryId':id,'userId':authData._id,'businessTypeId':0}, function(err, cat) {

            userCategory.deleteMany({'businessTypeId':id,'userId':authData._id}, function(err, cat) {

                artistSubService.updateMany({'serviceId':id,'artistId': authData._id},  {$set: {'deleteStatus':0}}, function(err, docs){
                    
                    artistservices.updateMany({'serviceId':id,'artistId': authData._id},  {$set: {'deleteStatus':0}}, function(err, docs){

                        res.json({status:"success",message: commonLang.SUCCESS_DELETE_BUSINESS_TYPE});
                    });
                });
            });
        });

    });
}


exports.deleteCategory = function(req, res) {

    let id = req.body.id;

    if(id == ''){

        res.json({status:"fail",message: commonLang.REQ_CATEGORY_ID});
        return;
    }
    userCategory.deleteMany({'categoryId':id,'userId':authData._id}, function(err, cat) {

        artistSubService.updateMany({'subServiceId':id,'artistId': authData._id},  {$set: {'deleteStatus':0}}, function(err, docs){

            artistservices.updateMany({'subServiceId':id,'artistId': authData._id},  {$set: {'deleteStatus':0}}, function(err, docs){

                res.json({status:"success",message: commonLang.SUCCESS_DELETE_CATEGORY});
            });
        });
    });
       
}


exports.artistAllService = function(req, res, next){

    if(req.body.artistId ==''){
        res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
        return;
    } 

    var query = artistservices.find({'artistId':Number(req.body.artistId),'status':1,'deleteStatus':1},{'_id':1,'subserviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'description':1})
    query.exec(function(err, aService) {

        if(aService){
            data = aService.map(function(n) {return Number(n._id);});
            next();
        }
     

    });

}


exports.artistService =function(req,res){
    if(req.body.artistId ==''){
        res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
        return;
    }

    bussinessTypeId = (req.body.bussinessTypeId) ? req.body.bussinessTypeId :''; 
    categoryId = (req.body.categoryId) ? req.body.categoryId :''; 
    serviceId = (req.body.serviceId) ? req.body.serviceId :''; 
    async.parallel([

     function(callback) {
          var query = artistMainService.find({'artistId':Number(req.body.artistId),'status':1,'deleteStatus':1},{'serviceId':1,'serviceName':1}).sort([ ['serviceName', 'ascending']])
          query.exec(function(err, ser) {
              if (err) {
                  callback(err);
              }
   
              callback(null, ser);
          });
      },
      function(callback) {
          var query = artistSubService.find({'artistId':Number(req.body.artistId),'status':1,'deleteStatus':1}).sort([ ['subServiceName', 'ascending']])
          query.exec(function(err, sub) {
              if (err) {
                  callback(err);
              }
   
              callback(null, sub);
          });
      },
      function(callback) {
          var query = artistservices.find({'artistId':Number(req.body.artistId),'status':1,'deleteStatus':1},{'_id':1,'subserviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'description':1}).sort([ ['title', 'ascending']])
          query.exec(function(err, s) {

              if (err) {
                  callback(err);
              }

              callback(null, s);
          });
      },

        function(callback) {

            search = {};
            search['businessId'] = Number(req.body.artistId);
            search['artistServiceId']   = {'$in':data};
            search['status']   = 1;
            search['deleteStatus']   = 1;
            var query = staffService.find(search,{'artistServiceId':1,'inCallPrice':1,'outCallPrice':1});
            query.exec(function(err, staff) {
                if (err) {
                    callback(err);
                }
                callback(null, staff);
            });
        },
        function(callback) {
          
            User.aggregate(
                [
                    { $match : { _id :Number(req.body.artistId) } },
                    {   "$project": {     
                        "userName":"$userName", 
                        "firstName":"$firstName", 
                        "lastName":"$lastName", 
                        "profileImage":"$profileImage", 
                        "ratingCount":"$ratingCount", 
                        "reviewCount":"$reviewCount", 
                        "postCount":"$postCount",
                        "businessName":"$businessName",  
                        "businessHo": "$businessHo",   
                        "serviceType": "$serviceType",   
                        "inCallpreprationTime": "$inCallpreprationTime",  
                        "outCallpreprationTime": "$outCallpreprationTime",   
                        "address": "$address",   
                        "latitude": "$latitude",   
                        "longitude": "$longitude",
                        "radius":"$radius",
                        "businessType":"$businessType",
                        "bankStatus":"$bankStatus",   
                        "payOption":"$payOption",   
                        "bookingSetting":"$bookingSetting",   
                        "walkingBlock":"$walkingBlock",
                        "activeStatus":"$status"   
                    }}, 
                    { "$lookup": {     
                            "from": "busineshours",     
                            "localField": "_id",     
                            "foreignField": "artistId",     
                            "as": "busineshours"   
                    }},
                   
                ],function(err, data){

                    var baseUrl =  req.protocol + '://'+req.headers['host'];   
                    if(data[0].profileImage){ 
                        data[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[0].profileImage;

                    }
                    callback(null, data[0]);

                });

        },function(callback) {

            search = {};
            search['artistId'] = Number(req.body.artistId);
            search['userId']   = req.body.userId ? Number(req.body.userId) : authData._id;
            search['bookingStatus']   = 0;
            search['bookingType']   = req.body.bookingType ? req.body.bookingType : 'online';
            var query = bookingService.find(search);
            query.exec(function(err, booking) {
      
                if (err) {
                    callback(err);
                }
                callback(null, booking);
            });
        }
   
  ],
   
  //Compute all results
  function(err, results) {
     
       if(results[0]){
       serArr =[];
      
      for(var i = 0; i < results[0].length; i++) {
          subArr =[];
          for(var j=0;j<results[1].length;j++){
             if(results[0][i].serviceId == results[1][j].serviceId){
        
                  subArr.push({
                              _id: results[1][j]._id,
                               serviceId: results[1][j].serviceId,
                               subServiceId: results[1][j].subServiceId,
                               subServiceName: results[1][j].subServiceName,
                               isSelected : (categoryId) ? (results[1][j].subServiceId == categoryId) ? true :false :false,
                          });
              }
         }
         serArr.push({
              serviceId:results[0][i].serviceId,
              serviceName:results[0][i].serviceName,
              subServies:subArr,
              isSelected : (bussinessTypeId) ? (results[1][i].serviceId == bussinessTypeId) ? true :false :false,

          }); 
      }
      if(serArr){

          for (var s = 0; s < serArr.length; s++) {
               
              for (var k = 0; k < serArr[s].subServies.length; k++) {

                  jsArr = [];
                  for (var r = 0; r < results[2].length; r++) {
                      if (serArr[s].subServies[k].subServiceId == results[2][r].subserviceId) {

                          bookingType = "Both";

                          if(parseFloat(results[2][r].inCallPrice) != 0.00 && parseFloat(results[2][r].outCallPrice) != 0.00){
                             bookingType = "Both";
                          }else if(parseFloat(results[2][r].inCallPrice) != 0.00 && parseFloat(results[2][r].outCallPrice) == 0.00){
                             bookingType = "Incall";
                          }else if(parseFloat(results[2][r].inCallPrice) == 0.00 && parseFloat(results[2][r].outCallPrice) != 0.00){
                             bookingType = "Outcall";
                          }

                        var picked = lodash.filter(results[3], { 'artistServiceId': results[2][r]._id} );
                        var incallStaff = false;
                        var outcallStaff = false;

                        if(picked.length){
                            for (var a = 0; a < picked.length; a++) {

                                if(parseFloat(picked[a].inCallPrice) != 0.00){
                                    incallStaff = true;
                                }
                                if(parseFloat(picked[a].outCallPrice) != 0.00){
                                    outcallStaff = true;
                                } 
                            }
                        }

                        jsArr.push({
                            _id: results[2][r]._id,
                            title: results[2][r].title,
                            serviceId: results[0][s].serviceId,
                            subServiceId: results[1][k].subServiceId,
                            inCallPrice: parseFloat(results[2][r].inCallPrice),
                            outCallPrice: parseFloat(results[2][r].outCallPrice),
                            completionTime: results[2][r].completionTime,
                            description: results[2][r].description,
                            bookingType : bookingType,
                            isStaff     : (picked.length) ? true : false,
                            incallStaff : incallStaff,
                            outcallStaff : outcallStaff,
                            isSelected : (serviceId) ? (results[2][r]._id == serviceId) ? true :false :false,

                        });
                        

                      }
                  }
                  serArr[s].subServies[k].artistservices = jsArr;
              }
          }

      } 
      
        results[4].isAlreadybooked = (results[5].length) ? true : false;

        res.json({status: "success",message: 'ok',artistServices: serArr,artistDetail:results[4]});
        return;
      
  }else{
     res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
         return;
  }
          
         
  });

}


/*api for add artist service*/
exports.addService = async function(req, res){

    let serviceId       = req.body.serviceId;
    let subserviceId    = req.body.subserviceId;
    let title           = common.titleCase(req.body.title);
    let description     = req.body.description;
    let inCallPrice     = req.body.inCallPrice;
    let outCallPrice    = req.body.outCallPrice;
    let completionTime  = req.body.completionTime;
    let artistId        = authData._id;
    let id              = req.body.id;

    if(serviceId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_SERVICE_ID_FIELD});
        return;    
    }

    if(subserviceId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_SUB_SERVICE_ID_FIELD});
        return;    
    }


    if(title==''){
        
        res.json({'status': "fail","message": commonLang.REQ_TITLE_FIELD});
        return;    
    }

    if(description==''){
        
        res.json({'status': "fail","message": commonLang.REQ_DESCRIPTION_FIELD});
        return;    
    }

    if(inCallPrice==''){
        
        res.json({'status': "fail","message": commonLang.REQ_IN_CALL_PRICE_FIELD});
        return;    
    }

    if(outCallPrice==''){
        
        res.json({'status': "fail","message": commonLang.REQ_OUT_CALL_PRICE_FIELD});
        return;    
    }

    if(completionTime==''){
        
        res.json({'status': "fail","message": commonLang.REQ_COMPLETION_TIME_FIELD});
        return;    
    }


    jsArr ={
        artistId: Number(authData._id),
        serviceId: serviceId,
        subserviceId: subserviceId,
        title: title,
        description: description,
        inCallPrice:inCallPrice,
        outCallPrice: outCallPrice,
        completionTime:completionTime
    };  
    // code for business as staff 
    var staffData    =  await getStaffInfo(authData._id,authData._id); 
    var staffCount   =  await checkStaffCount(staffData._id);
    var stfServiceId =  await getStaffServiceLatID();
    var stfId = 0;
        if(stfServiceId){
             stfId = stfServiceId._id; 
             stfId = stfId + 1;
    }
    var staffSerArr = {
           
        inCallPrice:inCallPrice,
        outCallPrice: outCallPrice,
        completionTime:completionTime
             
    }
   // console.log('staffCount data ',staffCount); return false;
    // end  

    if(id){
       //Also its not allowing me to add same service name to a different category. It should be for same category not for different category.
       artistservices.findOne({serviceId:serviceId,subserviceId:subserviceId,'title':jsArr.title,artistId:artistId,deleteStatus:1,_id: {'$ne':id}}).exec(function(err,data) {

            if(data){

                res.json({status:'fail',error: commonLang.EXIST_SERVICE_NAME});
                return;

            }else{

                artistservices.updateOne({_id:id},{$set: jsArr},function(err, docs){
                    if( staffCount<=0 ){
                        staffService.updateOne({serviceId:serviceId,subserviceId:subserviceId,artistServiceId:id},{$set: staffSerArr},function(err, docs){})
                    }

                    res.json({status:'success',message: commonLang.SUCCESS_UPDATE_ARTIST_SERVICE});
                    return;

                });
            }

        });

    }else{

        artistservices.findOne({serviceId:serviceId,subserviceId:subserviceId,'title':jsArr.title,'artistId':artistId,deleteStatus:1}).exec(function(err,data) {

            if(data){

                res.json({status:'fail',error: commonLang.EXIST_SERVICE_NAME});
                return;

            }else{

                setData = {};
                setData['serviceCount'] = Number(authData.serviceCount)+Number(1);



                let query = artistservices.findOne().sort([['_id', 'descending']]);
                query.exec(function(err, s) {

                    let autoId = (s) ? Number(s._id) + Number(1) : 1;

                    inc = Number(autoId);

                    jsArr._id = inc;

                    artistservices.insertMany(jsArr,function(err,result){
                        //code for business as staff
                        staffSerArr._id = stfId;
                        staffSerArr.staffId= staffData._id;
                        staffSerArr.artistId= Number(authData._id);
                        staffSerArr.businessId= Number(authData._id);
                        staffSerArr.serviceId= serviceId;
                        staffSerArr.subserviceId= subserviceId;
                        staffSerArr.artistServiceId=inc;
                        var staffServiceId = staffData.staffServiceId;
                        staffServiceId.push(String(inc)); 
                        staffService.insertMany(staffSerArr);
                        staff.updateOne({ _id:staffData._id},{$set:{staffServiceId:staffServiceId}},function(err, docs) {});
                        //end
                        
                        artistservices.findOne({'artistId':authData._id}).count().exec(function(err,ct) {
                            User.updateOne({ _id:authData._id},{$set:setData},function(err, docs) {
                              
                                res.json({'status': "success","message": commonLang.SUCCESS_ARTIST_ADD_SERVICE});
                                return;
                                
                            });
                        });

                    });
                  
                    
                });  
            }
        }); 
    }

}

async function checkStaffCount(staffId){
    return await staff.findOne({'businessId':authData._id,'_id':{'$ne':staffId}}).count().exec()
}

/*api for delete artist service*/
exports.deleteService = function(req, res){

    let id = req.body.id;
    if(id==''){
        
        res.json({'status': "fail","message": commonLang.REQ_ID_FIELD});
        return;    
    }

    setData = {};
    let count = Number(authData.serviceCount) - Number(1);
    setData['serviceCount'] = (count!=-1) ? count : 0;

    artistservices.updateOne({_id:id},  {$set: {'deleteStatus':0}}, function(err, docs){

        User.updateOne({ _id:authData._id},{$set:setData},function(err, docs) {});

        res.json({status:'success',message: commonLang.SUCCESS_DELETE_ARTIST_SERVICE});
        return;

    });
        
    
}


exports.getMostBookedService =function(req,res){

    if(req.body.artistId ==''){
        res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
        return;
    }

    if(req.body.userId ==''){
        res.json({'status': "fail","message": commonLang.REQ_USER_ID});
        return;
    }

    async.parallel([
        function(callback) {
          var query = artistservices.find({'artistId':Number(req.body.artistId),'status':1,'deleteStatus':1},{'_id':1,'subserviceId':1,'serviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'description':1})
          query.exec(function(err, s) {
              if (err) {
                  callback(err);
              }

              callback(null, s);
          });

        },function(callback) {

            search1 = {};
            search1['artistId'] = Number(req.body.artistId);
           // search1['userId']   = req.body.userId ? Number(req.body.userId) : authData._id;// code comment for new changeeeeeeeeeeeeeeeeee
            search1['bookingStatus']   = 1;
            var query = bookingService.find(search1);
            query.exec(function(err, booking1) {
      
                if (err) {
                    callback(err);
                }
                callback(null, booking1);
            });
        }
   
  ],
   
  //Compute all results
  function(err, results) {
     
       if(results[0]){

           serArr =[];
           for (var r = 0; r < results[0].length; r++) {

                var picked = lodash.filter(results[1], { 'artistServiceId': results[0][r]._id} );

                serArr.push({
                    _id: results[0][r]._id,
                    serviceId: results[0][r].serviceId,
                    subserviceId: results[0][r].subserviceId,
                    bookingCount : picked.length

                });
            
            }


            var data = serArr.sort(function(a, b) {
                var a1 = a.bookingCount,
                b1 = b.bookingCount;
                if (a1 == b1) return 0;
                return a1 < b1 ? 1 : -1;
            });
          
            res.json({status: "success",message: 'ok',artistServices: data[0]});
            return;
      
    }else{

        res.json({status: "success",message: 'ok',artistServices:[]});
        return;
    }
          
         
  });

}


exports.artistServices = function(req, res){

    if(req.body.artistId ==''){
        res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
        return;
    } 

    var search = req.body.search;

    datae = {};
    datae['artistId'] = Number(req.body.artistId);
    datae['status'] = 1;
    datae['deleteStatus'] = 1;
    datae['title'] = { $regex : search,'$options' : 'i' };

   var query =  artistservices.aggregate([
                { 
                    $lookup:{

                        from: 'services',
                        localField: 'serviceId',
                        foreignField: '_id',
                        as: 'businessType'
                    }
                 
                },
                { 
                    $lookup:{

                        from: 'subservices',
                        localField: 'subserviceId',
                        foreignField: '_id',
                        as: 'category'
                    }
                 
                },
                {
                    $match: datae
                }

            ]);

    query.exec(function(err, aService) {

        if(aService.length!=0){

            res.json({status: "success",message: 'ok',artistServices: aService});
            return;

        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        }

    });

}
exports.allExploreCategory =function(req,res){

    async.parallel([

        function(callback) {
           var query = subService.find({'deleteStatus':1,'status':'1'}).sort([['title', 'ascending']])
           query.exec(function(err, sub) {
                if (err) {
                  callback(err);
                }

              callback(null, sub);
            });
        },
        function(callback) {
            var query = artistservices.find({'status':1,'deleteStatus':1},{'_id':1,'serviceId':1,'subserviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'description':1}).sort([ ['title', 'ascending']])
            query.exec(function(err, s) {
                  if (err) {
                      callback(err);
                  }

              callback(null, s);
            });
        },
        function(callback) {
           var query = Category.find({'deleteStatus':1,'status':'1'}).sort([['title', 'ascending']])
           query.exec(function(err, sub) {
                if (err) {
                  callback(err);
                }

              callback(null, sub);
            });
        },   
    ],
   
  //Compute all results
   function(err, results) {
     
       if(results[0]){
      
            serArr = results[0];
            jsArr = [];

            if(serArr){

                for (var s = 0; s < serArr.length; s++) {
                    var service_name = lodash.filter(results[2], { '_id': serArr[s].serviceId} );
                    var picked = lodash.filter(results[1], { 'subserviceId': serArr[s]._id} );
                   jsArr.push({
                        _id: serArr[s]._id,
                        title: serArr[s].title,
                        type: serArr[s].type,
                        serviceId: serArr[s].serviceId,
                        serviceName: service_name[0].title,
                        deleteStatus: serArr[s].deleteStatus,
                        created_date: serArr[s].created_date,
                        updated_date: serArr[s].updated_date,
                        status: serArr[s].status,
                        artistservices: picked

                    });

                }


            } 

            res.json({status: "success",message: 'ok',data: jsArr});
            return;
          
        }else{
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
             return;
        }
         
    });

}
exports.allExploreBusiness =function(req,res){
 
  async.parallel([

        function(callback) {
           var query = Category.find({'deleteStatus':1,'status':'1'}).sort([['title', 'ascending']])
           query.exec(function(err, sub) {
                if (err) {
                  callback(err);
                }

              callback(null, sub);
            });
        },
        function(callback) {
            var query = subService.find({'status':1,'deleteStatus':1}).sort([ ['title', 'ascending']])
            query.exec(function(err, s) {
                  if (err) {
                      callback(err);
                  }

              callback(null, s);
            });
        },   
    ],
   
  //Compute all results
   function(err, results) {
     
       if(results[0]){
      
            serArr = results[0];
          /*  jsArrNew = [];
            if(results[1].length){ 
                 for (j = 0 ; j < results[1].length ; j++) {
                    jsArrNew.push({
                        _id: results[1][j]._id,
                         title: results[1][j].title,
                         serviceId: results[1][j].serviceId, 
                         subServiceId:results[1][j].serviceId,
                    });
                 }
            }*/
            jsArr = [];

            if(serArr){

                for (var s = 0; s < serArr.length; s++) {
                   
                   //var picked = lodash.filter(jsArrNew, { 'serviceId': serArr[s]._id} );
                    var picked = lodash.filter(results[1], { 'serviceId': serArr[s]._id} );
                    //console.log(picked);
                   jsArr.push({
                        _id: serArr[s]._id,
                        title: serArr[s].title,
                        type: serArr[s].type,
                        serviceId: serArr[s].serviceId,
                        deleteStatus: serArr[s].deleteStatus,
                        created_date: serArr[s].created_date,
                        updated_date: serArr[s].updated_date,
                        status: serArr[s].status,
                        category: picked

                    });

                }


            } 

            res.json({status: "success",message: 'ok',businessType: jsArr});
            return;
          
        }else{
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
             return;
        }
         
    });

}