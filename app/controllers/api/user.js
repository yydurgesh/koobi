var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var businessHour        =   require('../../models/front/businesshours.js');
var artistservices      =   require('../../models/front/artistService.js');
var artistMainService   =   require('../../models/front/artistMainService.js');
var artistSubService    =   require('../../models/front/artistSubService.js');
var artistCertificate   =   require('../../models/front/artistCertificate.js');
var artistFavorite      =   require('../../models/front/artisteFavorite.js');
var followUnfollow      =   require('../../models/front/followersFollowing.js');
var likes               =   require('../../models/front/like.js');
var feed                =   require('../../models/front/feed.js');
var tag                 =   require('../../models/front/tag.js');
var story               =   require('../../models/front/myStory.js');
var bookingService      =   require('../../models/front/bookingService.js');
var booking             =   require('../../models/front/booking.js');
var addNotification     =   require('../../models/front/notification.js');
var reportReason        =   require('../../models/front/reportReason_model.js');
var async               =   require('async');
var formidable          =   require('formidable');//it user for get form or post data by http
var fs                  =   require('fs');
var artistfavorite      =   require('../../models/front/artisteFavorite.js');
var multiparty          =   require('multiparty');
var staff               =   require('../../models/front/staff_model.js');
var validUrl            =   require('valid-url');
var escapere            =   require('escape-regexp');
var notify              =   require('../../../lib/notification.js');
var lodash              =   require('lodash');
var moment              =   require('moment');            
var staff               =   require('../../models/front/staff_model.js');
var report              =   require('../../models/front/userReport_model.js');
var ratingReview        =   require('../../models/front/ratingReview_model');

var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
var FileUpload      =   require('../../../lib/FileUpload');
const S3            =   require("../../../lib/aws-s3");
const commonJs      =   require("../../../lib/common");
var uniqid          = require('uniqid'); 
const fileUploadObj = new FileUpload();

var commonLib         =   require('../../../lib/common.js');


exports.artistSearch = function(req, res,next){

    datae       = {};
    search      = {};
    sortData    = {};
    baseUrl         =  req.protocol + '://'+req.headers['host'];
    var subservice  = req.body.subservice;
    var service     = req.body.service;
    var artservice     = req.body.artistService;
    serviceType     = req.body.serviceType ? Number(req.body.serviceType) : '';
    var city        = req.body.city;
    var rating      = req.body.rating;
    var day         = req.body.day;
    var time        = req.body.time;
    var sortType    = req.body.sortType;
    var sortSearch  = req.body.sortSearch;
    var minPrice    = req.body.minPrice;
    var maxPrice    = req.body.maxPrice;
    dist       = (req.body.distance) ? Number(req.body.distance) : 5;
    distance        =  80* 1609.344;
    search['walkingBlock'] = {'$ne':1};
    if(req.body.text){


        var text = req.body.text;

        datae = {$or: [{'cate.serviceName' : { $regex : text,'$options' : 'i' } },{'subcate.subServiceName' : { $regex : text,'$options' : 'i' } },{'service.title' : { $regex : text,'$options' : 'i' } }, {'userName' : { $regex : text,'$options' : 'i' }}]};

    }

    search['userType'] = 'artist';
    search['isDocument'] = 3;
    search['status'] = "1";
    if(sortSearch=='price'){
        /*if(sortType==1){
            sortData["service.inCallPrice"] = -1;
        }else{
          sortData["service.inCallPrice"] = 1;
        }*/
    }else{
/*
         if(sortType==1){

           sortData["distance"] = -1;
         }else{
           sortData["distance"] = 1;
         }*/

    }

    sortData["ratingCount"] = -1;
    sortData["reviewCount"] = -1;
    if(req.body.text ==''){
        if(serviceType){

            search['serviceType'] = {'$ne':serviceType};

        }else{

            search['serviceType'] = {'$ne':2};

        }
    }

    if(minPrice){
        price2 = 0;
        price1 = 0;
        if(maxPrice!=1){
            var price1 = minPrice.length<2 ? 0+minPrice : minPrice;
            var maxPrice2 = maxPrice.length<2 ? 0+maxPrice : maxPrice;
            var price2 = maxPrice2+".00";
        }
        if(serviceType==1){

            if(price2!=0){
                if(price2=='100+'){

                    datae['service.outCallPrice'] = {$gte: price1};

                }else{

                    datae['service.outCallPrice']  = {$gte: price1,$lte: price2};
                }
            }
        }else{


            if(price2!=0){  

                if(price2=='100+'){

                    datae['service.inCallPrice']  = {$gte: price1};

                }else{

                    datae['service.inCallPrice']  = {$gte: price1,$lte: price2};
                }
                
            }
            
        }
    }


    datae['_id'] = {'$ne':authData._id};
    if(service){
        var ser = service.split(",");
        var service = ser.map(function(n) { return Number(n); });
        search['service.serviceId'] = { $in: service};
     }
    if(filter==1){
        search['_id'] = { $in: certificateData};
    }
    if(subservice){
        var subSer = subservice.split(",");
        var subservice = subSer.map(function(n) {return Number(n);});
        search['service.subserviceId'] = { $in: subservice};
    }
    if(artservice){
        var artser = artservice.split(",");
        var artService = artser.map(function(n) {return Number(n);});
        search['service._id'] = { $in: artService};
    }
   if(rating && rating!=0 && rating!='null'){

      /*  var rat = rating.split(",");
        var rating = rat.map(function(n) {return String(n);});
        search['ratingCount'] = { $in: rating};*/
       search['ratingCount'] = {$gte:parseFloat(rating)};
       datae['ratingCount']  = {'$ne':'0'};

    }

    if(day){
        search['businesshours.day'] = Number(day);
    }
    search["serviceCount"] = {$ne:'0'};
    if(time){
        time =  notify.timeConvert(time);
        search["businesshours.fullStartTime"] = {$lte:time};
        search["businesshours.fullEndTime"] = {$gte:time};
    }
    search['service.deleteStatus'] = 1;
    search['subcate.deleteStatus'] = 1;
  

        User.aggregate([
        /*
        {
            "$geoNear": {
                  "near": {
                         "type": "Point",
                         "coordinates": [parseFloat(req.body.latitude), parseFloat(req.body.longitude)]
                          },
            maxDistance: distance,
            "spherical": true,
            "distanceField": "distance",
            distanceMultiplier: 1/1609.344 // calculate distance in miles "item.name": { $eq: "ab" } }
            }
        },*/
     
        {  
            $lookup:{
                    from: "artistservices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "service"
            }
         
        }, 
        {  
            $lookup:{
                    from: "artistmainservices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "cate"
            }
         
        }, 
        {  
            $lookup:{
                    from: "artistsubsrervices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "subcate"
            }
         
        }, 
        {  
            $lookup:{
                    from: "busineshours", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "businesshours"
            }
         
        },
        { 
            $match:search 
        },
        { $sort:sortData},
        { 
            "$project": {
                "_id": 1,
                "userName": 1,
                "firstName": 1,
                "profileImage":1,
                "reviewCount":1,
                "ratingCount":1,
                "businessType":1,
                "serviceCount":1,
                "businessName":"$businessName",
                "distance":1,
                "cate.serviceName":1,
                "subcate._id":1,
                "subcate.subServiceName":1,
                "subcate.deleteStatus":1,
                "service._id":1,
                "service.serviceId":1,
                "service.subserviceId":1,
                "service.title":1,
                "service.description":1,
                "service.inCallPrice":1,
                "service.outCallPrice":1

            }
        },
        { 
             $match:datae 
        }
    ],function(err, data) {

        if(data){
            
            newData = data.length;
            next();
           
        }else{
            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        }
        
     
    }); 

}


exports.finalData = function(req,res){
   
    if (req.body.page) {
            page = Number(req.body.page)*Number(req.body.limit);
        } else {
                pg=0;
        }
        if (req.body.limit) {
             limit = Number(req.body.limit);
        } else {
                lmt=10;
        }
        
        User.aggregate([      
        {  
            $lookup:{
                from: "artistservices", 
                localField: "_id", 
                foreignField: "artistId",
                as: "service"
            }
         
        }, 
        {  
            $lookup:{
                from: "artistmainservices", 
                localField: "_id", 
                foreignField: "artistId",
                as: "cate"
            }
         
        }, 
        {  
            $lookup:{
                from: "artistsubsrervices", 
                localField: "_id", 
                foreignField: "artistId",
                as: "subcate"
            }
         
        }, 
        {  
            $lookup:{
                from: "busineshours", 
                localField: "_id", 
                foreignField: "artistId",
                as: "businesshours"
            }
         
        },
         { 
             $match:search 
         },
        { $sort:sortData},
        
       { 
            "$project": {
                "_id": 1,
                "userName": 1,
                "firstName": 1,
                "profileImage":1,
                "reviewCount":1,
                "ratingCount":1,
                "businessType":1,
                "latitude":1,
                "longitude":1,
                "businessName":"$businessName",
                "distance":1,
                "outCallLatitude":1,
                "outCallLongitude":1,
                "walkingBlock":1,
                "cate.serviceName":1,
                "cate.deleteStatus":1,
                "subcate._id":1,
                "subcate.subServiceName":1,
                "subcate.deleteStatus":1,
                "service._id":1,
                "service.serviceId":1,
                "service.subserviceId":1,
                "service.title":1,
                "service.description":1,
                "service.inCallPrice":1,
                "service.outCallPrice":1,
                "service.deleteStatus":1,
                
             }
        },
        { 
             $match:datae 
        },
        { $skip:page },
        { $limit:limit }

    ],function(err, data) {


        var newData12 = [];
        if(data){
            for (i = 0 ; i < data.length ; i++) {

                service = data[i].service;
                subcate = data[i].subcate;
                newService = [];
                newSubCat = [];
                if(service.length){
                    for (s = 0 ; s < service.length ; s++) {

                        if(service[s].deleteStatus==1){                        

                            newService.push(service[s]);
                        }
                    }
                }

                if(subcate.length){
                    for (sb = 0 ; sb < subcate.length ; sb++) {

                        if(subcate[sb].deleteStatus==1){                        

                            newSubCat.push(subcate[sb]);
                        }
                    }
                }
                
                data[i].service =  newService; 
                data[i].subcate =  newSubCat; 
                
                if(data[i].profileImage){ 
                    //data[i].profileImage = baseUrl+"/uploads/profile/"+data[i].profileImage;
                    data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;


                }

                lat = data[i].latitude;
                long = data[i].longitude;

                if(serviceType){

                    lat = data[i].outCallLatitude;
                    long = data[i].outCallLongitude;
                }
               

                data[i].distance = getDistanceFromLatLonInKm(lat,long,req.body.latitude,req.body.longitude);
                if(req.body.text){
                    newData12.push(data[i]);
                    newData = Number(newData)-Number(1);

                }else{
                        if(dist>=data[i].distance){
                            newData12.push(data[i]);
                        }
                        if(dist<data[i].distance){
                            newData = Number(newData)-Number(1);
                        }
                }

            }
            //console.log(lodash.sortBy(newData12, ['distance']));
           res.json({status: "success",message: 'successfully',totalCount:newData,artistList:lodash.orderBy(newData12, ['ratingCount', 'reviewCount','distance'], ['desc', 'desc','asc'])});

        }else{
            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
        }
        
     
    }); 
     

}


getDistanceFromLatLonInKm = function(lat1,lon1,lat2,lon2) {
    
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d / 1.6;

}


deg2rad = function(deg) {
    
    return deg * (Math.PI/180)

}


exports.addFavorite = function(req,res){
    userId = req.body.userId;
    artistId = req.body.artistId;
    type = req.body.type;
   
    if(userId ==''){
       res.json({status: 'fail',message: commonLang.REQ_USER_ID});
       return;
    }
    if(artistId == ''){
         res.json({status: 'fail',message: commonLang.REQ_ARTIST_ID});
         return;
    }
    if(type == ''){
         res.json({status: 'fail',message: commonLang.REQ_TYPE});
         return;

    }
     var addNew = {
            userId:userId,
            artistId:artistId
         };
    
    if(type == 'favorite'){
        autoId = 1;
        artistFavorite.find().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

            if (result.length > 0) {
                addNew._id = result[0]._id + 1;
            }
       
            artistFavorite(addNew).save(function(err, data) {
                if (err) {
                    res.json({status:"fail",message:err});
                    return;
                } else {
                        /*code for notification*/ 
                        var typ = '14';
                        var sender     =userId;
                        var receiver   = artistId; 
                        var notifyId   = artistId;
                        var notifyType = 'social';  
    
                        if(sender!=receiver){
                           notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 
                        }

                        /*end notification code*/ 

                    res.json({status:"success",message: 'ok'});
                    return;
                }

            });
      
        });

    }else{
          artistFavorite.deleteMany(addNew, function(err, results){
          if(err) throw err;
            res.json({status:"success",message: 'ok'});
          });
    }


}


exports.updateRecord = function(req,res){
 
    if(req.body.location){
        //old flow
        //req.body.location = [req.body.latitude,req.body.longitude];
         req.body.location = {  
                                type: "Point",
                                coordinates: [
                                   Number(req.body.longitude), //longitude
                                   Number(req.body.latitude) //latitude
                                   ]
                              }
    }
    if(req.body){
        User.updateOne({ _id:authData._id },{ $set:req.body},function(err, result) {

            User.aggregate([{  
                $lookup:{
                        from: "busineshours", 
                        localField: "_id", 
                        foreignField: "artistId",
                        as: "businesshours"
                }
             
            },
            {
                $lookup:{

                        from: "artistmainservices", 
                        localField: "_id", 
                        foreignField: "artistId",
                        as: "bizCount"
                },
            },
            { 
                $match:{_id:authData._id} 

            }],function(err, userData) {

                user = userData[0];
                let baseUrl = req.protocol + '://'+req.headers['host'];
                user.businesshours  =    (user.businesshours.length) ? 1 : 0;
                
                if(user.profileImage) 
                   // user.profileImage = baseUrl+"/uploads/profile/" + user.profileImage;
                   user.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+user.profileImage;

                if(user.dob)
                    user.dob =  moment(user.dob).format('DD/MM/YYYY');
                
                user.bizCount  =  (user.bizCount.length) ? user.bizCount.length : 0; 


                res.json({status:"success",message: commonLang.SUCCESS_UPDATE_RECORD,user:user});
            
            });


            
        });

    }
}


function readAndWriteFile(singleImg, newPath) {

        fs.readFile(singleImg.path , function(err,data) {
            fs.writeFile(newPath,data, function(err) {
                if (err) console.log('ERRRRRR!! :'+err);
                console.log('Fitxer: '+singleImg.originalFilename +' - '+ newPath);
            })
        })
}


function readAndWriteFile1(singleImg, newPath) {
     

        fs.readFile(singleImg[0].path , function(err,data) {
            fs.writeFile(newPath,data, function(err) {
                if (err) console.log('ERRRRRR!! :'+err);
                console.log('Fitxer: '+singleImg.originalFilename +' - '+ newPath);
            })
        })
}


exports.addMyStory = function(req,res){
  
    var form = new multiparty.Form();
    var fs = require('fs');
    var moment = require('moment');
    var crd =  moment().format();
    form.parse(req, function(err, fields, files) { 
        followUnfollow.find({'userId':Number(fields.userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
            folInfo = [];   
            if(followData){

                 a = [];
                 a= followData.map(a => a.followerId);
                 a.push(Number(fields.userId));         
                 folInfo.flUser =a ;

            }else{
                folInfo.flUser = [];
                
            }

           if(fields.userId){
            var userId = Number(fields.userId);
           }else{
             var userId = authData._id;
           }
            
            var baseUrl =  req.protocol + '://'+req.headers['host'];
                jsArr = []
                thumbArr = []
                if(files.myStory){
                    
                    var imgArray = files.myStory;
                    for (var i = 0; i < imgArray.length; i++) {
                         
                    var newPath = './public/uploads/myStory/';
                   
                    var singleImg = imgArray[i];
                    nmFeed = Date.now()+i;
                    if(imgArray[i].headers['content-type']=='video/mp4'){
                         nm = nmFeed+ '.mp4';
                         feedUrl = nm;
                         //thumb   =  nmFeed+ '.jpg';
                         thumb   =  nmFeed+ '.jpg';

                    }else{
                         nm = Date.now()+i+ '.jpg';
                         feedUrl = nm;
                         thumb='';
                    }    
                    
                   
                    newPath+= nm;
                    //console.log(newPath);
                    videopath ='./public/uploads/myStory/'+nm;
                    readAndWriteFile(singleImg, newPath);
                  /* if(imgArray[i].headers['content-type']=='video/mp4'){
                     var ffmpeg=require('fluent-ffmpeg');
                     
                        ffmpeg(videopath).screenshots({
                                count: 1, //number of thumbnail want to generate
                                folder: './public/uploads/myStory',//path where you want to save
                                filename: nmFeed + '.jpg',
                                size: '320x240'
                            }).on('end', function () {
         
                           });
                            
                   }*/
                    jsArr.push({
                     myStory: feedUrl,
                     type:'image'
                     
                   
                });
                           
                    }  

                }

                if(files.videoThumb){
                    
                    var imgArray1 = files.videoThumb;
                    for (var i = 0; i < imgArray1.length; i++) {
                         
                    var newPath = './public/uploads/myStory/';
                   
                    var singleImg = imgArray1[i];
                    nmFeed = Date.now()+i;
                    nm = Date.now()+i+ '.jpg';
                    feedUrl = nm;
                   
                   
                    newPath+= nm;
                    
                    videopath ='./public/uploads/myStory/'+nm;
                    readAndWriteFile2(singleImg, newPath);
                     thumbArr.push({
                     videoThumb: feedUrl
                     
                   
                    });
                           
                    }  

                }

           /* feeds = jsArr;*/
            stor =[];
          if(thumbArr.length>0){
           for(var i = 0; i < jsArr.length; i++) {
                     
                for(var j = 0; j < thumbArr.length; j++) {
                     //console.log(thumbArr[j].videoThumb);
                     if(i==j){
                        stor.push({
                            myStory: jsArr[i].myStory,
                            videoThumb:thumbArr[j].videoThumb,
                            type:'video',
                            userId: userId
                        });
                    }

                }


           

               }
            }else{
                  for(var i = 0; i < jsArr.length; i++) {
                    stor.push({
                    myStory: jsArr[i].myStory,
                    videoThumb:'',
                    type:'image',
                    userId: userId


                    });
               }

            }
           
            var addNew = [];

            autoId = 1;
            
                story.find().sort([
                    ['_id', 'descending']
                ]).limit(1).exec(function(err, userdata) {
                    var autoId = 1;

                    if (userdata.length > 0) {
                        autoId = userdata[0]._id + 1;

                    }

                    for (var k = 0; k < stor.length; k++) {
                        inc = autoId + k;

                        addNew.push({
                            _id: inc,
                            myStory: stor[k].myStory,
                            videoThumb: stor[k].videoThumb,
                            type: stor[k].type,
                            userId: stor[k].userId,
                            crd: crd,
                            upd: crd
                            
                        });

                    }
                   // console.log(addNew);

                    story.insertMany(addNew);
                    if(folInfo.flUser){
                        console.log(folInfo.flUser);
                           /*code for notification*/   

                               var appUser = require("./user");  
                               req.body.notifincationType = '13';
                               req.body.notifyId   = inc;
                               req.body.notifyType = 'social'; 
                               req.body.userId = userId; 

                            //   appUser.sendMultiple(req,res); 
                                         
                            /*end notification code*/   
                    } 
                    res.json({
                        'status': "success",
                        "message": 'ok'
                    });
                   return;
                });
        });
    });
}

exports.addMyStoryV2 = function(req,res){
  
    var form = new multiparty.Form();
    var fs = require('fs');
    var moment = require('moment');
    var crd =  moment().format();
    form.parse(req, function(err, fields, files) {
        
        if(typeof(files.myStory) == "undefined" || files.myStory ==""){
            res.json({status:"fail",message: commonLang.REQ_STORY_IMAGE});
            return;
        }

        followUnfollow.find({'userId':Number(fields.userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
            folInfo = [];   
            if(followData){

                 a = [];
                 a= followData.map(a => a.followerId);
                 a.push(Number(fields.userId));         
                 folInfo.flUser =a ;

            }else{
                folInfo.flUser = [];
                
            }

           if(fields.userId){
            var userId = Number(fields.userId);
           }else{
             var userId = authData._id;
           }
            
            var baseUrl =  req.protocol + '://'+req.headers['host'];
                jsArr = []
                thumbArr = []
                let thumboptions = {};
                   // This code is responsible to upload feed media images
                    var imgArray = files.myStory ? files.myStory : [];
                    let feedMediaPromise = imgArray.map(async function(file){
                        let ext =  file.originalFilename.split('.').pop();
                        let fileName = uniqid()+'.'+ext;
                        let videoThumb = '';

                        if(file.headers['content-type'] == 'video/mp4'){
                            videoThumb = fileName
                        }
                        else{
                            let height = commonJs.calculateFeedImageThunmbHeight(file.path);
                            thumboptions = {
                                resize: { height: height },
                                thumbFolder:commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
                              }
                        }

                        jsArr.push({
                            feedPost: fileName,
                            videoThumb: videoThumb,

                        });
                        
                        return await fileUploadObj.uploadFile(file, commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);

                    });
               // This code is responsible to upload feed media video thumb images
                var vidThumbArray = files.videoThumb ? files.videoThumb : [];
                let vidThumbPromise = vidThumbArray.map(async function(file){
                    let ext =  file.originalFilename.split('.').pop();
                    let fileName = uniqid()+'.'+ext;
                    let videoThumb = '';
                    let height = commonJs.calculateFeedImageThunmbHeight(file.path);
                    thumboptions = {
                        resize: { height: height },
                        thumbFolder:commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
                      }

                    thumbArr.push({
                        feedPost: fileName,
                        videoThumb: videoThumb,

                    });
                    
                    return await fileUploadObj.uploadFile(file, commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);

                });

           /* feeds = jsArr;*/
           let allPromise = feedMediaPromise.concat(vidThumbPromise);
          Promise.all(allPromise).then(function(values) {
            stor =[];
          if(thumbArr.length>0){
           for(var i = 0; i < jsArr.length; i++) {
                     
                for(var j = 0; j < thumbArr.length; j++) {
                     //console.log(thumbArr[j].videoThumb);
                     if(i==j){
                        console.log('mystory video image name',jsArr[i].feedPost);
                        stor.push({
                            myStory: jsArr[i].feedPost,
                            videoThumb:thumbArr[j].videoThumb,
                            type:'video',
                            userId: userId
                        });
                    }

                }


           

               }
            }else{
                  for(var i = 0; i < jsArr.length; i++) {
                     console.log('mystory image name',jsArr[i].feedPost);
                    stor.push({
                    myStory: jsArr[i].feedPost,
                    videoThumb:'',
                    type:'image',
                    userId: userId


                    });
               }

            }
           
            var addNew = [];

            autoId = 1;
            
                story.find().sort([
                    ['_id', 'descending']
                ]).limit(1).exec(function(err, userdata) {
                   
                    var autoId = 1;

                    if (userdata.length > 0) {
                        autoId = userdata[0]._id + 1;

                    }

                    for (var k = 0; k < stor.length; k++) {
                        inc = autoId + k;

                        addNew.push({
                            _id: inc,
                            myStory: stor[k].myStory,
                            videoThumb: stor[k].videoThumb,
                            type: stor[k].type,
                            userId: stor[k].userId,
                            crd: crd,
                            upd: crd
                            
                        });

                    }
                   // console.log(addNew);

                    story.insertMany(addNew);
                    if(folInfo.flUser){
                        console.log(folInfo.flUser);
                           /*code for notification*/   

                               var appUser = require("./user");  
                               req.body.notifincationType = '13';
                               req.body.notifyId   = inc;
                               req.body.notifyType = 'social'; 
                               req.body.userId = userId; 

                            //   appUser.sendMultiple(req,res); 
                                         
                            /*end notification code*/   
                    } 
                    res.json({
                        'status': "success",
                        "message": 'ok'
                    });
                   return;
                });
            }); 
        });
    });
}

function readAndWriteFile2(singleImg, newPath) {

        fs.readFile(singleImg.path , function(err,data) {
            fs.writeFile(newPath,data, function(err) {
                if (err) console.log('ERRRRRR!! :'+err);
                console.log('Fitxer: '+singleImg.originalFilename +' - '+ newPath);
            })
        })
}


exports.deleteOldStory = function(req,res,next){
     var moment = require('moment');
     var currentDate =  moment().format();
     //var then = "2018-03-26T08:54:34+00:00";

     nextData ={};
     nextData = req.body;
     story.find({},{'_id':1,'crd':1}, function(err, dataUser)
       {
        if (dataUser.length==0){
           res.json({status:"sucess",message: commonLang.RECORD_NOT_FOUND});
           return;
        }else{ 
             jsArr =[];
            for (d = 0 ; d < dataUser.length ; d++) {
                var endTime =  moment(currentDate).diff(moment(dataUser[d].crd), 'hours');
                 
                if(endTime>=24){
                    jsArr.push(Number(dataUser[d]._id));
                  
                }
          
            }
            story.remove({'_id':{'$in':jsArr}},function(err,result){
            addNotification.remove({'notifyId':{'$in':jsArr},'notifincationType':13},function(err,result){});
             nextData = result;
             next(); 
            })
        }
    });
}


exports.deleteStory = function(req,res){

    let id       = req.body.id;
    if(id==''){
        
        res.json({'status': "fail","message": commonLang.REQ_ID_FIELD});
        return;    
    }
    
    story.remove({'_id':id},function(err,result){
        addNotification.remove({'notifyId':id,'notifincationType':'13'},function(err,result){});
        res.json({status:'success',message: commonLang.SUCCESS_DELETE_STORY});
         return;
    })
}


exports.getMyStoryUser =function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    data = {};
    if(folInfo.flUser){

        if(folInfo.flUser.length){
           data['_id'] ={$in:folInfo.flUser};
        }else{

         data['_id'] = authData._id;
        }

    }else{

         data['_id'] = authData._id;
    }
    story.aggregate(
    [   {$group:{_id:"$userId", dups:{$push:"$userId"}, count: {$sum: 1}}},
     
        { "$lookup": {     
                "from": "users",     
                "localField": "_id",     
                "foreignField": "_id",     
                "as": "userInfo"   
        }},
        
         { "$unwind": "$userInfo" },

        { "$project": { 
            _id :"$userInfo._id",
            userName :"$userInfo.userName",
            firstName :"$userInfo.firstName",
            lastName :"$userInfo.lastName",
            profileImage :"$userInfo.profileImage",
            count:1
           
            }
        },
        {
            $match:data
        } 
    ],function(err,data){
        for (i = 0 ; i < data.length ; i++) {
            if(data[i].profileImage){ 
             //data[i].profileImage = baseUrl+"/uploads/profile/"+data[i].profileImage;
             data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;


            }
        }

            res.json({status: "success",message: 'ok',myStoryList: data});
        });

}


exports.getMyStory = function(req,res){
       var baseUrl =  req.protocol + '://'+req.headers['host'];
       if(req.body.userId){
         user_id =req.body.userId;        
        }else{
          user_id =authData._id;
        }
       story.find({'userId':user_id}, function(err, data)
       {
        if (data.length==0){
          res.json({status:"sucess",message: commonLang.RECORD_NOT_FOUND});
        }else{ 

            for (i = 0 ; i < data.length ; i++) {
               
              if(data[i].myStory){ 
                //data[i].myStory = baseUrl+"/uploads/myStory/"+data[i].myStory;
                data[i].myStory = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR+data[i].myStory;

              
              }
              if(data[i].videoThumb){ 
               // data[i].videoThumb = baseUrl+"/uploads/myStory/"+data[i].videoThumb;
                data[i].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.MYSTORY_MEDIA_UPLOAD_FOLDEFR+data[i].videoThumb;

              
              }
            }
          res.json({status:"success",message:'ok',allMyStory:data});
        }
      }).sort( { _id: 1 } );
}


exports.getCurrentTime = function(req, res,next) {

    var latitude = '22.7196';
    var longitude = '75.8577';
    if(req.body.latitude &  req.body.longitude){

        var latitude = req.body.latitude;
        var longitude = req.body.longitude;
    }
        
        var request = require('request');
        var loc = latitude+","+longitude // Tokyo expressed as lat,lng tuple
        var targetDate = new Date() // Current date/time of user computer
            
        var timestamp = targetDate.getTime()/1000 + targetDate.getTimezoneOffset() * 60 // Current UTC date/time expressed as seconds since midnight, January 1, 1970 UTC
        var apikey = 'AIzaSyAs3DGwYwltPsb8AdR48IX21rq5JHEimZs'
        var apicall = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + loc + '&timestamp=' + timestamp + '&key=' + apikey
           
            request(apicall, function (error, response, body) {
            if (!error && response.statusCode == 200) {

                        
                        var output = JSON.parse(body) // convert returned JSON string to JSON object
                        var offsets = output.dstOffset * 1000 + output.rawOffset * 1000 // get DST and time zone offsets in milliseconds
                        var localdate = new Date(timestamp * 1000 + offsets) // Date object containing current time of Tokyo (timestamp + dstOffset + rawOffset)
                        var hours = localdate.getHours();
                        var minutes = localdate.getMinutes();
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12; hours = hours ? hours : 12; // the hour '0' should be '12' minutes = minutes < 10 ? '0'+minutes : minutes;
                        var strTime = hours + ':' + minutes + ' ' + ampm;
                        dateTime = {};
                        //var curDate =  localdate.getUTCFullYear()+'-'+localdate.getUTCMonth()+'-'+localdate.getUTCDate();
                        var dd = localdate.getDate();
                        var mm = localdate.getMonth()+1; //January is 0!
                        var yyyy = localdate.getFullYear();

                        if(dd<10) {
                           dd = '0'+dd
                        } 
                        if(mm<10) {
                          mm = '0'+mm
                        } 
                        today = yyyy + '-' + mm + '-' + dd;
                        dateTime ={'curTime':strTime,'curDate':today,'localdate':localdate};
                        next();
                        /* res.json({'status':'success','message':'ok','serviceTime':today});
                        return;*/


            }
    });
     
}


exports.followFollowing = function(req, res) {
    

    var userId = req.body.userId;
    var followerId = req.body.followerId;
    fbData=[];
    fbData.userId =userId;
    fbData.id =followerId;
   // notify.socialBookingBadgeCount(fbData); 

    if(req.body.followerId){
        var addNew = {
            followerId:userId,
            userId: followerId
        }

    } else {
        
        return res.json({status: 'fail',message: commonLang.REQ_FOLLOWER_ID});
        
    }
    autoId = 1;
    followUnfollow.find().sort([
        ['_id', 'descending']
    ]).limit(1).exec(function(err, result) {
        if (result.length > 0) {
            addNew._id = result[0]._id + 1;
        }
        User.findOne({'_id':followerId},{'followersCount':1},function(err, fwCount) {
                       
             User.findOne({'_id':userId},{'followingCount':1},function(err, userFlCount) {
                       
                followUnfollow.findOne({'followerId': userId,'userId':followerId},function(err,data){
                    
                    if (data) {
                        if (data.status == 1) {

                                setValue = {'status': 0}
                                followerUserCount = Number(fwCount.followersCount) - 1;
                                followingUserCount = Number(userFlCount.followingCount) - 1;
                        }else{
                                setValue = {'status':1}
                                followerUserCount = Number(fwCount.followersCount)+1;
                                followingUserCount = Number(userFlCount.followingCount)+1;
                        }

                        User.updateOne({_id:followerId},{$set:{followersCount:followerUserCount}},function(err, result) {});
                        User.updateOne({_id:userId},{$set:{followingCount: followingUserCount}},function(err, result) {});    
                    
                        followUnfollow.updateOne({_id:data._id},{$set:setValue},function(err, docs) {

                            if(data.status==0){

                                /*code for notification*/ 
                                    var type = '12';
                                    var sender     =userId;
                                    var receiver   = followerId; 
                                    var notifyId   = userId;
                                    var notifyType = 'social';  
                
                                    if(sender!=receiver){
                                       notify.notificationUser(sender,receiver,type,notifyId,notifyType); 

                                    }

                                /*end notification code*/ 
                            }
                            if (err) res.json(err);
                            return res.json({status: "success",message: 'ok'});
                        });

                    }else{
                            followUnfollow(addNew).save(function(err, data) {
                                if (err) {
                                    return res.json({status:"fail",message:err});
                                } else {
                                        followerUserCount = Number(fwCount.followersCount)+1;
                                        followingUserCount = Number(userFlCount.followingCount)+1;
                                                  
                                        User.updateOne({_id:followerId},{$set:{followersCount:followerUserCount}},function(err, result) {});
                                        User.updateOne({_id:userId},{$set:{followingCount: followingUserCount}},function(err, result) {});
                                            
                                         /*code for notification*/ 
                                            var type = '12';
                                            var sender     =userId;
                                            var receiver   = followerId; 
                                            var notifyId   = userId;
                                            var notifyType = 'social';  
                        
                                            if(sender!=receiver){
                                               notify.notificationUser(sender,receiver,type,notifyId,notifyType); 
                                            }

                                         /*end notification code*/ 
                                        return res.json({status:"success",message:'ok'});
                                }

                            });

                    }

                });      
            }); 

        });

    });
}


exports.followerList = function(req,res,next){
  
    folData = {};
    folData = req.body;
   var userName = req.body.userName ? req.body.userName : '';


    followUnfollow.aggregate([
                    {$lookup: {from: "users",localField: "followerId",foreignField: "_id",as:"userInfo"}},
                    { "$unwind": "$userInfo" },

                    {
                        $match: {

                            userId:Number(req.body.userId),
                            status:1,
                            'userInfo.userName':{ $regex : userName,'$options' : 'i' }
                        }
                    },
                    {   
                        $project:{
                            followerId :"$userInfo._id",
                            userName :"$userInfo.userName",
                            firstName :"$userInfo.userName",
                            lastName :"$userInfo.userName",
                            profileImage :"$userInfo.profileImage"
                            
                        } 
                    }

         ],function(err,data){
              if (data.length==0){
                    res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
                }else{ 

                    folData.total = data.length;
                    folData.mData = mData;
                    next();
                     
                }
      
    });
 
}

exports.followingList = function(req,res,next){

    folData = {};
    folData = req.body;
    var userName = req.body.userName ? req.body.userName : '';
    followUnfollow.aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as:"userInfo"
                        }
                    },
                    
                    { "$unwind": "$userInfo" },
                    
                    {
                        $match: {
                            followerId:Number(req.body.userId),
                            status:1,
                            'userInfo.userName':{ $regex : userName,'$options' : 'i' }
                        }
                    },
                    
                    {   
                        $project:{
                            userId :"$userInfo._id",
                            userName :"$userInfo.userName",
                            firstName :"$userInfo.userName",
                            lastName :"$userInfo.userName",
                            profileImage :"$userInfo.profileImage"
                            
                        } 
                    }

        ],function(err,data){

        if (data.length==0){

            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});

        }else{ 

           folData.total = data.length;
           folData.mData = mData;
           next();
             
        }
      
    });
 
}


exports.userList = function(req, res) {
    var baseUrl =  req.protocol + '://'+req.headers['host'];
    if(req.body.userId ==''){
         res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
         return;

    }
        User.aggregate([
            { 

            "$project": {     
            "userName":1, 
            "firstName":1, 
            "lastName":1, 
            "profileImage":1   
             
        }
    }],function(err,data){
           if(data){
                for (i = 0 ; i < data.length ; i++) {
                    if(data[i].profileImage){ 
                        // data[i].profileImage = baseUrl+"/uploads/profile/"+ data[i].profileImage;
                        data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;

                    }

                }
                res.json({status: "success",message: 'ok',userList:data});
           }
          
        });
}


exports.exploreSearch = function(req, res,next) {
    var baseUrl =  req.protocol + '://'+req.headers['host'];
   // var Value_match = new RegExp(req.body.search);
   var ser = req.body.search;
   var ser = common.escapeRegExp(ser);
   var Value_match = ser;
   var distance =   80* 1609.344;
    var id =[];
    id =  Number(req.body.userId); 
    search   = {};
    sortData    = {};
    search['_id'] = { $nin: id};
    search['userName'] = {$regex:ser,$options:'i'}
    search['OTP'] = 'checked';
    search['status'] = '1';
    sortData["ratingCount"] = -1;
    sortData["reviewCount"] = -1; 
    if(req.body.userId ==''){
         res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
         return;

    }
    if(req.body.type ==''){
         res.json({'status': "fail","massage": commonLang.REQ_TYPE});
         return;

    }
    var type = 'top';
    if(req.body.type){
         var type =  req.body.type;

    }
    if(type =='top'){
        var usrLat = req.body.latitude;
        var usrLong = req.body.longitude;
        radius              = parseFloat(10);
        if(usrLat && usrLong){
        }else{
          usrLat  = authData.latitude;
          usrLong = authData.longitude;
        }
        topSearch = {};
        userSer = {};
        topSearch['feedInfo.serviceTagId'] = {'$ne':[]};
        topSearch['_id'] ={'$nin':[id]};
        topSearch['userName'] = {$regex:ser,$options:'i'}
        topSearch['OTP'] = 'checked';
        topSearch['status'] = '1';
        topSearch['postCount'] =  {$gt: 0};
        // User.find(search,{"_id":1,"userName":1,"firstName":1,"lastName":1,"profileImage":1,'postCount':1,'userType' : 1  }).exec(function(err, data) {
        //         if(data){
        //             nextDataTotal = data.length;
        //             next();

        //        }           
        //  });
             aggregateArray = [

                    // {
                    //     "$geoNear": {
                    //           "near": {
                    //                  "type": "Point",
                    //                  "coordinates": [parseFloat(usrLong), parseFloat(usrLat)]
                    //                   },
                    //     maxDistance: commonLib.milesToMeters(radius),
                    //     "spherical": true,
                    //     "distanceField": "dist.calculated",
                    //     distanceMultiplier: 1/1609.344 // calculate distance in meters "item.name": { $eq: "ab" } }
                    //     }
                    // }, 

               
                {
                    "$lookup": {
                        from: "feeds",
                        localField: "_id",
                        foreignField: "userId",
                        as: "feedInfo"
                    }
                },
                { "$unwind": {
                        path:"$feedInfo", 
                        preserveNullAndEmptyArrays:false
                    }
                },
               // {"$match":{"feedInfo.serviceTagId":{'$ne':[]}}},
                {
                  $match:topSearch
                },
                {$sort:sortData},
                 { "$group": {
                    "_id": "$_id",
                    "userName": {"$first": "$userName"},
                    "firstName": {"$first": "$firstName"},
                    "lastName":{"$first": "$lastName"},
                    "profileImage":{"$first": "$profileImage"},
                    'postCount':{"$first": "$postCount"},
                    "reviewCount":{"$first": "$reviewCount"},
                    "ratingCount":{"$first": "$ratingCount"},
                    "latitude":{"$first": "$latitude"},
                    "longitude":{"$first": "$longitude"},
                    "userType":{"$first": "$userType"},
                   }
                 },
                { 
                    "$project": {
                        "_id": 1,
                        "userName": 1,
                        "firstName": 1,
                        "lastName":1,
                        "profileImage":1,
                        'postCount':1 ,
                        "reviewCount":1,
                        "ratingCount":1,
                        "latitude":1,
                        "longitude":1,
                        "userType":1,
                        
                        
                     }
                },

             ];

            var query = User.aggregate(aggregateArray);
            
             query.exec(function(err, data) {
                     if(data){
                        
                        nextDataTotal = data.length;
                        next();

                   }   

             })


    }else if(type =='people'){
         
        
         User.find(search,{"_id":1,"userName":1,"firstName":1,"lastName":1,"profileImage":1,'postCount':1,'userType' : 1 }).exec(function(err, data) {
                if(data){
                    nextDataTotal = data.length;
                    next();

               }           
         });

    }else if(type == 'hasTag'){
         tag.find({'type':'hastag','tag':{$regex:Value_match,$options:'i'}},{"_id":1,"tag":1,"tagCount":1}).exec(function(err, data) {
            if(data){

                nextDataTotal = data.length;
                next();

               }           
         });

    }else if(type == 'place'){
       
        feed.find({'location':{$regex:Value_match,$options:'i'},'feedType':{'$ne':'text'}}).distinct("location").exec(function(err, data) {
            if(data){

                nextDataTotal = data.length;
                next();

               }           
         });
    }else if(type =='serviceTag'){
        
        res.json({status: "success",message: 'ok',totalCount:0,serviceTagList:[]});
        return; 

    }

}


exports.exploreSearchFinal = function(req,res){
    var baseUrl =  req.protocol + '://'+req.headers['host'];
   // var Value_match = new RegExp(req.body.search);
    var type = req.body.type;
    var ser = req.body.search;
    var ser = common.escapeRegExp(ser);
    var Value_match = ser;
    var id =[];
    id =  Number(req.body.userId); 
    search   = {};
    /*    search['_id'] = { $nin: id};*/
    search['userName'] = {$regex:ser,$options:'i'}
    search['OTP'] = 'checked';
    if (req.body.page) {
          page = Number(req.body.page)*Number(req.body.limit);
    } else {
      page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }
    if(req.body.userId ==''){
         res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
         return;

    }
    var type = 'top';
    if(req.body.type){
         var type =  req.body.type;

    }
    if(type =='top'){  
         var id =[];
         id =  Number(req.body.userId); 
         var usrLat = req.body.latitude;
         var usrLong = req.body.longitude;
         search['postCount'] =  {$gt: 0};
   //  code comment 09-09-20
        // var query = feed.find({'serviceTagId':{'$ne':[]}})
        // query.exec(function(err, count) {
        // User.find(search,{"_id":1,"userName":1,"firstName":1,"lastName":1,"profileImage":1,'postCount':1 ,'userType' : 1, 'reviewCount':1,'ratingCount':1, 'latitude':1, 'longitude':1 }).skip(page).limit(limit).exec(function(err, jsArr) {
       //end
           aggregateArray.push({ $skip:page });
           aggregateArray.push({ $limit:limit });

            var query1 = User.aggregate(aggregateArray);
            
             query1.exec(function(err, jsArr) {

                    if(jsArr){
                       
                       if(usrLat && usrLong){

                       }else{
                        usrLat  = authData.latitude;
                        usrLong = authData.longitude;
                       }


                        sData = [];
                        for (i = 0 ; i < jsArr.length ; i++) {

                            if(jsArr[i].profileImage){ 
                                 //jsArr[i].profileImage = baseUrl+"/uploads/profile/"+ jsArr[i].profileImage;
                                 jsArr[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].profileImage;

                            }
                            // add code for top list by distance
                            var lat = jsArr[i].latitude;
                            var long = jsArr[i].longitude;
                            jsArr[i].distance = getDistanceFromLatLonInKm(lat,long,usrLat,usrLong); 
                           // console.log('distance data',jsArr[i].distance);
                            // end code

                            if(mData){

                                var picked = lodash.filter(mData, { 'followerId': jsArr[i]._id} );
                                if(picked.length){
                                    followerStatus = 1;    
                                }else{
                                   followerStatus = 0;
                                }

                            }

                            var pCount = 0;
                          //  code comment 09-09-20
                            // if(count){

                            //     var pCount = lodash.filter(count, { 'userId': jsArr[i]._id} );
                            //     var pCount = pCount.length;

                            // }
                           // ebd
                            sData1 = [];
                           /* if(pCount!=0){
*/

                                sData1 = {
                                    'profileImage':jsArr[i].profileImage,
                                    'followerStatus' : followerStatus,
                                    '_id'            : jsArr[i]._id,
                                    'userName'       : jsArr[i].userName,
                                    'lastName'       : jsArr[i].lastName,
                                    'postCount'      : jsArr[i].postCount,
                                    //'postCount'      : pCount,
                                    'userType'       : jsArr[i].userType,
                                    'distance'       : jsArr[i].distance,
                                    'ratingCount'    : jsArr[i].ratingCount 
                                }

                                // sData[i] = sData1;

                            /*}*/
                          // code for distance and rating filter
                         // console.log('jsArr[i]jsArr[i]',sData1);
                            if(5>=jsArr[i].distance){
                                sData.push(sData1);
                              //  console.log('jsArr[i].distance',jsArr[i]);

                            }
                            if(5<jsArr[i].distance){
                               nextDataTotal = Number(nextDataTotal)-Number(1);
                            }

                          //end

                        }

                        res.json({status: "success",message: 'ok',totalCount:nextDataTotal,topList:lodash.orderBy(sData, ['ratingCount', 'reviewCount','distance'], ['desc', 'desc','asc'])});
                   }           
             });
      //  });

    }else if(type =='people'){

        var query = feed.find({})
        query.exec(function(err, count) {
         
             var id =[];
             id =  Number(req.body.userId); 
             User.find(search,{"_id":1,"userName":1,"firstName":1,"lastName":1,"profileImage":1,'postCount':1 ,'userType' : 1 }).skip(page).limit(limit).exec(function(err, jsArr) {
                    if(jsArr){
                        
                        sData = [];

                        for (i = 0 ; i < jsArr.length ; i++) {

                            if(jsArr[i].profileImage){ 
                                 //jsArr[i].profileImage = baseUrl+"/uploads/profile/"+ jsArr[i].profileImage;
                                 jsArr[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].profileImage;

                            }
                           

                            if(mData){

                                var picked = lodash.filter(mData, { 'followerId': jsArr[i]._id} );
                                if(picked.length){
                                    followerStatus = 1;    
                                }else{
                                   followerStatus = 0;
                                }

                            }

                            var pCount = 0;

                            if(count){

                                var pCount = lodash.filter(count, { 'userId': jsArr[i]._id} );
                                var pCount = pCount.length;

                            }

                            sData1 = {
                                'profileImage':jsArr[i].profileImage,
                                'followerStatus' : followerStatus,
                                '_id'            : jsArr[i]._id,
                                'userName'       : jsArr[i].userName,
                                'lastName'       : jsArr[i].lastName,
                                'postCount'      : pCount,
                                'userType'       : jsArr[i].userType,
                            }

                            sData[i] = sData1;
                        }
                        res.json({status: "success",message: 'ok',totalCount:nextDataTotal,peopleList:sData});
                   }           
             });
         });

    }else if(type =='hasTag'){
            tag.find({'type':'hastag','tag':{$regex:Value_match,$options:'i'}},{"_id":1,"tag":1,"tagCount":1 }).sort([['tagCount', 'descending']]).skip(page).limit(limit).exec(function(err, data) {
                if(data){
                  
                    res.json({status: "success",message: 'ok',totalCount:nextDataTotal,hasTagList:data});
               }           
         });

    }else if(type =='place'){

        var query = feed.find({feedType:{'$ne':'text'}})
        query.exec(function(err, count) {

            feed.aggregate([ 
                { $match : { location : {$regex:Value_match,$options:'i'},feedType:{'$ne':'text'}} },
                { "$group": { "_id": "$location" } },
/*                { "$skip": page },
                { "$limit": limit },*/
               ],function(err,data){
                jsArr =[];
                if(data.length>0){
                    a = 1;
                     for (var i = 0; i < data.length; i++) {

                       var pCount = 0;

                        if(count){

                            var pCount = lodash.filter(count, { 'location': data[i]._id} );
                            var pCount = pCount.length;

                        }


                        jsArr.push({
                            _id: a,
                            location: data[i]._id,
                            count   : pCount
                      
                        });
                         a++;
                    }

                }

                newData12 = jsArr.sort(function(a, b) {
                  var a1 = a.count,
                      b1 = b.count;
                  if (a1 == b1) return 0;
                  return a1 < b1 ? 1 : -1;
                });

                dataArrays = [], 
                dataList = []; 
                page = req.body.page;
                limit = limit;
                //split list into groups
                while (newData12.length > 0) {
                    dataArrays.push(newData12.splice(0, Number(limit)));
                }
                //show list of students from group
                dataList = dataArrays[+page - 1];

                res.json({status: "success",message: 'ok',totalCount:nextDataTotal,placeList:dataList});
             }); 
        });  

    }

}


exports.userFeedByTag = function(req,res,next){
    tagId = {};
    var findData = req.body.findData;
    if(req.body.type =='hasTag'){
         if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_TAG});
             return;
        }
        
        tag.findOne({'tag':req.body.findData,'type':'hastag'},{'_id':1}).sort([['_id', 'ascending']]).limit(1).exec(function(err, adata) {

        if(adata){
            tagId = adata;
            next();
        }
        });
    }else{
        tagId = '';  
        next();
    }

}

exports.userFeed = function(req,res,next){
  
 var baseUrl =  req.protocol + '://'+req.headers['host'];
 var Value_match = {$regex:req.body.feedType};
 var type = req.body.type;
 var feedSearch ={};
 var findData = req.body.findData;
 // feedSearch['feedType'] =Value_match;
 feedSearch['feedType'] ={'$ne':'text'};
 
  if(req.body.type =='user'){
    if(findData == ''){
        res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
        return;
    }
    
    feedSearch['userId'] = Number(findData);
    if(req.body.peopleType){

        feedSearch['serviceTagId'] = {'$ne':[]};
        
    }
     
 }
 if(req.body.type =='hasTag'){
    if(findData == ''){
        res.json({'status': "fail","massage": commonLang.REQ_TAG});
        return;
    }
      if(tagId){
        feedSearch['tagId'] = { $in: [tagId._id]};
      }
    
     
 }
 if(req.body.type =='place'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_PLACE});
          return;
        }
       feedSearch['location'] = {$regex:findData};
 }
   
   feed.aggregate([
   {
     $lookup:
       {
         from: "users",
         localField: "userId",
         foreignField: "_id",
         as: "userInfo"
       }
  },
  {
    $sort: {_id: -1}
  },
   {
     $match:feedSearch
  },
  {   
        "$project":{
            "_id":1,
            "feedType":1,
            "feedData":1,
            "caption":1,
            "city":1,
            "country":1,
            "location":1,
            "likeCount":1,
            "crd":1,
            "commentCount":1,
            "peopleTag":1,
            "userInfo._id":1,
            "userInfo.userName":1,
            "userInfo.firstName":1,
            "userInfo.lastName":1,
            "userInfo.profileImage":1
            

        } 
    }
],function(err, dataLength){
   
    if(dataLength){
        newdata = dataLength.length; 
    }else{
        newdata = 0;
    }
    next();
      


   });

}


exports.finalUserFeed = function(req, res) {
     
     search   = {};
     search['likeById'] =  Number(req.body.userId);
     search['type'] =  'feed';
     search['status'] =  1;
     var Value_match = {$regex:req.body.feedType};
     var feedSearch ={};
     var findData = req.body.findData;
    // feedSearch['feedType'] =Value_match;
    feedSearch['feedType'] ={'$ne':'text'};
    if(req.body.type =='user'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
         return;
        }

        feedSearch['userId'] = Number(findData);
        if(req.body.peopleType){

            feedSearch['serviceTagId'] = {'$ne':[]};
            
        }

    }
     if(req.body.type =='hasTag'){
        if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_TAG});
            return;
        }
          if(tagId){
            feedSearch['tagId'] = { $in: [tagId._id]};
        }

         
     }
     if(req.body.type =='place'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_PLACE});
         return;
        }
       feedSearch['location'] = {$regex:findData};
    }  


    if (req.body.page) {
          page = Number(req.body.page)*Number(req.body.limit);
    } else {
      page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }
    var baseUrl =  req.protocol + '://'+req.headers['host'];
   
    async.parallel([

        function(callback) {
            aggregateArray = [
                                          
                {
                    $match:feedSearch
                },
                {
                    $match : {'userId':{$nin: bdata}}
                },
                {
                    "$lookup": {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                { "$unwind": {
                        path:"$serviceTag", 
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.artistServiseTagId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.staffServiseTagId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.staffId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.artistId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$lookup": {
                        from: "artistservices",
                        localField: "serviceTag.tagDetails.artistServiseTagId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail"
                    }
                }, 
                {
                    "$lookup": {
                        from: "staffservices",
                        localField: "serviceTag.tagDetails.staffServiseTagId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail"
                    }
                },  
                {
                    "$lookup": {
                        from: "users",
                        localField: "serviceTag.tagDetails.staffId",
                        foreignField: "_id",
                        as: "staffDetail"
                    }
                }, 
                {
                    "$lookup": {
                        from: "users",
                        localField: "serviceTag.tagDetails.artistId",
                        foreignField: "_id",
                        as: "artistDetail"
                    }
                }, 
                { "$unwind": {
                        path:"$arstistServiceTagDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$staffServiceTagDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$staffDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$artistDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$lookup": {
                        from: "services",
                        localField: "arstistServiceTagDetail.serviceId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail.serviceId"
                    }
                },
                
                {
                    "$lookup": {
                        from: "subservices",
                        localField: "arstistServiceTagDetail.subserviceId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail.subserviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "services",
                        localField: "staffServiceTagDetail.serviceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.serviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "subservices",
                        localField: "staffServiceTagDetail.subserviceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.subserviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "artistservices",
                        localField: "staffServiceTagDetail.artistServiceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.artistServiceName"
                    }
                },
                { "$group": {
                        "_id":  "$_id",
                        "feedData": {"$first": "$feedData"},
                        "feedType": {"$first": "$feedType"},
                        "caption": {"$first": "$caption"},
                        "city": {"$first": "$city"},
                        "country": {"$first": "$country"},
                        "location": {"$first": "$location"},
                        "likeCount": {"$first": "$likeCount"},
                        "crd": {"$first": "$crd"},
                        "feedImageRatio": {"$first": "$feedImageRatio"},
                        "commentCount": {"$first": "$commentCount"},
                        "peopleTag": {"$first": "$peopleTag"},
                        "serviceTag": {"$first": "$serviceTag"},
                        "serviceTagId": {"$first": "$serviceTagId"},
                        "latitude": {"$first": "$latitude"},
                        "longitude": {"$first": "$longitude"},
                        "deleteStatus": {"$first": "$deleteStatus"},
                        "userId": {"$first": "$userId"},
                        "userInfo": {"$first": "$userInfo"},
                        "serviceTag": {
                            "$push": {
                                tag:"$serviceTag", 
                                arstistServiceTagDetail: {
                                    artistServiceID:"$arstistServiceTagDetail._id",
                                    title:"$arstistServiceTagDetail.title",
                                    description:"$arstistServiceTagDetail.description",
                                    inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                                    outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                                    completionTime:"$arstistServiceTagDetail.completionTime",
                                    buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                                    buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                                    categoryID:"$arstistServiceTagDetail.subserviceId._id",
                                    categoryName:"$arstistServiceTagDetail.subserviceId.title"
                                    
                                },
                                staffServiceTagDetail: {
                                    staffServiceID:"$staffServiceTagDetail.artistServiceId",
                                    title:"$staffServiceTagDetail.artistServiceName.title",
                                    description:"$staffServiceTagDetail.artistServiceName.description",
                                    inCallPrice:"$staffServiceTagDetail.inCallPrice",
                                    outCallPrice:"$staffServiceTagDetail.outCallPrice",
                                    completionTime:"$staffServiceTagDetail.completionTime",
                                    buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                                    buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                                    categoryID:"$staffServiceTagDetail.subserviceId._id",
                                    categoryName:"$staffServiceTagDetail.subserviceId.title"
                                },
                                staffDetail: {
                                    firstName:"$staffDetail.firstName",
                                    lastName:"$staffDetail.lastName"
                                },
                                artistDetail:{
                                    firstName:"$artistDetail.firstName",
                                    lastName:"$artistDetail.lastName"
                                }
                            }
                        }
                        
                    }
                },
                {
                $sort: {_id: -1}
                }, 
                {
                    "$project": {
                        "_id":1,
                        "feedData":1,
                        "feedType":1,
                        "caption":1,
                        "city":1,
                        "country":1,
                        "location":1,
                        "likeCount":1,
                        "crd":1,
                        "feedImageRatio":1,
                        "commentCount":1,
                        "peopleTag":1,
                        "serviceTag":1,
                        "serviceTagId":1,
                        "latitude":1,
                        "longitude":1,
                        "deleteStatus":1,
                        "userId":1,
                        "userInfo._id":1,
                        "userInfo.userName":1,
                        "userInfo.firstName":1,
                        "userInfo.lastName":1,
                        "userInfo.profileImage":1,
                         "userInfo.userType":1,
                         "userInfo.ratingCount":1,
                         "staffDetail":1
                    }
                }
            ];
            aggregateArray.push({ $skip:page });
            aggregateArray.push({ $limit:limit });

            var query = feed.aggregate(aggregateArray);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


     function(callback) {
        var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
        query.exec(function(err, ser1) {
            if (err) {
                callback(err);
            }
           
            callback(null, ser1);
        });
     },
        function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, flow) {
            if (err) {
                callback(err);
            }
 
            callback(null, flow);
        });
    }   

    ],
     
    function(err, results) {
               
   if (results[0]) {
     
              
       jsArr = [];
    for (var i = 0; i < results[0].length; i++) {
            var likeFeed = results[1].map(a => a.feedId);
             var flowSt = results[2].map(a => a.userId);
           if(results[1].length>0){
                var a = likeFeed.indexOf(results[0][i]._id);

                if(a >-1){
                     results[0][i].isLike = 1
                }else{
                     results[0][i].isLike = 0
                }
           
        }else{
                results[0][i].isLike = 0

        }
       
        if(results[2].length>0){
           var b = flowSt.indexOf(results[0][i].userId);

        if(b >-1){
            results[0][i].followerStatus = 1
        }else{
           results[0][i].followerStatus = 0
        }

        }else{
          results[0][i].followerStatus = 0

        }  

        if(folderData){
                  
            var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
              
            if(folderData1.length){

               results[0][i].isSave = 1;  
                 
            }else{

               results[0][i].isSave = 0;
            }
        } 
        
      
    }
    jsArr =results[0] ;   

       if(jsArr){
        var moment = require('moment');
            for (i = 0 ; i < jsArr.length ; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
               
              
                if(jsArr[i].feedData.length){ 
                                
                    for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                        if(jsArr[i].feedData[j].feedPost){ 
                         //jsArr[i].feedData[j].feedPost = baseUrl+"/uploads/feeds/"+ jsArr[i].feedData[j].feedPost;
                         jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+jsArr[i].feedData[j].feedPost;
                        }
                        if(jsArr[i].feedData[j].videoThumb){ 
                        // jsArr[i].feedData[j].videoThumb = baseUrl+"/uploads/feeds/"+ jsArr[i].feedData[j].videoThumb;
                        jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+jsArr[i].feedData[j].videoThumb;
                        }
                    }
                            
                }
                
                if(jsArr[i].userInfo[0]){
                     if(jsArr[i].userInfo[0].profileImage){ 
                        //jsArr[i].userInfo[0].profileImage = baseUrl+"/uploads/profile/"+jsArr[i].userInfo[0].profileImage;
                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                      }
               }else{
                 jsArr[i].userInfo[0] =[];
               }
            }
        
       }


         res.json({status: "success",message: 'successfully',AllUserFeeds: jsArr,total:newdata});
         return;   
    } else {
           res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllUserFeeds:[] });
           return;
    }
      
 });

}


/* api for user side show favorite list*/
exports.favoriteArtist = function(req,res,next){
    data = {};
    data = req.body;
    userId = req.body.userId;
   
    if(userId ==''){
         res.json({status: 'fail',message: commonLang.REQ_USER_ID});
         return;
    }
    search = {};
    sortData = {};
    search['userId']=Number(req.body.userId);
    //sortData['_id']='descending';
    artistFavorite.find(search,function(err,favData){
        
        if(favData.length){
            data.total = favData.length;
            data.favoriteArtist = favData;

            next();
         
        }else{
            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND,favoriteList:[]});
        }

    }).sort( { _id: -1 } );

}
exports.favoriteList = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var subservice = req.body.subservice;
    var service = req.body.service;
    serviceType = Number(req.body.serviceType);
    var city = req.body.city;
    var rating = req.body.rating;
    var day = req.body.day;
    var time = req.body.time;
    var minPrice    = req.body.minPrice;
    var maxPrice    = req.body.maxPrice;
    var sortType = req.body.sortType;
    var sortSearch = req.body.sortSearch;
    datae = {};

   /* if(req.body.text){
        var text = escapere(req.body.text);
        datae = {$or: [{'cate.serviceName' : { $regex : text,'$options' : 'i' } },{'subcate.subServiceName' : { $regex : text,'$options' : 'i' } },{'service.title' : { $regex : text,'$options' : 'i' } }, {'userName' : { $regex : text,'$options' : 'i' }}]};
    }  */
    search = {};
 
  
    search['userType'] = 'artist';
    search['isDocument'] = 3;
 /*   if(sortSearch=='price'){
        if(sortType==1){
            sortData["service.inCallPrice"] = -1;
        }else{
          sortData["service.inCallPrice"] = 1;
        }
    }else{
         if(sortType==1){
           sortData["distance"] = -1;
         }else{
           sortData["distance"] = 1;
         }
    }
    if(serviceType){
      search['serviceType'] = {'$ne':serviceType};
    }else{
         search['serviceType'] = {'$ne':2};
    }

    if(minPrice){
        var price1 = minPrice.length<2 ? 0+minPrice : minPrice;
        var price2 = maxPrice+".00";
        if(serviceType==1){

            if(price2!=0){
                if(price2=='100+'){

                    datae['service.outCallPrice'] = {$gte: price1};

                }else{

                    datae['service.outCallPrice']  = {$gte: price1,$lte: price2};
                }
            }
        }else{


            if(price2!=0){  

                if(price2=='100+'){

                    datae['service.inCallPrice']  = {$gte: price1};

                }else{

                    datae['service.inCallPrice']  = {$gte: price1,$lte: price2};
                }
                
            }
            
        }
    }*/

    search['_id'] = {'$ne':authData._id};
  /*  if(service){
        var ser = service.split(",");
        var service = ser.map(function(n) { return Number(n); });
        search['service.serviceId'] = { $in: service};
     }
   
    if(subservice){
        var subSer = subservice.split(",");
        var subservice = subSer.map(function(n) {return Number(n);});
        search['service.subserviceId'] = { $in: subservice};
    }

    if(rating && rating!=0){

        var rat = rating.split(",");
        var rating = rat.map(function(n) {return Number(n);});
        search['ratingCount'] = { $in: rating};
    }

    if(day){
        search['businesshours.day'] = Number(day);
    }

    search["serviceCount"] = {$ne:'0'};

    if(time){
        time =  notify.timeConvert(time);
        search["businesshours.fullStartTime"] = {$lte:time};
        search["businesshours.fullEndTime"] = {$gte:time};
    }*/

    dist       = (req.body.distance) ? Number(req.body.distance) : 5;
    distance   =  80* 1609.344;


    if (req.body.page) {
            page = Number(req.body.page)* 10;
    } else {
            page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;;
    }

    a = [];
    a= data.favoriteArtist.map(a => a.artistId);   
    search['_id'] = {$in:a};  

        User.aggregate([

        {  
            $lookup:{
                    from: "artistservices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "service"
            }
         
        },
        {  
            $lookup:{
                    from: "artistmainservices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "cate"
            }
         
        }, 
        {  
            $lookup:{
                    from: "artistsubsrervices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "subcate"
            }
         
        },  
        {  
            $lookup:{
                    from: "busineshours", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "businesshours"
            }
         
        },
        {$match:search},
        { $skip:page },
        { $limit:limit },
        
       { 
            "$project": {
                "_id": 1,
                "userName": 1,
                "firstName": 1,
                "profileImage":1,
                "reviewCount":1,
                "ratingCount":1,
                "businessType":1,
                "latitude":1,
                "longitude":1,
                "businessName":"$businessName",
                "distance":1,
                "outCallLatitude":1,
                "outCallLongitude":1,
                "cate.serviceName":1,
                "subcate.subServiceName":1,
                "service._id":1,
                "service.serviceId":1,
                "service.subserviceId":1,
                "service.title":1,
                "service.description":1,
                "service.inCallPrice":1,
                "service.outCallPrice":1,
                "service.deleteStatus":1
               
             }
        }
      
    ],function(err, result) {
        
        newData12 = [];
        if(result){
            newService = [];

            for (i = 0 ; i < result.length ; i++) {


                service = result[i].service;
                newService = [];
                if(service){
                    if(service.length){
                        for (s = 0 ; s < service.length ; s++) {

                            if(service[s].deleteStatus==1){                        

                                newService.push(service[s]);
                            }
                        }
                    }
                }

                result[i].service =  newService; 

                if(result[i].profileImage){
                    if(!validUrl.isUri(result[i].profileImage)){
                          //result[i].profileImage = baseUrl+"/uploads/profile/"+result[i].profileImage;
                          result[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+result[i].profileImage;

                    } 
                }

                lat = result[i].latitude;
                long = result[i].longitude;

              /*  if(serviceType){

                    lat = result[i].outCallLatitude;
                    long = result[i].outCallLongitude;
                }
*/
                
                result[i].distance = getDistanceFromLatLonInKm(lat,long,req.body.latitude,req.body.longitude);

/*                if(dist>=result[i].distance){
                    newData12.push(result[i]);
                }

                if(dist<result[i].distance){
                    data.total = Number(data.total)-Number(1);
                }*/
            }
           
            res.json({status: "success",message: 'ok',total:data.total,artistList:result});
        }else{
            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
        }
        
     
    }); 
}

exports.getProfile = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    userId = Number(req.body.userId);
    if(userId ==''){
        res.json({status: "fail",message: commonLang.REQ_USER_ID});
        return;
    }
    search ={};
    search['_id'] = userId;
    search['status'] ='1';
   // search['isDocument'] = 3;
    User.aggregate( [

      { $match :search },

      { 
        "$lookup": {     
                "from": "busineshours",     
                "localField": "_id",     
                "foreignField":"artistId",     
                "as": "businessHour"   
            }
      },
      {  $project: {
         _id:1,
         firstName:1,
         lastName:1,
         userName:1,
         profileImage:1,
         businessName:1,
         businesspostalCode:1,
         buildingNumber:1,
         businessType:1,
         email:1,
         dob:1,
         gender:1,
         address:1,
         address2:1,
         countryCode:1,
         contactNo:1,
         userType:1,
         followersCount:1,
         followingCount:1,
         serviceCount:1,
         certificateCount:1,
         postCount:1,
         reviewCount:1,
         ratingCount:1,
         socialReviewCount:1,
         socialRatingCount:1,
         bio:1,
         latitude:1,
         longitude:1,         
         serviceType:1,
         radius:1,
         businessEmail:1,
         businessContactNo:1,
         inCallpreprationTime:1,
         outCallpreprationTime:1,
         walkingBlock:1,
        "businessHour._id":1,
        "businessHour.startTime":1,
        "businessHour.endTime":1
         }
      },
      
    ],function(err,jsArr){
       if(jsArr==''){

          res.json({status: "fail",message: commonLang.ACCOUNT_INACTIVATE_BY_ADMIN,userDetail: jsArr});
          return;

       }else{

            cData = {};
        
            if(userId==authData._id){
                cData = {'artistId':userId};
            }else{
                cData = {'artistId':userId,'status':1};
            }
            artistCertificate.findOne(cData).count().exec(function(err, data){

                cData1 = {'artistId':userId,'status':1};

                artistCertificate.findOne(cData1).count().exec(function(err, sddata){

                    feed.findOne({'userId':userId}).count().exec(function(err, postData){
                        artistservices.findOne({'artistId':userId,'status':1,'deleteStatus':1}).count().exec(function(err, serviceData){            
                            ratingReview.findOne({'artistId':userId}).count().exec( async function(err, rCount){            
                                  if(jsArr!=''){
                                        // code for radius replace by distance between 2 lat and long 03-09-20
                                        jsArr[0].radius = getDistanceFromLatLonInKm(authData.latitude,authData.longitude,jsArr[0].latitude,jsArr[0].longitude);
                                        jsArr[0].postCount      = postData;
                                        jsArr[0].serviceCount   = serviceData;
                                       // jsArr[0].reviewCount    = rCount;//code comment according to sepeate review rating
                                        
                                        jsArr[0].certificateCount = data;  

                                        if(sddata>0){
                                            jsArr[0].isCertificateVerify = 1;
                                        }else{
                                             jsArr[0].isCertificateVerify =0;
                                        }
                                       
                                        if(jsArr[0].profileImage){
                                            if(!validUrl.isUri(jsArr[0].profileImage)){
                                                 //jsArr[0].profileImage = baseUrl+"/uploads/profile/"+jsArr[0].profileImage;
                                                 jsArr[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[0].profileImage;

                                            } 
                                         
                                        }

                                       if(mData){
                                                
                                            var picked = lodash.filter(mData, { 'followerId': jsArr[0]._id} );
                                            if(picked.length){

                                               jsArr[0].followerStatus = 1;    
                                            }else{

                                               jsArr[0].followerStatus = 0;
                                            }
                                        }
                                      
                                        if(favData){
                                                
                                            var picked = lodash.filter(favData, { 'artistId': jsArr[0]._id} );
                                            if(picked.length){

                                               jsArr[0].favoriteStatus = 1;    
                                            }else{

                                               jsArr[0].favoriteStatus = 0;
                                            }
                                        }

                                        if(invitationCheck){

                                            jsArr[0].isInvitation = invitationCheck.length ? 1 : 0;

                                        }
                                        if(adminCommission){
                                            jsArr[0].isAdminComission = adminCommission.length ? 1 : 0;
                                        }
                                   

                                    res.json({status: "success",message: 'ok',userDetail: jsArr});
                                }
                            });
                        });
                    });
                });
                
            }); 
    }

});

}


exports.sendNotification = function(senderId,receiverId,type,dataInfo,notifyId,notifyType,serviceName,paymentType,startDate,endDate){
      
            console.log('sender data',senderId,'type',type);
            var autoPay='';
            switch (type) {

                 case '1':

                     var body = commonLang.SENT_BOOKING_REQUEST;
                     var title = 'Booking Request';
                     break;

                 case '2':
                    var body = commonLang.ACCEPT_BOOKING_REQUEST;
                    if(paymentType==1){
                        var body = commonLang.CHECK_BOOKING_REQUEST;
                    }   var title = 'Booking Accept';
                     break;

                 case '3':
                     var body = commonLang.REJECT_BOOKING_REQUEST;
                     var title = 'Booking Reject';
                     break;

                 case '4':
                     var body = commonLang.CHECK_BOOKING_CANCEL;
                     var title = 'Booking Cancel';
                     break;

                 case '5':
                     var body = commonLang.COMPLETE_BOOKING_REQUEST;
                     var title = 'Booking Complete';
                     break;

                 case '6':

                     var body = commonLang.GIVEN_REVIEW_BOOKING;
                     if(serviceName =='3'){
                       var body = commonLang.GIVEN_REVIEW_BOOKING_ARTIST;
                     }
                     var title = 'Booking Review';
                     break;

                 case '7':
                     var body = commonLang.ADD_NEW_POST;
                     var title = 'New Post';
                     break;

                case '8':
                    var body = commonLang.CONFIRM_BOOKING;
                    var title = 'Booking Request';
                    break;

                 case '9':
                     var body = commonLang.COMMENT_POST;
                     var title = 'Comment';
                     break;

                 case '10':
                     var body = commonLang.LIKED_POST;
                     var title = 'Post Like';
                     break;

                 case '11':
                     var body = commonLang.LIKED_COMMENT;
                     var title = 'Comment Like';
                     break;
                 case '12':
                     var body = commonLang.START_FOLLOW;
                     var title = 'Following';
                     break;

                case '13':
                     var body = commonLang.ADD_STORY;
                     var title = 'Story';
                     break;
               
                case '14':
                     var body = commonLang.ADD_FAVOURITE;
                     var title = 'Favourites';
                     break;  
                case '16':
                 var body = commonLang.TAG_POST;
                 var title = 'Tag';
                 break;    
                case '17':
                 var body = commonLang.CHECK_STAFF_INVITATION;
                 var title = 'Staff invitation';
                 break;    
                case '18':
                 var body = commonLang.ACCEPT_INVITATION;
                 var title = 'Staff invitation';
                break;  
                case '19':
                 var body = commonLang.ON_WAY_LOCATION;
                 var title = 'Booking Service';
                break; 
                case '20':
                // var body = serviceName+commonLang.SERVICE_START;
                 var body =  commonLang.SERVICE_START;
                 var title = 'Booking Started';
                break; 
                case '21':
                 //var body = serviceName+commonLang.SERVICE_COMPLETE;
                 var body =  commonLang.SERVICE_COMPLETE;
                 var title = 'Booking Finished';
                break;         
                case '22':
                 var body = commonLang.REACH_ON_LOCATION;
                 var title = 'Booking Service';
                break; 
                case '24':
                 var body = commonLang.ALLOTED_HOLIDAY+' '+startDate+' to '+endDate;
                 var title = 'Staff holiday';
                break;
                case '26':
                 var body = commonLang.REPORTED_YOU+' '+serviceName;
                 var title = 'Booking Report';
                break; 
                case '27':
                 var body = commonLang.CHANGE_PAYMENT_STATUS;
                 var title = 'Payment Status';
                 break;
                 case '29':
                 var body = 'Booking has been automatically confirmed & Payment completed by';
                 var title = 'Booking Request';
                 break;
                 case '30':
                  var title = 'Booking Accept';
                  var body = 'has Accepted your Booking & Your Payment was processed successfully';
                  break;
                  case '31':
                  var title = 'Booking Reminder';
                  var body = 'Hi Customer name, just a quick reminder your Service name booking is confirmed at booking at';
                  break;

            }

   
    notification = {
        title:title, 
        body: body,
        notifincationType:type,
        sound: "default",
        'notifyId' : notifyId,
        click_action:"ChatActivity"
    };
   
    data = {
        title:title, 
        body: body,
        notifincationType:type,
        notifyId : notifyId,
        click_action:"ChatActivity"
    };


    webData = {
        title:title,
        body:body,
        url:'/allBookinghistory'
    }
   
    if(dataInfo){
  
        var i = 0;

      
        async.each(dataInfo, function(rs, callback){
        
        if(rs._id==receiverId){

            token = rs.firebaseToken;
            deviceType = rs.deviceType;
            appType = rs.appType;

            //userName = rs.userName;

            }else{

               userName = rs.userName;
               profileImage = "http://koobi.co.uk:3000/uploads/profile/"+rs.profileImage;
               userType = rs.userType;
               businessName = rs.businessName;

            }

            callback();
            i++;
        },function(){
            console.log('userName id data data',receiverId,userName);
            uName = '';

           if(userName){ 
                uName =   userName[0].toUpperCase() + userName.slice(1);
            }

            if(type =='1'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;
            }else if(type == '2'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;  
            }else if(type =='3'){
                notification.body = 'Sorry!'+' '+uName+' '+body;
                data.body         = 'Sorry!'+' '+uName+' '+body;
                webData.body      = 'Sorry!'+' '+uName+' '+body;
            } else if(type =='4'){
                notification.body = 'Sorry! Your appointment with'+' '+uName+' '+body;
                data.body         = 'Sorry! Your appointment with'+' '+uName+' '+body;
                webData.body      = 'Sorry! Your appointment with'+' '+uName+' '+body;
            }else if(type =='5'){
                 notification.body = 'Appointment with'+' '+uName+' '+body;
                 data.body         = 'Appointment with'+' '+uName+' '+body;
                 webData.body      = 'Appointment with'+' '+uName+' '+body;
            }else if(type =='6'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='7'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='8'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='9'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='10'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='11'){
                  notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='12'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='13'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='14'){
                 notification.body = 'Great work!'+' '+uName+' '+body;
                 data.body         = 'Great work!'+' '+uName+' '+body;
                 webData.body      = 'Great work!'+' '+uName+' '+body;

            }else if(type =='15'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='16'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='17'){
                  notification.body = businessName+' '+body;
                data.body = businessName+' '+body;
                webData.body = businessName+' '+body;

            }else if(type =='18'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='19'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='20'){
                 notification.body =  serviceName+' with '+uName+' '+body;
                 data.body         =  serviceName +' with '+uName+' '+body;
                 webData.body      =  serviceName+' with '+uName+' '+body;

            }else if(type =='21'){
                 notification.body =  serviceName+' with '+uName+' '+body;
                 data.body         =  serviceName +' with '+uName+' '+body;
                 webData.body      =  serviceName+' with '+uName+' '+body;

            }else if(type =='22'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='23'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='24'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='25'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='26'){
                notification.body = uName+' '+body;
                data.body = uName+' '+body;
                webData.body = uName+' '+body;

            }else if(type =='27'){
               notification.body = uName+' '+body;
               data.body = uName+' '+body;
               webData.body = uName+' '+body;

            }else if(type=='29'){
                notification.body = body+' '+uName;
                data.body         = body+' '+uName;
                webData.body      = body+' '+uName;  
            }else if(type=='30'){
               notification.body =  uName+' '+body;
                data.body         =  uName+' '+body;
                webData.body      =  uName+' '+body;  
            }else if(type=='31'){
                notification.body =  uName+' '+body;
                data.body         =  uName+' '+body;
                webData.body      =  uName+' '+body;  

            }else{

            }

           /* if(type=='1'){

                notification.body = 'Your booking has been sent to '+uName+' for confirmation. Click here to see the status of your booking.';
                data.body = 'Your booking has been sent to '+uName+' for confirmation. Click here to see the status of your booking.';
                webData.body = 'Your booking has been sent to '+uName+' for confirmation. Click here to see the status of your booking.';


            }*/
        
            data.urlImageString = notification.urlImageString = profileImage;
            data.userType =  notification.userType =  userType;
            data.userName =  notification.userName =  uName;
  
            var moment = require('moment');
            var crd = moment().format();
            insertData={};
            if(type =='29'){
                insertData['notifincationType'] =1;
            }else if(type =='30'){
                insertData['notifincationType'] =8;

            }else{
                insertData['notifincationType'] =type;
            }
            if(paymentType==1){
              insertData['paymentType'] =1 ;
            } 
            insertData['senderId']   = senderId;
            insertData['receiverId'] = receiverId;
            insertData['crd'] = crd;
            insertData['upd'] = crd;
            insertData['notifyId'] = notifyId;
            insertData['type'] = notifyType;
            insertData['serviceName'] = serviceName;
            if(type=="24"){
               insertData['startDate'] = startDate;
               insertData['endDate'] = endDate;
            }
         
            if(type=="10" || type=="11" || type=="12" || type=="14"){
                    
                addNotification.deleteMany({'notifyId':notifyId,'notifincationType':type,'senderId':senderId,'receiverId':receiverId,'type':notifyType},function(err, results) {});

            }
                        
                                                   
            addNotification.find().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {
               
                if (result.length > 0) {
                    insertData._id = result[0]._id + 1;
                }else{
                    insertData._id = 1;
                }
                //console.log('result[0]._idresult[0]._id',insertData);
                addNotification(insertData).save(function(err, notifyData) {

                    //if(type=="24" || type=="21"){ change for notification view issue
                    if(type=="21"){
                    
                        addNotification.deleteMany({'_id':insertData._id},function(err, results) {});

                    }

                    if(deviceType =='3'){
                    
                        notify.sendWebNotification(receiverId,webData);
                    
                    }else{

                     console.log(appType);
 
                        if(appType){

                            if((appType=="biz") && (type=="1" || type=="4" || type=="6" || type=="7" || type=="8" || type=="9" || type=="10" || type=="11" || type=="12" || type=="14" || type=="15" || type=="16" || type=="17" || type=="18" || type=="19" || type=="20" || type=="21" || type=="22" || type=="23" || type=="24" || type=="25" || type=="26" || type=="27" || type=="28")){
                                console.log('aaa');

                                notify.bedgeCount(receiverId,notifyType,'businessBookingBadgeCount',type);

                                notify.sendNotification(token,notification,data,receiverId,'businessBookingBadgeCount');

                            }else  if((appType=="social") && (type=="2" || type=="3" || type=="5" || type=="6"  || type=="7" || type=="8" || type=="9" || type=="10" || type=="11" || type=="12" || type=="13" || type=="16" || type=="17" || type=="18" || type=="19" || type=="20" || type=="21Remove" || type=="22" || type=="23" || type=="24" || type=="25" || type=="26" || type=="27" || type=="28")){
                               console.log('bb');
                                notify.bedgeCount(receiverId,notifyType,'socialBookingBadgeCount',type);
                                notify.sendNotification(token,notification,data,receiverId,'socialBookingBadgeCount');
                            }


                            if((appType=="biz") && (type=="29")){
                                console.log('automatic biz msg');

                                notify.bedgeCount(receiverId,notifyType,'businessBookingBadgeCount','1');

                                notify.sendNotification(token,notification,data,receiverId,'businessBookingBadgeCount');

                            }else  if((appType=="social") && (type=="30")){
                                console.log('automatic social msg');
                                notify.bedgeCount(receiverId,notifyType,'socialBookingBadgeCount','8');
                                notify.sendNotification(token,notification,data,receiverId,'socialBookingBadgeCount');
                            }
                             console.log('dddd',type,"app type",appType);
                        }else{
                              console.log('cc');
                              notify.sendNotification(token,notification,data);

                        }
                    }
                });
          
           });   

            
        });
    } 
}
exports.followrsCheckData = function(req,res, next){

    userId = req.body.userId ? Number(req.body.userId) : authData._id; 
    loginUserId = req.body.loginUserId ? Number(req.body.loginUserId) : authData._id;

    if(userId == ''){
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
        return;

    }
    if(loginUserId ==''){
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
        return;

    }
    if(loginUserId){

        followUnfollow.aggregate([
            {
                $match: {followerId:loginUserId,status:1}
            },

            {
                $lookup: {
                    from: "users",
                    localField: "userId",
                    foreignField: "_id",
                    as:"userInfo"
                    }
            },

            { "$unwind": "$userInfo" },

            {   
                $project:{
                    followerId :"$userInfo._id",
                    userName :"$userInfo.userName",
                    firstName :"$userInfo.firstName",
                    lastName :"$userInfo.lastName",
                    profileImage :"$userInfo.profileImage"
                    
                } 

            }],function(err,data){

                
                if(data){
                
                    mData = data;
                    

                }

                next();
         });

    }else{

        mData = [];
        next();
    }


}

exports.followersData = function(req, res, next){
  
    var baseUrl =  req.protocol + '://'+req.headers['host'];

    if (req.body.page) {
         page = Number(req.body.page)*Number(req.body.limit);
    } else {
         page=0;
    }
    if (req.body.limit) {
         limit = Number(req.body.limit);
    } else {
         limit=10;
    }
    var userName = req.body.userName ? req.body.userName : '';
    followUnfollow.aggregate([

        {
            $lookup: {
                from: "users",
                localField: "followerId",
                foreignField: "_id",
                as:"userInfo"
                }
        },

        {
            $match: {

                userId:userId,
                status:1,
                'userInfo.userName':{ $regex : userName,'$options' : 'i' }
            }
        },

        { "$unwind": "$userInfo" },

        { $skip:page },
 
        { $limit:limit },
 
        {   
            $project:{
                userId:1,
                followerId :"$userInfo._id",
                userName :"$userInfo.userName",
                firstName :"$userInfo.firstName",
                lastName :"$userInfo.lastName",
                profileImage :"$userInfo.profileImage"
                
            } 

        }],function(err,data){
                
               if(data){ 
                    for (i = 0 ; i < data.length ; i++) {
                        if(data[i].profileImage){ 
                            //data[i].profileImage = baseUrl+"/uploads/profile/"+ data[i].profileImage;
                            data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;

                        }

                        if(folData.mData){

                 
                            var picked = lodash.filter(mData, { 'followerId': data[i].followerId} );
                            if(picked.length){

                                data[i].followerStatus = 1;    

                            }else{

                                data[i].followerStatus = 0;
                            }
                        }
          

                    }
                    res.json({status: "success",message: 'successfully',followerList: data,total:folData.total});
                    return;  
                } else {
                   res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,followerList:[] });
                    return;
                }

                            
      
    });
 
}
exports.followingData = function(req, res, next){
  
    var baseUrl =  req.protocol + '://'+req.headers['host'];

    if (req.body.page) {
         page = Number(req.body.page)*Number(req.body.limit);
    } else {
         page=0;
    }
    if (req.body.limit) {
         limit = Number(req.body.limit);
    } else {
         limit=10;
    }
    var userName = req.body.userName ? req.body.userName : '';

    followUnfollow.aggregate([


        {
            $lookup: {

                from: "users",
                localField: "userId",
                foreignField: "_id",
                as:"userInfo"
            }
        },

        { "$unwind": "$userInfo" },

        {
            $match: {

               followerId:userId,
               status:1,
               'userInfo.userName':{ $regex : userName,'$options' : 'i'}
            }
        },

        { $skip:page },
 
        { $limit:limit },
 
        {   
            $project:{
                userId:1,
                followerId :"$userInfo._id",
                userName :"$userInfo.userName",
                firstName :"$userInfo.firstName",
                lastName :"$userInfo.lastName",
                profileImage :"$userInfo.profileImage"
                
            } 

        }],function(err,data){

                
               if(data){ 
                    for (i = 0 ; i < data.length ; i++) {
                        if(data[i].profileImage){ 
                            //data[i].profileImage = baseUrl+"/uploads/profile/"+ data[i].profileImage;
                            data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;

                        }

                        if(folData.mData){

                 
                            var picked = lodash.filter(mData, { 'followerId': data[i].followerId} );
                            if(picked.length){

                                data[i].followerStatus = 1;    
                            }else{

                                data[i].followerStatus = 0;
                            }
                        }
          

                    }
                    res.json({status: "success",message: 'successfully',followingList: data,total:folData.total});
                    return;  
                } else {
                   res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,followingList:[] });
                    return;
                }

                            
      
    });
 
}

exports.getNotificationList = function(req,res,next){
   
    userId  = Number(req.body.userId);
    type    = req.body.type;
    search  = {};
    search.receiverId = userId;
    search.notifincationType = {$in:[2,3,5,6,7,9,10,11,12,13,16,17,26,27,20,21,22,26,27,31]};

    if(authData.appType){
        if(authData.appType=="biz"){

           search.notifincationType = {$in:[1,4,6,8,9,10,11,12,13,14,15,17,18,24,26,26]};
        }
    }
    if(userId == ''){
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
        return;
    }

    addNotification.aggregate([
    {
        $match:search
    },
     {
        $lookup: {
            from: "users",
            localField: "receiverId",
            foreignField: "_id",
            as:"reminderUser"
            }
    },
    {
        $lookup: {
            from: "users",
            localField: "senderId",
            foreignField: "_id",
            as:"userInfo"
            }
    },

    { "$unwind": "$userInfo" },

    {   
        $project:{
            senderId :"$userInfo._id",
            userName :"$userInfo.userName",
            firstName :"$userInfo.firstName",
            lastName :"$userInfo.lastName",
            profileImage :"$userInfo.profileImage",
            reminderUser :"$reminderUser.userName",
            userType :"$userInfo.userType",
            notifincationType :1,
            type :1,
            readStatus :1
            
        } 

    }],function(err,data){
      
        
        if(data){
        
            total = data.length;
            

        }
        next();
 });


}
exports.getNotificationListFinal = function(req,res){

    var Cryptr = require('cryptr'),
    cryptr = new Cryptr('1234567890');
    
    var baseUrl =  req.protocol + '://'+req.headers['host'];

    if (req.body.page) {
         page = Number(req.body.page)*Number(req.body.limit);
    } else {
         page=0;
    }
    if (req.body.limit) {
         limit = Number(req.body.limit);
    } else {
         limit=10;
    }

    sortData = {};
    
    sortData["_id"] = -1;

    addNotification.aggregate([
    {
        $match:search
    },
    { $sort:sortData},
    {
        $lookup: {
            from: "users",
            localField: "receiverId",
            foreignField: "_id",
            as:"reminderUser"
            }
    },
    {
        $lookup: {
            from: "users",
            localField: "senderId",
            foreignField: "_id",
            as:"userInfo"
            }
    },

    { "$unwind": "$userInfo" },
    { $skip:page },
    { $limit:limit },
    {   
        $project:{
            senderId :"$userInfo._id",
            userName :"$userInfo.userName",
            firstName :"$userInfo.firstName",
            lastName :"$userInfo.lastName",
            profileImage :"$userInfo.profileImage",
            reminderUser :"$reminderUser.userName",
            userType :"$userInfo.userType",
            notifincationType :1,
            paymentType :1,
            notifyId :1,
            type :1,
            readStatus :1,
            crd:1,
            serviceName:1,
            startDate:1,
            endDate:1
            
        } 

    }],function(err,data)
    {
        var moment = require('moment');            
        if(data){
        
            for (i = 0 ; i < data.length ; i++) {
                
                if(data[i].profileImage){

                    if(!validUrl.isUri(data[i].profileImage)){
                         //data[i].profileImage = baseUrl+"/uploads/profile/"+data[i].profileImage;
                         data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data[i].profileImage;

                    } 
                 
                }
                data[i].redirectId = cryptr.encrypt(data[i].notifyId);
                data[i].timeElapsed = moment(data[i].crd).fromNow();
                switch (String(data[i].notifincationType)) {

                    case '1':

                         var body = data[i].userName+' '+commonLang.SENT_BOOKING_REQUEST;
                         var title = 'Booking Request';
                         break;

                     case '2':
                         var body = data[i].userName+' '+commonLang.ACCEPT_BOOKING_REQUEST;
                          if(data[i].paymentType==1){
                            var body = data[i].userName+' '+commonLang.CHECK_BOOKING_REQUEST;
                          }
                         var title = 'Booking Accept';
                         break;

                     case '3':
                         var body =  'Sorry!'+' '+data[i].userName+' '+commonLang.REJECT_BOOKING_REQUEST;
                         var title = 'Booking Reject';
                         break;

                     case '4':
                         var body = 'Sorry! Your appointment with'+' '+data[i].userName+' '+commonLang.CHECK_BOOKING_CANCEL;
                         var title = 'Booking Cancel';
                         break;

                     case '5':
                         var body = 'Appoinment with'+' '+data[i].userName+' '+commonLang.COMPLETE_BOOKING_REQUEST;
                         var title = 'Booking Complete';
                         break;

                     case '6':

                         var body = data[i].userName+' '+commonLang.GIVEN_REVIEW_BOOKING;
                         if(data[i].serviceName =='3'){
                           var body = data[i].userName+' '+commonLang.GIVEN_REVIEW_BOOKING_ARTIST;
                          }
                         var title = 'Booking Review';
                         break;

                     case '7':
                         var body = data[i].userName+' '+commonLang.ADD_NEW_POST;
                         var title = 'New Post';
                         break;

                    case '8':
                        var body = commonLang.CONFIRM_BOOKING;
                        var title = 'Payment';
                        break;

                     case '9':
                         var body = data[i].userName+' '+commonLang.COMMENT_POST;
                         var title = 'Comment';
                         break;

                     case '10':
                         var body = data[i].userName+' '+commonLang.LIKED_POST;
                         var title = 'Post Like';
                         break;

                     case '11':
                         var body =data[i].userName+' '+ commonLang.LIKED_COMMENT;
                         var title = 'Comment Like';
                         break;
                     case '12':
                         var body = data[i].userName+' '+commonLang.START_FOLLOW;
                         var title = 'Following';
                         break;

                    case '13':
                         var body = data[i].userName+' '+commonLang.ADD_STORY;
                         var title = 'Story';
                         break;
                   
                    case '14':
                         var body =  'Great work!'+' '+data[i].userName+' '+commonLang.ADD_FAVOURITE;
                         var title = 'Favourites';
                         break;  

                    case '15':

                        data[i].profileImage = baseUrl+'/front/img/loader.png';
                        data[i].userName = '';
                        var body = 'Awesome!'+' '+data[i].userName+' '+commonLang.CERTIFICATE_VERIFY_BY_ADMIN;
                        var title = 'Certificate verified';
                        break;   
                    case '16':
                        var body = data[i].userName+' '+commonLang.TAG_POST;
                        var title = 'Tag';
                     break; 
                    case '17':
                     var body =data[i].userName+' '+ commonLang.CHECK_STAFF_INVITATION;
                     var title = 'Staff invitation';
                     break;    
                    case '18':
                     var body = data[i].userName+' '+commonLang.ACCEPT_INVITATION;
                     var title = 'Staff invitation';
                     break;  
                    case '19':
                     var body = data[i].userName+' '+commonLang.ON_WAY_LOCATION;
                     var title = 'Booking Service';
                    break; 
                    case '20':
                     var body = data[i].serviceName+' with '+data[i].userName+' '+commonLang.SERVICE_START;
                              
                     var title = 'Booking Started';
                    break; 
                    case '21':
                    var body = data[i].serviceName+' with '+data[i].userName+' '+commonLang.SERVICE_COMPLETE;

                     var title = 'Booking Finished';
                    break;         
                    case '22':
                     var body = data[i].userName+' '+commonLang.REACH_ON_LOCATION;
                     var title = 'Booking Service';
                    break; 
                    case '24':
                     var body = commonLang.ALLOTED_HOLIDAY+' '+data[i].startDate+' to '+data[i].endDate;
                     var title = 'Staff holiday';
                    break;
                    case '26':
                        var body = 'Oh No!'+' '+data[i].userName+' '+commonLang.REPORTED_YOU+' '+data[i].serviceName;
                        var title = 'Booking Report';
                     break;  
                    case '27':
                        var body = commonLang.CHANGE_PAYMENT_STATUS+ data[i].userName;
                        var title = 'Payment Status';
                     break;
                    case '31':
                        var body = 'Hi '+data[i].reminderUser+' '+commonLang.BOOKING_REMINDER_TODAY+' '+data[i].serviceName;
                        var title = 'Payment Status';
                     break;  

                }
                data[i].message = body;
               
                if(data[i].notifincationType =='8'){
                    data[i].message = body+' '+data[i].userName;

                }else if(data[i].notifincationType !='15'){
                      //  data[i].message = data[i].userName+' '+body;
                }
            }
            res.json({status:"success",message:'ok',total:total,notificationList:data});

        }else{
               res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
        }
    });
}

exports.readNotification = function(req,res){

    notificationId = Number(req.body.notificationId);

    if(notificationId == ''){
   
        res.json({status:"fail",message: commonLang.REQ_NOTIFICATION_ID});
        return;

    }

    addNotification.updateMany({'_id':notificationId},{$set:{readStatus:1}}, function(err, result){
      if(result.n){
          res.json({status:"success",message:'ok'});
      }else{
         res.json({status:"fail",message: commonLang.FAIL});
      }
  
    });
}

exports.checkFavorite = function(req,res,next){
    artistFavorite.find({'userId':Number(loginUserId)}).exec(function(err,userData){
        if(userData){
             favData = userData;
        }else{
             favData = [];
        }
       
        next();
    });

}

exports.invitationCheck = function(req,res,next){

    staff.find({'artistId':Number(loginUserId),'status':0}).exec(function(err,userData){
        if(userData){
            invitationCheck = userData;
        }else{
            invitationCheck = [];
        }
       
        next();
    });

}
exports.paymentList = function(req,res,next){
     var userId = Number(req.body.userId);
     var userType = req.body.userType;
     var baseUrl =  req.protocol + '://'+req.headers['host'];
     if(userId == ''){
   
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
        return;

    }
    if(userType == ''){
   
        res.json({status:"fail",message: commonLang.REQ_USER_TYPE});
        return;

    }
    whereData = {};
 
    whereData['bookStatus'] = '3';  

    if(userType =='artist'){
        
         whereData['artistId'] = userId; 
     
     }else{
        
         whereData['userId'] = userId;
     }
     

    booking.aggregate([ 
      { $match:whereData},
      { $sort : { 'bookingDate':-1,'timeCount':-1 } },
      {   
        "$project":{
            "_id":1
        } 
      }
    ],function(err, dataLength){
   
    if(dataLength){
        total = dataLength.length; 
    }else{
        total = 0;
    }
    next();
      
  });



}
exports.paymentListInfo= function(req,res,next){
  if (req.body.page) {
          page = Number(req.body.page)*Number(req.body.limit);
    } else {
      page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }

 booking.aggregate([
   { $match:whereData},
   { $sort : { 'bookingDate':-1,'timeCount':-1 } },
    { $skip:page},
    { $limit:limit },
   {
        "$lookup": {
            "from": "users",
            "localField": "userId",
            "foreignField": "_id",
            "as": "userDetail"
        }
    },           
    {
    "$lookup": {
       "from": "users",
       "localField": "artistId",
       "foreignField": "_id",
       "as": "artistDetail"
    }
    },
   
    {
        "$project": {
            "_id": 1,
            "bookingDate": 1,
            "bookingTime": 1,
            "totalPrice": 1,
            "paymentType": 1,
            "paymentStatus": 1,
            "location": 1,
            "bookStatus": 1,
            "timeCount":1,
            "reviewStatus":1,
            "reviewByUser":1,
            "reviewByArtist":1,
            "userRating":1,
            "artistRating":1,
            "transjectionId":1,
            "artistId": { "$arrayElemAt": [ "$artistDetail._id",0] },
            "artistName":{ "$arrayElemAt": [ "$artistDetail.userName",0] },
            "artistProfileImage":{ "$arrayElemAt": [ "$artistDetail.profileImage",0] },
            "userId": { "$arrayElemAt": [ "$userDetail._id",0] },
            "userName":{ "$arrayElemAt": [ "$userDetail.userName",0] },
            "userProfileImage":{ "$arrayElemAt": [ "$userDetail.profileImage",0] }
            
           
        }
    },
   
],function(err, data){
   
    if(data){
        
        bookingInfo = data;
        next();
    }else{
         res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
        return;
    }
   
      


   });
}
exports.paymentListFinal = function(req,res){
    var baseUrl =  req.protocol + '://'+req.headers['host'];
    bookId = [];
    bookId= bookingInfo.map(bookId => bookId._id);
     
    
    bookingService.aggregate([
    {
        "$lookup": {
           "from": "users",
           "localField": "staff",
           "foreignField": "_id",
           "as": "staffInfo"
        }
    },
       
    {
        "$lookup": {
            "from": "artistservices",
            "localField": "artistServiceId",
            "foreignField": "_id",
            "as": "artistService"
        }
    },
    
    { $match:{
               bookingId:{$in:bookId}
             }
    },
              
    {
        "$project": {
            "bookingId": 1,
            "artistServiceId": 1,
            "artistServiceName": { "$arrayElemAt": [ "$artistService.title",0] }
     
                  
        }
    },
   
],function(err, data){
    
    if(data){
              
        for (var i = 0; i < bookingInfo.length; i++) {
              if(bookingInfo[i].userProfileImage){
                   // bookingInfo[i].userProfileImage =  baseUrl+"/uploads/profile/"+bookingInfo[i].userProfileImage; 
                    bookingInfo[i].userProfileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+bookingInfo[i].userProfileImage;

                }
               if(bookingInfo[i].artistProfileImage){
                    //bookingInfo[i].artistProfileImage =  baseUrl+"/uploads/profile/"+bookingInfo[i].artistProfileImage;
                      bookingInfo[i].artistProfileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+bookingInfo[i].artistProfileImage;
 
                }
                 serviceArr = [];
                for (var j = 0; j < data.length; j++) {
                   
                    if (bookingInfo[i]._id == data[j].bookingId) {
                         
                        serviceArr.push(data[j].artistServiceName)


                    }
                }
                bookingInfo[i].artistService = serviceArr;
        }
         res.json({status: "success",message: 'ok',total:total,paymentList:bookingInfo});

    }else{
         res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
        return;
    }
 });

}


exports.sendMultiple = function(req, res) {

     userId = Number(req.body.userId);
     senderId = userId;
     notifincationType = req.body.notifincationType;
     notifyId = req.body.notifyId;
     nId = req.body.notifyId;
     notifyType = req.body.notifyType;

     User.find({'_id':{$in:folInfo.flUser}},{'_id':1,'userName':1,'businessName':1,'userType':1,'firebaseToken': 1,'deviceType': 1,'profileImage': 1,'appType': 1
     }).exec(function(err, userData) {



         if (userData.length>0) {
            
             var filteredAry = userData.filter(function(e) {
                 return e._id !== userId
             });

              var myArray = userData.filter(function(i) {
                 return i._id == userId
             });

             userName = myArray[0].userName;
             userType = myArray[0].userType;
             var baseUrl =  req.protocol + '://'+req.headers['host'];
             //profileImage = baseUrl+"/uploads/profile/"+myArray[0].profileImage;
               profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+myArray[0].profileImage;

            var notifyId = req.body.notifyId;
            switch (notifincationType) {

                 case '1':

                     var body = commonLang.SENT_BOOKING_REQUEST;
                     var title = 'Booking Request';
                     break;

                 case '2':
                     var body = commonLang.ACCEPT_BOOKING_REQUEST;
                     var title = 'Booking Accept';
                     break;

                 case '3':
                     var body = commonLang.REJECT_BOOKING_REQUEST;
                     var title = 'Booking Reject';
                     break;

                 case '4':
                     var body = commonLang.CHECK_BOOKING_CANCEL;
                     var title = 'Booking Cancel';
                     break;

                 case '5':
                     var body = commonLang.COMPLETE_BOOKING_REQUEST;
                     var title = 'Booking Complete';
                     break;

                 case '6':
                     var body = commonLang.GIVEN_REVIEW_BOOKING;
                     var title = 'Booking Review';
                     break;

                 case '7':
                     var body = commonLang.ADD_NEW_POST;
                     var title = 'New Post';
                     break;

                case '8':
                    var body = commonLang.PAYMENT_COMPLETED;
                    var title = 'Payment';
                    break;

                 case '9':
                     var body = commonLang.COMMENT_POST;
                     var title = 'Comment';
                     break;

                 case '10':
                     var body = commonLang.LIKE_POST;
                     var title = 'Post Like';
                     break;

                 case '11':
                     var body = commonLang.LIKES_COMMENT;
                     var title = 'Comment Like';
                     break;
                 case '12':
                     var body = commonLang.START_FOLLOW;
                     var title = 'Following';
                     break;

                case '13':
                     var body = commonLang.ADD_STORY;
                     var title = 'Story';
                     var notifyId = userId;
                     break;
               
                case '14':
                     var body = commonLang.ADD_FAVOURITE;
                     var title = 'Favourites';
                     break;  
                case '16':
                 var body = commonLang.TAG_POST;
                 var title = 'Tag';
                 break;            


             }

             notification = {
                 title: title,
                 body: body,
                 notifincationType: notifincationType,
                 sound: "default",
                 notifyId : notifyId,
                 click_action:"ChatActivity"
             };

             data = {
                 title: title,
                 body: body,
                 notifincationType: notifincationType,
                 notifyId : notifyId,
                 click_action:"ChatActivity"
             };
       
             webData = {
                 title: title,
                 body: body,
                 url: '/allBookinghistory'
             }

             multiDta = [];
             tok = [];
             appType = [];
             async.each(filteredAry, function(rs, callback) {

                 var moment = require('moment');
                 var crd = moment().format();
                 insertData = {};

                 insertData['senderId'] = senderId;
                 insertData['receiverId'] = rs._id;
                 insertData['notifincationType'] = notifincationType;
                 insertData['crd'] = crd;
                 insertData['upd'] = crd;
                 insertData['appType'] = rs.appType;
                 token = '';
                 multiDta.push(insertData);
                 tok.push(rs.firebaseToken);
                 appType.push(rs.appType);

                 callback();

             }, function() {



                addNotification.find().sort([['_id', 'descending']]).limit(1).exec(function(err, nData) {
                     var autoId = 1;

                     if (nData.length > 0) {
                        
                         autoId = nData[0]._id + 1;

                     }
                     jsArr = [];

                    uName = userName[0].toUpperCase() + userName.slice(1);
                    notification.body = uName + ' ' + body;

                    data.body = uName + ' ' + body;
                    data.urlImageString = profileImage;
                    data.userType = notification.userType = userType;
                    data.userName = notification.userName = uName;
                    webData.body = uName + ' ' + body;
                    

                    for (var i = 0; i < multiDta.length; i++) {

                        if(multiDta[i].notifincationType=="10" || multiDta[i].notifincationType=="11" || multiDta[i].notifincationType=="12" || multiDta[i].notifincationType=="14"){
                    
                            addNotification.deleteMany({'notifyId':nId,'notifincationType':multiDta[i].notifincationType,'senderId':multiDta[i].senderId,'receiverId':multiDta[i].receiverId,'type':notifyType},function(err, results) {});

                        }

                        if((multiDta[i].notifincationType=="7" || multiDta[i].notifincationType=="16" || multiDta[i].notifincationType=="13") && multiDta[i].appType=="biz"){
                    
                            addNotification.deleteMany({'notifyId':nId,'notifincationType':multiDta[i].notifincationType,'senderId':multiDta[i].senderId,'receiverId':multiDta[i].receiverId,'type':notifyType},function(err, results) {});

                        }
                        
                         inc = autoId + i;

                         jsArr.push({
                             _id: inc,
                             senderId: multiDta[i].senderId,
                             receiverId: multiDta[i].receiverId,
                             notifincationType: multiDta[i].notifincationType,
                             notifyId: nId,
                             type: notifyType,
                             crd: multiDta[i].crd,
                             upd: multiDta[i].upd
                         });

                        token = tok[i];
                        receiverId = multiDta[i].receiverId;
                        notifyType = notifyType;
                        appType = multiDta[i].appType;


                        if((appType=="social") ){
                            notify.bedgeCount(receiverId,notifyType,'socialBookingBadgeCount');

                            notify.sendNotification(token,notification,data,receiverId,'socialBookingBadgeCount');

                        }else  if((appType=="biz" && multiDta[i].notifincationType!="7" && multiDta[i].notifincationType!="13" && multiDta[i].notifincationType!="16")){
                           
                            notify.bedgeCount(receiverId,notifyType,'businessBookingBadgeCount');
                            notify.sendNotification(token,notification,data,receiverId,'businessBookingBadgeCount');
                        }
                         

                     }
                    addNotification.insertMany(jsArr);
                  //  notify.sendNotificationMultiple(tok, notification, data);

                 });

            });
        }
    });

}
/*api for user  active status*/

exports.checkUserStatus = function(req,res){
    userId = req.body.userId;
    if(userId ==''){
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
        return; 
    }
    User.findOne({'_id':userId},{_id:1,status:1}).exec(function(err,result){
         if(result){
             res.json({status: "success",message: 'ok',userStatus:result});
         }else{
            res.json({status:'fail',message: commonLang.RECORD_NOT_FOUND});
         }
    });

}

/*api for like status*/

exports.checkLike = function(req,res,next){
    likes.find({'likeById':Number(loginUserId),'type':'feed','status':1}).exec(function(err,userData){

        if(userData){
             likeData = userData;
        }else{
             likeData = [];
        }
       
        next();
    });

}



exports.profileUpdate = function(req, res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files){
            var baseUrl =  req.protocol + '://'+req.headers['host'];
            var imageName = "";
            
           if(files.profileImage){

/*                var oldpath = files.profileImage.path;
                var imageName = Date.now() + ".jpg";
                var newpath = './public/uploads/profile/' + imageName;
                fs.rename(oldpath, newpath, function(err) {
                    if (err) throw err;
                }); */   

                // hear code for add image using s3 bucket
           
                var imageObj = files.profileImage;
           
                let ext =  imageObj.name.split('.').pop();
                let fileName = uniqid()+'.'+ext;
               
                let height = commonJs.calculateFeedImageThunmbHeight(imageObj.path);
                thumboptions = {
                    resize: { height: height },
                    thumbFolder:commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
                }
                
                fileUploadObj.uploadFile(imageObj, commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);
                imageName =  fileName;

                // end          
            }

            var  email = fields.email? fields.email : authData.email;
            var  userId = fields.userId ? fields.userId : authData._id;
            var  userName = fields.userName ? fields.userName : authData.userName;

            if(userId==''){

               res.json({status:'fail',message: commonLang.REQ_USER_ID});
               return;

            }else{

                User.findOne({ 'email' :  email,_id: {'$ne':userId}},function(err, user){
                    if (err) res.json(err);
                    if (user && user.email) {
                        
                        res.json({status: "fail", message: commonLang.EXIST_EMAIL});
                        return;

                    }else{


                        var data = {};

                        if(fields.firstName){

                            data['firstName'] = fields.firstName;
                        }

                        if(fields.lastName){

                            data['lastName'] = fields.lastName;
                        }

                        if(fields.email){

                            data['email'] = fields.email.toLowerCase();
                        }

                        if(fields.contactNo){

                            data['countryCode'] = fields.countryCode;
                            data['contactNo'] = fields.contactNo;
                        }
                        if(fields.city){

                            data['city'] = fields.city;
                        }

                        if(fields.state){

                            data['state'] = fields.state;
                        }

                        if(fields.country){

                            data['country'] = fields.country;
                        }

                        if(fields.gender){

                            data['gender'] = fields.gender;
                        }

                        
                        if(fields.dob){

                            data['dob'] = fields.dob;
                        }

                        if(fields.isDocument)

                            data['isDocument'] = fields.isDocument;

                         if(fields.bookingSetting)

                            data['bookingSetting'] = fields.bookingSetting;
                        


                        if(fields.address){

                            data['address'] = fields.address;
                            data['longitude'] = fields.longitude;
                            data['latitude'] = fields.latitude;
                           //old flow 
                           // data['location'] = [fields.latitude,fields.longitude];
                           data['location'] = {  
                                                type: "Point",
                                                coordinates: [
                                                   Number(fields.longitude), //longitude
                                                   Number(fields.latitude) //latitude
                                                   ]
                                              }   
                        }

                        if(imageName){

                            data['profileImage'] = imageName;
                            re = {
                                'userName' : userName,
                                'profileImage' : imageName
                            };
                           staff.updateMany({artistId:Number(userId)},{$set: {'staffInfo':re}},function(err, docs){});
                        }

                        User.findOne({'_id': userId}, function(err, checkData) {

                            if(imageName && checkData){

                                let imgPath = commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+checkData.profileImage;
                                let imgPathThumb = commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER+checkData.profileImage;
                                S3.deleteFile(imgPath);
                                S3.deleteFile(imgPathThumb);

                            }
                          
                            User.updateOne({_id:userId},{$set: data},function(err, docs){


                                User.aggregate([{  
                                    $lookup:{
                                            from: "busineshours", 
                                            localField: "_id", 
                                            foreignField: "artistId",
                                            as: "businesshours"
                                    }
                                 
                                },
                                { 
                                    $match:{_id:Number(userId)} 

                                }],function(err, userData) {

                                    user = userData[0];
                                    user.businesshours  =    (user.businesshours.length) ? 1 : 0; 

                                    if(user){

                                        delete user.password;

                                        if(user.profileImage)                                  
                                           // user['profileImage'] = baseUrl+"/uploads/profile/"+user.profileImage;
                                           user['profileImage'] = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+user.profileImage;

                                        if(user.dob)
                                            user.dob =  moment(user.dob).format('DD/MM/YYYY');

                                        res.json({status: "success", message: commonLang.SUCCESS_UPDATE_PROFILE,users : user});
                                        return;   
                                    }
                                   
                                });

                            });
                        });

                    }

                });
            }
       
    });
}

exports.reportReason = function(req,res){

   /* if(req.query.type!="user"){

        reportReason.find({'type':'booking'}).exec(function(err,result){

            if(result){
                res.json({status: "success",message: 'ok',data:result});
            }else{
                res.json({status:'fail',message:'No record found.'});
            }
        });

    }*/
     if(req.query.type=="user"){

        reportReason.find({'type':'user'}).exec(function(err,result){

            if(result){
                res.json({status: "success",message: 'ok',data:result});
            }else{
                res.json({status:'fail',message: commonLang.RECORD_NOT_FOUND});
            }
        });


    }else if(req.query.type=="group"){
        reportReason.find({'type':'group'}).exec(function(err,result){
            if(result){
                res.json({status: "success",message: 'ok',data:result});
            }else{
                res.json({status:'fail',message: commonLang.RECORD_NOT_FOUND});
            }
        });


    }else if(req.query.type=="feed"){
        reportReason.find({'type':'feed'}).exec(function(err,result){
            if(result){
                res.json({status: "success",message: 'ok',data:result});
            }else{
                res.json({status:'fail',message: commonLang.RECORD_NOT_FOUND});
            }
        });


    }else{

        reportReason.find({'type':"booking"}).exec(function(err,result){

            if(result){
                res.json({status: "success",message: 'ok',data:result});
            }else{
                res.json({status:'fail',message: commonLang.RECORD_NOT_FOUND});
            }
        });

    }

}

exports.profileByUserName = function(req,res){
    var baseUrl =  req.protocol + '://'+req.headers['host'];
    userName = req.body.userName;
    if(userName ==''){
        res.json({status: "fail",message: commonLang.REQ_UERNAME});
        return;
    }
    search ={};
    search['userName'] = userName;
    search['status'] ='1';

    // search['isDocument'] = 3;
    User.aggregate( [

      { $match :search },
      {  $project: {
         _id:1,
         firstName:1,
         lastName:1,
         userName:1,
         profileImage:1,
         businessName:1,
         businesspostalCode:1,
         buildingNumber:1,
         businessType:1,
         email:1,
         dob:1,
         gender:1,
         address:1,
         address2:1,
         countryCode:1,
         contactNo:1,
         userType:1,
         followersCount:1,
         followingCount:1,
         serviceCount:1,
         certificateCount:1,
         postCount:1,
         reviewCount:1,
         ratingCount:1,
         bio:1,
         serviceType:1,
         radius:1
         }
      },
      
    ],function(err,jsArr){
       if(jsArr==''){

          res.json({status: "fail",message: 'This user is no longer active.',userDetail: jsArr});
          return;

       }else{
            
            if(jsArr!=''){

                    if(jsArr[0].status==0){

                        res.json({status: "fail",message: commonLang.ACCOUNT_INACTIVATE_BY_ADMIN,userDetail: jsArr});
                        return;

                     
                    }
                    
                    if(jsArr[0].profileImage){
                        if(!validUrl.isUri(jsArr[0].profileImage)){
                             //jsArr[0].profileImage = baseUrl+"/uploads/profile/"+jsArr[0].profileImage;
                             jsArr[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[0].profileImage;

                        } 
                     
                    }

                res.json({status: "success",message: 'ok',userDetail: jsArr[0]});
            }
         
        }

    });

}



exports.reportSubmit = function(req, res) {

    let title           = req.body.title; 
    let description     = req.body.description; 
    let reportByUser    = req.body.reportByUser; 
    let reportForUser   = req.body.reportForUser; 
    let reportDate      = req.body.reportDate; 

    if(title==""){
        
        res.json({'status':'fail','message': commonLang.REQ_TITLE});
        return;
    }

    if(description==""){
        
        res.json({'status':'fail','message': commonLang.REQ_DESCRIPTION});
        return;
    }

    if(reportByUser==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_BY_USER});
            return;
    }

    if(reportForUser==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_FOR_USER});
            return;
    }

    if(reportDate==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_DATE});
            return;
    }

    report.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

        var data1 = {
            'title'          : title,
            'description'    : description,
            'reportByUser'   : reportByUser,
            'reportForUser'  : reportForUser,
             '_id'           : (aat) ? Number(aat._id)+Number(1) : 1,
            'crd'            : reportDate,
            'upd'            : reportDate
        };

        report.insertMany(data1);
    
        res.json({'status': "success","message": commonLang.SUCCESS_REPORT});
        return;
    });

 
}
exports.checkAdminCommission = function(req,res,next){


    booking.find({'paymentType':2,'bookStatus':'3','commissionStatus':0,'artistId':Number(loginUserId)}).exec(function(err,adminComi){
       // console.log('test data');
        //console.log(adminComi);
        if(adminComi){
            adminCommission = adminComi;
        }else{
            adminCommission = [];
        }
       
        next();
    });

}

exports.getServiceByMiles = function( req, res, next ){
    let search = {};
    search['artistId'] ={'$in':userIds};
    search['deleteStatus']=1;
    search['status'] =1;

    async.parallel([

        function(callback) {
           var query = artistMainService.find(search).sort([['serviceName', 'ascending']])
           query.exec(function(err, sub) {
                if (err) {
                  callback(err);
                }

              callback(null, sub);
            });
        },
        function(callback) {
           var query = artistSubService.find(search).sort([['subServiceName', 'ascending']])
           query.exec(function(err, sub) {
                if (err) {
                  callback(err);
                }

              callback(null, sub);
            });
        },
        function(callback) {
            var query = artistservices.find(search).sort([ ['title', 'ascending']])
            query.exec(function(err, s) {
                  if (err) {
                      callback(err);
                  }

              callback(null, s);
            });
        }   
    ],
   
  //Compute all results
   function(err, results) {
    serArr =[];
   
    if(results[0]){
      
        results[0] =  lodash.uniqBy( results[0], 'serviceId');
        results[1]=  lodash.uniqBy( results[1], 'subServiceId');
        
        for(const catTyp of results[1]) {

            var cate = lodash.filter(results[2], { 'subserviceId': catTyp.subServiceId} );
           
            subArr =[];
            subArr = cate;

            if(subArr.length){
              
               serArr.push({
                    subServiceId:catTyp.subServiceId,
                    serviceId:catTyp.serviceId,
                    subServiceName:catTyp.subServiceName,
                    services:subArr
                });
            }
                
        }
        main_cat= serArr;
        bnsData = results[0];
        next();
         // res.json({status: "success",message: 'ok',Allcagtegories: serArr});
         //   return;             
       
        }else{
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
            return;
        }
         
    });

}
exports.getService = function( req, res ){
    var finalResult = [];
    for(const bnsType of bnsData ) { 

        var serv = lodash.filter(main_cat, { 'serviceId': bnsType.serviceId} );

        finalResult.push({
            serviceId:bnsType.serviceId,
            serviceName:bnsType.serviceName,
            categories:serv
        });
    }
    res.json({status: "success",message: 'ok',Allcagtegories: finalResult});
    return;     

}
exports.getServiceByMiles1old = function( req, res ){
         let search = {};
         search['artistId'] ={'$in':userIds};

         var query = artistMainService.aggregate([

            {
            $match:search
            },
                          
             {  
                $lookup:{
                    from: "artistsubsrervices", 
                    localField: "serviceId", 
                    foreignField: "serviceId",
                    as: "subcate"
                }
             
             },
            //  {  
            //     $lookup:{
            //         from: "artistservices", 
            //         localField: "artistId", 
            //         foreignField: "artistId",
            //         as: "service"
            //     }
             
            // },   
          // { "$group": {
          //           "_id": "$_id",
          //            "serviceTagDetail": { "$push": "$subcate" },
          //           // "lastName":{"$first": "$lastName"},
          //           // "profileImage":{"$first": "$profileImage"},
          //           // 'postCount':{"$first": "$postCount"},
          //           // "reviewCount":{"$first": "$reviewCount"},
          //           // "ratingCount":{"$first": "$ratingCount"},
          //           // "latitude":{"$first": "$latitude"},
          //           // "longitude":{"$first": "$longitude"},
          //           // "userType":{"$first": "$userType"},
          //          }
          //        },   
           { 
            "$project": {
                "_id": 1,
                "serviceId":1,
                "serviceName":1,
                "subcate.subServiceId":1,
                "subcate.subServiceName":1,
                //"subcate.deleteStatus":1,
                // "service._id":1,
                // "service.serviceId":1,
                // "service.subserviceId":1,
                // "service.title":1,
                // "service.description":1,
                // "service.inCallPrice":1,
                // "service.outCallPrice":1,
                // "service.deleteStatus":1,
                
             }
        },                           
          
  
        ]);  
        query.exec(function(err, data) {
            console.log('eeeerrorrrr',err);
            var newData = lodash.uniqBy(data, 'serviceId')
             var i=0;
        newData.map(async function(Services){
                // newData[i].subcate = lodash.uniqBy(Services.subcate, 'subServiceId')
                 i++;
/*            let serviceData = Services;
            serviceData.map(async function(value){
                if(value.length != 0){
                    if(value.serviceId[0] != undefined){
                    if(!lodash.find(finalResult, {_id: value.serviceId[0]._id})) {
                      let buisnesType = value.serviceId[0];
                      buisnesType['categories'] = [];

                      let categories = value.subserviceId[0];
                      categories['services'] = [];
                      value['businessId'] = value.serviceId[0]._id;
                      value['categoryId'] = value.subserviceId[0]._id;
                      
                      delete value.subserviceId;
                      delete value.serviceId;
                      categories['services'].push(value);
                     
                      buisnesType['categories'].push(categories);
                      
                      finalResult.push(buisnesType);
                    }else{
                        // let objIndex = lodash.findIndex(finalResult, {_id: value.serviceId[0]._id});
                        // let catObj = finalResult[objIndex]['categories'];

                        // if(!lodash.find(catObj, {_id: value.subserviceId[0]._id})) {
                        //     let categories = value.subserviceId[0];
                        //     categories['services'] = [];
                        //     value['businessId'] = value.serviceId[0]._id;
                        //     value['categoryId'] = value.subserviceId[0]._id;
                        //     delete value.subserviceId;
                        //     delete value.serviceId;
                        //     categories['services'].push(value);
                        //     catObj.push(categories);
                        // }else{

                        //     let catObjIndex = lodash.findIndex(catObj, {_id: value.subserviceId[0]._id});
                        //     let serviceObj = catObj[catObjIndex]['services'];

                        //     if(!lodash.find(serviceObj, {_id: value._id})) {
                        //         value['businessId'] = value.serviceId[0]._id;
                        //         value['categoryId'] = value.subserviceId[0]._id;
                        //         delete value.subserviceId;
                        //         delete value.serviceId;
                        //         serviceObj.push(value);
                        //     }
                        // }
                    }
                }
                    
                }
            });*/
        });   

            res.json({status: "success",message: 'successfully',Allcagtegories: newData});
             return;
        })


}
exports.getArtistByMiles = function( req, res, next ){
        let userSearch ={};
        userSearch['userType'] = 'artist';
        userSearch['isDocument'] = 3;
        userSearch['status'] = "1";
        var usrLat = req.body.latitude;
        var usrLong = req.body.longitude;
        var serviceType = Number(req.body.isOutCall);
        radius              = parseFloat(req.body.radius);
        
       // if(usrLat==""){
       //      res.json({'status':'fail','message':commonLang.REQ_LONGITUDE});
       //      return;
       //  }

       //  if(usrLong==""){
       //      res.json({'status':'fail','message':commonLang.REQ_LATITUDE});
       //      return;
       //  }
        if(radius=="" || isNaN(radius)){
            res.json({'status':'fail','message':'Radius is required.'});
            return;
        }
        if(serviceType){

            userSearch['serviceType'] = {'$ne':serviceType};

        }else{

            userSearch['serviceType'] = {'$ne':2};

        }

        if(usrLat && usrLong){
        }else{
          usrLat  = authData.latitude;
          usrLong = authData.longitude;
        }
        var query = User.aggregate([
          
            {
                "$geoNear": {
                      "near": {
                             "type": "Point",
                             "coordinates": [parseFloat(usrLong), parseFloat(usrLat)]
                              },
                maxDistance: commonLib.milesToMeters(radius),
                "spherical": true,
                "distanceField": "dist.calculated",
                distanceMultiplier: 1/1609.344, // calculate distance in meters "item.name": { $eq: "ab" } }
                key: 'location'
                }
            },
            { $match:userSearch },                 
            { 
                "$project": {
                   "_id": 1,
                                  
                 }
            },                                  
          

        ]);  
        query.exec(function(err, data) {
            userIds= data.map(a => a._id);
             next();
        })


}
