var uniqid          = require('uniqid');
var sizeOf          = require('image-size');
var feed            =   require('../../models/front/feed.js');
var staffServiceModel =   require('../../models/front/staffService.js');
var User            =   require('../../models/front/home.js');//it user for table and coulamn information
var moment          =   require('moment');
var likes           =   require('../../models/front/like.js');
var followUnfollow  =   require('../../models/front/followersFollowing.js');
var tag             =   require('../../models/front/tag.js');
var async           =   require('async');
var formidable      =   require('formidable');//it user for get form or post data by http
var fs              =   require('fs');
var lodash          =   require('lodash');
var dateFormat      =   require('dateformat');
var multiparty      =   require('multiparty');
var escapere        =   require('escape-regexp');
var addNotification =   require('../../models/front/notification.js');
var comment         =   require('../../models/front/comment.js');
var commonLib         =   require('../../../lib/common.js');
var commonConstants =   require('../../../config/commanConstants');
var FileUpload 		=   require('../../../lib/FileUpload');
const S3 			=   require("../../../lib/aws-s3");
var constanPath = require('../../../config/envConstants.js');

//Class Objects
const fileUploadObj = new FileUpload();



exports.followerFeed = function(req,res,next){


    folInfo = {};
    folInfo =req.body;
    
    if(req.body.userId ==''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }
     type ='newsFeed';
    if(req.body.type ==''){
       res.json({status: "fail",message: commonLang.REQ_TYPE});
       return;
    }else{
         type = req.body.type;
    }

    if(type =='newsFeed' || type =='explore'){



        followUnfollow.find({'followerId':Number(req.body.userId),'status':1,'userId':{'$nin': bdata}}).sort([['_id', 'ascending']]).exec(function(err, followData) {

            if(followData){

                 a = [];
                 c = [];
                 a= followData.map(a => a.userId);
                 a.push(Number(req.body.userId));         
                 folInfo.flUser =a ;
                folInfo.isFollow = false;   

                if(req.body.search){ 

                    User.find({'_id':{'$in':a}}).sort([['_id', 'ascending']]).exec(function(err, followData1) {

                        c= followData1.map(c =>c._id);
                        folInfo.flUser = c;   

                    });

                }




            }else{
                folInfo.flUser = [];
            }
            next();
        });
    }else{

       folInfo.flUser = [];
       next();
    }

}


exports.followerStory = function(req,res,next){
    folInfo = {};
    folInfo =req.body;
    
    if(req.body.userId ==''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }

    followUnfollow.find({'followerId':Number(req.body.userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
       
        if(followData){

             a = [];
             c = [];
             a= followData.map(a => a.userId);
             a.push(Number(req.body.userId));         
             folInfo.flUser =a ;
            folInfo.isFollow = false;   

            if(req.body.search){ 

                User.find({'_id':{'$in':a}}).sort([['_id', 'ascending']]).exec(function(err, followData) {

                    c= followData.map(c =>c._id);
                    folInfo.flUser = c;   

                });

            }




        }else{
            folInfo.flUser = [];
        }

         next();
    });
}


exports.profileFeed = function(req,res,next){

    feedSearch = {}
    afeedSearch = {};
    baseUrl = req.protocol + '://' + req.headers['host'];
    userId = req.body.userId;
    grid = req.body.grid;

    if(userId == ''){
       res.json({status:"fail",message: commonLang.REQ_USER_ID});
    } 
    Value_match = {$regex:req.body.feedType};

    if(sData.searchData && sData.searchData!=''){

        feedSearch['tagId'] = {$in:sData.searchData};

    }
    
    if(sData.peaopleData  !=  '' && sData.search==''){
     
        feedSearch['peopleTagData'] = {$in:sData.peaopleData};

    }else if(sData.peaopleData  ==  '' && sData.search  !=  ''){

        feedSearch['caption'] = {$regex:req.body.search,$options:'i'};

    }else if(sData.peaopleData!='' && sData.search!=''){

     	feedSearch = {$or: [{ 'peopleTagData': {$in:sData.peaopleData} }, { 'caption': {$regex:req.body.search,$options:'i'}}]};

    }
    if(grid=='grid'){

        afeedSearch['feedType'] = {$ne:'text'};
    }
    feedSearch['feedType'] =Value_match;
    feedSearch['userId'] =Number(userId);
    feedSearch['deleteStatus'] = 1;

     feed.aggregate([
       
      {
        $sort: {_id: -1}
      },

      {
        $match:feedSearch
      },

      {
        $match:afeedSearch
      },

      {   
        "$project":{
            "_id":1,
        } 
      }

    ],function(err, dataLength){

       
        if(dataLength){
            newdata = dataLength.length; 
        }else{
            newdata = 0;
        }


        next();
          
    });


}
exports.profileFeedV2 = function(req,res,next){

    feedSearch = {}
    afeedSearch = {};
    baseUrl = req.protocol + '://' + req.headers['host'];
    userId = req.body.userId;
    grid = req.body.grid;

    if(userId == ''){
       res.json({status:"fail",message: commonLang.REQ_USER_ID});
    } 
    Value_match = {$regex:req.body.feedType};

    if(sData.searchData && sData.searchData!=''){

        feedSearch['tagId'] = {$in:sData.searchData};

    }
    
    if(sData.peaopleData  !=  '' && sData.search==''){
     
        feedSearch['peopleTagData'] = {$in:sData.peaopleData};

    }else if(sData.peaopleData  ==  '' && sData.search  !=  ''){

        feedSearch['caption'] = {$regex:req.body.search,$options:'i'};

    }else if(sData.peaopleData!='' && sData.search!=''){

        feedSearch = {$or: [{ 'peopleTagData': {$in:sData.peaopleData} }, { 'caption': {$regex:req.body.search,$options:'i'}}]};

    }
    if(grid=='grid'){

        afeedSearch['feedType'] = {$ne:'text'};
    }
    feedSearch['feedType'] =Value_match;
    feedSearch['userId'] =Number(userId);
    feedSearch['deleteStatus'] = 1;

    aggregateArray = [
                               
        {
            $match:feedSearch
        },
        {
            $match:afeedSearch 
        },
        {
            "$lookup": {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": {
                path:"$serviceTag", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "serviceTag.tagDetails.artistServiseTagId",
                foreignField: "_id",
                as: "arstistServiceTagDetail"
            }
        }, 
        {
            "$lookup": {
                from: "staffservices",
                localField: "serviceTag.tagDetails.staffServiseTagId",
                foreignField: "_id",
                as: "staffServiceTagDetail"
            }
        },  
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.staffId",
                foreignField: "_id",
                as: "staffDetail"
            }
        }, 
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.artistId",
                foreignField: "_id",
                as: "artistDetail"
            }
        }, 
        { "$unwind": {
                path:"$arstistServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$artistDetail", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "arstistServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.serviceId"
            }
        },
        
        {
            "$lookup": {
                from: "subservices",
                localField: "arstistServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "staffServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.serviceId"
            }
        },
        {
            "$lookup": {
                from: "subservices",
                localField: "staffServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "staffServiceTagDetail.artistServiceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.artistServiceName"
            }
        },
        { "$group": {
                "_id":  "$_id",
                "feedData": {"$first": "$feedData"},
                "feedType": {"$first": "$feedType"},
                "caption": {"$first": "$caption"},
                "city": {"$first": "$city"},
                "country": {"$first": "$country"},
                "location": {"$first": "$location"},
                "likeCount": {"$first": "$likeCount"},
                "crd": {"$first": "$crd"},
                "feedImageRatio": {"$first": "$feedImageRatio"},
                "commentCount": {"$first": "$commentCount"},
                "peopleTag": {"$first": "$peopleTag"},
                "serviceTag": {"$first": "$serviceTag"},
                "serviceTagId": {"$first": "$serviceTagId"},
                "latitude": {"$first": "$latitude"},
                "longitude": {"$first": "$longitude"},
                "deleteStatus": {"$first": "$deleteStatus"},
                "userId": {"$first": "$userId"},
                "userInfo": {"$first": "$userInfo"},
                "serviceTag": {
                    "$push": {
                        tag:"$serviceTag", 
                        arstistServiceTagDetail: {
                            artistServiceID:"$arstistServiceTagDetail._id",
                            title:"$arstistServiceTagDetail.title",
                            description:"$arstistServiceTagDetail.description",
                            inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                            outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                            completionTime:"$arstistServiceTagDetail.completionTime",
                            buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                            categoryID:"$arstistServiceTagDetail.subserviceId._id",
                            categoryName:"$arstistServiceTagDetail.subserviceId.title"
                            
                        },
                        staffServiceTagDetail: {
                            staffServiceID:"$staffServiceTagDetail.artistServiceId",
                            title:"$staffServiceTagDetail.artistServiceName.title",
                            description:"$staffServiceTagDetail.artistServiceName.description",
                            inCallPrice:"$staffServiceTagDetail.inCallPrice",
                            outCallPrice:"$staffServiceTagDetail.outCallPrice",
                            completionTime:"$staffServiceTagDetail.completionTime",
                            buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                            categoryID:"$staffServiceTagDetail.subserviceId._id",
                            categoryName:"$staffServiceTagDetail.subserviceId.title"
                        },
                        staffDetail: {
                            firstName:"$staffDetail.firstName",
                            lastName:"$staffDetail.lastName"
                        },
                        artistDetail:{
                            firstName:"$artistDetail.firstName",
                            lastName:"$artistDetail.lastName"
                        }
                    }
                }
                
            }
        },
        {
        $sort: {_id: -1}
        },   
        {
            "$project": {
                "_id":1,
                "feedData":1,
                "feedType":1,
                "caption":1,
                "city":1,
                "country":1,
                "location":1,
                "likeCount":1,
                "crd":1,
                "feedImageRatio":1,
                "commentCount":1,
                "peopleTag":1,
                "serviceTag":1,
                "serviceTagId":1,
                "latitude":1,
                "longitude":1,
                "deleteStatus":1,
                "userId":1,
                "userInfo._id":1,
                "userInfo.userName":1,
                "userInfo.firstName":1,
                "userInfo.lastName":1,
                "userInfo.profileImage":1,
                 "userInfo.userType":1,
                 "userInfo.ratingCount":1,
                 "staffDetail":1
            }
        }
    ];

    var query = feed.aggregate(aggregateArray);
    query.exec(function(err, data) {
        if (err) {
            console.log(err);
        }

        if(data){
                newdata = data.length; 
            }else{
                newdata = 0;
            }
            next();
        });


}

exports.finalProfileFeedV2 = function(req, res) {

    if (req.body.page) {
        page = Number(req.body.page) * Number(req.body.limit);
    } else {
        page = 0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit = 10;
    }
    aggregateArray.push({ $skip:page });
    aggregateArray.push({ $limit:limit });
  
    var query = feed.aggregate(aggregateArray);

    query.exec(function(err, jsArr) {

        if (err) {
            res.json({status: "success",message: err});
            return;
        }
        if (jsArr.length) {
            for (i = 0; i < jsArr.length; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                if(mData){
                        
                    var picked = lodash.filter(mData, { 'followerId': jsArr[i].userId} );
                    if(picked.length){

                       jsArr[i].followerStatus = 1;    
                    }else{

                       jsArr[i].followerStatus = 0;
                    }
                }

                if(likeData){
                  
                    var picked1 = lodash.filter(likeData, { 'feedId': jsArr[i]._id} );
                      
                    if(picked1.length){

                       jsArr[i].isLike = 1;    
                    }else{

                       jsArr[i].isLike = 0;
                    }
                }
                
                if(folderData){
                  
                    var folderData1 = lodash.filter(folderData, { 'feedId': jsArr[i]._id} );
                      
                    if(folderData1.length){

                       jsArr[i].isSave = 1;    
                    }else{

                       jsArr[i].isSave = 0;
                    }
                }


                if (jsArr[i].feedData.length) {

                    for (j = 0; j < jsArr[i].feedData.length; j++) {
                        if (jsArr[i].feedData[j].feedPost) {
                            jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if (jsArr[i].feedData[j].videoThumb) {
                            jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR + jsArr[i].feedData[j].videoThumb;

                        }
                    }

                }

                if (jsArr[i].userInfo[0]) {

                    if (jsArr[i].userInfo[0].profileImage) {

                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR + jsArr[i].userInfo[0].profileImage;

                    }

                } else {

                    jsArr[i].userInfo[0] = [];

                }
            }
            res.json({status: "success",message: 'successfully', AllFeeds: jsArr,total: newdata});
            return;

        } else {
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds: []});
            return;
        }

    });

}

exports.finalProfileFeed = function(req, res) {

    if (req.body.page) {
        page = Number(req.body.page) * Number(req.body.limit);
    } else {
        page = 0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit = 10;
    }

    var query = feed.aggregate([{
            $match: feedSearch
        },
        {
            $match:afeedSearch
        },
        {
            "$lookup": {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        {
            $sort: {
                _id: -1
            }
        },
        {
            $skip: page
        },
        {
            $limit: limit
        },
        {
            "$project": {
                "_id": 1,
                "feedType": 1,
                "feedData": 1,
                "caption": 1,
                "city": 1,
                "country": 1,
                "location": 1,
                "likeCount": 1,
                "crd": 1,
                "feedImageRatio": 1,
                "commentCount": 1,
                "peopleTag":1,
                "serviceTag":1,
                "userId": 1,
                "userInfo._id": 1,
                "userInfo.userName": 1,
                "userInfo.firstName": 1,
                "userInfo.lastName": 1,
                "userInfo.profileImage": 1
            }
        }

    ]);


    query.exec(function(err, jsArr) {

        if (err) {
            res.json({status: "success",message: err});
            return;
        }
        if (jsArr.length) {
            for (i = 0; i < jsArr.length; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                if(mData){
                        
                    var picked = lodash.filter(mData, { 'followerId': jsArr[i].userId} );
                    if(picked.length){

                       jsArr[i].followerStatus = 1;    
                    }else{

                       jsArr[i].followerStatus = 0;
                    }
                }

                if(likeData){
                  
                    var picked1 = lodash.filter(likeData, { 'feedId': jsArr[i]._id} );
                      
                    if(picked1.length){

                       jsArr[i].isLike = 1;    
                    }else{

                       jsArr[i].isLike = 0;
                    }
                }
                
                if(folderData){
                  
                    var folderData1 = lodash.filter(folderData, { 'feedId': jsArr[i]._id} );
                      
                    if(folderData1.length){

                       jsArr[i].isSave = 1;    
                    }else{

                       jsArr[i].isSave = 0;
                    }
                }


                if (jsArr[i].feedData.length) {

                    for (j = 0; j < jsArr[i].feedData.length; j++) {
                        if (jsArr[i].feedData[j].feedPost) {
                            jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if (jsArr[i].feedData[j].videoThumb) {
                            jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR + jsArr[i].feedData[j].videoThumb;

                        }
                    }

                }

                if (jsArr[i].userInfo[0]) {

                    if (jsArr[i].userInfo[0].profileImage) {

                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR + jsArr[i].userInfo[0].profileImage;

                    }

                } else {

                    jsArr[i].userInfo[0] = [];

                }
            }
            res.json({status: "success",message: 'successfully', AllFeeds: jsArr,total: newdata});
            return;

        } else {
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds: []});
            return;
        }

    });

}

  
exports.getAllFeeds = function(req,res,next){

    baseUrl =  req.protocol + '://'+req.headers['host'];
    var Value_match = {$regex:req.body.feedType};
    feedSearch ={};

     if(sData.searchData && sData.searchData!=''){

        feedSearch['tagId'] = {$in:sData.searchData};

    }
    
    if(sData.peaopleData  !=  '' && sData.search==''){
     
        feedSearch['peopleTagData'] = {$in:sData.peaopleData};

    }else if(sData.peaopleData  ==  '' && sData.search  !=  ''){

        feedSearch['caption'] = {$regex:req.body.search,$options:'i'};

    }else if(sData.peaopleData!='' && sData.search!=''){

        feedSearch = {$or: [{ 'peopleTagData': {$in:sData.peaopleData} }, { 'caption': {$regex:req.body.search,$options:'i'}}]};

    }
     
    feedSearch['feedType'] = Value_match;
    feedSearch['deleteStatus'] = 1;

    if(folInfo.flUser){

        if(folInfo.flUser.length){
           feedSearch['userId'] ={$in:folInfo.flUser};

        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
            return;
        }

    }


    feed.aggregate([
            {
            $lookup:
                {
                    from: "users",
                    localField: "userId",
                    foreignField: "_id",
                    as: "userInfo"
                }
            },
            {
                $sort: {_id: -1}
            },
            {
                $match:feedSearch
            },
            {   
                "$project":{
                    "_id":1,
                    "feedType":1,
                    "feedData":1,
                    "caption":1,
                    "city":1,
                    "country":1,
                    "location":1,
                    "likeCount":1,
                    "crd":1,
                    "commentCount":1,
                    "peopleTag":1,
                    "serviceTag":1,
                    "userInfo._id":1,
                    "userInfo.userName":1,
                    "userInfo.firstName":1,
                    "userInfo.lastName":1,
                    "userInfo.profileImage":1,
                    "userInfo.userType":1,
                } 
            }
        ],function(err, dataLength){


            if(dataLength){

                newdata = dataLength.length; 

            }else{

                newdata = 0;
            }
            next();

        });

}
exports.getAllFeedsV2 = function(req,res,next){

    baseUrl =  req.protocol + '://'+req.headers['host'];
    var Value_match = {$regex:req.body.feedType};
    feedSearch ={};

     if(sData.searchData && sData.searchData!=''){

        feedSearch['tagId'] = {$in:sData.searchData};

    }
    
    if(sData.peaopleData  !=  '' && sData.search==''){
     
        //feedSearch['peopleTagData'] = {$in:sData.peaopleData};
        feedSearch['userId'] = {$in:sData.peaopleData};


    }else if(sData.peaopleData  ==  '' && sData.search  !=  ''){

        feedSearch['caption'] = {$regex:req.body.search,$options:'i'};

    }else if(sData.peaopleData!='' && sData.search!=''){

        feedSearch = {$or: [{ 'userId': {$in:sData.peaopleData} }, { 'caption': {$regex:req.body.search,$options:'i'}}]};

    }
     
    feedSearch['feedType'] = Value_match;
    feedSearch['deleteStatus'] = 1;

    if(folInfo.flUser){

        if(folInfo.flUser.length){
           feedSearch['userId'] ={$in:folInfo.flUser};

        }else{

            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
            return;
        }

    }
  console.log('feedSearch', feedSearch)
    aggregateArray = [
                               
        {
            $match:feedSearch
        },
        {
            "$lookup": {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": {
                path:"$serviceTag", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "serviceTag.tagDetails.artistServiseTagId",
                foreignField: "_id",
                as: "arstistServiceTagDetail"
            }
        }, 
        {
            "$lookup": {
                from: "staffservices",
                localField: "serviceTag.tagDetails.staffServiseTagId",
                foreignField: "_id",
                as: "staffServiceTagDetail"
            }
        },  
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.staffId",
                foreignField: "_id",
                as: "staffDetail"
            }
        }, 
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.artistId",
                foreignField: "_id",
                as: "artistDetail"
            }
        }, 
        { "$unwind": {
                path:"$arstistServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$artistDetail", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "arstistServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.serviceId"
            }
        },
        
        {
            "$lookup": {
                from: "subservices",
                localField: "arstistServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "staffServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.serviceId"
            }
        },
        {
            "$lookup": {
                from: "subservices",
                localField: "staffServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "staffServiceTagDetail.artistServiceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.artistServiceName"
            }
        },
        { "$group": {
                "_id":  "$_id",
                "feedData": {"$first": "$feedData"},
                "feedType": {"$first": "$feedType"},
                "caption": {"$first": "$caption"},
                "city": {"$first": "$city"},
                "country": {"$first": "$country"},
                "location": {"$first": "$location"},
                "likeCount": {"$first": "$likeCount"},
                "crd": {"$first": "$crd"},
                "feedImageRatio": {"$first": "$feedImageRatio"},
                "commentCount": {"$first": "$commentCount"},
                "peopleTag": {"$first": "$peopleTag"},
                "serviceTag": {"$first": "$serviceTag"},
                "serviceTagId": {"$first": "$serviceTagId"},
                "latitude": {"$first": "$latitude"},
                "longitude": {"$first": "$longitude"},
                "deleteStatus": {"$first": "$deleteStatus"},
                "userId": {"$first": "$userId"},
                "userInfo": {"$first": "$userInfo"},
                "serviceTag": {
                    "$push": {
                        tag:"$serviceTag", 
                        arstistServiceTagDetail: {
                            artistServiceID:"$arstistServiceTagDetail._id",
                            title:"$arstistServiceTagDetail.title",
                            description:"$arstistServiceTagDetail.description",
                            inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                            outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                            completionTime:"$arstistServiceTagDetail.completionTime",
                            buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                            categoryID:"$arstistServiceTagDetail.subserviceId._id",
                            categoryName:"$arstistServiceTagDetail.subserviceId.title"
                            
                        },
                        staffServiceTagDetail: {
                            staffServiceID:"$staffServiceTagDetail.artistServiceId",
                            title:"$staffServiceTagDetail.artistServiceName.title",
                            description:"$staffServiceTagDetail.artistServiceName.description",
                            inCallPrice:"$staffServiceTagDetail.inCallPrice",
                            outCallPrice:"$staffServiceTagDetail.outCallPrice",
                            completionTime:"$staffServiceTagDetail.completionTime",
                            buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                            categoryID:"$staffServiceTagDetail.subserviceId._id",
                            categoryName:"$staffServiceTagDetail.subserviceId.title"
                        },
                        staffDetail: {
                            firstName:"$staffDetail.firstName",
                            lastName:"$staffDetail.lastName"
                        },
                        artistDetail:{
                            firstName:"$artistDetail.firstName",
                            lastName:"$artistDetail.lastName"
                        }
                    }
                }
                
            }
        },
        {
        $sort: {_id: -1}
        },   
        {
            "$project": {
                "_id":1,
                "feedData":1,
                "feedType":1,
                "caption":1,
                "city":1,
                "country":1,
                "location":1,
                "likeCount":1,
                "crd":1,
                "feedImageRatio":1,
                "commentCount":1,
                "peopleTag":1,
                "serviceTag":1,
                "serviceTagId":1,
                "latitude":1,
                "longitude":1,
                "deleteStatus":1,
                "userId":1,
                "userInfo._id":1,
                "userInfo.userName":1,
                "userInfo.firstName":1,
                "userInfo.lastName":1,
                "userInfo.profileImage":1,
                 "userInfo.userType":1,
                 "userInfo.ratingCount":1,
                 "staffDetail":1
            }
        }
    ];

    var query = feed.aggregate(aggregateArray);
    query.exec(function(err, data) {
        if (err) {
            console.log(err);
        }

        if(data){
                newdata = data.length; 
            }else{
                newdata = 0;
            }
           // console.log('newdata', newdata)
            next();
        }); 



}
exports.finalFeedV2 = function(req, res) {

    if (req.body.page) {

        page = Number(req.body.page)*Number(req.body.limit);

    } else {

        page=0;

    }
    if (req.body.limit) {

        limit = Number(req.body.limit);
    } else {
        
        limit=10;
    }
    
    async.parallel([

        function(callback) {
            aggregateArray.push({ $skip:page });
            aggregateArray.push({ $limit:limit });
           
            var query = feed.aggregate(aggregateArray);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


     function(callback) {
        var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
        query.exec(function(err, ser1) {
            if (err) {
                callback(err);
            }
           
            callback(null, ser1);
        });
     },
        function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, flow) {
            if (err) {
                callback(err);
            }
 
            callback(null, flow);
        });
    }   

    ],
  
    //Compute all results
    function(err, results) {
               
   if (results[0]) {
     
       
       
       jsArr = [];
    for (var i = 0; i < results[0].length; i++) {

            var likeFeed = results[1].map(a => a.feedId);
             var flowSt = results[2].map(a => a.userId);
            

           if(results[1].length>0){
                var a = likeFeed.indexOf(results[0][i]._id);

                if(a >-1){
                     results[0][i].isLike = 1
                }else{
                     results[0][i].isLike = 0
                }
           
        }else{
                results[0][i].isLike = 0

        }
                //
        if(results[2].length>0){
           var b = flowSt.indexOf(results[0][i].userId);

        if(b >-1){
            results[0][i].followerStatus = 1
        }else{
           results[0][i].followerStatus = 0
        }

        }else{
          results[0][i].followerStatus = 0

        }  
        // 

        if(folderData){
                  
            var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
              
            if(folderData1.length){

               results[0][i].isSave = 1;  
                 
            }else{

               results[0][i].isSave = 0;
            }
        } 
      
    }
    jsArr =results[0] ;   

       if(jsArr){

            for (i = 0 ; i < jsArr.length ; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
               
              
                if(jsArr[i].feedData.length){ 
                                
                    for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                        if(jsArr[i].feedData[j].feedPost){ 
                         jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if(jsArr[i].feedData[j].videoThumb){ 
                         jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                        }
                    }
                            
                }
                
                if(jsArr[i].userInfo[0]){

                    if(jsArr[i].userInfo[0].profileImage){ 
                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                    }

               }else{

                    jsArr[i].userInfo[0] =[];

               }
            }
        
       }


        res.json({status: "success",message: 'successfully',AllFeeds: jsArr,total:newdata});
        return;

    } else {

       res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
       return;
    }
      
 });
}

exports.finalFeed = function(req, res) {

    if (req.body.page) {

        page = Number(req.body.page)*Number(req.body.limit);

    } else {

        page=0;

    }
    if (req.body.limit) {

        limit = Number(req.body.limit);
    } else {
        
        limit=10;
    }
    
    async.parallel([

        function(callback) {
            var query = feed.aggregate([
                {
                    $match:feedSearch 
                },
                
                {
                    "$lookup": {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                {
                   $sort: {_id: -1}
                },
                { $skip:page },
                { $limit:limit },
                {
                    "$project": {
                        "_id":1,
                        "feedType":1,
                        "feedData":1,
                        "caption":1,
                        "city":1,
                        "country":1,
                        "location":1,
                        "likeCount":1,
                        "crd":1,
                        "feedImageRatio":1,
                        "commentCount":1,
                        "peopleTag":1,
                        "serviceTag":1,
                        "userId":1,
                        "userInfo._id":1,
                        "userInfo.userName":1,
                        "userInfo.firstName":1,
                        "userInfo.lastName":1,
                        "userInfo.profileImage":1,
                         "userInfo.userType":1,
                         "userInfo.ratingCount":1,

                    }
                }

            ]);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


     function(callback) {
        var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
        query.exec(function(err, ser1) {
            if (err) {
                callback(err);
            }
           
            callback(null, ser1);
        });
     },
        function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, flow) {
            if (err) {
                callback(err);
            }
 
            callback(null, flow);
        });
    }   

    ],
  
    //Compute all results
    function(err, results) {
               
   if (results[0]) {
     
       
       
       jsArr = [];
    for (var i = 0; i < results[0].length; i++) {

            var likeFeed = results[1].map(a => a.feedId);
             var flowSt = results[2].map(a => a.userId);
            

           if(results[1].length>0){
                var a = likeFeed.indexOf(results[0][i]._id);

                if(a >-1){
                     results[0][i].isLike = 1
                }else{
                     results[0][i].isLike = 0
                }
           
        }else{
                results[0][i].isLike = 0

        }
                //
        if(results[2].length>0){
           var b = flowSt.indexOf(results[0][i].userId);

        if(b >-1){
            results[0][i].followerStatus = 1
        }else{
           results[0][i].followerStatus = 0
        }

        }else{
          results[0][i].followerStatus = 0

        }  
        // 

        if(folderData){
                  
            var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
              
            if(folderData1.length){

               results[0][i].isSave = 1;  
                 
            }else{

               results[0][i].isSave = 0;
            }
        } 
      
    }
    jsArr =results[0] ;   

       if(jsArr){

            for (i = 0 ; i < jsArr.length ; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
               
              
                if(jsArr[i].feedData.length){ 
                                
                    for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                        if(jsArr[i].feedData[j].feedPost){ 
                         jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if(jsArr[i].feedData[j].videoThumb){ 
                         jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                        }
                    }
                            
                }
                
                if(jsArr[i].userInfo[0]){

                    if(jsArr[i].userInfo[0].profileImage){ 
                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                    }

               }else{

                    jsArr[i].userInfo[0] =[];

               }
            }
        
       }


        res.json({status: "success",message: 'successfully',AllFeeds: jsArr,total:newdata});
        return;

    } else {

       res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
       return;
    }
      
 });
}


exports.feedDetails = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var feedSearch ={};

    search   = {};
    search['likeById'] =  Number(req.body.userId);
    search['type'] =  'feed';
    search['status'] =  1;
    search['deleteStatus'] = 1;


    if(req.body.feedId ==''){
       
        res.json({'status': "fail","massage": commonLang.REQ_FEED_ID});
        return;
          
    }

    if(req.body.userId ==''){
        res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
        return;
    }
    feedSearch['_id']   =  Number(req.body.feedId);
    feedSearch['userId'] = {$nin: data};

    async.parallel([

        function(callback) {
            var query = feed.aggregate([
                {
                    $match:feedSearch 
                },
                
                {
                    "$lookup": {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                {
                    "$project": {
                        "_id":1,
                        "feedType":1,
                        "feedData":1,
                        "caption":1,
                        "city":1,
                        "country":1,
                        "location":1,
                        "likeCount":1,
                        "peopleTag":1,
                        "crd":1,
                        "feedImageRatio":1,
                        "commentCount":1,
                        "peopleTag":1,
                        "serviceTag":1,
                        "userId":1,
                        "userInfo._id":1,
                        "userInfo.userName":1,
                        "userInfo.firstName":1,
                        "userInfo.lastName":1,
                        "userInfo.profileImage":1
                    }
                }

            ]);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


     function(callback) {
        var query = likes.find({'likeById':Number(req.body.userId),'feedId':Number(req.body.feedId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
        query.exec(function(err, ser1) {
            if (err) {
                callback(err);
            }
           
            callback(null, ser1);
        });
     },
        function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, flow) {
            if (err) {
                callback(err);
            }
 
            callback(null, flow);
        });
    }   

    ],
     
    function(err, results) {
       
               
   if (results[0]) {
     
              
       jsArr = [];
        for (var i = 0; i < results[0].length; i++) {
                var likeFeed = results[1].map(a => a.feedId);
                var flowSt = results[2].map(a => a.userId);
                if(results[1].length>0){
                        var a = likeFeed.indexOf(results[0][i]._id);

                        if(a >-1){
                             results[0][i].isLike = 1
                        }else{
                             results[0][i].isLike = 0
                        }
                   
                }else{
                        results[0][i].isLike = 0

                }
           
                if(results[2].length>0){
                   var b = flowSt.indexOf(results[0][i].userId);

                if(b >-1){
                    results[0][i].followerStatus = 1
                }else{
                   results[0][i].followerStatus = 0
                }

                }else{
                  results[0][i].followerStatus = 0

                }  

                if(folderData){
                          
                    var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                      
                    if(folderData1.length){

                       results[0][i].isSave = 1;  
                         
                    }else{

                       results[0][i].isSave = 0;
                    }
                } 
            
          
        }
        jsArr =results[0] ;   

       if(jsArr){
            for (i = 0 ; i < jsArr.length ; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
               
              
                if(jsArr[i].feedData.length){ 
                                
                    for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                        if(jsArr[i].feedData[j].feedPost){ 
                         jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if(jsArr[i].feedData[j].videoThumb){ 
                         jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                        }
                    }
                            
                }
                
                if(jsArr[i].userInfo[0]){
                     if(jsArr[i].userInfo[0].profileImage){ 
                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                      }
               }else{
                 jsArr[i].userInfo[0] =[];
               }
            }
        
       }


        res.json({status: "success",message: 'successfully',feedDetail: jsArr});
        return;  

    } else {

       res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,feedDetail:[] });
       return;
    }
      
 });

}
exports.feedDetailsV2 = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var feedSearch ={};

    search   = {};
    search['likeById'] =  Number(req.body.userId);
    search['type'] =  'feed';
    search['status'] =  1;
    search['deleteStatus'] = 1;


    if(req.body.feedId ==''){
       
        res.json({'status': "fail","massage": commonLang.REQ_FEED_ID});
        return;
          
    }

    if(req.body.userId ==''){
        res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
        return;
    }
    feedSearch['_id']   =  Number(req.body.feedId);
    feedSearch['userId'] = {$nin: data};
    aggregateArray = [
                                  
        {
            $match:feedSearch
        },
        {
            "$lookup": {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": {
                path:"$serviceTag", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "serviceTag.tagDetails.artistServiseTagId",
                foreignField: "_id",
                as: "arstistServiceTagDetail"
            }
        }, 
        {
            "$lookup": {
                from: "staffservices",
                localField: "serviceTag.tagDetails.staffServiseTagId",
                foreignField: "_id",
                as: "staffServiceTagDetail"
            }
        },  
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.staffId",
                foreignField: "_id",
                as: "staffDetail"
            }
        }, 
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.artistId",
                foreignField: "_id",
                as: "artistDetail"
            }
        }, 
        { "$unwind": {
                path:"$arstistServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$artistDetail", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "arstistServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.serviceId"
            }
        },
        
        {
            "$lookup": {
                from: "subservices",
                localField: "arstistServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "staffServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.serviceId"
            }
        },
        {
            "$lookup": {
                from: "subservices",
                localField: "staffServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "staffServiceTagDetail.artistServiceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.artistServiceName"
            }
        },
        { "$group": {
                "_id":  "$_id",
                "feedData": {"$first": "$feedData"},
                "feedType": {"$first": "$feedType"},
                "caption": {"$first": "$caption"},
                "city": {"$first": "$city"},
                "country": {"$first": "$country"},
                "location": {"$first": "$location"},
                "likeCount": {"$first": "$likeCount"},
                "crd": {"$first": "$crd"},
                "feedImageRatio": {"$first": "$feedImageRatio"},
                "commentCount": {"$first": "$commentCount"},
                "peopleTag": {"$first": "$peopleTag"},
                "serviceTag": {"$first": "$serviceTag"},
                "serviceTagId": {"$first": "$serviceTagId"},
                "latitude": {"$first": "$latitude"},
                "longitude": {"$first": "$longitude"},
                "deleteStatus": {"$first": "$deleteStatus"},
                "userId": {"$first": "$userId"},
                "userInfo": {"$first": "$userInfo"},
                "serviceTag": {
                    "$push": {
                        tag:"$serviceTag", 
                        arstistServiceTagDetail: {
                            artistServiceID:"$arstistServiceTagDetail._id",
                            title:"$arstistServiceTagDetail.title",
                            description:"$arstistServiceTagDetail.description",
                            inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                            outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                            completionTime:"$arstistServiceTagDetail.completionTime",
                            buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                            categoryID:"$arstistServiceTagDetail.subserviceId._id",
                            categoryName:"$arstistServiceTagDetail.subserviceId.title"
                            
                        },
                        staffServiceTagDetail: {
                            staffServiceID:"$staffServiceTagDetail.artistServiceId",
                            title:"$staffServiceTagDetail.artistServiceName.title",
                            description:"$staffServiceTagDetail.artistServiceName.description",
                            inCallPrice:"$staffServiceTagDetail.inCallPrice",
                            outCallPrice:"$staffServiceTagDetail.outCallPrice",
                            completionTime:"$staffServiceTagDetail.completionTime",
                            buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                            categoryID:"$staffServiceTagDetail.subserviceId._id",
                            categoryName:"$staffServiceTagDetail.subserviceId.title"
                        },
                        staffDetail: {
                            firstName:"$staffDetail.firstName",
                            lastName:"$staffDetail.lastName"
                        },
                        artistDetail:{
                            firstName:"$artistDetail.firstName",
                            lastName:"$artistDetail.lastName"
                        }
                    }
                }
                
            }
        },
        {
            "$project": {
                "_id":1,
                "feedData":1,
                "feedType":1,
                "caption":1,
                "city":1,
                "country":1,
                "location":1,
                "likeCount":1,
                "crd":1,
                "feedImageRatio":1,
                "commentCount":1,
                "peopleTag":1,
                "serviceTag":1,
                "serviceTagId":1,
                "latitude":1,
                "longitude":1,
                "deleteStatus":1,
                "userId":1,
                "userInfo._id":1,
                "userInfo.userName":1,
                "userInfo.firstName":1,
                "userInfo.lastName":1,
                "userInfo.profileImage":1,
                 "userInfo.userType":1,
                 "userInfo.ratingCount":1,
                 "staffDetail":1
            }
        }
    ];

    async.parallel([

        function(callback) {
            var query = feed.aggregate(aggregateArray);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


     function(callback) {
        var query = likes.find({'likeById':Number(req.body.userId),'feedId':Number(req.body.feedId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
        query.exec(function(err, ser1) {
            if (err) {
                callback(err);
            }
           
            callback(null, ser1);
        });
     },
        function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, flow) {
            if (err) {
                callback(err);
            }
 
            callback(null, flow);
        });
    }   

    ],
     
    function(err, results) {
       
               
   if (results[0]) {
     
              
       jsArr = [];
        for (var i = 0; i < results[0].length; i++) {
                var likeFeed = results[1].map(a => a.feedId);
                var flowSt = results[2].map(a => a.userId);
                if(results[1].length>0){
                        var a = likeFeed.indexOf(results[0][i]._id);

                        if(a >-1){
                             results[0][i].isLike = 1
                        }else{
                             results[0][i].isLike = 0
                        }
                   
                }else{
                        results[0][i].isLike = 0

                }
           
                if(results[2].length>0){
                   var b = flowSt.indexOf(results[0][i].userId);

                if(b >-1){
                    results[0][i].followerStatus = 1
                }else{
                   results[0][i].followerStatus = 0
                }

                }else{
                  results[0][i].followerStatus = 0

                }  

                if(folderData){
                          
                    var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                      
                    if(folderData1.length){

                       results[0][i].isSave = 1;  
                         
                    }else{

                       results[0][i].isSave = 0;
                    }
                } 
            
          
        }
        jsArr =results[0] ;   

       if(jsArr){
            for (i = 0 ; i < jsArr.length ; i++) {
                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
               
              
                if(jsArr[i].feedData.length){ 
                                
                    for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                        if(jsArr[i].feedData[j].feedPost){ 
                         jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                        }
                        if(jsArr[i].feedData[j].videoThumb){ 
                         jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                        }
                    }
                            
                }
                
                if(jsArr[i].userInfo[0]){
                     if(jsArr[i].userInfo[0].profileImage){ 
                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                      }
               }else{
                 jsArr[i].userInfo[0] =[];
               }
            }
        
       }


        res.json({status: "success",message: 'successfully',feedDetail: jsArr});
        return;  

    } else {

       res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,feedDetail:[] });
       return;
    }
      
 });

}

exports.addTag = function(req, res, next) {

    var form = new multiparty.Form();
    var crd = moment().format();
    fields = {};
    files = {};
    oldTag = [];
    newTag = [];
    tagInfo = [];
    tagId = {};
    form.parse(req, function(err, fields1, files1) {
        fields = fields1;  
        files = files1;  
        if (fields.tag[0]) {
            tags = fields.tag;
            if (tags[0]) {
                var ser = tags[0].split(",");
                var unique = ser.map(function(n) {
                    return n;
                });
            }
            var tagInfo = unique.filter( onlyUnique ); 
            avlTag = [];

            tag.find().sort([ ['_id', 'descending']]).limit(1).exec(function(err, userdata) {
                var autoIdT = 1;

                if (userdata.length > 0) {
                    autoIdT = userdata[0]._id + 1;

                }

                search = { $in: tagInfo };

                jsArrT = [];
                a = 0;
                tag.find({'tag': search }, { '_id': 1,'tag': 1, 'tagCount': 1}).exec(function(err, tgdata) {

                    if (tgdata.length > 0) {
                        count = 0;
                        tgdata.forEach(function(rs) {
                            oldTag.push(rs._id);
                            avlTag.push(rs.tag);
                            count = rs.tagCount + 1;
                            tag.updateOne({_id: rs._id}, {$set: {'tagCount': count}}, function(err, upd) {});
                        });

                    } else {


                    }

                    inc = autoIdT + a;
                    tagInfo = tagInfo.filter(function(val) {
                        return avlTag.indexOf(val) == -1;
                    });
                    for (var t = 0; t < tagInfo.length; t++) {
                        incT = autoIdT + t;
                        newTag.push(incT);
                        jsArrT.push({
                            _id: incT,
                            userId: Number(fields.userId),
                            tag: tagInfo[t],
                            type: 'hastag',
                            tagCount: 1,
                            crd: crd,
                            upd: crd
                        });

                    }

                    if (jsArrT.length > 0) {
                        tag.insertMany(jsArrT);

                    }

                    var c = oldTag.concat(newTag);
                    tagId = c;
                    req.body.userId = Number(fields.userId);
                    req.body.type ='newsFeed'
                    next();
                });
            });


        } else {
            tagId = '';
            next();
        }

    });

}


exports.followerFeedGet = function(req,res,next){

    folInfo = {};
    
    if(fields.userId ==''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }

    followUnfollow.find({'userId':Number(fields.userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
      
        if(followData){

             a = [];
             a= followData.map(a => a.followerId);
             a.push(Number(fields.userId));         
             folInfo.flUser =a ;

        }else{
            folInfo.flUser = [];
            
        }

        next();

    });

}


exports.addFeed = function(req, res) {

    var crd = moment().format();
    if (fields.userId) {
        var userId = Number(fields.userId);

    } else {
        var userId = authData._id;
    }
    if (fields.city) {
        var city = fields.city;

    } else {
        var city = '';
    }
    if (fields.country) {
        var country = fields.country;

    } else {
        var country = '';

    }

    var tagData = fields.tagData ? fields.tagData :'';

    count = Number(authData.postCount) + 1;

    var baseUrl = req.protocol + '://' + req.headers['host'];
    jsArr = []
    thumbArr = []
    stor = [];
    oldTag = [];
    newTag = [];
    tagInfo = [];


    if (files.feed) {

        var imgArray = files.feed;
        for (var i = 0; i < imgArray.length; i++) {

            var newPath = './public/uploads/feeds/';

            var singleImg = imgArray[i];
            nmFeed = Date.now() + i;
            if (imgArray[i].headers['content-type'] == 'video/mp4') {
                nm = nmFeed + '.mp4';
                feedUrl = nm;
                thumb = nmFeed + '.jpg';

            } else {
                nm = Date.now() + i + '.jpg';
                feedUrl = nm;
                thumb = '';
            }

            newPath += nm;

            videopath = './public/uploads/feeds/' + nm;
            readAndWriteFile(singleImg, newPath);

            jsArr.push({
                feedPost: feedUrl,
                videoThumb: thumb,

            });

        }

    }
    if (files.videoThumb) {

        var imgArray1 = files.videoThumb;
        for (var i = 0; i < imgArray1.length; i++) {
            var newPath = './public/uploads/feeds/';
            var singleImg = imgArray1[i];
            nmFeed = Date.now() + i;
            nm = Date.now() + i + '.jpg';
            feedUrl = nm;
            newPath += nm;

            videopath = './public/uploads/feeds/' + nm;
            readAndWriteFile2(singleImg, newPath);
            thumbArr.push({
                videoThumb: feedUrl
            });

        }

    }
    if (thumbArr.length > 0) {
        for (var k = 0; k < jsArr.length; k++) {

            for (var j = 0; j < thumbArr.length; j++) {
                if (k == j) {
                    stor.push({
                        feedPost: jsArr[k].feedPost,
                        videoThumb: thumbArr[j].videoThumb

                    });
                }

            }


        }
    } else {
        for (var i = 0; i < jsArr.length; i++) {
            stor.push({
                feedPost: jsArr[i].feedPost,
                videoThumb: ''
            });
        }

    }
    if (fields.feed) {
        ff = fields.feed;
        feeds = [];
    } else {
        feeds = stor;
    }
    if (!tagId) {
        tagId = '';
    }

    let glatitude = Number(fields.latitude[0]);
    let glongitude = Number(fields.longitude[0]);

    var addNew = {
        userId: userId,
        feedType: fields.feedType,
        feedData: feeds,
        caption: fields.caption,
        city: city,
        country: country,
        location: fields.location,
        latitude: fields.latitude,
        longitude: fields.longitude,
        feedImageRatio: fields.feedImageRatio ? Number(fields.feedImageRatio) : 1,
        tagId: tagId,
        peopleTag: fields.peopleTag ? fields.peopleTag[0] ? JSON.parse(fields.peopleTag) : [] : [],
        peopleTagData: fields.peopleTagData ? fields.peopleTagData[0] ? JSON.parse(fields.peopleTagData) : [] : [],
        serviceTag: fields.serviceTag ? fields.serviceTag[0] ? JSON.parse(fields.serviceTag) : [] : [],
        serviceTagId: fields.businessTypeId ? fields.businessTypeId[0] ? fields.businessTypeId[0].split(",") : [] : [],
        businessTypeServiceTagId: fields.businessTypeServiceTagId ? fields.businessTypeServiceTagId[0] ? fields.businessTypeServiceTagId[0].split(",") : [] : [],
        artistServiceId: fields.artistServiceId ? fields.artistServiceId[0] ? fields.artistServiceId[0].split(",") : [] : [],
        oucallOrIncall: fields.oucallOrIncall ? fields.oucallOrIncall[0] ? fields.oucallOrIncall[0].split(",") : [] : [],
        crd: crd,
        upd: crd,
        geoLocation:{
            type: "Point",  //geo Type
            coordinates: [
              glongitude, //longitude
              glatitude //latitude
            ]
          }
    };

    autoId = 1; 
    feed.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

        if (result) {
            autoId     = result._id + 1;
            addNew._id = autoId;
        }
        if(JSON.parse(tagData)){

            t = JSON.parse(tagData);
            t.push(userId);
            var appUser = require("./user");  
            req.body.notifincationType = '16';
            req.body.notifyId   = autoId;
            req.body.notifyType = 'social'; 
            req.body.userId = userId; 
            folInfo.flUser = t;
            appUser.sendMultiple(req,res); 
        }

        feed(addNew).save(function(err, data) {

            if (err) {
                res.json({status: "fail", message: err});
                return;
            } else {
                if(fields.feedType != 'text'){
                    User.updateOne({ _id: userId},{$set:{postCount: count}},function(err, docs) {});
                }
                feed.findOne({'_id': autoId}).exec(function(err, data) {
                    if (data) {

                        followUnfollow.find({'userId':Number(userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
                            folInfo.flUser  = [];
                            if(followData){

                                 a = [];
                                 a= followData.map(a => a.followerId);
                                 a.push(Number(fields.userId));         
                                 folInfo.flUser =a ;

                            }            
                            if(folInfo.flUser){
                               /*code for notification*/  
                                 var appUser = require("./user");  
                                   req.body.notifincationType = '7';
                                   req.body.notifyId   = autoId;
                                   req.body.notifyType = 'social'; 
                                   req.body.userId = userId; 
                                //   appUser.sendMultiple(req,res); 
                                             
                                /*end notification code*/   
                            } 

                        }); 
                        res.json({status: "success",message: commonLang.SUCCESS_UPLOAD_POST,feeds:data});
                    }
                })

            }

        });

    });

}

exports.addFeedv2 = function(req, res) {
	
    var crd = moment().format();
    if (fields.userId) {
        var userId = Number(fields.userId);

    } else {
        var userId = authData._id;
    }
    if (fields.city) {
        var city = fields.city;

    } else {
        var city = '';
    }
    if (fields.country) {
        var country = fields.country;

    } else {
        var country = '';

    }

    var tagData = fields.tagData ? fields.tagData :'';
    let thumboptions = {};

    count = Number(authData.postCount) + 1;

    var baseUrl = req.protocol + '://' + req.headers['host'];
    jsArr = []
    thumbArr = []
    stor = [];
    oldTag = [];
    newTag = [];
    tagInfo = [];
  
    // This code is responsible to upload feed media images
    var imgArray = files.feed ? files.feed : [];
    let feedMediaPromise = imgArray.map(async function(file){
    	let ext =  file.originalFilename.split('.').pop();
    	let fileName = uniqid()+'.'+ext;
        let videoThumb = '';

        if(file.headers['content-type'] == 'video/mp4'){
        	videoThumb = fileName
        }
        else{
        	let height = calculateFeedImageThunmbHeight(file.path);
        	thumboptions = {
			    resize: { height: height },
			    thumbFolder:commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+commonConstants.FEED_THUMB_FOLDER
			  }
        }

        jsArr.push({
            feedPost: fileName,
            videoThumb: videoThumb,

        });
        
        return await fileUploadObj.uploadFile(file, commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);

    });

    // This code is responsible to upload feed media video thumb images
    var vidThumbArray = files.videoThumb ? files.videoThumb : [];
    let vidThumbPromise = vidThumbArray.map(async function(file){
    	let ext =  file.originalFilename.split('.').pop();
    	let fileName = uniqid()+'.'+ext;
        let videoThumb = '';
    	let height = calculateFeedImageThunmbHeight(file.path);
    	thumboptions = {
		    resize: { height: height },
		    thumbFolder:commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+commonConstants.FEED_THUMB_FOLDER
		  }

        thumbArr.push({
            feedPost: fileName,
            videoThumb: videoThumb,

        });
        
        return await fileUploadObj.uploadFile(file, commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);

    });
    

    let allPromise = feedMediaPromise.concat(vidThumbPromise);

    Promise.all(allPromise).then(function(values) {
	    if (thumbArr.length > 0) {
	        for (var k = 0; k < jsArr.length; k++) {

	            for (var j = 0; j < thumbArr.length; j++) {
	                if (k == j) {
                        
	                    stor.push({
	                        feedPost: jsArr[k].feedPost,
	                        videoThumb: thumbArr[j].feedPost

	                    });
	                }

	            }


	        }
	    } else {
	        for (var i = 0; i < jsArr.length; i++) {
	            stor.push({
	                feedPost: jsArr[i].feedPost,
	                videoThumb: ''
	            });
	        }

	    }
	    if (fields.feed) {
	        ff = fields.feed;
	        feeds = [];
	    } else {
	        feeds = stor;
	    }
	    if (!tagId) {
	        tagId = '';
	    }
        if(fields.feedType!='text'&&(stor[0].feedPost=='' || typeof stor[0].feedPost === "undefined")){

         res.json({status: "fail",message:'Post uploading fail',feeds:[]});
         return ;
        }
        let glatitude = Number(fields.latitude[0]);
        let glongitude = Number(fields.longitude[0]);
	    var addNew = {
	        userId: userId,
	        feedType: fields.feedType,
	        feedData: feeds,
	        caption: fields.caption,
	        city: city,
	        country: country,
	        location: fields.location,
	        latitude: fields.latitude,
	        longitude: fields.longitude,
	        feedImageRatio: fields.feedImageRatio ? Number(fields.feedImageRatio) : 1,
	        tagId: tagId,
	        peopleTag: fields.peopleTag ? fields.peopleTag[0] ? JSON.parse(fields.peopleTag) : [] : [],
	        peopleTagData: fields.peopleTagData ? fields.peopleTagData[0] ? JSON.parse(fields.peopleTagData) : [] : [],
	        serviceTag: fields.serviceTag ? fields.serviceTag[0] ? JSON.parse(fields.serviceTag) : [] : [],
	        serviceTagId: fields.businessTypeId ? fields.businessTypeId[0] ? fields.businessTypeId[0].split(",") : [] : [],
	        businessTypeServiceTagId: fields.businessTypeServiceTagId ? fields.businessTypeServiceTagId[0] ? fields.businessTypeServiceTagId[0].split(",") : [] : [],
	        artistServiceId: fields.artistServiceId ? fields.artistServiceId[0] ? fields.artistServiceId[0].split(",") : [] : [],
	        oucallOrIncall: fields.oucallOrIncall ? fields.oucallOrIncall[0] ? fields.oucallOrIncall[0].split(",") : [] : [],
	        crd: crd,
	        upd: crd,
            geoLocation:{
                type: "Point",  //geo Type
                coordinates: [
                  glongitude, //longitude
                  glatitude //latitude
                ]
          }
	    };
	    
	    autoId = 1; 
	    feed.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

	        if (result) {
	            autoId     = result._id + 1;
	            addNew._id = autoId;
	        }
	        if(JSON.parse(tagData)){

	            t = JSON.parse(tagData);
	            t.push(userId);
	            var appUser = require("./user");  
	            req.body.notifincationType = '16';
	            req.body.notifyId   = autoId;
	            req.body.notifyType = 'social'; 
	            req.body.userId = userId; 
	            folInfo.flUser = t;
	            appUser.sendMultiple(req,res); 
	        }

	        feed(addNew).save(function(err, data) {

	            if (err) {
	                res.json({status: "fail", message: err});
	                return;
	            } else {
	                if(fields.feedType != 'text'){
	                    User.updateOne({ _id: userId},{$set:{postCount: count}},function(err, docs) {});
	                }
	                feed.findOne({'_id': autoId}).exec(function(err, data) {
	                    if (data) {

	                        followUnfollow.find({'userId':Number(userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
	                            folInfo.flUser  = [];
	                            if(followData){

	                                 a = [];
	                                 a= followData.map(a => a.followerId);
	                                 a.push(Number(fields.userId));         
	                                 folInfo.flUser =a ;

	                            }            
	                            if(folInfo.flUser){
	                                 var appUser = require("./user");  
	                                   req.body.notifincationType = '7';
	                                   req.body.notifyId   = autoId;
	                                   req.body.notifyType = 'social'; 
	                                   req.body.userId = userId; 
	                                             
	                            } 

	                        }); 
	                        res.json({status: "success",message:commonLang.SUCCESS_UPLOAD_POST,feeds:data});
	                    }
	                })

	            }

	        });

	    });
	});
}

/*
 * @Purpose: This function is responsible to calculate image height 
 * 			 to make thumb according to ratio
 * @Parmas: imagePath (Contains image path)
 *
*/
function calculateFeedImageThunmbHeight(imagePath){
	let dimensions = sizeOf(imagePath);
	let height;
	if(dimensions.height > dimensions.width){
		height = 600;
	}else if(dimensions.height < dimensions.width){
		height = 360;
	}else{
		height = 480;
	}
	return height;
}


function readAndWriteFile(singleImg, newPath) {

    fs.readFile(singleImg.path , function(err,data) {
        fs.writeFile(newPath,data, function(err) {
            if (err) console.log('ERRRRRR!! :'+err);
            console.log('Fitxer: '+singleImg.originalFilename +' - '+ newPath);
        })
    })
}


function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}


function readAndWriteFile2(singleImg, newPath) {

    fs.readFile(singleImg.path , function(err,data) {
        fs.writeFile(newPath,data, function(err) {
            if (err) console.log('ERRRRRR!! :'+err);
            console.log('Fitxer: '+singleImg.originalFilename +' - '+ newPath);
        })
    })
}/*like api start*/
  

exports.like = function(req,res){
    if(req.body.userId){
       var userId = Number(req.body.userId);
    }else{
        res.json({status: 'fail',message: commonLang.REQ_USER_ID});
        return;
    }
     
    if(req.body){
        var addNew = {
            feedId:req.body.feedId,
            userId:req.body.userId,
            likeById:req.body.likeById,
            gender:req.body.gender ? req.body.gender : 'male',
            age:req.body.age ? req.body.age : 20,
            city:req.body.city ? req.body.city : 'indore',
            state:req.body.state ? req.body.state : 'mp',
            country:req.body.country ? req.body.country : 'India',
            type:req.body.type
            
        };
    }else{

        res.json({status: 'fail',message: commonLang.REQ_FEED_ID});
        return;
    }
    autoId = 1;
    likes.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {
        if (result) {
            addNew._id = result._id + 1;
        }
        likes.findOne({'feedId':req.body.feedId,'likeById':req.body.likeById,'type':'feed'}, function(err, data) {
            if(data){

                feed.findOne({'_id':req.body.feedId,'status':1}, function(err, result1) {
                    
                    if(data.status==1){
                           setValue = {'status':0}
                           if(result1.likeCount>0){
                            count = result1.likeCount-1;
                        }else{
                            count =0;
                        }
                           
                    }else{
                            setValue = {'status':1}
                            count = Number(result1.likeCount)+1;
                    }
                    likes.updateOne({ _id:data._id },{ $set:setValue},function(err, result2) { });
                    feed.updateOne({ _id:req.body.feedId },{ $set:{likeCount:count}},
                    function(err, result2) {

                        if(data.status==0){

                           /*code for notification*/ 
                            var typ = '10';
                            var sender     = addNew.likeById;
                            var receiver   = addNew.userId; 
                            var notifyId   = addNew.feedId;
                            var notifyType = 'social';  
                            if(sender!=receiver){
                               notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 
                            }

                            /*end notification code*/
                            
                        } 
                        res.json({status: "success",message: 'ok'});
                        return; 
                    });

                   
                });

            }else{

                likes(addNew).save(function(err, data) {
 
                    if (err) {
                        res.json({status: "fail",message:err});
                        return;
                    } else {

                        feed.findOne({'_id':req.body.feedId,'status':1}, function(err, result4) {
                          
                            count = Number(result4.likeCount)+1;
                            feed.updateOne({ _id:req.body.feedId },{ $set:{likeCount:count}},function(err, result5) { });
                         
                        });

                        /*code for notification*/ 
                        var typ = '10';
                        var sender     = addNew.likeById;
                        var receiver   = addNew.userId; 
                        var notifyId   = addNew.feedId;
                        var notifyType = 'social';  
                        if(sender!=receiver){
                           notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 
                        }

                        /*end notification code*/ 
                        res.json({status: "success",message: 'ok'});
                        return;
                    }
                });
            }
       });
    });       
}


exports.likeList = function (req,res,next){ 
    baseUrl =  req.protocol + '://'+req.headers['host'];
     //var Value_match = new RegExp(req.body.search);
    Value_match =  escapere(req.body.search);
    async.parallel([function(callback) {

            var query = likes.aggregate([
                        
                {
                    $lookup: {
                        from: "users",
                        localField: "likeById",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                { "$unwind": "$userInfo" },
                {
                    $match: {
                        'userInfo.userName':{$regex:Value_match,$options:'i'},
                        'feedId':Number(req.body.feedId),
                        'type':'feed',
                        'status':1
                    }
                },
                {   
                    $project:{
                        "likeById" :1,
                        userName :"$userInfo.userName",
                        firstName :"$userInfo.firstName",
                        lastName :"$userInfo.lastName",
                        profileImage :"$userInfo.profileImage"

                    } 
                }

            ]);
            query.exec(function(err, g) {

                if (err) {
                    callback(err);
                }
                callback(null, g);
            });
        },
     
        function(callback) {
            var query = followUnfollow.find({'userId':req.body.userId,'status':1})
            query.exec(function(err, s) {
                if (err) {
                    callback(err);
                }
     
                callback(null, s);
            });
        },
     
    ],
 
    //Compute all results
    function(err, results) {
        if(results[0]){
            newData = results[0].length;

      }else{
            newData = 0;
     
      }
      next();  

    });
}


exports.likeListFinal = function (req,res){ 
    if (req.body.page) {
        page = Number(req.body.page)*Number(req.body.limit);
    } else {
        page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }
    async.parallel([function(callback) {
        var query =     likes.aggregate([

            {
                $lookup: {
                    from: "users",
                    localField: "likeById",
                    foreignField: "_id",
                    as: "userInfo"
                }
            },
            { "$unwind": "$userInfo" },
            {
                $match: {
                    'userInfo.userName':{$regex:Value_match,$options:'i'},
                    'feedId':Number(req.body.feedId),
                    'type':'feed',
                    'status':1
                }
            },
            { $skip:page },
            { $limit:limit },
            {   
                $project:{
                    "likeById" :1,
                    userName :"$userInfo.userName",
                    firstName :"$userInfo.firstName",
                    lastName :"$userInfo.lastName",
                    profileImage :"$userInfo.profileImage"

                } 
            }

        ]);
        query.exec(function(err, g) {
            if (err) {
                callback(err);
            }
            callback(null, g);
        });
    },

    function(callback) {
        var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
        query.exec(function(err, s) {
            if (err) {
                callback(err);
            }

            callback(null, s);
        });
    },

    ],

    //Compute all results
    function(err, results) {

        if (err) {
            return res.send(400);
        }

        if (results == null) {
            return res.send(400);
        }
        //

        if(results[0]){

            for(var i = 0; i < results[0].length; i++) {

                if(results[0][i].profileImage){ 
                    results[0][i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ results[0][i].profileImage;
                }
                var likeFeed = results[1].map(a => a.userId);

                if(results[1].length>0){

                    var a = likeFeed.indexOf(results[0][i].likeById);

                    if(a >-1){

                        results[0][i].followerStatus = 1;
                    }else{

                        results[0][i].followerStatus = 0;
                    }

                }else{

                    results[0][i].followerStatus = 0;

                }     


            }

        }else{

            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});
        }
        // var combineResults =   { goal1: results[0], goal: results[1]};
        var combineResults =    results[0];
        res.json({status:"success",message:'ok',likeList:combineResults,'total':newData});
    });
}/* like api end*/

/* Tag Search Api start*/
exports.tagSearch = function(req,res){

    var type =  req.body.type;
    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var Value_match = {$regex:req.body.search,$options:'i'};
    if(req.body.type){
        User.find({'isDocument':3,'userName':Value_match},{'_id':1,'firstName':1,'lastName':1,'userName':1,'profileImage':1}).exec(function(err, data) {
            if (data) {
                  for (i = 0 ; i < data.length ; i++) {
                    if(data[i].profileImage){ 
                        data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ data[i].profileImage;
                    }
                }
                res.json({
                    'status': "success",
                    "massage": "ok",
                    "allTags":data
                });
               return;
            }else{
            res.json({
                    'status': "fail",
                    "massage": "No record found."
                });
               return;
              }
        });
    
    }else{
        tag.find({'tag':Value_match},{'_id':1,'tag':1}).exec(function(err, data) {
            if (data) {
             
                res.json({
                    'status': "success",
                    "massage": "ok",
                    "allTags":data
                });
               return;
            }else{
            res.json({
                    'status': "fail",
                    "massage": "No record found."
                });
               return;
              }
        });

    }

}/* Tag Search Api end*/

/* Feed According Tag Get Api start*/
exports.userFeedByTag = function(req,res,next){
    tagId = {};
    var findData = req.body.findData;
    if(req.body.type =='hasTag'){
         if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_TAG});
             return;
        }
        
        tag.findOne({'tag':req.body.findData,'type':'hastag'},{'_id':1}).sort([['_id', 'ascending']]).limit(1).exec(function(err, adata) {

        if(adata){
            tagId = adata;
            next();
        }
        });
    }else{
        tagId = '';  
        next();
    }

}/* Feed According Tag Get Api end*/

/* Feed According user Get Api start*/
exports.userFeed = function(req,res,next){

    baseUrl =  req.protocol + '://'+req.headers['host'];
    Value_match = {$regex:req.body.feedType};
    type = req.body.type;
    feedSearch ={};
    findData = req.body.findData;
    // feedSearch['feedType'] =Value_match;
     feedSearch['feedType'] ={'$ne':'text'};
    feedSearch['deleteStatus'] = 1;

     
    if(req.body.type =='user'){
        if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
            return;
        }
        
        feedSearch['userId'] = Number(findData);
        if(req.body.peopleType){

            feedSearch['serviceTagId'] = {'$ne':[]};
        
        }
         
    }
    if(req.body.type =='hasTag'){
        if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_TAG});
            return;
        }
        if(tagId){
            feedSearch['tagId'] = { $in: [tagId._id]};
        }
        
         
    }
    if(req.body.type =='place'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_PLACE});
          return;
        }
       feedSearch['location'] = {$regex:findData};
    }
       

    feed.aggregate([
        {
             $lookup:
               {
                 from: "users",
                 localField: "userId",
                 foreignField: "_id",
                 as: "userInfo"
               }
        },
        {
            $sort: {_id: -1}
        },
        {
             $match:feedSearch
       },
       {   
            "$project":{
                "_id":1,
                "feedType":1,
                "feedData":1,
                "caption":1,
                "city":1,
                "country":1,
                "location":1,
                "likeCount":1,
                "crd":1,
                "feedImageRatio":1,
                "commentCount":1,
                "peopleTag":1,
                "serviceTag":1,
                "userInfo._id":1,
                "userInfo.userName":1,
                "userInfo.firstName":1,
                "userInfo.lastName":1,
                "userInfo.profileImage":1,
                "userInfo.userType":1,
            } 
         }

    ],function(err, dataLength){
       
        if(dataLength){
            newdata = dataLength.length; 
        }else{
            newdata = 0;
        }
        next();
           
    });

}


exports.finalUserFeed = function(req, res) {
    search   = {};
    search['likeById'] =  Number(req.body.userId);
    search['type'] =  'feed';
    search['status'] =  1;
    search['deleteStatus'] = 1;

    var feedSearch ={};
    // feedSearch['feedType'] =Value_match;
    feedSearch['feedType'] ={'$ne':'text'};
    if(req.body.type =='user'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_USER_ID});
         return;
        }
        feedSearch['userId'] = Number(findData);
        if(req.body.peopleType){

            feedSearch['serviceTagId'] = {'$ne':[]};
        
        }
    }
    if(req.body.type =='hasTag'){
        if(findData == ''){
            res.json({'status': "fail","massage": commonLang.REQ_TAG});
            return;
        }
          if(tagId){
            feedSearch['tagId'] = { $in: [tagId._id]};
        }
         
    }
     if(req.body.type =='place'){
        if(findData == ''){
          res.json({'status': "fail","massage": commonLang.REQ_PLACE});
         return;
        }
       feedSearch['location'] = {$regex:findData};
    }       
    if (req.body.page) {
          page = Number(req.body.page)*Number(req.body.limit);
    } else {
      page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }

    async.parallel([

        function(callback) {
            var query = feed.aggregate([
                {
                    $match:feedSearch 
                },
                
                {
                    "$lookup": {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                {
                   $sort: {_id: -1}
                },
                { $skip:page },
                { $limit:limit },
                {   
                    "$project":{
                        "_id":1,
                        "feedType":1,
                        "feedData":1,
                        "caption":1,
                        "city":1,
                        "country":1,
                        "location":1,
                        "likeCount":1,
                        "crd":1,
                        "feedImageRatio":1,
                        "commentCount":1,
                        "peopleTag":1,
                        "serviceTag":1,
                        "userInfo._id":1,
                        "userInfo.userName":1,
                        "userInfo.firstName":1,
                        "userInfo.lastName":1,
                        "userInfo.profileImage":1,
                        "userInfo.userType":1,
                    } 
                }

            ]);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }
                
                callback(null, g);
            });
        },


        function(callback) {
            var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
            query.exec(function(err, ser1) {
                if (err) {
                    callback(err);
                }
               
                callback(null, ser1);
            });
        },
        function(callback) {
            var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
            query.exec(function(err, flow) {
                if (err) {
                    callback(err);
                }
     
                callback(null, flow);
            });
        }   

    ],
     
    function(err, results) {
               
        if (results[0]) {
         
                  
            jsArr = [];
            for (var i = 0; i < results[0].length; i++) {
                    var likeFeed = results[1].map(a => a.feedId);
                     var flowSt = results[2].map(a => a.userId);
                   if(results[1].length>0){
                        var a = likeFeed.indexOf(results[0][i]._id);

                        if(a >-1){
                             results[0][i].isLike = 1
                        }else{
                             results[0][i].isLike = 0
                        }
                   
                }else{
                        results[0][i].isLike = 0

                }
               
                if(results[2].length>0){
                   var b = flowSt.indexOf(results[0][i].userId);

                if(b >-1){
                    results[0][i].followerStatus = 1
                }else{
                   results[0][i].followerStatus = 0
                }

                }else{
                  results[0][i].followerStatus = 0

                }  

                if(folderData){
                          
                    var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                      
                    if(folderData1.length){

                       results[0][i].isSave = 1;  
                         
                    }else{

                       results[0][i].isSave = 0;
                    }
                } 
                
              
            }
            jsArr =results[0] ;   

           if(jsArr){

                for (i = 0 ; i < jsArr.length ; i++) {
                    jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                   
                  
                    if(jsArr[i].feedData.length){ 
                                    
                        for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                            if(jsArr[i].feedData[j].feedPost){ 
                             jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                            }
                            if(jsArr[i].feedData[j].videoThumb){ 
                             jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                            }
                        }
                                
                    }
                    
                    if(jsArr[i].userInfo[0]){
                         if(jsArr[i].userInfo[0].profileImage){ 
                            jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                          }
                   }else{
                     jsArr[i].userInfo[0] =[];
                   }
                }
            
            }


            res.json({status: "success",message: 'successfully',AllUserFeeds: jsArr,total:newdata});
            return;   
        } else {
            res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllUserFeeds:[] });
            return;
        }
      
    });
}/* Feed According user Get Api end*/


exports.userFeedTagSearch =  function(req,res,next){

    var search = req.body.search;
    sData = req.body;
    sData.peaopleData = [];
    sData.searchData = [];

    if(search!=''){

        var Value_match = {$regex:req.body.search,$options:'i'};
        tag.find({'tag':Value_match},{'_id':1}).exec(function(err, data) {
            if (data) {


                sData.searchData = data.map(function (rd) {
                    return rd._id;
                });

            }

            User.find({'userName':Value_match},{'_id':1},function(err,uData){
                if(uData.length){

                    sData.peaopleData = uData.map(function (rd) {
                       //  return string(rd._id);
                        return (rd._id);
                    });                
                }
                next();
            });
              
         });

      

    }else{

        next();

    }

}


exports.exploreAllFeeds = function(req,res,next){   
        
    baseUrl =  req.protocol + '://'+req.headers['host'];
    feedSearch ={};
    search = {};
    geoNearReadiusSearch = {};
    feedSearch['feedType'] = 'image';
    search['serviceTagId'] = {'$ne':[]};
    var isOutCall      = req.body.isOutCall; //user for in out call filter
    var categpryId      = req.body.serviceTagId;
    var artistServiceId = req.body.artistServiceId;
    var rating          = req.body.rating;

    radius              = parseFloat(req.body.radius);
    latitude            = req.body.latitude;
    longitude           = req.body.longitude;

    if (latitude == '' || longitude == '') {
        res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
        return;
    }

    feedSearch['userId'] ={'$nin':bdata};

    if(categpryId){
        var businessTypeId = categpryId.split(",");
        search['businessTypeServiceTagId'] = { $in: businessTypeId};

       /* var service = categpryId.split(",");
        search['serviceTagId'] = { $in: service};*/
    }

    if(artistServiceId){
         var service = artistServiceId.split(",");

        search['serviceTagId'] = { $in: service};
        /*var aServiceId = artistServiceId.split(",");
        search['artistServiceId'] = { $in: aServiceId};*/
    }
    if(isOutCall){
        var OutCall = isOutCall.split(",");
        search['oucallOrIncall'] = {$in:OutCall};
    }

    search['deleteStatus'] = 1;

    if(rating && rating!=0 && rating!='null' && rating!='0.0'){

       feedSearch['userInfo.ratingCount'] = {$gte:rating};
    }    
    feed.aggregate([
          {
                "$geoNear": {
                      "near": {
                             "type": "Point",
                             "coordinates": [parseFloat(longitude), parseFloat(latitude)]
                              },
                maxDistance: commonLib.milesToMeters(radius),
                "spherical": true,
                "distanceField": "dist.calculated",
                distanceMultiplier: 1/1609.344 // calculate distance in meters "item.name": { $eq: "ab" } }
                }
            },
            {
                $lookup: {

                    from: "users",
                    localField: "userId",
                    foreignField: "_id",
                    as: "userInfo"
                }
            },
            {
                $sort: {_id: -1}
            }, 
            {
                $match:search
            },          
            {   
                "$project":{
                    "_id":1,
                    "feedType":1,
                    "feedData":1,
                    "caption":1,
                    "city":1,
                    "country":1,
                    "location":1,
                    "likeCount":1,
                    "crd":1,
                    "serviceTagId":1,
                    "commentCount":1,
                    "peopleTag":1,
                    "serviceTag":1,
                    "latitude":1,
                    "longitude":1,
                    "userId":1,
                    "userInfo._id":1,
                    "userInfo.userName":1,
                    "userInfo.firstName":1,
                    "userInfo.lastName":1,
                    "userInfo.profileImage":1,
                    "userInfo.userType":1,
                    "userInfo.ratingCount":1,
                    "dist":1
                } 
            },
            {
                $match:feedSearch
            }
        ],function(err, dataLength){
            if(dataLength){
                newdata = dataLength.length; 
            }else{
                newdata = 0;
            }
            next();
        });

}

exports.exploreFinalFeed = function(req, res) {


        if (req.body.page) {

            page = ((Number(req.body.page)*Number(req.body.limit)) - Number(req.body.limit));

        } else {

            page=0;

        }
        if (req.body.limit) {

            limit = Number(req.body.limit);
        } else {
            
            limit=10;
        }

        async.parallel([

            function(callback) {
                var query = feed.aggregate([
                    {
                        "$geoNear": {
                              "near": {
                                     "type": "Point",
                                     "coordinates": [parseFloat(longitude), parseFloat(latitude)]
                                      },
                        maxDistance: commonLib.milesToMeters(radius),
                        "spherical": true,
                        "distanceField": "dist.calculated",
                        distanceMultiplier: 1/1609.344 // calculate distance in meters "item.name": { $eq: "ab" } }
                        }
                    },                                   
                    {
                        "$lookup": {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userInfo"
                        }
                    },                       
                   
                    {
                       $sort: {_id: -1, likeCount: -1}
                    },
                    {
                        $match:search
                    },
                    {
                        "$project": {
                            "_id":1,
                            "feedType":1,
                            "feedData":1,
                            "caption":1,
                            "city":1,
                            "country":1,
                            "location":1,
                            "likeCount":1,
                            "crd":1,
                            "feedImageRatio":1,
                            "commentCount":1,
                            "peopleTag":1,
                            "serviceTag":1,
                            "serviceTagId":1,
                            "latitude":1,
                            "longitude":1,
                            "deleteStatus":1,
                            "userId":1,
                            "userInfo._id":1,
                            "userInfo.userName":1,
                            "userInfo.firstName":1,
                            "userInfo.lastName":1,
                            "userInfo.profileImage":1,
                             "userInfo.userType":1,
                             "userInfo.ratingCount":1
                        }
                    },
                    {
                        $match:feedSearch 
                    }
                    ,
                    { $skip:page },
                    { $limit:limit }

                ]);
                query.exec(function(err, g) {

                    if (err) {
                        callback(err);
                    }
                    
                    callback(null, g);
                });
            },
            function(callback) {
                var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            },
            function(callback) {

                var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
                query.exec(function(err, flow) {
                    if (err) {
                        callback(err);
                    }
         
                    callback(null, flow);
                });
            },
            function(callback) {

                var previesDate = new Date();
                var curentDate = new Date();
                previesDate.setHours(previesDate.getHours() - 24);

                var query = likes.find({'crd':{'$gte':(previesDate),'$lte':(curentDate)},'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            } 

        ],
      
        //Compute all results
        function(err, results) {


       if (results[0]) {     
                  
           jsArr = [];
        for (var i = 0; i < results[0].length; i++) {

                var likeFeed = results[1].map(a => a.feedId);
                 var flowSt = results[2].map(a => a.userId);
               if(results[1].length>0){
                    var a = likeFeed.indexOf(results[0][i]._id);

                    if(a >-1){
                         results[0][i].isLike = 1
                    }else{
                         results[0][i].isLike = 0
                    }
               
            }else{
                    results[0][i].isLike = 0

            }
                    //
            if(results[2].length>0){
               var b = flowSt.indexOf(results[0][i].userId);

            if(b >-1){
                results[0][i].followerStatus = 1
            }else{
               results[0][i].followerStatus = 0
            }

            }else{
              results[0][i].followerStatus = 0

            }  
            // 

            if(folderData){
                      
                var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                  
                if(folderData1.length){

                   results[0][i].isSave = 1;  
                     
                }else{

                   results[0][i].isSave = 0;
                }
            } 
          
        }
        jsArr = results[0] ;   
        var newData12 = [];

           if(jsArr){

                for (i = 0 ; i < jsArr.length ; i++) {
                   
                   //change for new responce category replace by business type title = catagory demo
/*                   if(jsArr[i].serviceTag.length){ 
                       ln = jsArr[i].serviceTag[0].length;
                                    
                        for (l = 0 ; l < jsArr[i].serviceTag.length ; l++) {
                            
                            if(jsArr[i].serviceTag[l][0]){ 
                             
                             jsArr[i].serviceTag[l][0].tagDetails.tagId =jsArr[i].serviceTag[l][0].tagDetails.categoryId;
                             jsArr[i].serviceTag[l][0].tagDetails.title =jsArr[i].serviceTag[l][0].tagDetails.categoryName;
                             jsArr[i].serviceTag[l][0].tagDetails.categoryId =jsArr[i].serviceTag[l][0].tagDetails.businessTypeId;              
                             jsArr[i].serviceTag[l][0].tagDetails.categoryName =jsArr[i].serviceTag[l][0].tagDetails.businessTypeName; 
                           
                             }      
            
                        }
                                
                    }*/
                    //end
                    jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                   
                  
                    if(jsArr[i].feedData.length){ 
                                    
                        for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                            if(jsArr[i].feedData[j].feedPost){ 
                             jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                            }
                            if(jsArr[i].feedData[j].videoThumb){ 
                             jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                            }
                        }
                                
                    }
                    
                    if(jsArr[i].userInfo[0]){
                         if(jsArr[i].userInfo[0].profileImage){ 
                            jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                          }
                   }else{
                     jsArr[i].userInfo[0] =[];
                   }

                    // if(latitude && longitude){

                    //     lat = jsArr[i].latitude;
                    //     long = jsArr[i].longitude;

                    //     jsArr[i].distance = getDistanceFromLatLonInKm(lat,long,latitude,longitude);


                    //      if(radius>=jsArr[i].distance){

                    //         var picked = lodash.filter( results[3], { 'feedId': jsArr[i]._id} );
                    //         jsArr[i].topLike = picked.length;

                    //         newData12.push(jsArr[i]);
                           
                             
                    //     }

                    //     if(radius<jsArr[i].distance){


   if (latitude == '' || longitude == '') {

        res.json({status: "fail",message: 'No record found.',AllFeeds:[] });
        return;

   }

    feedSearch['userId'] ={'$nin':bdata};

                    //        newdata = Number(newdata)-Number(1);


                    //     }

                    // }else{

                    //     newData12.push(jsArr[i]);
                    // }
                    newData12.push(jsArr[i]);


                }

           }

           // newData12 = newData12.sort(function(a, b) {
           //    var a1 = a.topLike,
           //        b1 = b.topLike;
           //    if (a1 == b1) return 0;
           //    return a1 < b1 ? 1 : -1;
           //  });

           //  dataArrays = [], 
           //  dataList = []; 
           //  page = req.body.page;
           //  limit = limit;
           //  //split list into groups
           //  while (newData12.length > 0) {
           //      dataArrays.push(newData12.splice(0, Number(limit)));
           //  }
           //  //show list of students from group
           //  dataList = dataArrays[+page - 1];

            res.json({status: "success",message: 'successfully',AllFeeds: newData12,total:newdata});
            return;

        } else {

           res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
           return;
        }
      
    });
}

// Now we are using V2 functions for getting explore feeds
exports.exploreAllFeedsV2 = function(req,res,next){   
    baseUrl =  req.protocol + '://'+req.headers['host'];
    feedSearch ={};
    search = {};
    geoNearReadiusSearch = {};
    feedSearch['feedType'] = 'image';
    search['serviceTagId'] = {'$ne':[]};
    var isOutCall      = req.body.isOutCall; //user for in out call filter
    var businessId      = req.body.businessId;
    var categpryId      = req.body.categoryId;
    var artistServiceId = req.body.artistServiceId;
    var rating          = req.body.rating;

    radius              = parseFloat(req.body.radius);
    latitude            = req.body.latitude;
    longitude           = req.body.longitude;

    if(latitude.trim() == "" ||  longitude.trim() == ""){
        res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
        return;
    }

    feedSearch['userId'] ={'$nin':bdata};
    if(businessId){
        var businessTypeId = businessId.split(",");
        search['businessTypeServiceTagId'] = { $in: businessTypeId};

       /* var service = categpryId.split(",");
        search['serviceTagId'] = { $in: service};*/
    }
    if(categpryId){
        var categoryService = categpryId.split(",");
        search['serviceTagId'] = { $in: categoryService};
    }

    if(artistServiceId){
         var service = artistServiceId.split(",");

        search['artistServiceId'] = { $in: service};
        /*var aServiceId = artistServiceId.split(",");
        search['artistServiceId'] = { $in: aServiceId};*/
    }
    if(isOutCall){
        var OutCall = isOutCall.split(",");
        search['oucallOrIncall'] = {$in:OutCall};
    }

    search['deleteStatus'] = 1;

    if(rating && rating!=0 && rating!='null' && rating!='0.0'){

       feedSearch['userInfo.ratingCount'] = {$gte:parseFloat(rating)};
    }    
   
    aggregateArray = [
        {
            "$geoNear": {
                  "near": {
                         "type": "Point",
                         "coordinates": [parseFloat(longitude), parseFloat(latitude)]
                          },
            maxDistance: commonLib.milesToMeters(radius),
            "spherical": true,
            "distanceField": "dist.calculated",
            distanceMultiplier: 1/1609.344 // calculate distance in meters "item.name": { $eq: "ab" } }
            }
        },
                                         
        {
            $match:search
        },
      
        {
            "$lookup": {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
            }
        },
        { "$unwind": {
                path:"$serviceTag", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffServiseTagId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.staffId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$unwind":{
                path:"$serviceTag.tagDetails.artistId",
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "serviceTag.tagDetails.artistServiseTagId",
                foreignField: "_id",
                as: "arstistServiceTagDetail"
            }
        }, 
        {
            "$lookup": {
                from: "staffservices",
                localField: "serviceTag.tagDetails.staffServiseTagId",
                foreignField: "_id",
                as: "staffServiceTagDetail"
            }
        },  
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.staffId",
                foreignField: "_id",
                as: "staffDetail"
            }
        }, 
        {
            "$lookup": {
                from: "users",
                localField: "serviceTag.tagDetails.artistId",
                foreignField: "_id",
                as: "artistDetail"
            }
        }, 
        { "$unwind": {
                path:"$arstistServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffServiceTagDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$staffDetail", 
                preserveNullAndEmptyArrays:true
            }
        }, 
        { "$unwind": {
                path:"$artistDetail", 
                preserveNullAndEmptyArrays:true
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "arstistServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.serviceId"
            }
        },
        
        {
            "$lookup": {
                from: "subservices",
                localField: "arstistServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "arstistServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "services",
                localField: "staffServiceTagDetail.serviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.serviceId"
            }
        },
        {
            "$lookup": {
                from: "subservices",
                localField: "staffServiceTagDetail.subserviceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.subserviceId"
            }
        },
        {
            "$lookup": {
                from: "artistservices",
                localField: "staffServiceTagDetail.artistServiceId",
                foreignField: "_id",
                as: "staffServiceTagDetail.artistServiceName"
            }
        },
        {
            $match:feedSearch 
        },
        { "$group": {
                "_id":  "$_id",
                "feedData": {"$first": "$feedData"},
                "feedType": {"$first": "$feedType"},
                "caption": {"$first": "$caption"},
                "city": {"$first": "$city"},
                "country": {"$first": "$country"},
                "location": {"$first": "$location"},
                "likeCount": {"$first": "$likeCount"},
                "crd": {"$first": "$crd"},
                "feedImageRatio": {"$first": "$feedImageRatio"},
                "commentCount": {"$first": "$commentCount"},
                "peopleTag": {"$first": "$peopleTag"},
                "serviceTag": {"$first": "$serviceTag"},
                "serviceTagId": {"$first": "$serviceTagId"},
                "latitude": {"$first": "$latitude"},
                "longitude": {"$first": "$longitude"},
                "deleteStatus": {"$first": "$deleteStatus"},
                "userId": {"$first": "$userId"},
                "userInfo": {"$first": "$userInfo"},
                "serviceTag": {
                    "$push": {
                        tag:"$serviceTag", 
                        arstistServiceTagDetail: {
                            artistServiceID:"$arstistServiceTagDetail._id",
                            title:"$arstistServiceTagDetail.title",
                            description:"$arstistServiceTagDetail.description",
                            inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                            outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                            completionTime:"$arstistServiceTagDetail.completionTime",
                            buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                            categoryID:"$arstistServiceTagDetail.subserviceId._id",
                            categoryName:"$arstistServiceTagDetail.subserviceId.title"
                            
                        },
                        staffServiceTagDetail: {
                            staffServiceID:"$staffServiceTagDetail.artistServiceId",
                            title:"$staffServiceTagDetail.artistServiceName.title",
                            description:"$staffServiceTagDetail.artistServiceName.description",
                            inCallPrice:"$staffServiceTagDetail.inCallPrice",
                            outCallPrice:"$staffServiceTagDetail.outCallPrice",
                            completionTime:"$staffServiceTagDetail.completionTime",
                            buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                            buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                            categoryID:"$staffServiceTagDetail.subserviceId._id",
                            categoryName:"$staffServiceTagDetail.subserviceId.title"
                        },
                        staffDetail: {
                            firstName:"$staffDetail.firstName",
                            lastName:"$staffDetail.lastName"
                        },
                        artistDetail:{
                            firstName:"$artistDetail.firstName",
                            lastName:"$artistDetail.lastName"
                        }
                    }
                }
                
            }
        },
        {
            "$project": {
                "_id":1,
                "c":1,
                "feedData":1,
                "feedType":1,
                "caption":1,
                "city":1,
                "country":1,
                "location":1,
                "likeCount":1,
                "crd":1,
                "feedImageRatio":1,
                "commentCount":1,
                "peopleTag":1,
                "serviceTag":1,
                "serviceTagId":1,
                "latitude":1,
                "longitude":1,
                "deleteStatus":1,
                "userId":1,
                "userInfo._id":1,
                "userInfo.userName":1,
                "userInfo.firstName":1,
                "userInfo.lastName":1,
                "userInfo.profileImage":1,
                 "userInfo.userType":1,
                 "userInfo.ratingCount":1,
                 "staffDetail":1
            }
        }
    ];

    var query = feed.aggregate(aggregateArray);
    query.exec(function(err, data) {
        if (err) {
            console.log(err);
        }

        if(data){
                newdata = data.length; 
            }else{
                newdata = 0;
            }
            next();
        });

}

exports.exploreFinalFeedV2 = function(req, res) {
        if (req.body.page) {
            page = ((Number(req.body.page)*Number(req.body.limit)) - Number(req.body.limit));
        } else {
            page=0;
        }

        if (req.body.limit) {
            limit = Number(req.body.limit);
        } else {
            limit=10;
        }

        async.parallel([

            function(callback) {
                aggregateArray.push({"$sort":{ "likeCount": -1,_id: -1 }});
                aggregateArray.push({ $skip:page });
                aggregateArray.push({ $limit:limit });
                var query = feed.aggregate(aggregateArray);
                query.exec(function(err, g) {
                    if (err) {
                        callback(err);
                    }
          
                    callback(null, g);
                });
            },
            function(callback) {
                var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            },
            function(callback) {
                var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
                query.exec(function(err, flow) {
                    if (err) {
                        callback(err);
                    }
         
                    callback(null, flow);
                });
            },
            function(callback) {
                var previesDate = new Date();
                var curentDate = new Date();
                previesDate.setHours(previesDate.getHours() - 24);

                var query = likes.find({'crd':{'$gte':(previesDate),'$lte':(curentDate)},'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            } 

        ],
      
        //Compute all results
        function(err, results) {
       if (results[0]) {     
                  
           jsArr = [];
        for (var i = 0; i < results[0].length; i++) {

                var likeFeed = results[1].map(a => a.feedId);
                 var flowSt = results[2].map(a => a.userId);
               if(results[1].length>0){
                    var a = likeFeed.indexOf(results[0][i]._id);

                    if(a >-1){
                         results[0][i].isLike = 1
                    }else{
                         results[0][i].isLike = 0
                    }
               
            }else{
                    results[0][i].isLike = 0

            }
                    //
            if(results[2].length>0){
               var b = flowSt.indexOf(results[0][i].userId);

            if(b >-1){
                results[0][i].followerStatus = 1
            }else{
               results[0][i].followerStatus = 0
            }

            }else{
              results[0][i].followerStatus = 0

            }  
            // 

            if(folderData){
                      
                var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                  
                if(folderData1.length){

                   results[0][i].isSave = 1;  
                     
                }else{

                   results[0][i].isSave = 0;
                }
            } 
          
        }
        jsArr = results[0] ;   
        var newData12 = [];

           if(jsArr){

                for (i = 0 ; i < jsArr.length ; i++) {
                   
                   //change for new responce category replace by business type title = catagory demo
/*                   if(jsArr[i].serviceTag.length){ 
                       ln = jsArr[i].serviceTag[0].length;
                                    
                        for (l = 0 ; l < jsArr[i].serviceTag.length ; l++) {
                            
                            if(jsArr[i].serviceTag[l][0]){ 
                             
                             jsArr[i].serviceTag[l][0].tagDetails.tagId =jsArr[i].serviceTag[l][0].tagDetails.categoryId;
                             jsArr[i].serviceTag[l][0].tagDetails.title =jsArr[i].serviceTag[l][0].tagDetails.categoryName;
                             jsArr[i].serviceTag[l][0].tagDetails.categoryId =jsArr[i].serviceTag[l][0].tagDetails.businessTypeId;              
                             jsArr[i].serviceTag[l][0].tagDetails.categoryName =jsArr[i].serviceTag[l][0].tagDetails.businessTypeName; 
                           
                             }      
            
                        }
                                
                    }*/
                    //end
                    jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                   
                  
                    if(jsArr[i].feedData.length){ 
                                    
                        for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                            if(jsArr[i].feedData[j].feedPost){ 
                             jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                            }
                            if(jsArr[i].feedData[j].videoThumb){ 
                             jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                            }
                        }
                                
                    }
                    
                    if(jsArr[i].userInfo[0]){
                         if(jsArr[i].userInfo[0].profileImage){ 
                            jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                          }
                   }else{
                     jsArr[i].userInfo[0] =[];
                   }


                    // if(latitude && longitude){

                    //     lat = jsArr[i].latitude;
                    //     long = jsArr[i].longitude;

                    //     jsArr[i].distance = getDistanceFromLatLonInKm(lat,long,latitude,longitude);


                    //      if(radius>=jsArr[i].distance){

                    //         var picked = lodash.filter( results[3], { 'feedId': jsArr[i]._id} );
                    //         jsArr[i].topLike = picked.length;

                    //         newData12.push(jsArr[i]);
                           
                             
                    //     }

                    //     if(radius<jsArr[i].distance){

                    //        newdata = Number(newdata)-Number(1);

                    //     }

                    // }else{

                    //     newData12.push(jsArr[i]);
                    // }
                    newData12.push(jsArr[i]);


                }

           }

           // newData12 = newData12.sort(function(a, b) {
           //    var a1 = a.topLike,
           //        b1 = b.topLike;
           //    if (a1 == b1) return 0;
           //    return a1 < b1 ? 1 : -1;
           //  });

           //  dataArrays = [], 
           //  dataList = []; 
           //  page = req.body.page;
           //  limit = limit;
           //  //split list into groups
           //  while (newData12.length > 0) {
           //      dataArrays.push(newData12.splice(0, Number(limit)));
           //  }
           //  //show list of students from group
           //  dataList = dataArrays[+page - 1];

            res.json({status: "success",message: 'successfully',AllFeeds: newData12,total:newdata});
            return;

        } else {

           res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
           return;
        }
      
    });
}


// This function is responsible to get all buisnes type, categories and
// services which has atleast one feed post

exports.getBuisnesTypeAndCatBymiles = function(req, res){
    let feedSearch ={};
    let search = {};
    feedSearch['feedType'] = 'image';

    radius              = parseFloat(req.body.radius);
    latitude            = req.body.latitude;
    longitude           = req.body.longitude;

    var isOutCall      = req.body.isOutCall; //user for in out call filter

    if(isOutCall){
        
        var OutCall = isOutCall.split(",");
        search['oucallOrIncall'] = {$in:OutCall};
    }

    feedSearch['userId'] ={'$nin':bdata};
    search['deleteStatus'] = 1;
    search['serviceTagId'] = {'$ne':[]};

    if (latitude == '' || longitude == '') {
        res.json({status: "fail",message:'Koobi needs your current location.',AllFeeds:[] });
        return;
    }

    var query = feed.aggregate([

                    {
                        "$geoNear": {
                              "near": {
                                     "type": "Point",
                                     "coordinates": [parseFloat(longitude), parseFloat(latitude)]
                                      },
                        maxDistance: commonLib.milesToMeters(radius),
                        "spherical": true,
                        "distanceField": "dist.calculated",
                        distanceMultiplier: 1/1609.344 // calculate distance in meters "item.name": { $eq: "ab" } }
                        }
                    },                                  
                    {
                        $match:search
                    },
                    {
                        $match:feedSearch 
                    },
                    { "$unwind": {
                            path:"$serviceTag", 
                            preserveNullAndEmptyArrays:true
                        }
                    },
                    {
                        "$unwind":{
                            path:"$serviceTag.tagDetails",
                            preserveNullAndEmptyArrays:true
                        }
                    },
                    {
                        "$unwind":{
                            path:"$serviceTag.tagDetails.tagId",
                            preserveNullAndEmptyArrays:true
                        }
                    },
                    {
                        "$lookup": {
                            from: "artistservices",
                            localField: "serviceTag.tagDetails.tagId",
                            foreignField: "_id",
                            as: "serviceTagDetail"
                        }
                    }, 
                    { "$unwind": {
                            path:"$serviceTagDetail", 
                            preserveNullAndEmptyArrays:true
                        }
                    },
                    {
                        "$lookup": {
                            from: "services",
                            localField: "serviceTagDetail.serviceId",
                            foreignField: "_id",
                            as: "serviceTagDetail.serviceId"
                        }
                    },
                    {
                        "$lookup": {
                            from: "subservices",
                            localField: "serviceTagDetail.subserviceId",
                            foreignField: "_id",
                            as: "serviceTagDetail.subserviceId"
                        }
                    },
                    { "$group": {
                        "_id":  "$_id",
                        "serviceTagDetail": { "$push": "$serviceTagDetail" },
                    }}

                ]);
                query.exec(function(err, data) {
                    console.log('datadata',data);
                    var finalResult = [];
                    if (err) {
                        console.log(err);
                    }
                    
                    data.map(async function(Services){
                        let serviceData = Services.serviceTagDetail;
                        serviceData.map(async function(value){
                            if(value.length != 0){
                                if(value.serviceId[0] != undefined){
                                if(!lodash.find(finalResult, {_id: value.serviceId[0]._id})) {
                                  let buisnesType = value.serviceId[0];
                                  buisnesType['categories'] = [];

                                  let categories = value.subserviceId[0];
                                  categories['services'] = [];
                                  value['businessId'] = value.serviceId[0]._id;
                                  value['categoryId'] = value.subserviceId[0]._id;
                                  
                                  delete value.subserviceId;
                                  delete value.serviceId;
                                  categories['services'].push(value);
                                 
                                  buisnesType['categories'].push(categories);
                                  
                                  finalResult.push(buisnesType);
                                }else{
                                    let objIndex = lodash.findIndex(finalResult, {_id: value.serviceId[0]._id});
                                    let catObj = finalResult[objIndex]['categories'];

                                    if(!lodash.find(catObj, {_id: value.subserviceId[0]._id})) {
                                        let categories = value.subserviceId[0];
                                        categories['services'] = [];
                                        value['businessId'] = value.serviceId[0]._id;
                                        value['categoryId'] = value.subserviceId[0]._id;
                                        delete value.subserviceId;
                                        delete value.serviceId;
                                        categories['services'].push(value);
                                        catObj.push(categories);
                                    }else{

                                        let catObjIndex = lodash.findIndex(catObj, {_id: value.subserviceId[0]._id});
                                        let serviceObj = catObj[catObjIndex]['services'];

                                        if(!lodash.find(serviceObj, {_id: value._id})) {
                                            value['businessId'] = value.serviceId[0]._id;
                                            value['categoryId'] = value.subserviceId[0]._id;
                                            delete value.subserviceId;
                                            delete value.serviceId;
                                            serviceObj.push(value);
                                        }
                                    }
                                }
                            }
                                
                            }
                        });
                    });
                    res.json({status: "success",message: 'successfully',Allcagtegories: finalResult});
                    return;
                });
}

getDistanceFromLatLonInKm = function(lat1,lon1,lat2,lon2) {
    
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d / 1.6;

}


deg2rad = function(deg) {
    
    return deg * (Math.PI/180)

}


/*api for delete artist service*/
exports.deleteFeed = function(req, res){

    let id       = Number(req.body.id);
    let feedType = req.body.feedType;
    let userId = req.body.userId;
    if(id==''){
        
        res.json({'status': "fail","message": commonLang.REQ_ID_FIELD});
        return;    
    }
    if(feedType==''){
        
        res.json({'status': "fail","message": commonLang.REQ_FEED_TYPE});
        return;    
    }
    if(userId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_USER_ID_FIELD});
        return;    
    }

    User.findOne({'_id':userId}).exec(function(err, result) {


        if(feedType != 'text'){
            
            count = Number(result.postCount) - Number(1);
            User.updateOne({ _id: userId},{$set:{postCount: count}},function(err, docs) {});
        }

        feed.deleteMany({'_id':id},function(err, results) {

            likes.deleteMany({'feedId':id},function(err, results) {

                comment.deleteMany({'feedId':id},function(err, results) {

                    addNotification.deleteMany({'notifyId':id,'notifincationType':{$in:[7,9,10,11,16]}},function(err, results) {

                        res.json({status:'success',message: commonLang.SUCCESS_DELETE_FEED});
                        return;
                    });
                });
            });
        });

    });
        
    
}


exports.editFeed = function(req, res) {

    var crd = moment().format();
    if (fields.userId) {
        var userId = Number(fields.userId);

    } else {
        var userId = authData._id;
    }
    if (fields.city) {
        var city = fields.city;

    } else {
        var city = '';
    }
    if (fields.country) {
        var country = fields.country;

    } else {
        var country = '';

    }

   if(fields.feedId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_FEED_ID_FIELD});
        return;    
    }

    var tagData = fields.tagData ? fields.tagData :'';

    count = Number(authData.postCount) + 1;

    var baseUrl = req.protocol + '://' + req.headers['host'];
    jsArr = []
    thumbArr = []
    stor = [];
    oldTag = [];
    newTag = [];
    tagInfo = [];


    if (files.feed) {

        var imgArray = files.feed;
        for (var i = 0; i < imgArray.length; i++) {

            var newPath = './public/uploads/feeds/';

            var singleImg = imgArray[i];
            nmFeed = Date.now() + i;
            if (imgArray[i].headers['content-type'] == 'video/mp4') {
                nm = nmFeed + '.mp4';
                feedUrl = nm;
                thumb = nmFeed + '.jpg';

            } else {
                nm = Date.now() + i + '.jpg';
                feedUrl = nm;
                thumb = '';
            }

            newPath += nm;

            videopath = './public/uploads/feeds/' + nm;
            readAndWriteFile(singleImg, newPath);

            jsArr.push({
                feedPost: feedUrl,
                videoThumb: thumb,

            });

        }

    }
    if (files.videoThumb) {

        var imgArray1 = files.videoThumb;
        for (var i = 0; i < imgArray1.length; i++) {
            var newPath = './public/uploads/feeds/';
            var singleImg = imgArray1[i];
            nmFeed = Date.now() + i;
            nm = Date.now() + i + '.jpg';
            feedUrl = nm;
            newPath += nm;

            videopath = './public/uploads/feeds/' + nm;
            readAndWriteFile2(singleImg, newPath);
            thumbArr.push({
                videoThumb: feedUrl
            });

        }

    }
    if (thumbArr.length > 0) {
        for (var k = 0; k < jsArr.length; k++) {

            for (var j = 0; j < thumbArr.length; j++) {
                if (k == j) {
                    stor.push({
                        feedPost: jsArr[k].feedPost,
                        videoThumb: thumbArr[j].videoThumb

                    });
                }

            }


        }
    } else {
        for (var i = 0; i < jsArr.length; i++) {
            stor.push({
                feedPost: jsArr[i].feedPost,
                videoThumb: ''
            });
        }

    }
    if (fields.feed) {
        ff = fields.feed;
        feeds = [];
    } else {
        feeds = stor;
    }
    if (!tagId) {
        tagId = '';
    }
    var addNew = {
        userId: userId,
        feedType: fields.feedType,
        caption: fields.caption,
        city: city,
        country: country,
        location: fields.location,
        latitude: fields.latitude,
        longitude: fields.longitude,
        tagId: tagId,
        peopleTag: fields.peopleTag ? fields.peopleTag[0] ? JSON.parse(fields.peopleTag) : [] : [],
        peopleTagData: fields.peopleTagData ? fields.peopleTagData[0] ? JSON.parse(fields.peopleTagData) : [] : [],
        serviceTag: fields.serviceTag ? fields.serviceTag[0] ? JSON.parse(fields.serviceTag) : [] : [],
        serviceTagId: fields.businessTypeId ? fields.businessTypeId[0] ? fields.businessTypeId[0].split(",") : [] : [],
        artistServiceId: fields.artistServiceId ? fields.artistServiceId[0] ? fields.artistServiceId[0].split(",") : [] : [],
        crd: crd,
        upd: crd
    };

    if(fields.feed){

       addNew.feedData =  feeds;

    }
    

    if(JSON.parse(tagData)){

        t = JSON.parse(tagData);
        t.push(userId);
        var appUser = require("./user");  
        req.body.notifincationType = '16';
        req.body.notifyId   = autoId;
        req.body.notifyType = 'social'; 
        req.body.userId = userId; 
        folInfo.flUser = t;
        appUser.sendMultiple(req,res); 
    }

    feed.update({'_id':feedId},{$set:addNew}, function(err, result){

        
        if (err) {
            res.json({status: "fail", message: err});
            return;
        } else {


            followUnfollow.find({'userId':Number(userId),'status':1}).sort([['_id', 'ascending']]).exec(function(err, followData) {
                folInfo.flUser  = [];
                if(followData){

                     a = [];
                     a= followData.map(a => a.followerId);
                     a.push(Number(fields.userId));         
                     folInfo.flUser =a ;

                }            
                if(folInfo.flUser){
                   /*code for notification*/  
                     var appUser = require("./user");  
                       req.body.notifincationType = '7';
                       req.body.notifyId   = autoId;
                       req.body.notifyType = 'social'; 
                       req.body.userId = userId; 
                       appUser.sendMultiple(req,res); 
                                 
                    /*end notification code*/   
                } 

            }); 
            res.json({status: "success",message: commonLang.SUCCESS_UPLOAD_POST,feeds:[]});
       
        

        }

    });



}
//change code for new flow


exports.exploreAllFeedsNew = function(req,res,next){   
        
    baseUrl =  req.protocol + '://'+req.headers['host'];
    feedSearch ={};
    search = {};
    feedSearch['feedType'] = 'image';
    search['serviceTagId'] = {'$ne':[]};
    var categpryId      = req.body.serviceTagId;
    var artistServiceId = req.body.artistServiceId;
    var rating          = req.body.rating;
    radius              = req.body.radius;
    latitude            = req.body.latitude;
    longitude           = req.body.longitude;


    feedSearch['userId'] ={'$nin':bdata};

    if(categpryId){
        var service = categpryId.split(",");
        search['serviceTagId'] = { $in: service};
    }

    if(artistServiceId){
        var aServiceId = artistServiceId.split(",");
        search['artistServiceId'] = { $in: aServiceId};
    }

    search['deleteStatus'] = 1;

    if(rating && rating!=0 && rating!='null' && rating!='0.0'){

       feedSearch['userInfo.ratingCount'] = {$gte:rating};
    }    
    feed.aggregate([

          /*  {
                "$geoNear": {
                      "near": {
                             "type": "Point",
                             "coordinates": [parseFloat(latitude), parseFloat(longitude)]
                              },
                maxDistance: radius,
                "spherical": true,
                "distanceField": "distance",
                distanceMultiplier: 1/1609.344 // calculate distance in miles "item.name": { $eq: "ab" } }
                }
            },*/
            {
                $lookup: {

                    from: "users",
                    localField: "userId",
                    foreignField: "_id",
                    as: "userInfo"
                }
            },
            {
                $sort: {_id: -1}
            }, 
            {
                $match:search
            },          
            {   
                "$project":{
                    "_id":1,
                    "feedType":1,
                    "feedData":1,
                    "caption":1,
                    "city":1,
                    "country":1,
                    "location":1,
                    "likeCount":1,
                    "crd":1,
                    "serviceTagId":1,
                    "commentCount":1,
                    "peopleTag":1,
                    "serviceTag":1,
                    "latitude":1,
                    "longitude":1,
                    "userId":1,
                    "userInfo._id":1,
                    "userInfo.userName":1,
                    "userInfo.firstName":1,
                    "userInfo.lastName":1,
                    "userInfo.profileImage":1,
                    "userInfo.userType":1,
                    "userInfo.ratingCount":1,
                } 
            },
            {
                $match:feedSearch
            }
        ],function(err, dataLength){

            if(dataLength){

                newdata = dataLength.length; 



            }else{

                newdata = 0;
            }

            next();

        });

}

exports.exploreFinalFeedNew = function(req, res) {


        if (req.body.page) {

            page = Number(req.body.page)*Number(req.body.limit);

        } else {

            page=0;

        }
        if (req.body.limit) {

            limit = Number(req.body.limit);
        } else {
            
            limit=10;
        }
        
        async.parallel([

            function(callback) {
                var query = feed.aggregate([

                  /*  {
                        "$geoNear": {
                              "near": {
                                     "type": "Point",
                                     "coordinates": [parseFloat(latitude), parseFloat(longitude)]
                                      },
                        maxDistance: radius,
                        "spherical": true,
                        "distanceField": "distance",
                        distanceMultiplier: 1/1609.344 // calculate distance in miles "item.name": { $eq: "ab" } }
                        }
                    }, */                                   
                    {
                        "$lookup": {
                            from: "users",
                            localField: "userId",
                            foreignField: "_id",
                            as: "userInfo"
                        }
                    },                       
                   
                    {
                       $sort: {_id: -1}
                    },
                    {
                        $match:search
                    },
                    {
                        "$project": {
                            "_id":1,
                            "feedType":1,
                            "feedData":1,
                            "caption":1,
                            "city":1,
                            "country":1,
                            "location":1,
                            "likeCount":1,
                            "crd":1,
                            "feedImageRatio":1,
                            "commentCount":1,
                            "peopleTag":1,
                            "serviceTag":1,
                            "serviceTagId":1,
                            "latitude":1,
                            "longitude":1,
                            "deleteStatus":1,
                            "userId":1,
                            "userInfo._id":1,
                            "userInfo.userName":1,
                            "userInfo.firstName":1,
                            "userInfo.lastName":1,
                            "userInfo.profileImage":1,
                             "userInfo.userType":1,
                             "userInfo.ratingCount":1,

                        }
                    },
                    {
                        $match:feedSearch 
                    }/*,
                    { $skip:page },
                    { $limit:limit }*/

                ]);
                query.exec(function(err, g) {

                    if (err) {
                        callback(err);
                    }
                    
                    callback(null, g);
                });
            },


            function(callback) {
                var query = likes.find({'likeById':Number(req.body.userId),'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            },
            function(callback) {

                var query = followUnfollow.find({'followerId':req.body.userId,'status':1})
                query.exec(function(err, flow) {
                    if (err) {
                        callback(err);
                    }
         
                    callback(null, flow);
                });
            }, 
            function(callback) {

                var previesDate = new Date();
                var curentDate = new Date();
                previesDate.setHours(previesDate.getHours() - 24);

                var query = likes.find({'crd':{'$gte':(previesDate),'$lte':(curentDate)},'type':'feed','status':1},{'_id':1,'feedId':1,'status':1})
                query.exec(function(err, ser1) {
                    if (err) {
                        callback(err);
                    }
                   
                    callback(null, ser1);
                });
            } 

        ],
      
        //Compute all results
        function(err, results) {


       if (results[0]) {     
                  
           jsArr = [];
        for (var i = 0; i < results[0].length; i++) {

                var likeFeed = results[1].map(a => a.feedId);
                 var flowSt = results[2].map(a => a.userId);
               if(results[1].length>0){
                    var a = likeFeed.indexOf(results[0][i]._id);

                    if(a >-1){
                         results[0][i].isLike = 1
                    }else{
                         results[0][i].isLike = 0
                    }
               
            }else{
                    results[0][i].isLike = 0

            }
                    //
            if(results[2].length>0){
               var b = flowSt.indexOf(results[0][i].userId);

            if(b >-1){
                results[0][i].followerStatus = 1
            }else{
               results[0][i].followerStatus = 0
            }

            }else{
              results[0][i].followerStatus = 0

            }  
            // 

            if(folderData){
                      
                var folderData1 = lodash.filter(folderData, { 'feedId': results[0][i]._id} );
                  
                if(folderData1.length){

                   results[0][i].isSave = 1;  
                     
                }else{

                   results[0][i].isSave = 0;
                }
            } 
          
        }
        jsArr = results[0] ;   
        var newData12 = [];

           if(jsArr){

                for (i = 0 ; i < jsArr.length ; i++) {
                   
                   //change for new responce category replace by business type title = catagory 
                   if(jsArr[i].serviceTag.length){ 
                       ln = jsArr[i].serviceTag[0].length;
                                    
                        for (l = 0 ; l < jsArr[i].serviceTag.length ; l++) {
                            
                            if(jsArr[i].serviceTag[l][0]){ 
                             
                             jsArr[i].serviceTag[l][0].tagDetails.tagId =jsArr[i].serviceTag[l][0].tagDetails.categoryId;
                             jsArr[i].serviceTag[l][0].tagDetails.title =jsArr[i].serviceTag[l][0].tagDetails.categoryName;
                             jsArr[i].serviceTag[l][0].tagDetails.categoryId =jsArr[i].serviceTag[l][0].tagDetails.businessTypeId;              
                             jsArr[i].serviceTag[l][0].tagDetails.categoryName =jsArr[i].serviceTag[l][0].tagDetails.businessTypeName; 
                           
                             }      
            
                        }
                                
                    }
                    //end
                    jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();
                   
                  
                    if(jsArr[i].feedData.length){ 
                                    
                        for (j = 0 ; j < jsArr[i].feedData.length ; j++) {
                            if(jsArr[i].feedData[j].feedPost){ 
                             jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].feedPost;

                            }
                            if(jsArr[i].feedData[j].videoThumb){ 
                             jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR+ jsArr[i].feedData[j].videoThumb;

                            }
                        }
                                
                    }
                    
                    if(jsArr[i].userInfo[0]){
                         if(jsArr[i].userInfo[0].profileImage){ 
                            jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[i].userInfo[0].profileImage;

                          }
                   }else{
                     jsArr[i].userInfo[0] =[];
                   }

                    if(longitude && longitude){

                        lat = jsArr[i].latitude;
                        long = jsArr[i].longitude;

                        jsArr[i].distance = getDistanceFromLatLonInKm(lat,long,latitude,longitude);


                         if(radius>=jsArr[i].distance){

                            var picked = lodash.filter( results[3], { 'feedId': jsArr[i]._id} );
                            jsArr[i].topLike = picked.length;

                            newData12.push(jsArr[i]);
                           
                             
                        }

                        if(radius<jsArr[i].distance){

                           newdata = Number(newdata)-Number(1);

                        }

                    }else{

                        newData12.push(jsArr[i]);
                    }


                }

           }

           newData12 = newData12.sort(function(a, b) {
              var a1 = a.topLike,
                  b1 = b.topLike;
              if (a1 == b1) return 0;
              return a1 < b1 ? 1 : -1;
            });
            dataArrays = [], 
            dataList = []; 
            page = req.body.page;
            limit = limit;
            //split list into groups
            while (newData12.length > 0) {
                dataArrays.push(newData12.splice(0, Number(limit)));
            }
            //show list of students from group
            dataList = dataArrays[+page - 1];


            res.json({status: "success",message: 'successfully',AllFeeds: dataList,total:newdata});
            return;

        } else {

           res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND,AllFeeds:[] });
           return;
        }
      
    });
}
