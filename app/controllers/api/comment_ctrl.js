var comment     =   require('../../models/front/comment.js');
var feed        =   require('../../models/front/feed.js');
var likes       =   require('../../models/front/like.js');
var r_comment   =   require('../../models/front/replayComment_model.js');
var escapere    =   require('escape-regexp');
var formidable  =   require('formidable');//it user for get form or post data by http
var fs          =   require('fs');
var moment      =   require('moment');
var crd         =   moment().format();
var time        =  moment().toDate().getTime();
var lodash      =   require('lodash');
var addNotification  =   require('../../models/front/notification.js');


var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
var FileUpload      =   require('../../../lib/FileUpload');
const S3            =   require("../../../lib/aws-s3");
const commonJs      =   require("../../../lib/common");
var uniqid          = require('uniqid'); 
const fileUploadObj = new FileUpload();

exports.addComment = function(req,res){

    	var baseUrl =  req.protocol + '://'+req.headers['host'];
        var form = new formidable.IncomingForm();
        var crd = moment().format();
        form.parse(req, function(err, fields, files) {

            req.body    =   fields;
            var userId = req.body.userId;
            if(fields.type ==''){
                 res.json({status: 'fail',message: commonLang.REQ_COMMENT_TYPE});
                 return;
            }

            var addNew = {

                _id         :  1,
                feedId      :  req.body.feedId,
                postUserId  :  req.body.postUserId,
                commentById :  req.body.userId,
                comment     :  req.body.comment,
                gender      :  req.body.gender ? req.body.gender : 'male' ,
                age         :  req.body.age ? req.body.age : 20,
                city        :  req.body.city ? req.body.city : 'Indore',
                state       :  req.body.state ? req.body.state : 'mp',
                country     :  req.body.country ? req.body.country : 'India',
                type        :  req.body.type,
                crd         :  crd,
                upd         :  crd
            };

            if(files.comment){

/*                var oldpath = files.comment.path;
                var imageName = Date.now()+".jpg";
                var newpath = './public/uploads/commentImage/'+imageName;
                fs.rename(oldpath, newpath, function (err) {
                    if (err) throw err;
                });
                addNew.comment = imageName;*/

           // hear code for add image using s3 bucket
            var imageObj = files.comment;
       
            let ext =  imageObj.name.split('.').pop();
            let fileName = uniqid()+'.'+ext;
           
            let height = commonJs.calculateFeedImageThunmbHeight(imageObj.path);
            thumboptions = {
                resize: { height: height },
                thumbFolder:commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
            }
            
            fileUploadObj.uploadFile(imageObj, commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR,fileName,thumboptions);
            addNew.comment =  fileName;

            // end


            }
      

            comment.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

                if (result) {

                    addNew._id = result._id + 1;

                }

                comment(addNew).save(function(err, data) {

                    if (err) {

                        res.json({status: "fail",message: err});
                        return;

                    } else {

                        feed.findOne({'_id': addNew.feedId,'status': 1}, function(err, result) {

                            count = Number(result.commentCount) + 1;

                            feed.updateOne({_id: addNew.feedId}, { $set: {commentCount : count }},function(err, result) {

                                   /*code for notification*/   

                                    var typ = '9';
                                var sender     = userId;
                                var receiver   = addNew.postUserId; 
                                var notifyId   = addNew.feedId;
                                var notifyType = 'social'; 
                                if(sender!=receiver){
                                   notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 
                                }
                                /*end notification code*/  
                               addNew.commentCount  = count;
                              
                                if(addNew.type == 'image'){
                                    addNew.comment =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR+ addNew.comment; 

                                }  
                                res.json({status:"success",message:'ok',commentData:addNew});
                                return;

                            });
                        });
                        
                    }
                });
                
            });
        });
}


exports.commentList = function(req,res,next){

    baseUrl     =  req.protocol + '://'+req.headers['host'];
    Value_match =   escapere(req.body.search);
    comment.aggregate([ 
                    {$lookup: {from: "users",localField: "commentById",foreignField: "_id",as:"userInfo"}},
                    { "$unwind": "$userInfo" },
                     {
                        $match: {
                          'userInfo.userName':{$regex:Value_match,$options:'i'},
                          'feedId':Number(req.body.feedId),
                          
                          
                        }
                    },
                    {   
                        $project:{

                            commentById :1,
                            comment :1,
                            commentLikeCount :1,
                            crd :1,
                            userName :"$userInfo.userName",
                            firstName :"$userInfo.userName",
                            lastName :"$userInfo.userName",
                            profileImage :"$userInfo.profileImage"
                            
                        } 
                    }

        ],function(err,data1){

            if(data1){
        
                newdata = data1.length; 
        
            }else{
        
                newdata = 0;
        
            }
            next();
      
    });
    
}

exports.replayCommentList = function(req,res, next){

    rData = [];
    r_comment.aggregate([ { $sort : { _id : -1 } },
        {$lookup: {from: "users",localField: "commentById",foreignField: "_id",as:"userInfo"}},
        { "$unwind": "$userInfo" },
         {
            $match: {

                'feedId':Number(req.body.feedId)
              
            }
        },
        {   
            $project:{
                _id:1,
                commentById :1,
                comment :1,
                commentLikeCount :1,
                commentId :1,
                type :1,
                crd :1,
                userName :"$userInfo.userName",
                firstName :"$userInfo.userName",
                lastName :"$userInfo.userName",
                profileImage :"$userInfo.profileImage"
                
            } 
        }

    ],function(err,data){

        if(data){

            likes.find({'type':'replayLike','likeById':req.body.userId,'status':1}).exec(function(err,adata) { 
          
                
                for (i = 0 ; i < data.length ; i++) {

                    data[i].isLike = 0; 
                    if(adata.length!=0){
      
                        var like = lodash.filter(adata, { 'feedId': data[i]._id} );
                                                        
                        if(like.length){

                           data[i].isLike = 1;  
                             
                        }
                    } 
                    
                    data[i].timeElapsed = moment(data[i].crd).fromNow();

                    if(data[i].profileImage){ 
                        data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ data[i].profileImage;
                    }
                    if(data[i].type=='image'){ 
                        data[i].comment = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR+ data[i].comment;
                    }
                }
         
            });

            rData = data;

        }

        next();     
    });
    
}
exports.finalCommentList = function(req,res){

    if (req.body.page) {
          page = Number(req.body.page)*Number(req.body.limit);
    } else {
      page=0;
    }
    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }

    where = {

      'userInfo.userName':{$regex:Value_match,$options:'i'},
      'feedId':Number(req.body.feedId)
      
    };

    comment.aggregate([ 

                    {

                        $sort : { _id : -1 } },
                    
                    {
                        $lookup: {
                            from: "users",
                            localField: "commentById",
                            foreignField: "_id",
                            as:"userInfo"
                        }
                    },
                    
                    { "$unwind": "$userInfo" },
                    
                    {
                        $match: where
                    },
                    { $skip:page },
                    { $limit:limit },
                    {   
                        $project:{
                            _id:1,
                            commentById :1,
                            comment :1,
                            commentLikeCount :1,
                            feedId :1,
                            type :1,
                            crd :1,
                            userName :"$userInfo.userName",
                            firstName :"$userInfo.userName",
                            lastName :"$userInfo.userName",
                            profileImage :"$userInfo.profileImage"
                            
                        } 
                    }

         ],function(err,data){


              if (data.length==0){
                    res.json({status:"fail",message:'No record found'});
                }else{ 

                    if(data){
                       likes.find({'type':'comment','likeById':req.body.userId,'status':1}).exec(function(err,adata) { 
                                              
                        for (i = 0 ; i < data.length ; i++) {

                            data[i].isLike = 0; 
                            if(adata.length!=0){
              
                                var like = lodash.filter(adata, { 'feedId': data[i]._id} );
                                                                
                                if(like.length){

                                   data[i].isLike = 1;  
                                     
                                }
                            }    
                            data[i].timeElapsed = moment(data[i].crd).fromNow();

                            if(data[i].profileImage){ 
                                data[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ data[i].profileImage;
                            }
                            if(data[i].type=='image'){ 
                                data[i].comment = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR+ data[i].comment;
                            }
                            data[i].commentReplay = [];
                            if(rData){
                  
                                var rData1 = lodash.filter(rData, { 'commentId': data[i]._id} );

                                if(rData1.length){

                                   data[i].commentReplay = rData1;  
                                     
                                }
                            } 
        
                        }
                        res.json({status:"success",message:'ok',commentList:data,total:newdata});
                     
                       });



                    }
                    
                }
      
    });
    
}



exports.replayComment = function(req,res){

        var baseUrl =  req.protocol + '://'+req.headers['host'];
        var form = new formidable.IncomingForm();

        form.parse(req, function(err, fields, files) {

            req.body    =   fields;
            var userId = req.body.userId;
            if(fields.type == ''){
                
                res.json({status: 'fail',message: commonLang.REQ_COMMENT_TYPE});
                return;
            
            }

            var addNew = {

                _id         :  1,
                feedId      :  req.body.feedId,
                postUserId  :  req.body.postUserId,
                commentId  :   req.body.commentId,
                commentById :  req.body.userId,
                comment     :  req.body.comment,
                gender      :  req.body.gender ? req.body.gender : 'male' ,
                age         :  req.body.age ? req.body.age : 20,
                city        :  req.body.city ? req.body.city : 'Indore',
                state       :  req.body.state ? req.body.state : 'mp',
                country     :  req.body.country ? req.body.country : 'India',
                type        :  req.body.type,
                crd         :  crd,
                upd         :  crd
            };

            if(files.comment){

                var oldpath = files.comment.path;
                var imageName = Date.now()+".jpg";
                var newpath = './public/uploads/commentImage/'+imageName;
                fs.rename(oldpath, newpath, function (err) {
                    if (err) throw err;
                });
                addNew.comment = imageName;

            }
      

            r_comment.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

                if (result) {

                    addNew._id = result._id + 1;

                }

                r_comment(addNew).save(function(err, data) {

                    if (err) {

                        res.json({status: "fail",message: err});
                        return;
                    }
                       
                    res.json({status:"success",message:'ok',commentData:addNew});
                    return;
                       
                 
                });
                
            });
        });
}


/*userId = comment userId likeById login userId*/
exports.commentLike = function(req,res){

    var userId = req.body.userId;
    var likeById = req.body.likeById;
    var commentId = Number(req.body.commentId);
    var type = (req.body.type) ? req.body.type : 'comment';

    var q = (type=="comment") ? comment.findOne({'_id':commentId}) : r_comment.findOne({'_id':commentId});

    if(req.body){
        var addNew = {
            _id      :  1,
            feedId   :  commentId,
            userId   :  req.body.userId,
            likeById :  req.body.likeById,
            gender   :  req.body.gender ? req.body.gender : 'male',
            age      :  req.body.age ? req.body.age : 20,
            city     :  req.body.city ? req.body.city :'Indore',
            state    :  req.body.state ? req.body.state :'mp',
            country  :  req.body.country ? req.body.country : 'Indore',
            type     :  type            
        };

    }else{
        res.json({status: 'fail',message: commonLang.REQ_COMMENT_ID});
        return;
    }

    likes.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err, result1) {

        if (result1) {
            addNew._id = result1._id + 1;
        }
        
        likes.findOne({'feedId':commentId,'likeById':req.body.likeById,'type':type}, function(err, data) {
        
            if(data){


                q.exec(function(err, result) {

                    if(data.status==1){

                        setValue = {'status':0}
                        if(result.commentLikeCount==0){
                            count =0;
                        }else{
                            count = Number(result.commentLikeCount)-1;
                        }

                    }else{
                        setValue = {'status':1}
                        count = Number(result.commentLikeCount)+1;
                    }

                    likes.updateOne({ _id:data._id },{ $set:setValue},function(err, result) { });
                    if(type=="comment"){
                        comment.updateOne({ _id:commentId },{ $set:{commentLikeCount:count}},function(err, result) {});
                    }else{
                        r_comment.updateOne({ _id:commentId },{ $set:{commentLikeCount:count}},function(err, result) {});

                    }
                    res.json({status: "success",message: 'ok'});
                    return;
                });

            }else{

                likes(addNew).save(function(err, data) {

                    if (err) {

                        res.json({status: "fail",message:err});
                        return;

                    } else {

                        q.exec(function(err, result) {

                            count = Number(result.commentLikeCount)+1;
                            if(type=="comment"){
                                comment.updateOne({ _id:commentId },{ $set:{commentLikeCount:count}},function(err, result) {});
                            }else{
                                r_comment.updateOne({ _id:commentId },{ $set:{commentLikeCount:count}},function(err, result) {});

                            } /*code for notification (feed user and comment user) */
                            var typ = '11';
                            var sender     =likeById;
                            var notifyId   = result.feedId;
                            var notifyTyp = 'social';

                            if(result.postUserId != result.commentById && likeById != result.commentById){

                                if(sender!= result.postUserId){
                                    notify.notificationUser(sender,result.postUserId,typ,notifyId,notifyTyp);
                                }
                                if(sender!=result.commentById){
                                     notify.notificationUser(sender,result.commentById,typ,notifyId,notifyTyp); 

                                } 

                            }
                        /*end code*/
                         
                        });

                        res.json({status: "success",message: 'ok'});
                        return;
                    }
                });
            }
       });

    }); 
}  

/*api for delete artist service*/
exports.deleteComment = function(req, res){

    let id       = Number(req.body.id);
    let feedId   = Number(req.body.feedId);
    if(id==''){
        
        res.json({'status': "fail","message": commonLang.REQ_ID_FIELD});
        return;    
    }

    if(feedId==''){
        
        res.json({'status': "fail","message": commonLang.REQ_FEED_ID_FIELD});
        return;    
    }

    feed.findOne({_id:feedId}).exec(function(err, result) {

        count = Number(result.commentCount) - 1;

        feed.updateOne({_id: feedId}, { $set: {commentCount : count }},function(err, result) {

            comment.deleteMany({'_id':id},function(err, results) {

                likes.deleteMany({'feedId':id,'type':'comment'},function(err, results) {
                    
                    addNotification.deleteMany({'notifyId':id,'notifincationType':{$in:[9,11]}},function(err, results) {

                        res.json({status:'success',message: commonLang.SUCCESS_DELETE_COMMENT});
                        return;
                    });
                });
            });   
        }); 
    });
}

exports.editComment = function(req,res){


    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var form = new formidable.IncomingForm();
    var crd = moment().format();
    form.parse(req, function(err, fields, files) {

        req.body    =   fields;
        var userId = req.body.userId;
        let id       = req.body.id;
        
        if(id==''){
            
            res.json({'status': "fail","message": commonLang.REQ_ID_FIELD});
            return;    
        }
        if(fields.type ==''){
             res.json({status: 'fail',message: commonLang.REQ_COMMENT_TYPE});
             return;
        }

        var addNew = {
            feedId      :  req.body.feedId,
            postUserId  :  req.body.postUserId,
            commentById :  req.body.userId,
            comment     :  req.body.comment,
            gender      :  req.body.gender ? req.body.gender : 'male' ,
            age         :  req.body.age ? req.body.age : 20,
            city        :  req.body.city ? req.body.city : 'Indore',
            state       :  req.body.state ? req.body.state : 'mp',
            country     :  req.body.country ? req.body.country : 'India',
            type        :  req.body.type,
            crd         :  crd,
            upd         :  crd
        };

        if(files.comment){

            var oldpath = files.comment.path;
            var imageName = Date.now()+".jpg";
            var newpath = './public/uploads/commentImage/'+imageName;
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
            });
            addNew.comment = imageName;

        }
        comment.updateOne({ _id: Number(id)},{$set:addNew},function(err, docs) {

            if (err) {

                res.json({status: "fail",message: err});
                return;

            } else {

                if(addNew.type == 'image'){

                    addNew.comment =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.COMMENT_MEDIA_UPLOAD_FOLDEFR+ addNew.comment; 

                }  

                res.json({status:"success",message: commonLang.SUCCESS_UPDATE_COMMENT,commentData:addNew});
                return;
                
            }

        });
            
    });
    
}

