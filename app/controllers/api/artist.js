var constanPath = require('../../../config/envConstants.js');
var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var businessHour        =   require('../../models/front/businesshours.js');
var artistservices      =   require('../../models/front/artistService.js');
var artistMainService   =   require('../../models/front/artistMainService.js');
var artistSubService    =   require('../../models/front/artistSubService.js');
var artistCertificate   =   require('../../models/front/artistCertificate.js');
var feed                =   require('../../models/front/feed.js');
var followUnfollow      =   require('../../models/front/followersFollowing.js');
var likes               =   require('../../models/front/like.js');
var bankDetail          =   require('../../models/front/bankDetail_model.js');
var formidable          =   require('formidable');//it user for get form or post data by http
var fs                  =   require('fs');
var dateFormat          =   require('dateformat');
var multiparty          =   require('multiparty');
var stripe             =    require('stripe')(constanPath.STRIPE_KEY);

var async               =   require('async');
var bookingService      =   require('../../models/front/bookingService.js');
var booking             =   require('../../models/front/booking.js');
var staff               =   require('../../models/front/staff_model.js');
var staffService        =   require('../../models/front/staffService.js');
var validUrl            =   require('valid-url');
var escapere            =   require('escape-regexp');
var lodash              =   require('lodash');
var notify              =   require('../../../lib/notification.js');
var addNotification     =   require('../../models/front/notification.js');
var certificateTag      =   require('../../models/front/certificateTag_model.js');
var client              =   require('../../models/front/client_model.js');
var ratingReview        =   require('../../models/front/ratingReview_model');

var commonConstants =   require('../../../config/commanConstants');
var StripeLib =       require('../../../lib/StripeLib');


exports.artistMainServiceCount = function(req, res,next) {

  artistMainService.find({'artistId':Number(authData._id),'deleteStatus':1},function(err, data) {
      bizCount = data;
      next();
  });
}

exports.artistInfo = function(req, res) {

	var baseUrl =  req.protocol + '://'+req.headers['host'];

    User.aggregate([
        { 
            $match : { 

                _id :Number(authData._id)

            } 
        },
        {   "$project": {     
            "address":1, 
            "address2":1, 
            "city":1, 
            "state": 1,   
            "country": 1,
            "country": 1,
            "businesspostalCode": 1,
            "bankStatus": 1,   
            "payOption": 1,   
            "bio": 1,   
            "serviceType": 1,  
            "radius": 1,  
            "outCallpreprationTime": 1,  
            "inCallpreprationTime": 1,
            "longitude": 1,  
            "latitude": 1,  
            "location": 1,    
            "bookingSetting": 1    
        }
        }, 
        { 
            "$lookup": {     
                "from": "busineshours",     
                "localField": "_id",     
                "foreignField":"artistId",     
                "as": "businessHour"   
            }
        },
        
        { 
           "$lookup": {     
                "from": "artistservices",     
                "localField": "_id",     
                "foreignField":"artistId",     
                "as": "artistService"   
            }
        },
        {
            $lookup:{

                    from: "artistmainservices", 
                    localField: "_id", 
                    foreignField: "artistId",
                    as: "bizCount"
            },
        },
       
        ],function(err,data){

            ratingReview.findOne({'artistId':Number(authData._id)}).count().exec(function(err, rCount){            


                data[0].bizCount  =  (bizCount.length) ? bizCount.length : 0; 
                data[0].reviewCount = rCount;
                res.json({status: "success",message: 'successfully',artistRecord:data});
            });
        });
}


exports.businessHours = function(req, res) {
  timeSl = JSON.parse(req.body.businessHour);
  jsArr = [];
  sdata = [];
  sfirst = [];

  console.log("hour are api calling")

  businessHour.findOne({'artistId':authData._id},function(err, data) {
    //this code is use for add  business as staff 
      staff.findOne({'businessId':authData._id,'artistId':authData._id}).exec(function(err, checkStaff) {
       if(checkStaff && checkStaff.staffHours.length == 0){
           console.log("update first time ")
            for (var i = 0; i < timeSl.length; i++) {
                sfirst.push({
                    day: Number(timeSl[i].day),
                    startTime: timeSl[i].startTime,
                    endTime: timeSl[i].endTime
                });
                
            }
            staff.updateMany({'businessId':authData._id, 'artistId':authData._id},{$set: {'staffHours' : sfirst}}).exec();
        }
        if(!checkStaff){
          
            staff.find().sort([['_id', 'descending']]).limit(1).exec(function(err, staffInfo) {
                var stfId = 1;
                if (staffInfo.length > 0) {
                    stfId = staffInfo[0]._id + 1;
                }

                staffInfo =[];
                staffInfo = {"userName":authData.userName,"profileImage":authData.profileImage }
                var addNew = {
                    _id: stfId,
                    businessId: authData._id,
                    artistId: authData._id,
                    job: 'Expert',
                    holiday:7,
                    salaries:1,
                    staffServiceId:[],
                    staffInfo:staffInfo,
                    staffHours: timeSl,
                    bookingType:authData.serviceType,
                    status: 1
                                             
                };
                 staff(addNew).save(function(err,add) {}); 
            })


        }

      })
        
    // end business staff code

    if(data){

        console.log("foundhour are",jsArr)
        businessHour.deleteMany({'artistId':authData._id}, function(err, results){

           staff.find({'businessId':authData._id}).exec(function(err, staffData) {


                businessHour.find().sort([ ['_id', 'descending']]).limit(1).exec(function(err, userdata) {
                    var autoId = 1;

                    if (userdata.length > 0) {
                        autoId = userdata[0]._id + 1;

                    }

                    for (var i = 0; i < timeSl.length; i++) {
                        inc = autoId + i;

                        jsArr.push({
                            _id: inc,
                            day: Number(timeSl[i].day),
                            artistId: Number(authData._id),
                            startTime: timeSl[i].startTime,
                            endTime: timeSl[i].endTime,
                            fullStartTime: notify.timeConvert(timeSl[i].startTime),
                            fullEndTime: notify.timeConvert(timeSl[i].endTime),
                            status: 1
                        });
                        sdata.push({
                            day: Number(timeSl[i].day),
                            startTime: timeSl[i].startTime,
                            endTime: timeSl[i].endTime
                        });

                    }

                    staff.updateMany({'businessId':authData._id},{$set: {'staffHours' : sdata}}, function(err, docs){
                        
                        businessHour.insertMany(jsArr);
                        res.json({'status': "success","message": jsArr,'staffCount':staffData.length});
                        return;

                     });


                    
                });
            });

        });

    }else{

        
        console.log("not foundhour are",jsArr)
           businessHour.find().sort([['_id', 'descending']]).limit(1).exec(function(err, userdata) {
                var autoId = 1;

                if (userdata.length > 0) {
                    autoId = userdata[0]._id + 1;

                }
                for (var i = 0; i < timeSl.length; i++) {
                    inc = autoId + i;

                    jsArr.push({
                        _id: inc,
                        day: Number(timeSl[i].day),
                        artistId: Number(authData._id),
                        startTime: timeSl[i].startTime,
                        endTime: timeSl[i].endTime,
                        fullStartTime: notify.timeConvert(timeSl[i].startTime),
                        fullEndTime: notify.timeConvert(timeSl[i].endTime),
                        status: 1
                    });

                }
                console.log("hour are",jsArr)
                businessHour.insertMany(jsArr);
                res.json({'status': "success","message": jsArr,'staffCount':0});
                return;
            });
    }

  });


}


exports.skipPage = function(req,res){
    if(req.body.artistId){
      User.updateOne({_id:Number(req.body.artistId)},{$set: {isDocument:3}},function(err, docs) {
       res.json({status:"success",message:'ok'});
      return;
      });
   }else{
     res.json({status:"fail",message:commonLang.REQ_ARTIST_ID});
      return;
   }

}


exports.stripeaddAccount = function(req, res) {

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var country = req.body.country;
    var currency = req.body.currency;
    var sort_code = req.body.routingNumber;
    var accountNo = req.body.accountNo;
    var accountHolderType = 'individual';
    var userId = req.body.userId ? Number(req.body.userId) : authData._id;
    var dob = req.body.dob;
    var address = req.body.address;
    var city = req.body.city;
    var postalCode = req.body.postalCode;

    if(dob ==''){
         res.json({status: 'fail',message:commonLang.REQ_DOB});
         return;
    }
     if(address == ''){
         res.json({status: 'fail',message:commonLang.REQ_ADDRESS});
         return;
    }
     if(city == ''){
         res.json({status: 'fail',message:commonLang.REQ_CITY});
         return;
    }
    if(postalCode == ''){
         res.json({status: 'fail',message:commonLang.REQ_POSTAL});
         return;
    }

    var dobData = dob.split("-");
   
    /*country = 'US';
    currency = 'usd';
    holderName = 'Elijah Wilson';
    accountHolderType = 'individual';
    routingNumber = '110000000';
    accountNo = '000123456789';
    postal_code = '90046';
    ssnLast = '0000';*/

    stripe.accounts.create({
          country :  "GB",
          type : 'custom',
          external_account: {
            object:'bank_account',
            country: country,
            currency: currency,
            routing_number: sort_code,
            account_number: accountNo
          },
          individual:{
            dob: {
              day: dobData[0],
              month: dobData[1],
              year: dobData[2]
            },
            address: {
             line1: address,
              city: city,
              postal_code:postalCode
            },
            first_name :firstName,
            last_name : lastName,
           

          },
           business_type : accountHolderType,
          tos_acceptance:{

            date:Math.floor(Date.now() /1000),
            ip: req.connection.remoteAddress

           },
        }, function(err, token) {

            if(err){

                switch (err.type) {
                      case 'StripeCardError':

                      res.json({'status':'fail','message': err.message});
                        // A declined card error
                        // => e.g. "Your card's expiration year is invalid."
                        break;
                      case 'RateLimitError':

                      res.json({'status':'fail','message': err.message});
                        // Too many requests made to the API too quickly
                        break;
                      case 'StripeInvalidRequestError':

                      res.json({'status':'fail','message': err.message});

                        // Invalid parameters were supplied to Stripe's API
                        break;
                      case 'StripeAPIError':

                      res.json({'status':'fail','message': err.message});

                        // An error occurred internally with Stripe's API
                        break;
                      case 'StripeConnectionError':

                      res.json({'status':'fail','message': err.message});
                        // Some kind of error occurred during the HTTPS communication
                        break;
                      case 'StripeAuthenticationError':

                      res.json({'status':'fail','message': err.message});
                        // You probably used an incorrect API key
                        break;
                      default:

                      res.json({'status':'fail','message': err.message});
                        // Handle any other types of unexpected errors
                        break;
                    }
                }
                    if(token){
                        autoId = 1;

                        bankDetail.findOne({'artistId':userId},function(err, data) {

                            if(data){

                                bankDetail.updateOne({artistId:userId},{$set:{accountId:token.id,accountNo:accountNo,routingNumber:sort_code,firstName:firstName,lastName:lastName}},function(err, docs) {});    
                                User.updateOne({_id:userId},{$set:{isDocument: 3,bankStatus:1}},function(err, docs) {});    
                                res.json({status: "success",message: 'successfully'});
                                return;

                            }else{

                                bankDetail.find().sort([['_id', 'descending']]).limit(1).exec(function(err, result) {

                                 var addNew = new bankDetail({
                                      artistId:userId,
                                      accountId: token.id,
                                      accountNo:accountNo,
                                      firstName:firstName,
                                      lastName:lastName,
                                      routingNumber:sort_code                                                   
                                  }); 

                                  if (result.length > 0) {
                                      addNew._id = result[0]._id + 1;
                                  }


                                  
                                 bankDetail(addNew).save(function(err, data) {
                                    if (err) {
                                        res.json({status: "fail",message:commonLang.SEND_ALL_REQ_INFO });
                                        return;
                                    } else {

                                      User.updateOne({_id:userId},{$set:{isDocument: 3,bankStatus:1}},function(err, docs) {});    
                                      res.json({status: "success",message: 'successfully'});
                                      return;
                                    }

                                });             

                                });
                            }
                        });
                        //res.json({'status':'success','message': token.id});

                        
                    }
    
    });


}


exports.deleteRecord = function(req,res){

    var comment = require('../../models/front/comment.js');
    var story = require('../../models/front/myStory.js');
    if(req.query.contactNo){

        User.findOne({'contactNo':req.query.contactNo},function(err, data) {

            if(data){

                businessHour.deleteMany({'artistId':data._id}, function(err, results){ });
                artistservices.deleteMany({'artistId':data._id}, function(err, results){ });
                artistCertificate.deleteMany({'artistId':data._id}, function(err, results){ });
                bankDetail.deleteMany({'artistId':data._id}, function(err, results){ });
                feed.deleteMany({'userId':data._id}, function(err, results){ });
                User.deleteMany({'_id':data._id}, function(err, results){ });
                booking.deleteMany({'userId':data._id}, function(err, results){ });
                booking.deleteMany({'artistId':data._id}, function(err, results){ });
                bookingService.deleteMany({'userId':data._id}, function(err, results){ });
                bookingService.deleteMany({'artistId':data._id}, function(err, results){ });
                //
                 artistMainService.deleteMany({'artistId':data._id}, function(err, results){ });
                 artistSubService.deleteMany({'artistId':data._id}, function(err, results){ });
                 comment.deleteMany({'postUserId':data._id}, function(err, results){ });
                 comment.deleteMany({'commentById':data._id}, function(err, results){ });

                 followUnfollow.deleteMany({'followerId':data._id}, function(err, results){ });
                 followUnfollow.deleteMany({'userId':data._id}, function(err, results){ });

                 likes.deleteMany({'userId':data._id}, function(err, results){ });
                 likes.deleteMany({'likeById':data._id}, function(err, results){ });

                 story.deleteMany({'userId':data._id}, function(err, results){ });

                 staff.deleteMany({'businessId':data._id}, function(err, results){ });
                 staffService.deleteMany({'artistId':data._id}, function(err, results){ });

                res.json({status: "success",message:commonLang.SUCCESS_DELETE});

            }else{

                res.json({status: "success",message:commonLang.NOT_EXIST_CONTACT});

            }
        });

    }else{

        res.json({status: "success",message:commonLang.REQ_CONTACT });
    }

}


exports.getFeedFaverite = function(req,res,next){

    favoriteType = req.body.favoriteType;

    if(favoriteType){

        artistFavorite.find({'userId':Number(req.body.loginUserId)},function(err,favData){
        
            if(favData.length){

                favoriteType = favData.map(function (rd) {
                    return rd.artistId;
                });                
            }
            next();
        });
    }else{

        next();
    }


} 


exports.allArtist =function(req,res,next){
  var Value_match = escapere(req.body.search);
  var baseUrl =  req.protocol + '://'+req.headers['host'];
  staff.find({'businessId':req.body.artistId},{'artistId':1},function(err,data){
    jsArr =[];
    if(data.length>0){

        for (d = 0 ; d < data.length ; d++) {
            jsArr.push(Number(data[d].artistId));
        }
    }
    jsArr.push(Number(req.body.artistId));

    User.find({'_id':{'$nin':jsArr},'businessType':{$ne:'business'},'isDocument':3,
     $or: [{ "userName": {$regex:req.body.search,$options:'i'}}, {"email": {$regex:req.body.search,$options:'i'}
                    }]},{'_id':1,'userName':1,'profileImage':1}).sort([['_id', 'ascending']]).exec(function(err, artistData) {
        if(artistData){
            nextDataTotal = artistData.length;
            next();

        }
      
     });

   });

}


exports.finalAllArtist =function(req,res){
  var Value_match = escapere(req.body.search);
  var baseUrl =  req.protocol + '://'+req.headers['host'];
    if (req.body.page) {
      page = Number(req.body.page)*Number(req.body.limit);
    } else {
        page=0;
    }
    if (req.body.limit) {
    limit = Number(req.body.limit);
    } else {
        limit=10;
    }
  staff.find({'businessId':req.body.artistId},{'artistId':1},function(err,data){
    jsArr =[];
    if(data.length>0){

        for (d = 0 ; d < data.length ; d++) {
            jsArr.push(Number(data[d].artistId));
        }

    }
    jsArr.push(Number(req.body.artistId));

    User.find({'_id':{'$nin':jsArr},'businessType':{$ne:'business'},'isDocument':3,
     $or: [{ "userName": {$regex:req.body.search,$options:'i'}}, {"email": {$regex:req.body.search,$options:'i'}
                    }]},{'_id':1,'userName':1,'profileImage':1}).sort([['_id', 'ascending']]).skip(page).limit(limit).exec(function(err, artistData) {
        if(artistData){
            for (i = 0 ; i < artistData.length ; i++) {
                if(artistData[i].profileImage){
                    if(!validUrl.isUri(artistData[i].profileImage)){
                         //artistData[i].profileImage = baseUrl+"/uploads/profile/"+artistData[i].profileImage;
                       artistData[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+artistData[i].profileImage;
      
                    } 
                 
                }
            }
            res.json({status: "success",message: 'ok',totalCount:nextDataTotal,artistList: artistData});
        }
      
     });

   });

}

exports.deleteCompany = function(req,res){
    if(req.body.businessId==''){
        res.json({status:'fail',message:commonLang.REQ_BUSINESS_ID});
        return;
    
    }
    if(req.body.staffId==''){
        res.json({status:'fail',message:commonLang.REQ_STAFF_ID});
        return;
     
    }
     search ={};
      
      search['artistId']=Number(req.body.businessId);
      search['staff']=Number(req.body.staffId);
      search['bookingStatus']=1;

      bookingService.find(search).count().exec(function(err,count){
        if(count==0){
            staff.deleteOne({'businessId':req.body.businessId,'artistId':req.body.staffId}, function(err, results){
              if(err) throw err;
              staffService.deleteMany({'businessId':req.body.businessId,'artistId':req.body.staffId}, function(err, results){ });
              res.json({status:"success",message:commonLang.SUCCESS_REMOVE_COMPANY });
            });
          }else{
            res.json({status:"fail",message:commonLang.BOOKED_COMPANY_NOT_REMOVE});
          }
      });
} 


exports.companyInfo = function(req,res,next){
  var baseUrl =  req.protocol + '://'+req.headers['host'];
  if(req.body.artistId ==''){
    res.json({'status': "fail","message":commonLang.REQ_ARTIST_ID});
    return;

  }
  search = {};
  search['artistId']=Number(req.body.artistId);
  
  staff.aggregate( [
      {  
        $lookup:{
            from: "staffservices", 
            localField: "_id", 
            foreignField: "staffId",
            as: "staffSer"
        }
      },
      
      {  
        $lookup:{
            from: "users", 
            localField: "businessId", 
            foreignField: "_id",
            as: "userInfo"
        }
      },
      { $lookup:{

            from: 'staffbussinesstypes',
            localField: '_id',
            foreignField: 'staffId',
            as: 'businessType'
        }
      }, 
     { $match :search },
     { 
      "$project": {
          "_id": 1,
          "artistId": 1,
          "businessId": 1,
          "job": 1,
          "mediaAccess": 1,
          "holiday": 1,
          "staffHours": 1,
          "status": 1,
          "salaries": 1,
          "message": 1,
          "businessType.serviceName": 1,
          "staffService": "$staffSer",
          "businessName": { "$arrayElemAt": [ "$userInfo.businessName",0] },
         "userName": { "$arrayElemAt": [ "$userInfo.userName",0] },
         "profileImage": { "$arrayElemAt": [ "$userInfo.profileImage",0] },
         "address": { "$arrayElemAt": [ "$userInfo.address",0] }

       }
     },

     
    ],function(err,company){
          if(company){
            for (i = 0 ; i < company.length ; i++) {
                if(company[i].profileImage){
                    if(!validUrl.isUri(company[i].profileImage)){
                        //company[i].profileImage = baseUrl+"/uploads/profile/"+company[i].profileImage;
                        company[i].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+company[i].profileImage;

                       
                    } 
                 
                }

                staffservices = company[i].staffService;
                for (j = 0 ; j < staffservices.length ; j++) {

                    bookingType = "Both";
                    if(parseFloat(staffservices[j].inCallPrice) != 0.00 && parseFloat(staffservices[j].outCallPrice) != 0.00){
                        bookingType = "Both";
                    }else if(parseFloat(staffservices[j].inCallPrice) != 0.00 && parseFloat(staffservices[j].outCallPrice) == 0.00){
                        bookingType = "Incall";
                    }else if(parseFloat(staffservices[j].inCallPrice) == 0.00 && parseFloat(staffservices[j].outCallPrice) != 0.00){
                        bookingType = "Outcall";
                    }
                    staffservices[j].bookingType = bookingType;

                } 
            }
            
        }
         data = company;
      next();

    });

}

exports.staffService = function(req,res){

    search = {};
    search['artistId']=Number(req.body.artistId);
      
    staff.aggregate( [
      {  
        $lookup:{
            from: "staffservices", 
            localField: "_id", 
            foreignField: "staffId",
            as: "staffSer"
        }
      },
      { $unwind : "$staffSer" },
      {  
        $lookup:{
            from: "artistservices", 
            localField: "staffSer.artistServiceId", 
            foreignField: "_id",
            as: "artistService"
        }
      },
     { $match :search },
     { 
      "$project": {
          
          "artistServiceId": { "$arrayElemAt": [ "$artistService._id",0] },
          "title": { "$arrayElemAt": [ "$artistService.title",0] }
        
         
          

       }
     },

     
    ],function(err,company){
        if(company){
          if(data){
            for (i = 0 ; i < data.length ; i++) {

              if(data[i].staffService){
                
                for(j = 0; j<data[i].staffService.length;j++){
                  for(k = 0; k<company.length;k++){
                      if(data[i].staffService[j].artistServiceId == company[k].artistServiceId){
                          data[i].staffService[j].title = company[k].title;

                      }

                  }

                }
              }

            }

          }
          res.json({status: "success",message: 'ok',businessList:data});
        }

    });
   
}


exports.reportSubmit = function(req, res) {

    let bookingId  = req.body.bookingId; 
    let title      = req.body.title; 
    let artistId   = req.body.artistId; 
    var jsArr      = [];

    if(bookingId==""){

        res.json({'status':'fail','message':commonLang.REQ_BOOKING_ID});
        return;
    }

    if(title==""){
        
        res.json({'status':'fail','message':commonLang.REQ_TITLE});
        return;
    }

    if(artistId==""){
        
        res.json({'status':'fail','message':commonLang.REQ_ARTIST_ID});
        return;
    }

    bookingservices.updateOne({'_id':bookingId},{$set: {report:title,artistId:artistId}},function(err, docs){
            
        res.json({'status': "success","message":commonLang.SUCCESS_REPORT });
        return;
        
    });
}


exports.artistLocationUpdate = function(req, res) {

    let bookingId  = req.body.bookingId; 
    let latitude = req.body.latitude; 
    let longitude = req.body.longitude; 
    var jsArr = [];

    
    if(bookingId==""){
        res.json({'status':'fail','message':commonLang.REQ_BOOKING_ID});
        return;
    }

    if(longitude==""){
        res.json({'status':'fail','message':commonLang.REQ_LONGITUDE});
        return;
    }

    if(latitude==""){
        res.json({'status':'fail','message':commonLang.REQ_LATITUDE});
        return;
    }


   booking.updateOne({'_id':bookingId},{$set: {artistLocation:[latitude,longitude] }},function(err, docs){

        if(err) res.json(err);
        res.json({'status':"success","message":commonLang.SUCCESS_UPDATE_ARTIST_LOCATION});
    }); 

}



exports.addCustomer = function(req, res) {

    let firstName = req.body.firstName; 
    let lastName  = req.body.lastName; 
    let email     = (req.body.email).toLowerCase(); 
    let phone     = req.body.phone; 
    let userName  = req.body.userName ? req.body.userName : ''; 
    let artistId  = req.body.userId ? Number(req.body.userId) : authData._id; 
    var jsArr = [];

    
    if(firstName==""){
        res.json({'status':'fail','message':commonLang.REQ_FIRST_NAME});
        return;
    }

  /*  if(lastName==""){
        res.json({'status':'fail','message':commonLang.REQ_LAST_NAME});
        return;
    }*/

  /*  if(email==""){
        res.json({'status':'fail','message':commonLang.REQ_EMAIL});
        return;
    }

    if(phone==""){
        res.json({'status':'fail','message':commonLang.REQ_PHONE});
        return;
    }*/
    
    User.findOne({'_id':artistId,'walkingBlock':1}).exec(function(err, walkingData) {

        if(walkingData){

        res.json({'status': "fail","message":commonLang.BLOCKED_BY_ADMIN});
        return;

        }
                

       // booking.findOne({'artistId':artistId,'customerType':"walking","commissionStatus":0}).count().exec(function(err, bCount) {
        booking.findOne({'artistId':artistId,'paymentType':2,"commissionStatus":0,"inProgress":1,"paymentStatus":0,"bookStatus": { $ne: "2" }}).count().exec(function(err, bCount) {

            console.log({'artistId':artistId,'customerType':"walking","commissionStatus":0});
            console.log(bCount);
            
            if(bCount>=3){

                res.json({'status': "fail","message":commonLang.CHANGE_STATUS_PREVIUS_ADDED});
                return;

            }

            client.findOne({'phone':phone}).sort([['_id', 'descending']]).exec(function(err, userdata) {

                if( userdata && phone ){

                   /* if(userName){*/
                     
                      if(userdata.firstName==firstName && userdata.lastName==lastName && userdata.email==email){
                        
                        res.json({'status': "success","message":commonLang.EXIST_CLIENT,'data':userdata});
                        return;

                      }else{

                        res.json({'status': "fail","message":commonLang.CUSTOMER_NOT_MATCHED});
                        return;

                      }

                   /* }else{


                        res.json({'status': "fail","message": "client already registred"});
                        return;

                    }*/

                   

                }

                client.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {


                    jsArr = {
                        _id: (userdata) ? Number(userdata._id)+1 : 1,
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        phone: phone,
                        userName : userName,
                        artistId: artistId
                    };        

                    client.insertMany(jsArr);
                    res.json({'status': "success","message":commonLang.SUCCESS_ADD_CLIENT,'data':jsArr});
                    return;
                });

            });
        });
    });

}


exports.userSearchList = function(req, res, next) {

    let search = req.query.search ? req.query.search : '';
    let userId = req.query.userId ? Number(req.query.userId) : authData._id;
    uData = [];

    //data = {"contactNo": {$regex:search,$options:'i'},'_id':{'$ne':authData._id}};
    //add new filter first name 20-01-20
    User.find({ 
        $or: [{
            'contactNo': {$regex:search,$options:'i'}
        }, {
            'userName': {$regex:search,$options:'i'}
        },{
            'firstName': {$regex:search,$options:'i'}
        }],
        '__id' : {'$ne':userId}

      },{'firstName':1,'lastName':1,'email':1,'contactNo':1,'userName':1}).sort([['_id', 'descending']]).exec(function(err, userdata) {
        if(userdata.length!=0){

                serArr =[];
      
                for(var i = 0; i < userdata.length; i++) {
                   serArr.push({
                        firstName : userdata[i].firstName,
                        lastName  : userdata[i].lastName,
                        userName  : userdata[i].userName,
                        email     : userdata[i].email,
                        phone     : userdata[i].contactNo,
                        artistId  :  authData._id ,
                        crd       : '',
                        upd       : '',
                        _id       : userdata[i]._id,  
                        __v       : '' 

                    }); 
                }
              uData = serArr;
        }

       next();

    });
}



exports.customerList = function(req, res) {
console.log('test');
    let search = req.query.search ? req.query.search : '';

    // data = {
    //        'phone':{$ne:''},
    //       $or: [{
    //           'phone': {$regex:search,$options:'i'}
    //       }, {
    //           'userName': {$regex:search,$options:'i'}
    //       }, {
    //         'firstName': {$regex:search,$options:'i'}
    //     }]
    //   };
    data = {'artistId':authData._id,
           $or: [{
              'phone': {$regex:search,$options:'i'}
          }, {
              'userName': {$regex:search,$options:'i'}
          }, {
            'firstName': {$regex:search,$options:'i'}
        }]
      };
    client.find(data).sort([['_id', 'descending']]).exec(function(err, userdata) {
       allData = uData.concat(userdata)// not use for new req
        if(userdata){


            res.json({'status': "success","message": "ok",'data':userdata});
            return;

        }

        res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND});
        return;

    });
}


exports.walkingList = function(req, res) {

    let userId = req.query.artistId ? Number(req.query.artistId) : authData.__id;

    data = {'artistId' : userId};

    client.find(data).sort([['_id', 'descending']]).exec(function(err, allData) {

        if(allData){


            res.json({'status': "success","message": "ok",'data':allData});
            return;

        }

        res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND});
        return;

    });
}


exports.updateCustomer = function(req, res) {

    let firstName = req.body.firstName; 
    let lastName  = req.body.lastName; 
    let email     = (req.body.email).toLowerCase(); 
    let id        = Number(req.body.id); 
    
    if(firstName==""){

        res.json({'status':'fail','message':commonLang.REQ_FIRST_NAME});
        return;
    }

    if(lastName==""){

        res.json({'status':'fail','message':commonLang.REQ_LAST_NAME});
        return;
    }

    if(email==""){

        res.json({'status':'fail','message':commonLang.REQ_EMAIL});
        return;
    }

    if(id==""){

        res.json({'status':'fail','message':commonLang.REQ_ID});
        return;
    }
  
    client.find({'email':email,'_id': { $ne : id } }).exec(function(err, userdata) {

          if(userdata.length){
               
            res.json({'status': "fail","message":commonLang.EXIST_CLIENT});
            return;

          }else{

                jsArr = {

                    firstName: firstName,
                    lastName: lastName,
                    email: email
                };        
                
                client.updateMany({'_id':id},{$set: jsArr}, function(err, docs){

                    res.json({'status': "success","message":commonLang.SUCCESS_UPDATE_CLIENT});
                    return;

                });

                
            }

    });


}



exports.userBankDetail = function(req, res, next) {

    userId = req.body.userId ? Number(req.body.userId) : authData.__id;

    bankDetail.findOne({'artistId':userId},function(err, data) {
        if(data){
          userDetail = data;
          res.json({'status':'sucess',message: 'ok','data': userDetail});
          return;

        
        }else{
           
           res.json({status: "fail",message:commonLang.NOT_EXIST_BANK_AC});
           return;
        }
    });
}
exports.createAccount = async function(req, res) {

    let userId = req.body.userId; 
   //  let email     = (req.body.email).toLowerCase(); 
    if(userId==""){
        res.json({'status':'fail','message':commonLang.REQ_USER_ID});
        return;
    }
    const checkAccountStatus = await getAccountInfo(authData._id);
    if(checkAccountStatus){
            res.json({'status':'sucess',message: 'ok','data': checkAccountStatus});

    }else {
            const stripeData = await StripeLib.createAccount( authData.firstName, authData.lastName, authData.email );
            var lastRequest = await getBankDetailId();
          // let createAccount =
           var bank_id = 1;
           if(lastRequest){
              bank_id =lastRequest._id+1;
           }
          var addNew = new bankDetail({
            _id : bank_id,
            artistId:authData._id,
            accountId: stripeData,
                                                            
        }); 
       bankDetail(addNew).save(function(err, data) {
          if (err) {
              res.json({status: "fail",message:commonLang.SEND_ALL_REQ_INFO });
              return;
          } else {
    
            res.json({status: "success",message: 'successfully','data':data});
            return;
          }

      });             
    }



}
exports.connectOnboarding = async function(req, res) {

    let accountId = req.body.accountId; 
   //  let email     = (req.body.email).toLowerCase(); 
    if(accountId==""){
        res.json({'status':'fail','message':commonLang.ACCOUNT_ID});
        return;
    }
 //   const checkAccountStatus = await getAccountInfo(authData._id);
        const checkAccountStatus = await  StripeLib.retrieveAccount(accountId);
     // console.log('checkAccountStatuscheckAccountStatus',checkAccountStatus);
     // console.log('external_accounts',checkAccountStatus.external_accounts.total_count);
    if(checkAccountStatus&& checkAccountStatus.charges_enabled == false){
         var type = 'custom_account_verification';
         const accountLink = await  StripeLib.createAccountLink(accountId,type);
         res.json({'status':'sucess',message: 'ok','data': accountLink});

    }else {
            res.json({'status':'sucess',message: 'add bank information','data':{} });
    }
}
exports.addBankDetail = async function(req, res) {
    var account_id = req.body.accountId; 
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var country = req.body.country;
    var currency = req.body.currency;
    var routing_number = req.body.routingNumber;
    var account_number = req.body.accountNo;
    var userId = req.body.userId ? Number(req.body.userId) : authData._id;
  
    if(account_id ==''){
         res.json({status: 'fail',message:commonLang.ACCOUNT_ID});
         return;
    }
    
     const checkAccountStatus = await getAccountInfo(authData._id);
     if ( checkAccountStatus.status==0 ) {
         var tableModel = 'bankDetail';
         var where = {'_id':checkAccountStatus._id};
         var setData ={accountNo:account_number,routingNumber:routing_number,status:1};
         const addBank = await  StripeLib.createBankAccount( checkAccountStatus.accountId, account_number, routing_number, currency, country );
         const updateStatus =   await updateData( tableModel,where,setData);
         User.updateOne({_id:userId},{$set:{isDocument: 3,bankStatus:1}},function(err, docs) {});    
       res.json({'status':'success',message: 'ok','data': addBank});
     }else{
       res.json({'status':'success',message: 'ok','data': checkAccountStatus});

     }

}

async function getAccountInfo(userId){
    return await bankDetail.findOne({'artistId':userId}).exec()
}
async function getBankDetailId(){
    return await bankDetail.findOne().sort({ _id: -1 }).exec();
}
async function updateData( tableModel,where,setData ){
   return await bankDetail.updateMany(where,{$set:setData}).exec();
    
}