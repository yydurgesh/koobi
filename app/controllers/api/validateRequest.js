var User = require('../../models/front/home.js');
var formidable = require('formidable');
exports.checkaccessToken = function(req, res, next){
    //  console.log(JSON.stringify(req.headers));
         
        var token = req.headers['authtoken'];
         //console.log(token);
      if (token) { 
            User.findOne({authToken:token}, function(err, userData) {
             if(userData==null){
              res.status(401);
              res.json({status:'fail',message:commonLang.INVALID_AUTH_TOKEN,"authtoken":""});
              return;
              
            }else{ //console.log(userData);

              if(userData.status=='1'){
                authData = userData;
                next();
              }else{
                res.status(301);
                res.json({status:'fail',message:commonLang.ACCOUNT_INACTIVATE_BY_ADMIN,"authtoken":""});
                return;
              }
             
            }

        });

      } else {
            res.status(401);
             res.json({status:'fail',message:commonLang.INVALID_AUTH_TOKEN,"authtoken":""});
             return;
      }

}

exports.nextData = function(req, res, next){
    //  console.log(JSON.stringify(req.headers));
         
        var token = req.headers['authtoken'];
        // console.log(token);
      if (token) { 
            User.findOne({authToken:token}, function(err, userData) {
             if(userData==null){
              res.status(300);
              res.json({message:commonLang.INVALID_AUTH_TOKEN,"authtoken":""});
              return;
              
            }else{ //console.log(userData);
              authData = userData;
              next();
             
            }

        });

      } else {
            res.status(300);
            res.json({message:commonLang.INVALID_AUTH_TOKEN,"authtoken":""});
            return;
      }

}
