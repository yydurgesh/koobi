var constanPath = require('../../../config/envConstants.js');
var User        = require('../../models/front/home.js');//it user for table and coulamn information
var admin       = require('../../models/admin/login_model');
var formidable  = require('formidable');
var fs          = require('fs');
//var mv          = require('mv');
var dateFormat  = require('dateformat');
var bodyParser  = require('body-parser')
var bcrypt      = require('bcrypt-nodejs');
var accountSid  = constanPath.TWILIO_ID;
var authToken   = constanPath.TWILIO_TOKEN;
var client      = require('twilio')(accountSid, authToken);
var nodemailer  = require("nodemailer");
var ejs         = require("ejs");
var validUrl    = require('valid-url');
var moment      = require('moment');   
var stripe      =    require('stripe')(constanPath.STRIPE_KEY);
var ratingReview=   require('../../models/front/ratingReview_model');
var cronTest=   require('../../models/front/testCron');

var commonConstants =   require('../../../config/commanConstants');
var FileUpload      =   require('../../../lib/FileUpload');
const S3            =   require("../../../lib/aws-s3");
const commonJs      =   require("../../../lib/common");
var multiparty      =   require('multiparty');
var uniqid          = require('uniqid'); 

//Class Objects
const fileUploadObj = new FileUpload();


/*this api use for email and mobile  number verification */


exports.phonVerification = function(req, res) {
  
    var fields = req.body;
    var baseUrl =  req.protocol + '://'+req.headers['host'];
   
    var OTP = Math.floor(1000 + Math.random() * 9000);
    var contactNo = fields.countryCode + fields.contactNo;
    var select = {"_id": 1, "contactNo": 1,"email": 1,"userType":1};
    console.log('request Data info',req.body);
    console.log('contactNo info',contactNo);
    data = [];
    msg = commonLang.SUCCESS_VERIFICATION;

    if (fields.contactNo == '') {

        res.json({status: "fail", message: commonLang.REQ_CONTACT});
        return;

    }
    if (fields.socialId) {

        var where = {"contactNo": fields.contactNo};

    } else {

        if (fields.email && fields.contactNo) {
            var where = {
                "$or": [{
                    "contactNo": fields.contactNo
                }, {
                    "email": fields.email.toLowerCase()
                }]
            };

        } else if (fields.email) {

            var where = {
                "email": fields.email.toLowerCase()
            };

        } else if (fields.contactNo) {

            var where = {
                "contactNo": fields.contactNo
            };

        }
    }

    User.findOne(where, function(err, data) {

    check  = 0;
    test  = 0;

        if (data && data.contactNo == fields.contactNo  && data.userType == fields.userType) {

            res.json({ status: "fail",message:commonLang.EXIST_CONTACT});
            return;

        }else if (data && data.email == fields.email.toLowerCase()  && data.userType == fields.userType) {

            res.json({ status: "fail",message:commonLang.EXIST_EMAIL});
            return;
            
        }else if (data && data.email == fields.email.toLowerCase() ) {

            if(data.userType=="artist" && fields.userType=="artist"){
                res.json({status: "fail", message:commonLang.EXIST_EMAIL});
                return;

            }else if(data.contactNo == fields.contactNo && fields.userType=="artist" && data.userType=="user"){

                data.businesshours  = 0; 
                       
                if (data.profileImage)
                    data.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data.profileImage;
                                
                if(data.dob)
                    data.dob =  moment(data.dob).format('DD/MM/YYYY');

                msg =commonLang.EXIST_SOCIAL_EMAIL;
                check  = 1;
                test  = 2;
               /* res.json({status: "success", message: 'Email already exist by social app' , users:data});
                return;*/

                
            }else if(data.contactNo == fields.contactNo && fields.userType=="user" && data.userType=="artist"){

                data.businesshours  = 0; 
                       
                if (data.profileImage)
                    data.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+data.profileImage;
                                
                if(data.dob)
                    data.dob =  moment(data.dob).format('DD/MM/YYYY');

                msg =commonLang.EXIST_SOCIAL_EMAIL;
                check  = 1;
                test  = 2;
               /* res.json({status: "success", message: 'Email already exist by social app' , users:data});
                return;*/

                
            }else{
                 console.log('dd');
                res.json({status: "fail", message:commonLang.EXIST_EMAIL});
                return;

            }


        } else {

 console.log('ee');

            check  = 1;
        }
        if(check==1){
            //code send opt without trello
            /* res.json({status: 'success',message: msg, otp: OTP ,users:(test==2) ? data : []});
                    return;*/
            //        

            client.messages.create({

                to: contactNo,
                from: constanPath.TWILIO_FROM_NUMBER,
                body: commonLang.TWILIO_VERIFICATION + OTP,

            }, function(err, call) {

                console.log(err);

                if (err) {

                    if(err.message==commonLang.INVALID_TO_NUMBER){

                        res.json({status: 'fail',message:commonLang.VALID_COUNTRY_CODE});
                        return;

                    }else if(err.message   ==  "The 'To' number "+contactNo+" is not a valid phone number."){

                        res.json({status: 'fail',message:commonLang.VALID_CONTACT});
                        return;

                    }else{

                        res.json({status: 'fail',message: err.message});
                        return;
                    }

                } else {

                    res.json({status: 'success',message: msg, otp: OTP ,users:(test==2) ? data : []});
                    return;
                }
            });
        }
    });
}
/*this api is use for user registation (normal and social) both side */
exports.userRegistration = function(req, res){
  
    var baseUrl =  req.protocol + '://'+req.headers['host'];
    var form = new formidable.IncomingForm();// old
   // var form = new multiparty.Form();
    
    form.parse(req, function(err, fields, files){
        
        stripe.customers.create({
          email: fields.email.toLowerCase(),
        }, function(err, customer) {

            var baseUrl =  req.protocol + '://'+req.headers['host'];
            var imageName = "";

            if(fields.contactNo){
                var otpStatus = "checked";
            }else{
                var otpStatus = "unChecked";
            }
            
            if(fields.socialId){

                var otpStatus = "checked";
            }

            if(!fields.latitude){
              fields.latitude=0.0;
              fields.longitude=0.0;
            }
            var newUser = new User({

                userName        :   fields.userName,
                firstName       :   fields.firstName,
                lastName        :   fields.lastName,
                email           :   fields.email.toLowerCase(),
                countryCode     :   fields.countryCode,
                contactNo       :   fields.contactNo,
                gender          :   fields.gender,
                dob             :   fields.dob,
                address         :   fields.address,
                address2        :   fields.address2,
                city            :   fields.city,
                state           :   fields.state,
                country         :   fields.country,
                latitude        :   fields.latitude,
                longitude       :   fields.longitude,
                userType        :   fields.userType,
                socialId        :   fields.socialId,
                socialType      :   fields.socialType,
                deviceType      :   fields.deviceType,
                deviceToken     :   fields.deviceToken,
                chatId          :   fields.chatId,
                firebaseToken   :   fields.firebaseToken,
                OTP             :   otpStatus,
                customerId      :   customer.id,
                otpVerified     :   1,
                outCallLatitude :   fields.latitude,
                outCallLongitude:   fields.longitude,
                 location:{
                    type: "Point",  //geo Type
                    coordinates: [
                      Number(fields.longitude), //longitude
                      Number(fields.latitude) //latitude
                    ]
                 },
                 appType:   'social'
                 // location        :   [fields.latitude,fields.longitude],

            });
            newUser.password   =    newUser.generateHash(fields.password);
            newUser.authToken  =    newUser.authtoken();

            User.findOne({'userName': fields.userName }, function(err, user) {

                if (err) throw err;
                if (user) {
                    res.json({ status: "fail",message:commonLang.USER_REGISTERED});
                    return;
                }
               /*
               //this old code is user for add image on server or local 
               imageName = '';
                if (files.profileImage) {

                    var oldpath = files.profileImage.path;
                    var imageName = Date.now() + ".jpg";
                    var newpath = './public/uploads/profile/' + imageName;
                    fs.rename(oldpath, newpath, function(err) {
                        if (err){
                            res.json({status: "fail",message: err});
                            return;
                        }
                    });
                }
                newUser.profileImage =  imageName;
                */
               
                if(files.profileImage){
                  //this code is use for add image on aws bucket
                    var vidThumbArray = files.profileImage;
               
                    let ext =  files.profileImage.name.split('.').pop();
                    let fileName = uniqid()+'.'+ext;
                   
                    let height = commonJs.calculateFeedImageThunmbHeight(files.profileImage.path);
                    thumboptions = {
                        resize: { height: height },
                        thumbFolder:commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
                    }
                    
                    fileUploadObj.uploadFile(files.profileImage, commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);
                    newUser.profileImage =  fileName;

                } 
                admin.findOne({'_id' :  1}).exec(function(err, admindata) {

                    newUser.commission    = admindata.commission;
                    newUser.commissionCard= admindata.commissionCard;
                  
                    User.findOne().sort([ ['_id', 'descending']]).limit(1).exec(function(err, userdata) {

                        if (userdata) {
                            newUser._id = userdata._id + 1;
                        }
                      // save the user
                       newUser.save(function(err) {
                            if (err) {
                                res.json({status: "fail",message: err});
                                return;
                            } else {
                                if (newUser.profileImage)
                                   // newUser.profileImage = baseUrl+"/uploads/profile/" + newUser.profileImage;
                                    newUser.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+newUser.profileImage;

                                res.json({status: "success", message:commonLang.SUCCESS_REGISTRATION, users: newUser});
                                return;
                            }

                        });

                    });
                })

            });
        });

    });
}

/*this api is use for artist registation (normal and social) both side */
exports.artistRegistration = function(req, res) {

    var baseUrl = req.protocol + '://' + req.headers['host'];
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        stripe.customers.create({
        
            email: fields.email.toLowerCase(),
        
        }, function(err, customer) {

            var imageName = "";
            if (fields.contactNo) {
                var otpStatus = "checked";
            } else {
                var otpStatus = "unChecked";
            }
            User.findOne({'userName': fields.userName}, function(err, user) {

                if (err) throw err;
                if (user) {
                    return res.json({ status: "fail", message: commonLang.USER_REGISTERED});
                }
/*                if (files.profileImage) {

                    var oldpath     = files.profileImage.path;
                    var imageName   = Date.now() + ".jpg";
                    var newpath     = './public/uploads/profile/' + imageName;
                    fs.rename(oldpath, newpath, function(err) {
                        if (err) throw err;
                    });

                } */
                if(files.profileImage){
                  //this code is use for add image on aws bucket
                    var vidThumbArray = files.profileImage;
               
                    let ext =  files.profileImage.name.split('.').pop();
                    let fileName = uniqid()+'.'+ext;
                   
                    let height = commonJs.calculateFeedImageThunmbHeight(files.profileImage.path);
                    thumboptions = {
                        resize: { height: height },
                        thumbFolder:commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
                    }
                    
                    fileUploadObj.uploadFile(files.profileImage, commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);
                    imageName =  fileName;

                } 
                admin.findOne({'_id' :  1}).exec(function(err, admindata) {

                    User.findOne().sort([ ['_id', 'descending'] ]).exec(function(err, userdata) {


                        var newUser = new User({

                            userName            :   fields.userName,
                            firstName           :   fields.firstName,
                            lastName            :   fields.lastName,
                            email               :   fields.email.toLowerCase(),
                            countryCode         :   fields.countryCode,
                            contactNo           :   fields.contactNo,
                            profileImage        :   imageName,
                            customerId          :   customer.id,
                            dob                 :   fields.dob,
                            userType            :   fields.userType,
                            businessType        :   fields.businessType,
                            deviceType          :   fields.deviceType,
                            deviceToken         :   fields.deviceToken,
                            isDocument          :   0,
                            firebaseToken       :   fields.firebaseToken,
                            OTP                 :   otpStatus,
                            otpVerified         :   1,
                            appType             :   'biz',
                            commission          :admindata.commission,
                            commissionCard      :admindata.commissionCard,
                            // location:{
                            //     type: "Point",  //geo Type
                            //     coordinates: [
                            //       Number(fields.longitude), //longitude
                            //       Number(fields.latitude) //latitude
                            //     ]
                            // },
                        });

                        newUser.password   =    newUser.generateHash(fields.password);
                        newUser.authToken  =    newUser.authtoken();

                        if (userdata) {
                            newUser._id =   userdata._id + 1;
                        }
                        // save the user
                        newUser.save(function(err) {
                            if (err) {
                                res.json({ status: "fail", message:commonLang.USER_NOT_REGISTERED});
                                return;
                            } else {

                                
                                let firebaseToken = fields.firebaseToken ? fields.firebaseToken : '';        
                                let deviceToken = fields.deviceToken ? fields.deviceToken : '';
                                
                                //if (newUser.profileImage)
                                   // newUser.profileImage = baseUrl+"/uploads/profile/" + newUser.profileImage;

                                   User.aggregate([{  
                                        $lookup:{
                                                from: "busineshours", 
                                                localField: "_id", 
                                                foreignField: "artistId",
                                                as: "businesshours"
                                        }
                                     
                                    },
                                    { 
                                        $match:{_id:newUser._id} 

                                    }],function(err, userData) {

                                        user = userData[0];
                                        user.businesshours  =    (user.businesshours.length) ? 1 : 0; 
                               
                                        if (user.profileImage)
                                           // user.profileImage = baseUrl+"/uploads/profile/"+user.profileImage;
                                            newUser.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+user.profileImage;
                                        
                                        if(user.dob)
                                            user.dob =  moment(user.dob).format('DD/MM/YYYY');


                                       res.json({status: "success",message: commonLang.SUCCESS_REGISTRATION,users: user});
                                        return;
                                        
                                    });

                                
                            }

                        });

                    });
                })

            });
        
        });


    });
}
exports.userLogin = function(req, res) {

    var baseUrl         =   req.protocol + '://'+req.headers['host'];
    var fields          =   req.body;  
    var userName        =   fields.userName;
    var password        =   fields.password;
    var deviceToken     =   fields.deviceToken;
    var deviceType      =   fields.deviceType;
    var firebaseToken   =   fields.firebaseToken;
    var type            =   fields.userType;
    var appType         =   fields.appType;
   // console.log(fields);
    if (userName == '') {

        res.json({status: "fail",message: commonLang.REQ_EMAIL_OR_USERNAME});
        return;

    } else if (password == '') {

        res.json({status: "fail",message: commonLang.REq_REQ_PASS});
        return;

    }

    User.findOne({
        $or: [{
            'email': fields.userName.toLowerCase()
        }, {
            'userName': fields.userName
        }]
    }, function(err, user) {

        if (err) {
            res.status(500);
            res.json({ status: "fail",  message: err});
            return;

        } else if (!user) {

            res.json({status: "fail", message:commonLang.VALID_USERNAME_OR_EMAIL});
            return;

        } else if (!user.validPassword(password)) {

            res.json({status: "fail",message:commonLang.VALID_PASS});
            return;

        } else if (user.status === '0') {

            res.json({ status: "fail",message: commonLang.ACCOUNT_INACTIVATE_BY_ADMIN});
            return;

        } else {

            var t = false;
            if((user.userType=="artist" && type == "user") || (user.userType=="user" && type=="user") ){

                t = true;

            }else if(user.userType=="artist" && type == "artist"){

                t = true;
            }

            if(t){

                   //code for artist as user
                    if(user.userType =="artist" && appType=='social'){
                       
                         
                let dataArr = {isArtistUser:1};
                         User.updateOne({_id:user._id},{$set:dataArr},function(err, docsAU) {
                       /*   console.log(err);
                          console.log("///////");
                          console.log(docsAU);*/
                          });

                    }
                    //end

                var test = user.authtoken();

                let data = {
                        authToken       : test,
                        deviceType      : deviceType,
                        deviceToken     : deviceToken,
                        firebaseToken   : firebaseToken ? firebaseToken : deviceToken,
                        appType         : appType
                    };

                    User.updateOne({firebaseToken: firebaseToken}, {$set:{firebaseToken:''}},function(err, docs) {
                        
                        User.updateOne({firebaseToken: deviceToken}, {$set:{deviceToken:''}},function(err, docs) { 
                         
                            User.updateOne({_id: user._id}, {$set:data},function(err, docs) {

                                if (err) res.json(err);

                                  User.aggregate([

                                    {  
                                        $lookup:{

                                                from: "busineshours", 
                                                localField: "_id", 
                                                foreignField: "artistId",
                                                as: "businesshours"
                                        }                             
                                    },

                                    {
                                        $lookup:{

                                                from: "artistmainservices", 
                                                localField: "_id", 
                                                foreignField: "artistId",
                                                as: "bizCount"
                                        },
                                    },

                                    { 
                                        $match:{_id: user._id } 

                                    }],function(err, userData) {
                                        
                                        ratingReview.findOne({'artistId':user._id}).count().exec(function(err, rCount){            

                                            user = userData[0];                       
                                            user.authToken      =   test;
                                            user.deviceType     =   deviceType;
                                            user.deviceToken    =   deviceToken;
                                            user.firebaseToken  =   firebaseToken; 
                                            user.reviewCount    =   rCount;
                                            user.businesshours  =    (user.businesshours.length) ? 1 : 0; 
                                            user.bizCount  =  (user.bizCount.length) ? user.bizCount.length : 0; 
                                          
                                            if (user.profileImage)
                                                user.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+user.profileImage;

                                            if(user.dob)
                                                user.dob =  moment(user.dob).format('DD/MM/YYYY');

                                            res.json({ status: "success", message:commonLang.SUCCESS_USER_AUTH, users: user});
                                            return;
                                        });
                                    });
                            });
                        });
                     });

            }else{
                
                res.json({status: "fail", message:commonLang.VALID_USERNAME_OR_EMAIL});
                return;

            }
        }

    });
    

}

exports.forgotPassword = function(req, res,next) {
    
  if(req.body.email==""){

        res.json({status:"fail", message:"Email is required"});
        return;

    }  
    next();
}
exports.sendMail = function(req, res) {

        User.findOne({
            $or: [{
                'email': req.body.email.toLowerCase()
            }, {
                'userName': req.body.email
            }]

        }, function(err, user) {

            if (err) {
                res.status(500);
                res.json({
                    status: "fail",
                    message: err
                });
                return;

            } else if (!user) {
                res.json({
                    status: "error",
                    message: commonLang.NOT_EXIST_USER,
                });
                return;
            } else if (user.status === '0') {
                res.json({
                    status: "error",
                    message: commonLang.ACCOUNT_NOT_ACTIVATE
                });
                return;

            } else {

                var messagePass = Math.floor((Math.random() * 99999999) + 1);
                var newUser = new User({});
                newPass = newUser.generateHash(messagePass);
                var template = process.cwd() + '/app/templates/resetPassword.ejs';
                var templateData = {
                    Paaword: messagePass,
                    userName : user.userName,
                    baseUrl : req.protocol + '://'+req.headers['host'],
                    link : req.protocol + '://'+req.headers['host']+'/resetPassword?authToken='+user.authToken
                };
                ejs.renderFile(template, templateData, 'utf8', function(err, file) {
                    if (err) {
                        res.json({
                            'status': "fail",
                            "message": err
                        });
                    } else {

                        var smtpTransport = nodemailer.createTransport({
                            host:constanPath.SMTP_HOST,
                            port: constanPath.SMTP_PORT,
                            secure: true,
                            service:constanPath.SMTP_SERVICE,
                            secureConnection: true,
                            auth: {
                                user:constanPath.SMTP_AUTH_USER,
                                pass:constanPath.SMTP_AUTH_PASSWORD 
                            }
                        });
                        var mailOptions = {
                            from:constanPath.MAIL_FROM ,
                            to: user.email,
                            subject: constanPath.MAIL_SUBJECT_FORGOT,
                            html: file
                        }

                        smtpTransport.sendMail(mailOptions, function(error, response) {
                            if (error) {

                                res.json({
                                    'status': "fail",
                                    "message": error
                                });
                            } else {
                                
                                res.json({
                                    'status': "success",
                                    "message":commonLang.SENT_EMAIL_TO_RESET_PASS
                                });

                            }
                        });
                    }
                });

            }

        });

}

exports.checkUser = function(req,res){

    if(req.body.userName){

        User.findOne({'userName': req.body.userName}, function(err, user) {
            if (err) throw err;
            if (user) {

                res.json({status: "fail",message:commonLang.USER_REGISTERED});
                return;
            
            }else{
            
                res.json({status: "success",message: 'Not exist.'});
                return;

            }
        });

    }else{

        res.json({ status: "fail",message:commonLang.REQ_UERNAME});
        return;
    }
}


exports.checksocialLogin = function(req,res){
   
   if(req.body.socialId==""){
        res.json({'status':'fail','message':commonLang.REQ_SOCIAL_ID});
        return;
   }

   if(req.body.socialType==""){
        res.json({'status':'fail','message':commonLang.REQ_SOCIAL_TYPE});
        return;
   }

    if(req.body.deviceToken==""){
        res.json({'status':'fail','message':commonLang.REQ_DEVICE_TOKEN});
        return;
    }
    let deviceToken = req.body.deviceToken;
    User.findOne({'socialId': req.body.socialId,'socialType': req.body.socialType}, function(err, user) {
        
        if (err) throw err;
        if (user) {
            
            let baseUrl = req.protocol + '://'+req.headers['host'];
            user.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR + user.profileImage;

            User.updateOne({firebaseToken: deviceToken}, {$set:{firebaseToken:''}},function(err, docs) { });
            User.updateOne({socialId: req.body.socialId}, {$set:{firebaseToken:deviceToken,appType :'social'}},function(err, docs) { });

            return res.json({ status: "success",message: 'ok',data : user});

        }else{

            return res.json({status: "fail", message: 'Not exist.' });

        }
    });

  
}



exports.sendNotificationOld = function(req,res){

    var FCM = require('fcm-node');
    var serverKey = 'AAAAK1vRFPE:APA91bFDJlGE-pK5f7JarrELoglCDCZl2Bnnm495IBiYjWXte8BInV8ZSdNT9fcW-xx96LQFIQAAGiwvMXYpK8ap6uJX6qfiPXfMCEwbGbfd7KMXtSSm9MLdfpD6AhdpbHbzSQbew5wF'; //put your server key here
    var fcm = new FCM(serverKey);
 
    var message = {
       //this may vary according to the message type (single recipient, multicast, topic, et cetera)
      
        //to: 'eO-FFDWF1W4:APA91bGBmNojXuNPpZnYWYwZsiYOKyEMA73-coJ7c4n4hS_0PabxxX3yFWUUZeC7or_KHflw-lK8OJJOdAqeQ87WXBv1A2R_TioQXzDD4HOVo-XdFqa_kl3ARBUI0laz2u3arfFm-XIK', 
        //to: 'fBl62qfkPyU:APA91bEaduLriCWqlcsVy5NZp1pyOQ2G4w-LAkUFunWCxK6EST_jDY1q9Ol34oycimZhwYyv-Yz7r681mlX9wxpOlCyx4qODsdbdAdPnwBM3c7OSZDIHoHwXjYq6qlBnMPBIrwU8seO7', 
        to:'fg9Oe5V2QrY:APA91bFebUSErvOfyxOLmyO1szJqCFPWO_aRi8QXbOw0n-_CthKkDBoQRSu7aZxJn_Rw7Kk2wme-6Dnblcbwl5moAauyxH4HAB6vpy_0plWF1mFV8i320lzfuE70qr9ntjStyJ23dFPhAtFH5M5jmrm55IOXZs5y-A', /// android 
        collapse_key: 'your_collapse_key',

        delay_while_idle : false,
        priority : "high", 
        content_available: true,
        mutable_content: true,
        category : '',
         notification: {
            title: 'Koobi', 
            body: 'Welcome to koobi world.' 
        },
        
        data: {  //you can send only notification or only data(or include both)
            title: 'Koobi',
            urlImageString: 'https://res.cloudinary.com/demo/image/upload/sample.jpg',
        }

    };
    
    fcm.send(message, function(err, response){
        if (err) {
           console.log(err);
            console.log("Something has gone wrong!");
        } else {
            res.json({status:"success",message:'Successfully sent with response',data:response});
            //("Successfully sent with response: ", response);
        }
    });
}



/*this api is use for artist registation (normal and social) both side */
exports.thumbCheck = function(req, res) {

    var baseUrl = req.protocol + '://' + req.headers['host'];
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
            
            var imageName = "";
            if (files.image) {
    

                var oldpath     = files.image.path;
                var date        = Date.now();
                var imageName   = date + ".jpg";
                var newpath     = './public/uploads/test/' + imageName;

                fs(oldpath, newpath, function(err) {

                    var thumb = require('node-thumbnail').thumb;
                    var newI     = './public/uploads/test/thumb/';
                    thumb({
                      source: newpath,
                      destination: newI,
                      concurrency: 4,
                      width : 500,
                      height : 300,
                      quiet: true,
                      overwrite: true,
                      prefix: '',
                      suffix: '',
                      basename : date
                    }, function(files, err, stdout, stderr) {
                      console.log('All done!');
                    });
                    res.json({status: "success",message: 'ok'});
                    return;

                  
                });

            }

    });
}

function readAndWriteFile(singleImg, newpath) {

    fs.readFile(singleImg.path , function(err,data) {
        fs.writeFile(newpath,data, function(err) {
            if (err) console.log('ERRRRRR!! :'+err);
            console.log(singleImg);
            console.log('Fitxer: '+singleImg.originalFilename +' - '+ newpath);
        })
    })
}

exports.changepassword = function(req, res) {
        
    let userId           = req.body.userId ? Number(req.body.userId) : authData._id; 
    let oldPassword     = req.body.oldPassword; 
    let password        = req.body.password; 

    if(oldPassword==""){
        
        res.json({'status':'fail','message':commonLang.REQ_OLD_PASS});
        return;
    }

    if(password==""){
        
        res.json({'status':'fail','message':commonLang.REQ_PASS});
        return;
    }

    User.findOne({'_id':userId}).exec(function(err, data) {

        
        if (!data.validPassword(oldPassword)){

            res.json({'status':'fail',"message":commonLang.NOT_MATCH_OLD_PASS});
            return;
        

        }else{  

            var my = new User();
            var password =  my.generateHash(req.body.password);

            User.update({_id:data._id},{password:password},function(err, docs){
                
                if(err) res.json(err);
                else    {

                    res.json({'status':'success',"message":commonLang.SUCCESS_UPDATE_PASS});
                    return;
                };
            });
        }
    });
 
}
exports.updateCustomerStrip = function(req,res){
    console.log(req.body.email);
     stripe.customers.create({
          email: req.body.email.toLowerCase(),
        }, function(err, customer) {

            if(err) res.json(err);
            else{
                    res.json({'status':'success',"message":customer});
                    return;
            };
           
           console.log(customer.id);
           console.log("////////");
           console.log(customer);

    });
    return;

}


exports.testCronPath = function(req, res) {

    console.log('corn working');

    cronTest.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

        var data1 = {
            '_id'           : (aat) ? Number(aat._id)+Number(1) : 1,
            'crd'            :  moment().format()
            };

        cronTest.insertMany(data1);
    
        res.json({'status': "success","message":"test success"});
        return;
    });

 
}
exports.logout_user = function(req,res){
     
    var updData = {
        'deviceToken':'',
        'firebaseToken':''
    }
    User.updateOne({ _id:authData._id },{ $set:updData},function(err, result) {
       res.json({status:"success",message:"logout Successfully." });
        
    });

   
}
       


