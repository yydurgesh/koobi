var voucher             =   require('../../models/front/voucher_model.js');


exports.addVoucher = function(req, res){

    let voucherCode  = req.body.voucherCode; 
    let discountType = req.body.discountType; 
    let amount       = req.body.amount; 
    let startDate    = req.body.startDate; 
    let endDate      = req.body.endDate; 
    let id          = req.body.id ? req.body.id : ''; 
    var jsArr = [];
    if(voucherCode==""){
        res.json({'status':'fail','message':commonLang.REQ_VOUCHER_CODE});
        return;
    }

    if(discountType==""){
        res.json({'status':'fail','message':commonLang.REQ_DISCOUNT_TYPE});
        return;
    }

    if(amount==""){
        res.json({'status':'fail','message':commonLang.REQ_AMOUNT});
        return;
    }

    if(startDate==""){
        res.json({'status':'fail','message':commonLang.REQ_START_DATE});
        return;
    }

    if(endDate==""){
        res.json({'status':'fail','message':commonLang.REQ_END_DATE});
        return;
    }

    if(id){

        voucher.findOne({'voucherCode':voucherCode,'deleteStatus':1,'_id':{$ne:id},'artistId': authData._id}).sort([['_id', 'ascending']]).exec(function(err, voc) {
            console.log(voc);
            if(voc){

                res.json({'status': "fail","message": commonLang.EXIST_VOUCHER_CODE});
                return;
            }

            data = {
                voucherCode: voucherCode,
                discountType: discountType,
                amount: amount,
                startDate: startDate,
                endDate: endDate,
                artistId: authData._id
            }; 

            voucher.update({_id: id}, { $set:data},function(err, docs) {

                res.json({'status': "success","message":commonLang.SUCCESS_UPDATE_VOUCHER});
                return;

            });
             
        });

    }else{

        voucher.findOne({'voucherCode':voucherCode,'deleteStatus':1,'artistId': authData._id}).sort([['_id', 'ascending']]).exec(function(err, voc) {

            if(voc){

                res.json({'status': "fail","message":commonLang.EXIST_VOUCHER_CODE});
                return;
            }

            voucher.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {


                jsArr.push({
                    _id: (userdata) ? Number(userdata._id)+1 : 1,
                    voucherCode: voucherCode,
                    discountType: discountType,
                    amount: amount,
                    startDate: startDate,
                    endDate: endDate,
                    artistId: authData._id
                });        
                voucher.insertMany(jsArr);
                res.json({'status': "success","message":commonLang.SUCCESS_ADD_VOUCHER });
                return;
            }); 
        });

    }
}


exports.allvoucherList = function(req, res) {

    voucher.find({'artistId':authData._id,'deleteStatus':1}).sort([['_id', 'ascending']]).exec(function(err, ser) {
        
        if(ser.length){
            
            res.json({status: "success",message: 'ok',data: ser});
            return;

        }else{

            res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND});
            return;

        }
    
    });

}



exports.deleteVoucher = function(req, res){

    let id          = req.body.id ? req.body.id : ''; 
    if(id==""){
        res.json({'status':'fail','message':commonLang.REQ_VOUCHER_ID});
        return;
    }

    voucher.update({_id: id}, { $set:{'deleteStatus':0}},function(err, docs) {});
    res.json({'status': "success","message":commonLang.SUCCESS_DELETE_VOUCHER});
    return;

}



exports.voucherList = function(req, res) {

    let artistId     = Number(req.body.artistId); 

    if(artistId==""){
        res.json({'status':'fail','message':commonLang.REQ_ARTIST_ID});
        return;
    }
    data = {
            'artistId':artistId,
            'deleteStatus':1,
            'startDate':{$lte: dateTime.curDate},
            'endDate':{$gte: dateTime.curDate}
    };

    voucher.find(data).sort([['_id', 'ascending']]).exec(function(err, ser) {
        
        if(ser.length){
            
            res.json({status: "success",message: 'ok',data: ser});
            return;

        }else{

            res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND});
            return;

        }
    
    });

}

exports.applyVoucher = function(req, res) {

    let voucherCode  = req.body.voucherCode; 
    let artistId     = req.body.artistId; 

    if(voucherCode==""){
        res.json({'status':'fail','message':commonLang.REQ_VOUCHER_CODE});
        return;
    }

    if(artistId==""){
        res.json({'status':'fail','message':commonLang.REQ_ARTIST_ID});
        return;
    }

    data = {
        'artistId':artistId,
        'deleteStatus':1,
        'voucherCode':voucherCode,
        'startDate':{$lte: dateTime.curDate},
        'endDate':{$gte: dateTime.curDate}
    };

    voucher.findOne(data).sort([['_id', 'ascending']]).exec(function(err, ser) {
        
        if(ser){
            
            res.json({status: "success",message: 'ok',data: ser});
            return;

        }else{

            res.json({status: "fail",message:commonLang.INVALID_VOUCHER_CODE});
            return;

        }
    
    });

}
/*code for get voucher*/
exports.getVoucher = function(req, res) {

    let voucherId     = Number(req.body.voucherId); 

    if(voucherId==""){
        res.json({'status':'fail','message':commonLang.REQ_VOUCHER_ID});
        return;
    }

    voucher.findOne({'_id':voucherId,'deleteStatus':1}).exec(function(err, ser) {
        
        if(ser){
            
            res.json({status: "success",message: 'ok',data: ser});
            return;

        }else{

            res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND});
            return;

        }
    
    });

}
