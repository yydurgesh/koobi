var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var artistCertificate   =   require('../../models/front/artistCertificate.js');
var certificateTag      =   require('../../models/front/certificateTag_model.js');
var reportReason        =   require('../../models/front/reportReason_model.js');
var formidable          =   require('formidable');//it user for get form or post data by http
var fs                  =   require('fs');
var dateFormat          =   require('dateformat');

var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
var FileUpload      =   require('../../../lib/FileUpload');
const S3            =   require("../../../lib/aws-s3");
const commonJs      =   require("../../../lib/common");
var uniqid          = require('uniqid'); 
const fileUploadObj = new FileUpload();
exports.addReportreson = function(req,res){


   reportReason.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {

        jsArr = {
            _id: (userdata) ? Number(userdata._id)+1 : 1,
            title: req.query.title,
        };    

        reportReason.insertMany(jsArr);
        res.json({status:"success",message: commonLang.SUCCESS_ADD_REPORT_REASON});
        return;
        

    });

}


exports.addArtistCertificate = function(req,res){
  var form = new formidable.IncomingForm();
    form.parse(req,function(err,fields,files){
         //console.log(files.certificateImage);return;
        let title = fields.title;
        let description = fields.description;

        if(title==""){
            res.json({status:"fail",message: commonLang.REQ_TITLE_FIELD});
            return;
        }

        if(description==""){
            res.json({status:"fail",message: commonLang.REQ_DESCRIPTION_FIELD});
            return;
        }
        
        if(typeof(files) == "undefined" || files.certificateImage ==""){
            res.json({status:"fail",message: commonLang.REQ_QUALIFICATION_IMAGE_FIELD});
            return;
        }

        var baseUrl =  req.protocol + '://'+req.headers['host'];  
        artistId = authData._id;
        var imageName = "";
        if(files.certificateImage){

          /* var oldpath = files.certificateImage.path;
            var imageName = Date.now()+".jpg";
            var newpath = './public/uploads/certificateImage/'+imageName;
            fs.rename(oldpath, newpath, function (err) {
              if (err) throw err;
            });*/
            // hear code for add image using s3 bucket
            var imageObj = files.certificateImage;
       
            let ext =  imageObj.name.split('.').pop();
            let fileName = uniqid()+'.'+ext;
           
            let height = commonJs.calculateFeedImageThunmbHeight(imageObj.path);
            thumboptions = {
                resize: { height: height },
                thumbFolder:commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
            }
            
            fileUploadObj.uploadFile(imageObj, commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);
            imageName =  fileName;

            // end

            autoId = 1;
            artistCertificate.find().sort([['_id','descending']]).limit(1).exec(function(err,result) {
                  var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
                  var addNew = new artistCertificate({artistId:artistId,certificateImage:imageName,title:title,description:description});
                  if(err) throw err;
                  if(result.length>0){
                        addNew._id = result[0]._id+1;
                  }
                  count = Number(authData.certificateCount) + 1;
                  User.updateOne({_id: artistId}, { $set: {isDocument: 3, certificateCount: count  }   },function(err, docs) {});

                 addNew.save(function(err,data) {

                    certificateTag.findOne({'title': title}, function(err, data) {

                        if(data=='' || data==null){


                            certificateTag.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {


                                jsArr = {
                                    _id: (userdata) ? Number(userdata._id)+1 : 1,
                                    title: title,
                                    artistId: authData._id
                                };    

                                certificateTag.insertMany(jsArr);
                                

                            });

                        }


                        if (err){

                            res.json({status:"fail",message: commonLang.SEND_ALL_REQ_INFO});
                            return;

                        }else{

                            //addNew.certificateImage = baseUrl+"/uploads/certificateImage/"+addNew.certificateImage;
                            addNew.certificateImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+addNew.certificateImage;

                            res.json({status:"success",message: commonLang.SUCCESS_ADD_QUALIFICATION,certificate:addNew});
                            return;
                        }
                    
                    });


                });
            });
        } 
    });
}

exports.editArtistCertificate = function(req,res){ 

  var form = new formidable.IncomingForm();
    form.parse(req,function(err,fields,files){

        let title = fields.title;
        let description = fields.description;
        let id = fields.id;
       
        if(id==""){
            res.json({status:"fail",message: commonLang.REQ_ID_FIELD});
            return;
        }

        if(title==""){
            res.json({status:"fail",message: commonLang.REQ_TITLE_FIELD});
            return;
        }

        if(description==""){
            res.json({status:"fail",message: commonLang.REQ_DESCRIPTION_FIELD});
            return;
        }

        var baseUrl =  req.protocol + '://'+req.headers['host'];  
        artistId = authData._id;
        var imageName = "";


        if(files.certificateImage){

           /* var oldpath = files.certificateImage.path;
            var imageName = Date.now()+".jpg";
            var newpath = './public/uploads/certificateImage/'+imageName;
            fs.rename(oldpath, newpath, function (err) {
              if (err) throw err;
            });*/
            // hear code for add image using s3 bucket
            var imageObj = files.certificateImage;
       
            let ext =  imageObj.name.split('.').pop();
            let fileName = uniqid()+'.'+ext;
           
            let height = commonJs.calculateFeedImageThunmbHeight(imageObj.path);
            thumboptions = {
                resize: { height: height },
                thumbFolder:commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER
            }
            
            fileUploadObj.uploadFile(imageObj, commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR, fileName, thumboptions);
            imageName =  fileName;

            // end

        }
        var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
        var addNew = {title:title,description:description};

        if(imageName)
            addNew.certificateImage = imageName;

        artistCertificate.findOne({'_id': id}, function(err, checkData) {
             if(imageName && checkData){

                console.log(checkData);
                //S3.deleteFile('certificateImage/thumbs/6bq76lk1633q81.jpg');
                let imgPath = commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+checkData.certificateImage;
                let imgPathThumb = commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+commonConstants.THUMB_FOLDER+checkData.certificateImage;
                S3.deleteFile(imgPath);
                S3.deleteFile(imgPathThumb);

             }
            artistCertificate.updateOne({_id:id}, { $set: addNew},function(err, result) {

                certificateTag.findOne({'title': title}, function(err, data) {

                    if(data=='' || data==null){


                        certificateTag.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {


                            jsArr = {
                                _id: (userdata) ? Number(userdata._id)+1 : 1,
                                title: title,
                                artistId: authData._id
                            };    

                            certificateTag.insertMany(jsArr);
                            

                        });

                    }


                    if (err){

                        res.json({status:"fail",message: commonLang.SEND_ALL_REQ_INFO});
                        return;

                    }else{

                        res.json({status:"success",message: commonLang.SUCCESS_UPDATE_QUALIFICATION});
                        return;
                    }
                
                });


            });
        });

         
    });
}



exports.getAllCertificate = function(req, res) {

   var baseUrl =  req.protocol + '://'+req.headers['host'];
   artistId = req.body.artistId ? req.body.artistId : authData._id;
   type = req.body.type ? req.body.type : '';
   where ={};
   if(artistId == ''){
        res.json({status:"fail",message: commonLang.REQ_ARTIST_ID});     
        return;
    }
   if(type == ''){
        res.json({status:"fail",message: commonLang.REQ_TYPE_ID});
        return;
    }
   where['artistId'] = Number(artistId);

   if(artistId != authData._id){

        where['status'] =1;

   }


   artistCertificate.find(where, function(err, data)
   {
    if (data.length==0){
      res.json({status:"sucess",message: commonLang.RECORD_NOT_FOUND});
    }else{ 

        for (i = 0 ; i < data.length ; i++) {
           
          if(data[i].certificateImage){ 
            data[i].certificateImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.CERTIFICATE_MEDIA_UPLOAD_FOLDEFR+data[i].certificateImage;
          
          }
        }
      res.json({status:"success",message:'ok',allCertificate:data});
    }
  });
}

exports.deleteCertificate = function(req,res){

    artistId = req.body.artistId;
    certificateId = req.body.certificateId;
    if(artistId == ''){
        res.json({status:"fail",message: commonLang.REQ_ARTIST_ID});
        return;
    }
    if(certificateId == ''){
        res.json({status:"fail",message: commonLang.REQ_QUALIFICATION_ID});
        return;
    }
    if(req.body.certificateId){
        artistCertificate.deleteOne({'_id':certificateId,'artistId':artistId}, function(err, results){
        if(err) throw err;
        artistCertificate.find({'artistId':artistId}).count().exec(function(err,count){ 
              User.updateOne({_id: artistId},{$set:{certificateCount: count}},function(err, docs) {});
           });
        res.json({status:"success",message: commonLang.SUCCESS_DELETE_QUALIFICATION});
        });
    }else{
        res.json({status:"fail",message: commonLang.REQ_QUALIFICATION_ID});
    }

}



exports.certificateTag = function(req, res) {

    certificateTag.find(function(err, data){

        if (data.length==0){

          res.json({status:"sucess",message: commonLang.RECORD_NOT_FOUND});

        }else{ 

          res.json({status:"success",message:'ok',data:data});

        }
    });
}

exports.certificateFilter = function(req, res , next) {

      let certificate = req.body.certificate;
      filter = 0;
      certificateData = [];
      if(certificate){
        filter = 1;
        var cer = certificate.split(",");
        var certifi = cer.map(function(n) {return n;});
        artistCertificate.find({'title':{ $in: certifi},'status':1},function(err, data){

            if (data.length){ 

             certificateData = data.map(function (rd) {
                        return Number(rd.artistId);
                    });  


            }

            next();

        });

    }else{

        next();
    }
}