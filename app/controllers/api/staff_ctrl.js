var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var staff               =   require('../../models/front/staff_model.js');
var staffService        =   require('../../models/front/staffService.js');
var artistMainService   =   require('../../models/front/artistMainService.js');
var artistSubService    =   require('../../models/front/artistSubService.js');
var artistservices      =   require('../../models/front/artistService.js');
var async               =   require('async');
var bookingService      =   require('../../models/front/bookingService.js');
var staffBussinesType   =   require('../../models/front/staffBussinesType_model.js');
var staffCategory       =   require('../../models/front/staffCategory_model.js');
var holiday             =   require('../../models/front/holiday_model.js');
var nodemailer          =   require("nodemailer");
var ejs                 =   require("ejs");
var lodash              =   require('lodash');
var moment              =   require('moment');
var addNotification     =   require('../../models/front/notification.js');

var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
exports.addStaff = function(req,res){
    
    if(req.body.businessId ==''){
        res.json({'status': "fail","message": commonLang.REQ_BUSINESS_ID});
        return;
    }

    if(req.body.artistId ==''){

        res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
        return;

    }

    upd = new Date();
    serviceData = JSON.parse(req.body.staffService);
    staffHour = '';
    if(req.body.staffHours.length != 0){
        staffHour = JSON.parse(req.body.staffHours);
    }
   //console.log('staffHour', staffHour)
    //staffHour = JSON.parse(req.body.staffHours);
    let editId = req.body.editId;
    let type = req.body.type;
    let businessId = req.body.businessId;
    let mediaAccess = req.body.mediaAccess;
    let holiday = req.body.holiday;
    let artistId = req.body.artistId;
    let job = req.body.job;
    let bookingType = req.body.bookingType;
    let salaries = req.body.salaries;
    let message = req.body.message;
    jsArr = [];
    sortData = {};
    sortData['artistId'] = artistId;
    sortData['businessId'] = businessId; 
    let baseUrl = req.protocol + '://'+req.headers['host'];


    if(editId && type =='edit'){
        var data = {
            job: job,
            salaries: salaries,
            message: message,
            mediaAccess: mediaAccess,
            holiday:    holiday,
            staffServiceId:serviceData,
            staffHours: staffHour,
            bookingType: bookingType,
            upd:upd
        };

        staff.updateMany({'_id':editId},{$set: data}, function(err, docs){ 
           staffService.updateMany(sortData, {$set: {staffId:editId}},function(err, result) {});
           staffBussinesType.updateMany(sortData, {$set: {staffId:editId}},function(err, result) {});
           staffCategory.updateMany(sortData, {$set: {staffId:editId}},function(err, result) {});
        });
       res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_STAFF,'staffId':editId});
       return;
 
    }else{

        staff.find().sort([['_id', 'descending']]).limit(1).exec(function(err, staffData) {
            User.find({'_id':artistId},{'_id':1,'userName':1,'profileImage':1,'email':1}).exec(function(err,artistData){
                var autoId = 1;
                  if (staffData.length > 0) {
                    autoId = staffData[0]._id + 1;
                }
               staffInfo =[];
                staffInfo = {"userName":artistData[0].userName,"profileImage":artistData[0].profileImage }
                var addNew = {
                    _id: autoId,
                    businessId: businessId,
                    artistId: artistId,
                    job: job,
                    salaries: salaries,
                    message: message,
                    mediaAccess: mediaAccess,
                    holiday: holiday,
                    staffServiceId:serviceData,
                    staffInfo:staffInfo,
                    bookingType:bookingType,
                    staffHours: staffHour,
                    status: 0,
                    upd:upd                            
                };

                staff(addNew).save(function(err, data) {
                   if (err) {
                        res.json({status: "fail",message: err });
                        return;
                    } else {
                        staffService.updateMany(sortData, {$set: {staffId:autoId}},function(err, result) {

                            staffBussinesType.updateMany(sortData, {$set: {staffId:autoId}},function(err, result) {

                                staffCategory.updateMany(sortData, {$set: {staffId:autoId}},function(err, result) {

                                    if(artistData[0].email){
                                        sendInvitationEmail(baseUrl,artistData[0].email);
                                    }

                                    var typ = '17';
                                    var sender     = businessId;
                                    var receiver   = artistId; 
                                    var notifyId   = artistId;
                                    var notifyType = 'booking';          
                                    notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 

                                   res.json({status: "success", message: commonLang.SUCCESS_ADD_STAFF});
                                   return;
                                });
                            });
                        });
                        
                    }
                });
            
            });
           
         
        });
    }    
    

}


exports.addStaffService = function(req,res){


    if(req.body.businessId ==''){
         res.json({'status': "fail","message": commonLang.REQ_BUSINESS_ID});
         return;

    }
    if(req.body.artistId ==''){
         res.json({'status': "fail","message": commonLang.REQ_ARTIST_ID});
         return;

    }
    if(req.body.staffId ==''){
         res.json({'status': "fail","message": commonLang.REQ_STAFF_ID});
         return;

    }
       timeSl = JSON.parse(req.body.staffService);
        jsArr = []
        staffService.deleteMany({'businessId':req.body.businessId,'artistId':req.body.artistId}, function(err, results){});
        staffBussinesType.deleteMany({'businessId':req.body.businessId,'artistId':req.body.artistId}, function(err, results){});
        staffCategory.deleteMany({'businessId':req.body.businessId,'artistId':req.body.artistId}, function(err, results){});
        staffService.find().sort([['_id', 'descending']]).limit(1).exec(function(err, userdata) {
            var autoId = 1;

            if (userdata.length > 0) {
                autoId = userdata[0]._id + 1;

            }
            data = [];
            result1 = [];
            var lookup = {};
            var lookup1 = {};

            for (var i = 0; i < timeSl.length; i++) {
                inc = autoId + i;

                jsArr.push({
                    _id: inc,
                    businessId: req.body.businessId,
                    artistId: req.body.artistId,
                    staffId: req.body.staffId,
                    serviceId: timeSl[i].serviceId,
                    subserviceId:timeSl[i].subserviceId,
                    artistServiceId:timeSl[i].artistServiceId,
                    inCallPrice: timeSl[i].inCallPrice,
                    outCallPrice: timeSl[i].outCallPrice,
                    completionTime:timeSl[i].completionTime,
                    status:1
                });


                var name = timeSl[i].serviceName;
                var subId = timeSl[i].subserviceId;
                if (!(name in lookup)) {
                    lookup[name] = 1;
                    data.push({serviceName:timeSl[i].serviceName,serviceId:timeSl[i].serviceId});
                }

                if (!(subId in lookup1)) {
                    lookup1[subId] = 1;
                    result1.push({serviceId:timeSl[i].serviceId,subServiceId:subId,subServiceName:timeSl[i].subServiceName});
                }

            }
            var query = staffBussinesType.findOne().sort([['_id', 'descending']]);
            query.exec(function(err, ser) 
                {  
                 a = ser ? Number(ser._id )+ Number(1) :1;
                for (var i = 0; i < data.length; i++) {

                    data[i]._id = a+i;
                    data[i].artistId = req.body.artistId;
                    data[i].businessId = req.body.businessId;

                }
                staffCategory.findOne().sort([['_id', 'descending']]).exec(function(err, sub){ 

                    a = sub ? Number(sub._id )+ Number(1) :1;
                    for (var i = 0; i < result1.length; i++) {

                        result1[i]._id = a+i;
                        result1[i].artistId = req.body.artistId;
                        result1[i].businessId = req.body.businessId;

                    }
                    staffCategory.insertMany(result1);

                    staffBussinesType.insertMany(data);

                    staffService.insertMany(jsArr);
                    res.json({status: "success", message:'ok',staffServices:jsArr});
                    return;
                });
            });

           
        });

}


exports.staffInformation = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    search   = {};
    search['businessId'] = Number(req.body.businessId);
    search['artistId']   = Number(req.body.artistId);
    async.parallel([

   function(callback) {
        var query =  staff.aggregate([

            {
                "$lookup": {
                   "from": "holidays",
                   "localField": "_id",
                   "foreignField": "staffMainId",
                   "as": "holidayGiven"
                }
            },
            { 
               $match:search 
            },  
            { 
                "$project": {
                    "_id": 1,
                    "businessId": 1,
                    "staffHours": 1,
                    "job":1,
                    "mediaAccess":1,
                    "holiday":1,
                    "staffInfo":1,
                    "staffServiceId":1,
                    "serviceType":1,
                    "bookingType":1,
                    "status":1,
                    "artistId":1,
                    "salaries":1,
                    "message":1,
                    "holidayGiven.holidayGiven": 1            

                }
            }
        ])
        query.exec(function(err, ser) {
            if (err) {
                callback(err);
            }
 
            callback(null, ser);
        });
    },
    function(callback) {
        var query = staffService.find(search)
        query.exec(function(err, se) {
            if (err) {
                callback(err);
            }
 
            callback(null, se);
        });
    },
    function(callback) {
        var query = artistservices.find({'artistId':Number(req.body.businessId),'status':1,'deleteStatus':1},{'_id':1,'subserviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1})
        query.exec(function(err, s) {
            if (err) {
                callback(err);
            }
 
            callback(null, s);
        });
    }],
    function(err, results) {

        staffInfo = [];

        if(results[0].length>0){

            results[0][0].staffInfo.staffId = Number(req.body.artistId);     
            results[0][0].staffInfo.profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][0].staffInfo.profileImage;
            jsArrStaffService =[];
            for (var i = 0;i<results[1].length;i++) {  
                 
                for (var r = 0; r < results[2].length; r++) {
                    if (results[1][i].artistServiceId == results[2][r]._id) {
                       
                        results[1][i].title = results[2][r].title;
                    }

                }

                jsArrStaffService.push({
                    _id:results[1][i]._id,
                    artistId:results[1][i].artistId,
                    businessId:results[1][i].businessId,
                    serviceId:results[1][i].serviceId,
                    subserviceId:results[1][i].subserviceId,
                    artistServiceId:results[1][i].artistServiceId,
                    inCallPrice:results[1][i].inCallPrice,
                    outCallPrice:results[1][i].outCallPrice,
                    title:results[1][i].title,
                    completionTime:results[1][i].completionTime
                    
                });



            }
            results[0][0].staffService =jsArrStaffService;   
            res.json({status: "success", message:"ok",staffDetails:results[0]});
            return;

        }else{

            res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});

        }

    });

}


exports.artistStaff = function(req,res){

    var baseUrl =  req.protocol + '://'+req.headers['host'];
    search = {};
    search['businessId'] = authData._id; 
    search['staffInfo.userName'] = {$regex:req.body.search,$options:'i'};

    staffInfo = [];
    console.log(search);

    var query = staff.aggregate([

        { 

            $sort:{'upd':-1}

        },

        {
            "$lookup": {
               "from": "holidays",
               "localField": "_id",
               "foreignField": "staffMainId",
               "as": "holidayGiven"
            }
        },

        {
            "$lookup": {
               "from": "users",
               "localField": "artistId",
               "foreignField": "_id",
               "as": "userInfo"
            }
        },
        { 
            $match:search
        },
        {
            "$project": {
                "artistId":1,
                "staffInfo":1,
                "job":1,
                "status":1,
                "holiday":1,
                "holiday":1,
                "salaries":1,
                "message":1,
                "businessId":1,
                "holidayGiven.holidayGiven": 1,            
                "userInfo.businessType": 1            
               
            }
        }

    ]);

    query.exec(function(err, bdata) {

        console.log(bdata);
        
        if(bdata.length>0){

            for(var i=0;i< bdata.length; i++){

                staffInfo.push({

                    '_id'            : bdata[i]._id, 
                    'staffId'        : bdata[i].artistId, 
                    'staffName'      : bdata[i].staffInfo.userName, 
                    'businessType'   : bdata[i].userInfo[0].businessType, 
                    'job'            : bdata[i].job, 
                    'status'         : bdata[i].status, 
                    'holiday'        : bdata[i].holiday, 
                    'salaries'       : bdata[i].salaries, 
                    'message'        : bdata[i].message, 
                    'staffImage'     : commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ bdata[i].staffInfo.profileImage,
                    'holidayGiven'   : bdata[i].holidayGiven
                    
                }); 


            }
        
            res.json({status: "success",message: 'ok',staffList: staffInfo});

        }else{

          res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});

        }
    });
}


exports.deleteStaff = function(req,res){

    if(req.body.businessId==''){
        res.json({status:'fail',message: commonLang.REQ_BUSINESS_ID});
        return;
    
    }
    if(req.body.staffId==''){
        res.json({status:'fail',message: commonLang.REQ_STAFF_ID});
        return;
     
    }
    
    search ={};
    
    search['artistId']=Number(req.body.businessId);
    search['staff']=Number(req.body.staffId);
    search['bookingStatus']=1;

    bookingService.find(search).count().exec(function(err,count){
      if(count==0){
          staff.deleteOne({'businessId':req.body.businessId,'artistId':req.body.staffId}, function(err, results){
            if(err) throw err;
                staffService.deleteMany({'businessId':req.body.businessId,'artistId':req.body.staffId}, function(err, results){ });
                staffBussinesType.deleteMany({'businessId':req.body.businessId,'artistId':req.body.staffId},function(err, docs) { });
                staffCategory.deleteMany({'businessId':req.body.businessId,'artistId':req.body.staffId},function(err, docs) { });
                holiday.deleteMany({'businessId':req.body.businessId,'staffId':req.body.staffId},function(err, docs) { });
                staff.find({'businessId':authData._id}).exec(function(err, sdata) {
                    if(sdata.length==0){
                        User.updateOne({_id:authData._id},{$set:{businessType:"independent"}}, function(err, docs) {});
                    }
                    res.json({status:"success",message: commonLang.SUCCESS_REMOVE_STAFF});
                });
          });
        }else{
          res.json({status:"fail",message: commonLang.BOOKED_STAFF_SERVICE});
        }
    });

}


exports.deleteStaffService = function(req,res){
    if(req.body.addServiceId){
        staffService.deleteOne({'_id':req.body.addServiceId}, function(err, results){
        if(err) throw err;

        res.json({status:"success",message: commonLang.REMOVE_STAFF_SERVICE});
        });
    }else{
        res.json({status:"fail",message: commonLang.REQ_ADD_SERVICE_ID});
    }

}


exports.invitationUpdate = function(req,res){

    var id = req.body.id;
    var type = req.body.type;
    if(id==""){
      
        res.json({status:"fail",message: commonLang.REQ_ID});
        return;
    }

    if(type==""){
      
        res.json({status:"fail",message: commonLang.REQ_TYPE_FIELD});
        return;
    }

    if(authData.businessType=="business"){
      
        res.json({status:"fail",message: commonLang.NOT_ABLE_TO_ACCEPT_STAFF});
        return;
    }
    
    addNotification.deleteMany({'notifyId':authData._id,'notifincationType':'17','receiverId':authData._id,'type':'booking'},function(err, results) {});

    if(type=="accept"){

        staff.findOne({'_id':Number(id)}).exec(function(err, userdata) {


            staff.updateOne({_id:Number(id)},{$set: {status:1}},function(err, docs) {

                staffService.updateOne({staffId:Number(id)},{$set: {status:1}},function(err, docs) {

                    User.updateOne({_id:authData._id},{$set: {'userType':'artist','businessType':'independent'}},function(err, docs) {
                    User.updateOne({_id: userdata.businessId},{$set:{businessType:"business"}}, function(err, docs) {});

                        staff.deleteMany({artistId:userdata.businessId,'status':0},function(err, docs) {

                            staffService.deleteMany({artistId:userdata.businessId,'status':0},function(err, docs) {
                                    
                                var typ = '18';
                                var sender     = userdata.artistId;
                                var receiver   = userdata.businessId; 
                                var notifyId   = userdata.businessId;
                                var notifyType = 'booking';          
                                notify.notificationUser(sender,receiver,typ,notifyId,notifyType); 
                                res.json({status:"success",message: commonLang.SUCCESS_INVITATION});
                                return;


                            });
                        });
                       
                    });
                });

            });
        });

    }else{

        staff.deleteMany({_id:Number(id)},function(err, docs) {
            staffService.deleteMany({staffId:Number(id)},function(err, docs) {
                staffBussinesType.deleteMany({staffId:Number(id)},function(err, docs) {
                    staffCategory.deleteMany({staffId:Number(id)},function(err, docs) {
                        res.json({status:"success",message: commonLang.REJECT_INVITATION});
                        return;
                    });
                });
            });
        });


    }

}


function sendInvitationEmail(baseUrl,email){

    var template = process.cwd() + '/app/templates/staffInvitation.ejs';
    var templateData = {
        userName : authData.businessName,
        baseUrl : baseUrl
    };


    ejs.renderFile(template, templateData, 'utf8', function(err, file) {

        if (err) {
        /*    res.json({
                'status': "fail",
                "message": err
            });*/
        } else {

            var smtpTransport = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                service: "Gmail",
                secureConnection: true,
                auth: {
                    user: "sunil.mindiii@gmail.com",
                    pass: "sunil123456"
                }
            });
            var mailOptions = {
                to: email,
                subject: 'Staff invitation',
                html: file,
                from: '"Koobi App" <appkoobi@gmail.com>',

            }

            smtpTransport.sendMail(mailOptions, function(error, response) {

   
                if (error) {
                   /* res.json({
                        'status': "fail",
                        "message": error
                    });*/
                }
            });
        }
    });

}


exports.staffServicesDetails =function(req,res){

    if(req.body.staffId ==''){
        res.json({'status': "fail","message": commonLang.REQ_STAFF_ID});
        return;
    } 

    if(req.body.businessId ==''){
        res.json({'status': "fail","message": commonLang.REQ_BUSINESS_ID});
        return;
    } 
    async.parallel([

       function(callback) {
            var query = staffBussinesType.find({'staffId':Number(req.body.staffId)},{'serviceId':1,'serviceName':1})
            query.exec(function(err, ser) {
                if (err) {
                    callback(err);
                }
                callback(null, ser);
            });
        },
        function(callback) {
            var query = staffCategory.find({'staffId':Number(req.body.staffId)})
            query.exec(function(err, sub) {
                if (err) {
                    callback(err);
                }
     
                callback(null, sub);
            });
        },
        function(callback) {
            var query = staffService.find({'staffId':Number(req.body.staffId),'status':1,'deleteStatus':1})
            query.exec(function(err, s) {
                if (err) {
                    callback(err);
                }
     
                callback(null, s);
            });
        },
        function(callback) {
            var query = artistservices.find({'artistId':Number(req.body.businessId),'status':1,'deleteStatus':1},{'_id':1,'subserviceId':1,'title':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'description':1})
            query.exec(function(err, artistser) {
                if (err) {
                    callback(err);
                }
                callback(null, artistser);
            });
        }
     
    ],
     
    //Compute all results
    function(err, results) {
        
        console.log(req.body);
        console.log(err);
        console.log(results);

         if(results[0]){
         serArr =[];
        
        for(var i = 0; i < results[0].length; i++) {
            subArr =[];
            for(var j=0;j<results[1].length;j++){
               if(results[0][i].serviceId == results[1][j].serviceId){
          
                    subArr.push({
                                _id: results[1][j]._id,
                                 serviceId: results[1][j].serviceId,
                                 subServiceId: results[1][j].subServiceId,
                                 subServiceName: results[1][j].subServiceName
                            });
                }
           }
           serArr.push({
                serviceId:results[0][i].serviceId,
                serviceName:results[0][i].serviceName,
                subServies:subArr
            }); 
        }
        if(serArr){

            for (var s = 0; s < serArr.length; s++) {
                 
                for (var k = 0; k < serArr[s].subServies.length; k++) {

                    jsArr = [];

                    for (var r = 0; r < results[2].length; r++) {

                        if (serArr[s].subServies[k].subServiceId == results[2][r].subserviceId) {

                            var picked = lodash.filter(results[3], { '_id': results[2][r].artistServiceId} );

                            if(picked.length){

                                jsArr.push({
                                    _id: results[2][r].artistServiceId,
                                    title : picked[0].title,
                                    description: picked[0].description,
                                    inCallPrice: parseFloat(results[2][r].inCallPrice),
                                    outCallPrice: parseFloat(results[2][r].outCallPrice),
                                    completionTime: results[2][r].completionTime
                                    });
                            }

                        }
                    }
                    serArr[s].subServies[k].artistservices = jsArr;
                }
            }

        } 
        
      
         res.json({status: "success",message: 'ok',artistServices: serArr});
           return;
        
    }else{
       res.json({status: "fail",message: commonLang.RECORD_NOT_FOUND});
           return;
    }
            
           
    });

}


exports.artistInfo = function(req, res,next) {

    businessId  = req.body.businessId ? Number(req.body.businessId) : authData._id;
    baseUrl =  req.protocol + '://'+req.headers['host'];
    search ={};
    search['_id'] = businessId;
    search['status'] ='1';
    User.aggregate( [

      { $match :search },
      {  $project: {
         _id:1,
         firstName:1,
         lastName:1,
         userName:1,
         profileImage:1,
         businessName:1,
         businesspostalCode:1,
         buildingNumber:1,
         businessType:1,
         email:1,
         dob:1,
         gender:1,
         address:1,
         address2:1,
         countryCode:1,
         contactNo:1,
         userType:1,
         followersCount:1,
         followingCount:1,
         serviceCount:1,
         certificateCount:1,
         postCount:1,
         reviewCount:1,
         ratingCount:1,
         bio:1,
         serviceType:1,
         radius:1
         }
      },
      
    ],function(err,jsArr){

       if(jsArr!=''){
            if(jsArr[0].profileImage){
                jsArr[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+jsArr[0].profileImage;             
            }
            userData = jsArr[0];
            next();
        }

    });
}


exports.holidayStaff = function(req,res,next){

    businessId  = req.body.businessId ? Number(req.body.businessId) : '';
    let date  = req.body.date ? req.body.date : '';
    a = [];

    if(date){

        hWhere = {
            'businessId' : businessId,
             'startDate':{$lte: date},
             'endDate':{$gte: date}
        };

        holiday.find(hWhere,{'staffId':1}).sort([['upd', 'descending']]).exec(function(err, hdata) {

            if(hdata.length>0){

                a = hdata.map(a => a.staffId);

            }
            next();
        
        });

    }else{

        next();
    }

}


exports.serviceStaff = function(req,res){

    baseUrl =  req.protocol + '://'+req.headers['host'];
    
    search = {};
    let day  = req.body.day ? req.body.day : '';
    let staffId  = req.body.staffId ? req.body.staffId : '';

    search['businessId'] = Number(businessId);
    serviceId = req.body.artistServiceId;
    staffServiceId = [serviceId];
    search['staffServiceId']   = {'$in':staffServiceId};
    search['staffSer.artistServiceId']   = Number(serviceId);
    search['status']   = 1;

    if(day){
        search['staffHours.day']   = Number(day);


    }

    if(a.length>0){

        search['artistId']   = {'$nin':a};

    }


    staffInfo = [];
    artistservices.findOne(
        {
            '_id':serviceId
        },
        {
            '_id':1,
            'title':1,
            'description':1,
            'inCallPrice':1,
            'outCallPrice':1,
            'completionTime':1,
            'serviceId':1,
            'subserviceId':1,
            'artistId':1
        }).sort([['upd', 'descending']]).exec(function(err, sdata) {
        let query =  staff.aggregate([


            {  
                $lookup:{
                    from: "staffservices", 
                    localField: "_id", 
                    foreignField: "staffId",
                    as: "staffSer"
                }
            },
            { 
               $match:search 
            },
          
            { 
                "$project": {
                    "_id": 1,
                    "businessId": 1,
                    "artistId": 1,
                    "staffHours": 1,
                    "job":1,
                    "mediaAccess":1,
                    "holiday":1,
                    "staffInfo":1,
                    "staffServiceId":1,
                    "serviceType":1,
                    "status":1,
                    "staffHours":1,
                    "staffSer.artistServiceId":1,
                    "staffSer.inCallPrice":1,
                    "staffSer.outCallPrice":1,
                    "staffSer.completionTime":1,
                    "staffSer.artistServiceId":1,
                    "inCallPrice": { "$arrayElemAt": [ "$staffSer.inCallPrice",0] },
                    "outCallPrice": { "$arrayElemAt": [ "$staffSer.outCallPrice",0] },
                    "completionTime": { "$arrayElemAt": [ "$staffSer.completionTime",0] },
                }
            }
        ]);

        query.exec(function(err, bdata) {


            abookingType = "Both";

            if(parseFloat(sdata.inCallPrice) != 0.00 && parseFloat(sdata.outCallPrice) != 0.00){
             abookingType = "Both";
            }else if(parseFloat(sdata.inCallPrice) != 0.00 && parseFloat(sdata.outCallPrice) == 0.00){
             abookingType = "Incall";
            }else if(parseFloat(sdata.inCallPrice) == 0.00 && parseFloat(sdata.outCallPrice) != 0.00){
             abookingType = "Outcall";
            }
            // code comment  for business as staff
            // staffInfo.push({
            //     '_id':'', 
            //     'staffId':userData._id, 
            //     'staffName':userData.userName, 
            //     'job':'', 
            //     'status':0, 
            //     'inCallPrice':parseFloat(sdata.inCallPrice), 
            //     'outCallPrice':parseFloat(sdata.outCallPrice), 
            //     'completionTime':sdata.completionTime,
            //     'staffHours':[],
            //     'staffImage':userData.profileImage,
            //     'bookingType' : abookingType,
            //     'isSelected' : (staffId==userData._id) ? true : false,
            //  }); 


           if(bdata.length>0){


            for(var i=0;i< bdata.length; i++){

                var service = bdata[i].staffSer;


               var picked = lodash.filter(service, { 'artistServiceId': Number(serviceId)} );

               if(picked.length){
                    bdata[i].inCallPrice    = picked[0].inCallPrice;
                    bdata[i].outCallPrice   = picked[0].outCallPrice;
                    bdata[i].completionTime = picked[0].completionTime;
               }
               
               bookingType = "Both";

                if(parseFloat(bdata[i].inCallPrice) != 0.00 && parseFloat(bdata[i].outCallPrice) != 0.00){
                 bookingType = "Both";
                }else if(parseFloat(bdata[i].inCallPrice) != 0.00 && parseFloat(bdata[i].outCallPrice) == 0.00){
                 bookingType = "Incall";
                }else if(parseFloat(bdata[i].inCallPrice) == 0.00 && parseFloat(bdata[i].outCallPrice) != 0.00){
                 bookingType = "Outcall";
                }

                 staffInfo.push({
                    '_id':bdata[i]._id, 
                    'staffId':bdata[i].artistId, 
                    'staffName':bdata[i].staffInfo.userName, 
                    'job':bdata[i].job, 
                    'status':bdata[i].status, 
                    'inCallPrice':parseFloat(bdata[i].inCallPrice), 
                    'outCallPrice':parseFloat(bdata[i].outCallPrice), 
                    'completionTime':bdata[i].completionTime,
                    'staffHours':bdata[i].staffHours,
                    'staffImage':commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ bdata[i].staffInfo.profileImage,
                    'bookingType' : bookingType,
                    'isSelected' : (staffId) ? (businessId==bdata[i].businessId && staffId==bdata[i].artistId) ? true : false : false,
                 }); 

            }

           }

           res.json({status: "success",message: 'ok',serviceInfo: sdata,staffInfo:staffInfo});

        });
    });
}


exports.addHoliday = function(req, res) {

    let staffId       = req.body.staffId; 
    let holidayGiven  = req.body.holidayGiven; 
    let startDate     = req.body.startDate; 
    let endDate       = req.body.endDate; 
    let reason        = req.body.reason; 
    let myId          = req.body.myId; 
    let crdDate       = req.body.crdDate; 
    let staffMainId       = req.body.staffMainId; 
    var jsArr = [];

    if(staffId==""){
        res.json({'status':'fail','message': commonLang.REQ_STAFF_ID});
        return;
    }

    if(holidayGiven==""){
        res.json({'status':'fail','message': commonLang.REQ_HOLIDAY_GIVEN});
        return;
    }

    if(startDate==""){
        res.json({'status':'fail','message': commonLang.REQ_START_DATE});
        return;
    }

    if(endDate==""){
        res.json({'status':'fail','message': commonLang.REQ_END_DATE});
        return;
    }

    if(reason==""){
        res.json({'status':'fail','message': commonLang.REQ_REASON});
        return;
    }

    if(myId==""){
        res.json({'status':'fail','message': commonLang.REQ_MY_ID});
        return;
    }

    if(crdDate==""){
        res.json({'status':'fail','message': commonLang.REQ_CRD_DATE});
        return;
    }


    holiday.findOne().sort([['_id', 'descending']]).exec(function(err, userdata) {


        jsArr.push({
            _id: (userdata) ? Number(userdata._id)+1 : 1,
            holidayGiven: holidayGiven,
            startDate: startDate,
            staffId: staffId,
            endDate: endDate,
            reason: reason,
            businessId:myId,
            staffMainId:staffMainId,
            crd:crdDate,
            upd:crdDate,
        });

        var type = '24';
        var sender     = myId;
        var receiver   = staffId; 
        var notifyId   = staffId;
        var notifyType = 'booking';

        startDate =  moment(startDate).format('DD/MM/YYYY');
        endDate =  moment(endDate).format('DD/MM/YYYY');
        notify.notificationUser(sender,receiver,type,notifyId,notifyType,'','',startDate,endDate);         

        holiday.insertMany(jsArr);
        res.json({'status': "success","message": commonLang.SUCCESS_ADD_HOLIDAY,'data':jsArr});
        return;
    });
}


exports.holidayList = function(req,res){

    let myId      = req.body.myId; 
    let staffId      = req.body.staffId; 
    var jsArr = [];

    if(myId==""){
        res.json({'status':'fail','message': commonLang.REQ_MY_ID});
        return;
    }

    var baseUrl =  req.protocol + '://'+req.headers['host'];

    search = {};
    search['businessId']  = Number(myId);
    if(staffId){
        search['staffId']  = Number(staffId);
    }
     var query = holiday.aggregate([

            {
                "$lookup": {
                   "from": "users",
                   "localField": "staffId",
                   "foreignField": "_id",
                   "as": "staffInfo"
                }
            },
            {
                "$lookup": {
                   "from": "staffs",
                   "localField": "staffMainId",
                   "foreignField": "_id",
                   "as": "staff"
                }
            },
            { 
                $match:search
            },
            {
                "$project": {
                    "_id":1,
                    "startDate":1,
                    "endDate":1,
                    "holiday":1,
                    "crd":1,
                    "reason":1,
                    "holidayGiven":1,
                    "staffId":1,
                    "businessId":1,
                    "staffInfo._id": 1,
                    "staffInfo.userName": 1,
                    "staffInfo.profileImage":1,                     
                    "staff.holiday":1                     
                   
                }
            }

        ]);
        query.exec(function(err, bdata) {


        
            if(bdata.length!=0){

                for (var i = 0; i < bdata.length; i++) {

                    bdata[i].holiday = (bdata[i].staff.length!=0) ?  bdata[i].staff[0].holiday : 0;

                    if(bdata[i].staffInfo[0].profileImage){
                    
                        bdata[i].staffInfo[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ bdata[i].staffInfo[0].profileImage; 
                    }
                    
                }
                  
                res.json({status: "success",message: 'ok',data: bdata});

            }else{

                res.json({status:"fail",message: commonLang.RECORD_NOT_FOUND});

            }

        });
}


exports.staffServices = function(req,res){

    baseUrl =  req.protocol + '://'+req.headers['host'];
    serviceId = req.body.artistServiceId;

    if(serviceId==""){

        res.json({'status':'fail','message': commonLang.REQ_ARTIST_SERVICE_ID});
        return;
    }

  
    search = {};
    search['businessId'] = req.body.businessId ? Number(req.body.businessId) : authData._id;
    staffServiceId = [serviceId];
    search['staffServiceId']   = {'$in':staffServiceId};
    search['staffSer.artistServiceId']   = Number(serviceId);
    search['status']   = 1;
    staffInfo = [];

    artistservices.findOne({'_id':serviceId},{'_id':1,'title':1,'description':1,'inCallPrice':1,'outCallPrice':1,'completionTime':1,'serviceId':1,'subserviceId':1,'artistId':1}).sort([['upd', 'descending']]).exec(function(err, sdata) {
        let query =  staff.aggregate([


            {  
                $lookup:{
                    from: "staffservices", 
                    localField: "_id", 
                    foreignField: "staffId",
                    as: "staffSer"
                }
            },
            { 
               $match:search 
            },
          
            { 
                "$project": {
                    "_id": 1,
                    "businessId": 1,
                    "artistId": 1,
                    "staffHours": 1,
                    "job":1,
                    "mediaAccess":1,
                    "holiday":1,
                    "staffInfo":1,
                    "staffServiceId":1,
                    "serviceType":1,
                    "status":1,
                    "staffHours":1,
                    "staffSer._id":1,
                    "staffSer.artistServiceId":1,
                    "staffSer.inCallPrice":1,
                    "staffSer.outCallPrice":1,
                    "staffSer.completionTime":1,
                    "staffSer.artistServiceId":1,
                    "inCallPrice": { "$arrayElemAt": [ "$staffSer.inCallPrice",0] },
                    "outCallPrice": { "$arrayElemAt": [ "$staffSer.outCallPrice",0] },
                    "completionTime": { "$arrayElemAt": [ "$staffSer.completionTime",0] },
                }
            }
        ]);

        query.exec(function(err, bdata) {

            abookingType = "Both";

            if(parseFloat(sdata.inCallPrice) != 0.00 && parseFloat(sdata.outCallPrice) != 0.00){
             abookingType = "Both";
            }else if(parseFloat(sdata.inCallPrice) != 0.00 && parseFloat(sdata.outCallPrice) == 0.00){
             abookingType = "Incall";
            }else if(parseFloat(sdata.inCallPrice) == 0.00 && parseFloat(sdata.outCallPrice) != 0.00){
             abookingType = "Outcall";
            }
            
            // code comment on 15-05-20 discuss by team 
            // staffInfo.push({
            //     '_id':'', 
            //     'staffId':userData._id, 
            //     'staffName':userData.userName, 
            //     'job':'', 
            //     'status':0, 
            //     'inCallPrice':parseFloat(sdata.inCallPrice), 
            //     'outCallPrice':parseFloat(sdata.outCallPrice), 
            //     'completionTime':sdata.completionTime,
            //     'staffHours':[],
            //     'staffImage':userData.profileImage,
            //     'bookingType' : abookingType
            //  });    


           if(bdata.length>0){


            for(var i=0;i< bdata.length; i++){

                var service = bdata[i].staffSer;
                console.log('serviceservice',service);

               var picked = lodash.filter(service, { 'artistServiceId': Number(serviceId)} );

               if(picked.length){
                    bdata[i]['staffServicePrimaryID']    = picked[0]._id;    
                    bdata[i].inCallPrice    = picked[0].inCallPrice;
                    bdata[i].outCallPrice   = picked[0].outCallPrice;
                    bdata[i].completionTime = picked[0].completionTime;
               }
               
               bookingType = "Both";

                if(parseFloat(bdata[i].inCallPrice) != 0.00 && parseFloat(bdata[i].outCallPrice) != 0.00){
                 bookingType = "Both";
                }else if(parseFloat(bdata[i].inCallPrice) != 0.00 && parseFloat(bdata[i].outCallPrice) == 0.00){
                 bookingType = "Incall";
                }else if(parseFloat(bdata[i].inCallPrice) == 0.00 && parseFloat(bdata[i].outCallPrice) != 0.00){
                 bookingType = "Outcall";
                }


                staffInfo.push({
                    '_id':bdata[i]._id, 
                    'staffId':bdata[i].artistId, 
                    'staffName':bdata[i].staffInfo.userName, 
                    'job':bdata[i].job, 
                    'status':bdata[i].status, 
                    'staffServicePrimaryID':bdata[i].staffServicePrimaryID,
                    'inCallPrice':parseFloat(bdata[i].inCallPrice), 
                    'outCallPrice':parseFloat(bdata[i].outCallPrice), 
                    'completionTime':bdata[i].completionTime,
                    'staffHours':bdata[i].staffHours,
                    'staffImage':commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+ bdata[i].staffInfo.profileImage,
                    'bookingType' : bookingType
                }); 

            }

           }

           res.json({status: "success",message: 'ok',serviceInfo: sdata,staffInfo:staffInfo});

        });
    });
}
