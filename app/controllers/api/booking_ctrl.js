var constanPath = require('../../../config/envConstants.js');
var bookingService      =   require('../../models/front/bookingService.js');
var booking             =   require('../../models/front/booking.js');
var staff               =   require('../../models/front/staff_model.js');
var businessHour        =   require('../../models/front/businesshours.js');
var addNotification     =   require('../../models/front/notification.js');
var artistservices      =   require('../../models/front/artistService.js');
var artistMainService   =   require('../../models/front/artistMainService.js');
var async               =   require('async');
var moment              =   require('moment');
var User                =   require('../../models/front/home.js');//it user for table and coulamn information
var ratingReview        =   require('../../models/front/ratingReview_model');
var report              =   require('../../models/front/report_model');
var admin               =   require('../../models/admin/login_model');
var adminReminder       =   require('../../models/front/adminReminder_model');
var paymentDetail       = require('../../models/front/paymentDetail.js');
//var stripe              =   require('../front/stripe'); //comment why we call 
var stripe             =    require('stripe')(constanPath.STRIPE_KEY);
var commonConstants =   require('../../../config/commanConstants');
var lodash              =   require('lodash');
exports.bookArtist = function(req, res){
    //console.log('Aretist are',req.body);
   if(req.body.startTime==''){
       res.json({status: "fail",message: commonLang.REQ_START_TIME});
       return;
    }
    if(req.body.endTime==''){
        res.json({status: "fail",message: commonLang.REQ_END_TIME});
        return;

    }

    data = {};
    var staff = req.body.staff;
    var userId  = req.body.userId ? Number(req.body.userId) : authData._id;
    var bookingType  = req.body.bookingType ? req.body.bookingType : 'online';
    var endTimeBiz  = req.body.endTimeBiz ? req.body.endTimeBiz : '';
    data['artistId'] = req.body.artistId;
    staff1 = staff ? staff : 0; 
    if(staff){
        data['staff'] = staff;
    }
    data['serviceId']       = req.body.serviceId;
    data['subServiceId']    = req.body.subServiceId;
    data['artistServiceId'] = req.body.artistServiceId;
    data['bookingDate']     = req.body.bookingDate;
    data['startTime']       = req.body.startTime;
    data['endTime']         = req.body.endTime;
    data['userId']          = userId;
    data['serviceType']     = req.body.serviceType;
    data['bookingPrice']    = req.body.price;
    data['endTimeBiz']      = endTimeBiz;//change for new requirement

    if(req.body.bookingType){

        data['bookingType']     = req.body.bookingType;
    }

    data['timeCount']       = common.parseTime(common.timeConvert(req.body.startTime));
    BookingId               = req.body.bookingId;


    if(BookingId!='0' || BookingId!=0){

        bookingService.find({'artistId':data.artistId,'userId':userId,'bookingStatus':0,'bookingType':bookingType}).sort([['_id', 'descending']]).exec(function(err,brd) { 

                bookingService.findOne({'_id':BookingId}).sort([['_id', 'descending']]).exec(function(err,rd) { 

                if(rd){
     
                    if(rd.bookingDate==data.bookingDate && rd.startTime==data.startTime && data.endTime==rd.endTime && staff1==rd.staff){
                        res.json({'status':'success','message': commonLang.SERVICE_UPDATED,'bookingId':BookingId});
                        return;

                    }else{

                        if(rd.bookingDate>data.bookingDate || (brd[0].bookingDate>data.bookingDate)){
                            bookingService.updateMany({'_id':BookingId},{$set: data}, function(err, docs){  });
                            res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_SERVICE,'bookingId':BookingId});
                            return;


                        }else if(brd[0].bookingDate==data.bookingDate && common.parseTime(common.timeConvert(data.startTime))<=common.parseTime(common.timeConvert(brd[0].startTime))){
                            bookingService.updateMany({'_id':BookingId},{$set: data}, function(err, docs){  });
                            res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_SERVICE,'bookingId':BookingId});
                            return;


                        }else{

                            /**
                             *  Old code remove for edit service case
                             */

                            bookingService.updateMany({'_id':BookingId},{$set: data}, function(err, docs){  });

                            res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_SERVICE,'bookingId':BookingId});
                            return;

                            // bookingService.remove( {'_id':BookingId}, function(err, result) { });
                            // bookingService.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err,userdata) { 
                                    
                            //     var b = 1;
                            //     if(userdata){

                            //         b = userdata._id+1;
                            //     }
                            //     data['_id'] = b;

                               
                            //     bookingService.insertMany(data,function(err,my) {
                                                            
                            //     }); 

                            //     res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_SERVICE,'bookingId':data._id});
                            //     return;
                            // });
                        }


                    }
                }
                

            });

        });

    }else{


        bookingService.findOne({'userId':userId,'artistServiceId':data.artistServiceId,'bookingStatus':0,'bookingType':bookingType}).sort([['_id', 'descending']]).exec(function(err,red) { 
           
           /*code comment for new structure add more service*/
            // if(red){
                
            //     if(red.staff==staff ){
            //         res.json({'status':'fail','message':'Service already added','bookingId':red._id});
            //         return;

            //     }else{
            //          bookingService.updateMany({'_id':red._id},{$set: data}, function(err, docs){  });
            //          res.json({'status':'success','message': commonLang.SUCCESS_UPDATE_SERVICE,'bookingId':red._id});
            //          return;

            //     }


            // }else{
                bookingService.findOne({'bookingDate':data.bookingDate,'artistId':data.artistId,'startTime':data.startTime,'endTime':data.endTime,'staff':staff1,'bookingStatus':{$ne:2},'bookingType':bookingType}).sort([['_id', 'descending']]).exec(function(err,rd) { 
              

                    if(rd){
                        
                        res.json({'status':'fail','message': commonLang.TIME_SLOT_NOT_AVAIL});
                        return;

                    }else{
              

                        bookingService.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err,userdata) { 
                    
                        var b = 1;
                        if(userdata){

                            b = userdata._id+1;
                        }
                        data['_id'] = b;


                        bookingService.insertMany(data,function(err,my) {
                                                    
                        }); 
                        res.json({'status':'success','message': commonLang.SUCCESS_ADD_SERVICE,'bookingId':data._id});
                         return;
  
                        });
                    }
                });
         // }
        });
    }

}


exports.confirmBooking = function(req,res){

    let userId         = req.body.userId ? Number(req.body.userId) : authData._id;
    let artistId       = Number(req.body.artistId);
    let bookingDate    = req.body.bookingDate;
    let bookingTime    = req.body.bookingTime;
    let location       = req.body.location;
    let totalPrice     = req.body.totalPrice;
    let discountPrice  = req.body.discountPrice;
    let paymentType    = req.body.paymentType;
    let bookingType    = req.body.bookingType;
    let latitude       = req.body.latitude;
    let longitude      = req.body.longitude;
    let customerType   = req.body.customerType ? req.body.customerType : 'online';
    let adminCommision = req.body.adminCommision ? req.body.adminCommision : '';
    let adminAmount    = req.body.adminAmount ? req.body.adminAmount : '';
    let bookingSetting = req.body.bookingSetting ? req.body.bookingSetting : 0;
 
    if(artistId==''){
        res.json({status:'fail',message: commonLang.REQ_ARTIST_ID});
        return;
    }
    if(bookingDate==''){
        res.json({status:'fail',message: commonLang.REQ_BOOKING_DATE});
        return;
    }
    if(bookingTime==''){
        res.json({status:'fail',message: commonLang.REQ_BOOKING_TIME});
        return;
    }
    if(location==''){
        res.json({status:'fail',message: commonLang.REQ_LOCATION});
        return;
    }
    if(totalPrice==''){
        res.json({status:'fail',message: commonLang.REQ_TOT_PRICE});
        return;
    }
    if(paymentType==''){
        res.json({status:'fail',message: commonLang.REQ_PAYMENT_TYPE});
        return;
    }
    if(bookingType==''){
        res.json({status:'fail',message: commonLang.REQ_BOOKING_TYPE});
        return;
    }
    data = {};
    data['artistId']        = artistId;
    data['bookingDate']     = bookingDate;
    data['bookingTime']     = bookingTime;
    data['location']        = location;
    data['totalPrice']      = totalPrice;
    data['discountPrice']   = discountPrice;
    data['paymentType']     = paymentType;
    data['bookingType']     = bookingType;
    data['voucher']         = req.body.voucher ? JSON.parse(req.body.voucher) : '';
    data['timeCount']       = common.parseTime(common.timeConvert(req.body.bookingTime));
    data['userId']          = userId;
    data['latitude']        = latitude;
    data['longitude']       = longitude;
    data['adminCommision']  = adminCommision;
    data['adminAmount']     = adminAmount;
    data['upd']             = dateTime.localdate;
    data['createDate']      =  moment().format();
    
    if(bookingSetting==0){
        
        data['bookStatus']      = 1;
        data['paymentTime']     = dateTime.localdate;

    }
    if(customerType){  

        data['customerType']    = customerType;

        if(customerType=="walking"){  

            data['bookStatus']      = 1;
        }

        var s = {'userId':userId,'bookingType':customerType,'artistId':{'$ne': data['artistId'] },'bookingStatus':0};
    }

    bookingService.remove( s, function(err, result) { });
    
    getUserCardInfo(userId).then(async (userData)=>{
        let lastRequest = await getBookingLatID();
        
        if(userData){
           var customerId = userData.customerId;
           var token = userData.cardId;
        }
        var bookLastID = 1;
        if(data['voucher']){
          var p = Number(discountPrice)*100;
        }else{
          var p = Number(totalPrice)*100;
        }
        var  price = Number(p).toFixed(0);
        if(lastRequest){
            bookLastID = lastRequest._id+1;
        }
        data['_id'] = bookLastID;
        let newBookingData = new booking(data);
       
        sData = {'userId': (userId),'artistId':Number( data['artistId']),'bookingStatus':0,'bookingType':customerType};
        bookingSersetData =  {'bookingStatus':1,'bookingId':bookLastID};
        await updateBookingServiceStatus(sData,bookingSersetData);
          
        if((customerType=="online" || customerType=='walking') &&(data['paymentType']==2 || bookingSetting!=0)){ 
            
            return await newBookingData.save().then(async (record) => {
               let type = '1';
               let notifyId   = data['_id'];
               let notifyType = 'booking';
               
               if(customerType=="online"){

                 await callNotification(userId,artistId,type,notifyId,notifyType);
                 
               }
                res.json({'status':'success','message': commonLang.SUCCESS_ADD_BOOKING,'bookingId':data['_id'],'userData': userData});  
               // return record;
                    
            }).catch(function(error) {console.log('booking save error', error);});

        }else{
           
            if(data['paymentType']==1 && bookingSetting==0 ){
                return await newBookingData.save().then(async (record) => {
                var  price = Number(p).toFixed(0);
                strip_data = {
                    amount: price,
                    currency: "gbp",
                    source: token, // obtained with Stripe.js
                    description: "Charge for service booking"
                };
                strip_data.customer = customerId;

                return await chargeBySbripe(strip_data,newBookingData,res).then(async(charge) => {
                       tranId = charge.balance_transaction;
                        setTranj ={transjectionId:tranId,paymentStatus:1};
                        where3 = {'_id':newBookingData._id};
                        await updateBookingStatus(where3,setTranj);
                        lastPaymentId  =1;
                        let lastPayment =  await getPaymentDetailLatID();
                           if(lastPayment){
                           lastPaymentId = lastPayment._id+1;
                        }
                        paymentData = {_id: lastPaymentId,firstAdminPay:charge,bookingId:newBookingData._id};
                         let newpaymentDetail = new paymentDetail(paymentData);
                        return await newpaymentDetail.save().then(async (record) => {
                           //let type = '8';
                           let notifyId   = newBookingData._id;
                           let notifyType = 'booking'; 
                           await callNotification(userId,artistId,'29',notifyId,notifyType);//for social user
                            setTimeout(function () {
                             callNotification(artistId,userId,'30',notifyId,notifyType); // for biz user
                            },100); 

                           
                           res.json({'status':'success','message': commonLang.SUCCESS_ADD_BOOKING,'bookingId':newBookingData._id,'userData': userData});    
                           // return record;
                                
                        }).catch(function(error) {console.log('booking save error', error);});
              
                    }).catch(function(error) {
                     console.log('errorerrorerrorerror', error);
                    });
                  //return record;
                }).catch(function(error) {console.log('booking save error', error);});


            }

        }
         
    });

}

async function getUserCardInfo(userId){
    return await User.findOne({'_id':userId}).exec()
}
async function getBookingLatID(){
    return await booking.findOne().sort({ _id: -1 }).exec();
}
async function callNotification(userId,artistId,type,notifyId,notifyType){
    return await notify.notificationUser(userId,artistId,type,notifyId,notifyType); 
}
async function updateBookingServiceStatus(where,setData){
   return await bookingService.updateMany(where,{$set:setData}).exec();
    
}
async function updateBookingStatus(where,setData){
   return await booking.updateMany(where,{$set:setData}).exec();
    
}
async function getPaymentDetailLatID(){
    return await paymentDetail.findOne().sort({ _id: -1 }).exec();
}
async function chargeBySbripe(strip_data,newBookingData,res){
    return await stripe.charges.create(strip_data).then( async(charge) => {
       return charge;
    }).catch(function(err) {

             //console.log('new errror',err);
            where1 = {'_id':newBookingData._id};
             setInfo1 ={'bookStatus':2};
             where2 = {'bookingId':newBookingData._id};
             setInfo2 ={'bookingStatus':2};
             updateBookingStatus(where1,setInfo1);
             updateBookingServiceStatus(where2,setInfo2);
             res.json({'status':'fail','message': commonLang.BOOKING_NOT_ACCEPT});
            

    });

/*        if(charge){
            
            booking.updateMany({'_id':data['_id']},{$set: {transjectionId:tranId,paymentStatus:1}}, function(err, docs){

                paymentDetail.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err,userdata) { 

                   var id = userdata ? userdata._id+1 : 1;
                    jsArr = {_id: id,firstAdminPay:charge,bookingId: data['_id']};
                    paymentDetail.insertMany(jsArr,function(err,my) {});
                     var notifyId   = data['_id'];
                    var notifyTy = 'booking'; 
                    notifyType = '8';  
                    notify.notificationUser(req.body.artistId,authData._id,notifyType,notifyId,notifyTy,'',paymentType); 
                   //res.json({'status':'success','message':msg});  
                    //return;
                    res.json({'status':'success','message': commonLang.SUCCESS_ADD_BOOKING,'bookingId':data['_id'],'userData': userData});  
                    return;
                }); 
            });
        }
   */

}


exports.bookingInfo = function(req,res,next){
   //console.log('timeslot body',req.body);
    artistId        = Number(req.body.artistId);
    day             = Number(req.body.day);
    date            = req.body.date;
    userId          = req.body.userId ? Number(req.body.userId) : authData._id;
    type            = req.body.type;
    bookingId       = req.body.bookingId;
    staffId         = req.body.staffId;
    serviceTime     = req.body.serviceTime;
    currentTime     = req.body.currentTime;
    artistServiceId = req.body.artistServiceId;
    bookingType =     req.body.bookingType ? req.body.bookingType : 'online';

    if(artistId==""){
        res.json({'status':'fail','message': commonLang.REQ_ARTIST_ID});  
        return;
    }

    if(date==""){
        res.json({'status':'fail','message': commonLang.REQ_DATE});  
        return;
    }

    if(userId==""){
        res.json({'status':'fail','message': commonLang.REQ_USER_ID});  
        return;
    }
    
    if(serviceTime==""){
        res.json({'status':'fail','message': commonLang.REQ_SERVICE_TIME});  
        return;
    }

    bookInfo = {};
    bwhere = {'userId':userId,'artistId':artistId,'bookingStatus':0,'bookingType':bookingType};
    bookingService.findOne(bwhere).exec(function(err, bdata) {

        if(bdata){
            bookingService.findOne({'userId':userId,'artistId':artistId,'bookingStatus':0,'bookingType':bookingType},{'startTime':1,'bookingDate':1}).count().exec(function(err, bcdata) {
                bookingService.findOne({'userId':userId,'artistId':artistId,'bookingStatus':0,'artistServiceId':artistServiceId,'bookingType':bookingType}).exec(function(err, data) {
                    bookInfo.bookStartTime  =  '';
                    bookInfo.bookingDt      =  bdata.bookingDate;
                    bookInfo.totalBooking   =  bcdata; 
                    bookInfo.staff          =  bdata.staff; 
                    bookingId               =  0; 
                    type                    =  ""; 
                    if(data){
                    bookingId               =  data._id; 
                    bookInfo.type           =  "edit"; 
                  //comment for test hide booked slot when perform add more 21-10-20
                    //type                    =  "edit"; 
                    bookInfo.bookStartTime  =  data.startTime;
                    }
                    bookInfo =req.body;
                    next();
                });
            });
        }else{

            bookInfo =req.body;
            next();
        }
       
    });
 
}


exports.bookingTimeSlot = function(req, res) {
  

    datae = {};
    datae["artistId"] = artistId;
    datae["day"] = day;
    var newDate = date;
    var bookStaffId  =  (bookInfo.staff) ?   bookInfo.staff : '';

    if (date < dateTime.curDate) {
        res.json({'status': 'fail', 'message': commonLang.NOT_SELECT_PREVIOUS_DATE});
        return;
    }
    if (bookInfo.bookStartTime) {
        var BookingTime = bookInfo.bookStartTime;
    } else {
        var BookingTime = '';
    }
    if (bookInfo.bookingDt) {
        var bookingDate = bookInfo.bookingDt;
    } else {
        var bookingDate = '';
    }
    if (bookInfo.totalBooking) {
        var bookingCount = bookInfo.totalBooking;
    } else {
        var bookingCount = '';
    }



    if (dateTime) {
        curentTime = common.parseTime(common.timeConvert(dateTime.curTime));
    } else {
        curentTime = common.parseTime(common.timeConvert(currentTime));
    }
    serviceTime = serviceTime;
    //console.log('serviceTimeserviceTime',serviceTime);

    var bookingSTime = Array();
    var bookingETime = Array();
    // end under construction 
    staffSearch = {};
    if (staffId == ''|| staffId==0 || staffId=='0') {
        staffSearch["artistId"] = artistId;
    } else {
        staffSearch["artistId"] = staffId;
        staffSearch["businessId"] = artistId;

    }
    staffSearch["staffHours.day"] = day;
    staffSearch["status"] = 1;



    businessHour.find(datae).sort([['_id', 'ascending']]).exec(function(err, data) {
        //console.log('data', data)
        if (data) {

            var start_time = Array();
            var end_time = Array();
            var bussy_slot = Array();

            data.forEach(function(rs) {

                start_time.push(common.parseTime(common.timeConvert(rs.startTime)));
                end_time.push(common.parseTime(common.timeConvert(rs.endTime)));

            });
            //console.log('start_time', start_time)

            staff.find(staffSearch, {'staffHours.startTime': 1,'staffHours.endTime': 1,'staffHours.day': 1}).sort([['_id', 'ascending']]).exec(function(err, staffData) {
                
                var staff_start_time = Array();
                var staff_end_time = Array();

                if (staffData) {

                    staffData.forEach(function(rs1) {
                        sdt = rs1.staffHours;

                        sdt.forEach(function(rs) {
                            if (rs.day == day) {
                                staff_start_time.push(common.parseTime(common.timeConvert(rs.startTime)));
                                staff_end_time.push(common.parseTime(common.timeConvert(rs.endTime)));
                            }

                        });
                    });

                }
                // return res.send(staff_start_time);
                var bookData = {};
                bookData['artistId'] = artistId;
                bookData['bookingDate'] = date;
                bookData['bookingStatus'] = {
                    '$ne': 2
                }
   
                if (type == "edit") {
                                   
                    bookData['_id'] = {
                        '$ne': bookingId
                    };
                }else{
                    if (staffId) {
                        bookData['staff'] = Number(staffId);
                    }

                }
                bookingService.find(bookData).sort([['_id', 'ascending']]).exec(function(err, bdata) {
                    var t = 0;
                    if (type == "edit") {
                        var t = 10;
                    }
                    //console.log('bdata', bdata)
                    if (bdata) {
                        bdata.forEach(function(rs) {
                            bookingSTime.push(common.parseTime(common.timeConvert(rs.startTime)) + t);
                            bookingETime.push(common.parseTime(common.timeConvert(rs.endTime)));
                        });
                    }
                    bsSearch = {};
                    bsSearch['userId'] = userId;
                    bsSearch["artistId"] = artistId;
                    bsSearch["bookingStatus"] = 0;
                    bsSearch["bookingType"] = bookingType;
                    bsSearch['staff'] = Number(staffId);

                   var sortKey = 'descending';
                    if(type=="edit"){

                    var sortKey = 'ascending';

                    }
                    bookingService.find(bsSearch).sort([['_id',sortKey]]).limit(1).exec(function(err, adata) {

                        var interval = 10;
                        var AbookingTime = '';
                        var AbookingDate = '';
                      /*code comment new flow 21-10-20
                        if (adata.length > 0) {

                            var AbookingTime = common.parseTime(common.timeConvert(adata[0].endTime));
                            var AbookingDate = adata[0].bookingDate;

                       
                        }*/
                        if (type == "edit") {

                            var AbookingTime = '';
                            var AbookingDate = '';
                            if (bookingCount != 1 &&  adata.length != 0) {
                                if (bookingId != adata[0]._id) {
                                    if (adata.length > 0) {
                                        var AbookingTime = common.parseTime(common.timeConvert(adata[0].endTime));
                                        var AbookingDate = adata[0].bookingDate;

                                    }
                                }
                            }
                        }
                    
                        var Ntime_slots = Array();
                       //  console.log('bookingSTime',bookingSTime,'bookingETime',bookingETime,'interval',interval,'AbookingDate',AbookingDate,'date',date);
                        User.findOne({'_id' : artistId}, {'inCallpreprationTime':1,'outCallpreprationTime':1, '_id' :1}).exec(function(err, udata) {
                        if (AbookingDate <= date) { 

                            var bussy_slot = common.bookingTime(bookingSTime, bookingETime, interval);

                            /* code for new change 21-1020 */
                            var busy_slot_final = Array();
                           
                            /**
                             *  when atrist has no breake time 
                             */
                            
                                //console.log('serviceType is', req.body.serviceType);
                          
                            busy_slot_final = common.bookingStartTime(bookingSTime);
                           // console.log('busy_slot_final',busy_slot_final);
                            var finalBsy =  lodash.differenceBy(bussy_slot,busy_slot_final);
                            if(req.body.serviceType == 1 || req.body.serviceType == '1') {
                                if(udata.inCallpreprationTime == '' || udata.inCallpreprationTime ==  '00:00'){
                                    //console.log('icon')
                                finalBsy = bussy_slot ;
                            }
                            
                          }

                          if(req.body.serviceType == 2 || req.body.serviceType == '2') {
                              if(udata.outCallpreprationTime == '' || udata.outCallpreprationTime ==  '00:00'){
                                //console.log('2icon')
                                finalBsy = bussy_slot ;
                              }
                            
                        
                        }
                            //console.log('finalBsy',finalBsy);
                            /*end code*/
                            //console.log('busy slots',bussy_slot,'AbookingTime',AbookingTime);

                            if (staffId == '' || staffId==0 || staffId=='0') {
                              /*  var bussy_slot_staff = common.bookingTime(staff_start_time, staff_end_time, interval);
                                var bussy_slot = bussy_slot.concat(bussy_slot_staff);*/
                            } else {
                                start_time = staff_start_time;
                                end_time = staff_end_time;
                            }
                            var times_ara = common.calculate_time_slot(start_time, end_time, interval, date, finalBsy, AbookingTime, curentTime, AbookingDate, date);
                            var Ntime_slots = common.final_time_slot(times_ara, serviceTime);

                            //console.log('start_time',start_time,'end_time',end_time);
                            //console.log('times_ara',times_ara);
                        }

                        var selectData = Array();

                        if (type == "edit") {
 
                            staffId = staffId ? staffId : 0;
                            Ntime_slots.forEach(function(data) {

                                if (data == BookingTime && date == bookingDate && bookStaffId==staffId) {

                                    selectData.push(1);

                                } else {

                                    selectData.push(0);
                                }

                            });
                        }
 
                        res.json({'status': 'success','message': 'ok','timeSlots': Ntime_slots,selectData: selectData,bookingId:bookingId });
                        return;
                    });
                    });
                });
            });

        }


    });


}


exports.checkBooking = function(req,res,next){

   type     = req.body.type;
   newDate  = req.body.date;
   staffId  = req.body.staffId;
   artistId =  Number(req.body.artistId);
   loginId  =  req.body.userId ? Number(req.body.userId) : authData._id;

    if(type==""){

        res.json({status:"fail",message: commonLang.REQ_TYPE});
        return;
    }

    if(artistId==""){

        res.json({status:"fail",message: commonLang.REQ_ARTIST_ID});
        return;
    }

    if(newDate=="" && type!="Booking Request"){

        res.json({status:"fail",message: commonLang.REQ_DATE});
        return;
    }

   checkData = {}; 
   checkData["bookingDate"] = { $lte: dateTime.curDate};
   checkData['bookStatus']= '0';
   checkData['artistId']= artistId;
   serverTime = common.parseTime(common.timeConvert(dateTime.curTime));
   booking.aggregate([{$match:checkData}],function(err,userData){
     if(userData.length>0){
                       
               var i = 0;
              async.each(userData, function(element, callback){
                
                if (common.parseTime(common.timeConvert(userData[i].bookingTime)) <= serverTime) {
                     
                    booking.deleteMany({'_id':userData[i]._id},function(err, results) {});
                    addNotification.deleteMany({'notifyId':userData[i]._id,'type':'booking'},function(err, results) {});
                    bookingService.deleteMany({'bookingId':userData[i]._id},function(err, results) {});
                        
                }

                callback();
                    
            i++;
            },function(){
            
            });

    }
    next()   
   
  });            
  
}


exports.checkStaff = function(req,res,next){

    artistservices.find({artistId:loginId,status:1,deleteStatus:1}).exec(function(err, adata) {
        User.findOne({'_id':artistId}).exec(function(err, data) {

            userData = data;            
            serviceCount = adata.length;

            if(loginId!=artistId){



                    staff.findOne({'businessId':artistId,artistId:loginId}).exec(function(err, bdata) {
                        
                        if(bdata==null || bdata==''){

                            res.json({status:'fail',message: commonLang.NOT_EXIST_COMPANY,businessType:authData.businessType,userData:userData});
                            return;

                        }else{
                            next();
                        }
                    });


            }else{

                next();
            }
        });
    });

}


exports.bookingHistory = function(req,res){

    sortData = {};
    searchData = {};
    searchU = {};
    datae = {};

    let bookingType = req.body.bookingType ? req.body.bookingType : '';
    let search = req.body.search;
    let artistId = req.body.artistId;

   if(staffId!='' && staffId!='0'){

        searchData['bookingData.staff']= Number(staffId);
        datae['bookingData.staff']= Number(staffId);

        if(loginId==staffId){

            datae['bookingData.staff']= 0;
            searchData['bookingData.staff']= 0;
        }

        searchData['artistId']= Number(artistId);
        datae['artistId']= Number(artistId);

    }else{



        searchData['artistId']= Number(artistId);
        datae['artistId']= Number(artistId);

    }


    if(bookingType){

        searchData['bookingType']= Number(bookingType);
        datae['bookingType']= Number(bookingType);

    }

    if(loginId!=artistId){

        datae['bookingData.staff']      = loginId;
    }

    if(search){
      
        var searchU = {

            "$or": [{
                "userDetail.userName": {$regex:req.body.search,$options:'i'}
            }, {
                "client.firstName": {$regex:req.body.search,$options:'i'}
            }]

        };
    }

    switch (type){

            case 'Pending Booking':

                searchData['bookStatus']      = '1';
                datae['bookStatus']           = '1';
                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

            case 'Booking Request':

                searchData['bookStatus']      = '0';
                datae['bookStatus']           = '0';
                if(newDate){
                    datae['bookingDate']          = newDate;
                    searchData['bookingDate']     = newDate;
                }
            break;


            case 'Todays Booking':

                searchData['bookStatus']      = '1';
                datae['bookStatus']           = '1';
                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

            case 'Cancelled Booking':

                searchData['bookStatus']      = '2';
                datae['bookStatus']           = '2';
                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

            case 'All Booking':

                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

            case 'Confirm Booking':

                searchData['bookStatus']      = '1';
                datae['bookStatus']           = '1';
                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

            case 'Completed Booking':

                searchData['bookStatus']      = '3';
                datae['bookStatus']           = '3';
                searchData['paymentStatus']   = 1;
                datae['paymentStatus']        = 1;
                datae['bookingDate']          = newDate;
                searchData['bookingDate']     = newDate;

            break;

    }
  
    sortData["timeCount"] =  1;
    sortData["bookingDate"] = 1;
   var baseUrl =  req.protocol + '://'+req.headers['host'];


    async.parallel([

        function(callback) {

            var query = booking.aggregate([
              
                { 

                    $sort:sortData

                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "userDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "clients",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "client"
                    }
                },
                {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                {
                    $match: datae
                },
                {
                    $match: searchU
                },
                {
                    "$project": {
                        "_id": 1,
                        "bookingDate": 1,
                        "artistId": 1,
                        "userId": 1,
                        "bookingTime": 1,
                        "totalPrice": 1,
                        "paymentType": 1,
                        "paymentStatus": 1,
                        "location": 1,
                        "bookStatus": 1,
                        "timeCount":1,
                        "bookingType":1,
                        "paymentTime":1,
                        "customerType":1,
                        "upd":1,
                        "userDetail._id": 1,
                        "userDetail.userName": 1,
                        "userDetail.profileImage":1,
                        "client._id": 1,
                        "client.firstName": 1,
                        "client.lastName":1,
                        "client.phone":1
                       
                    }
                }

            ]);
            query.exec(function(err, g) {
                if (err) {
                    callback(err);
                }

                callback(null, g);
            });
        },

        function(callback) {
            var query = booking.aggregate([
               { 

                    $sort:sortData

               },
               {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                  { $unwind : "$bookingData" },
                       {
                            "$lookup": {
                                "from": "artistservices",
                                "localField": "bookingData.artistServiceId",
                                "foreignField": "_id",
                                "as": "artistService"
                            }
                        },
                        {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.staff",
                               "foreignField": "_id",
                               "as": "staffInfo"
                            }
                        },
                         {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.artistId",
                               "foreignField": "_id",
                               "as": "artistInfo"
                            }
                        },
                { $match:searchData},
                {
                    "$project": {
                        "_id":1,
                        "bookingData._id":1,
                        "bookingData.bookingPrice":1,
                        "bookingData.serviceId":1,
                        "bookingData.subServiceId":1,
                        "bookingData.artistServiceId":1,
                        "bookingData.bookingDate":1,
                        "bookingData.startTime":1,
                        "bookingData.endTime":1,
                        "bookingData.endTimeBiz":1,
                        "bookingData.staff":1,
                        "bookingData.status":1,
                        "artistService._id":1,
                        "artistService.title":1,
                        "staffInfo._id": 1,
                        "staffInfo.userName": 1,
                        "staffInfo.profileImage":1,
                        "artistInfo._id": 1,
                        "artistInfo.userName": 1,
                        "artistInfo.profileImage":1,
                        "artistInfo.businessName":1
                       
                    }
                }

            ]);
            query.exec(function(err, s) {
                if (err) {
                    callback(err);
                }

                callback(null, s);
            });
        },

    ],

    function(err, results) {
     
        if (results[0]) {
           for (var i = 0; i < results[0].length; i++) {
                
                results[0][i].creationDate =  moment(results[0][i].upd).format('DD/MM/YYYY');
                results[0][i].bookingDate =  moment(results[0][i].bookingDate).format('DD/MM/YYYY');
                results[0][i].creationTime =  moment(results[0][i].upd).format('hh:mm a');
                if(results[0][i].customerType=="online"){

                    if(results[0][i].userDetail[0].profileImage){
                        //results[0][i].userDetail[0].profileImage =  baseUrl+"/uploads/profile/"+results[0][i].userDetail[0].profileImage; 
                        results[0][i].userDetail[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].userDetail[0].profileImage;
                        
                                   
                    }

                }
                jsArr = [];
                for (var j = 0; j < results[1].length; j++) {
                   
                    if (results[0][i]._id == results[1][j]._id) {

                           
                            if(results[1][j].staffInfo ==0){
                                
                             
                               staffId       =  results[1][j].artistInfo[0]._id;
                               staffName     =  results[1][j].artistInfo[0].userName;
                               staffImage    =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                               companyId     =  results[1][j].artistInfo[0]._id;
                               companyName   =  results[1][j].artistInfo[0].businessName;
                               companyImage  =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                              
                            }else{
                                
                                if(results[1][j].staffInfo[0].profileImage){
                                   results[1][j].staffInfo[0].profileImage =commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].staffInfo[0].profileImage; 
                                }

                                staffId       =  results[1][j].bookingData.staff;
                                staffName     =  results[1][j].staffInfo[0].userName;
                                staffImage    =  results[1][j].staffInfo[0].profileImage;
                                companyId     =  results[1][j].artistInfo[0]._id;
                                companyName   =  results[1][j].artistInfo[0].businessName;
                                companyImage  = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                                    
                            }


                            if(results[1][j].bookingData.status!=0 &&  results[0][i].bookStatus == 1){
                                results[0][i].bookStatus =  5;

                            }

                            jsArr.push({
                                _id: results[1][j].bookingData._id,
                                bookingPrice: results[1][j].bookingData.bookingPrice,
                                serviceId: results[1][j].bookingData.serviceId,
                                subServiceId: results[1][j].bookingData.subServiceId,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                bookingDate: results[1][j].bookingData.bookingDate,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                startTime: results[1][j].bookingData.startTime,
                                endTime: results[1][j].bookingData.endTime,
                                endTimeBiz: results[1][j].bookingData.endTimeBiz,
                                status: results[1][j].bookingData.status,
                                artistServiceName: results[1][j].artistService[0].title,
                                staffId : staffId,
                                staffName : staffName,
                                staffImage : staffImage,
                                companyId : companyId,
                                companyName : companyName,
                                companyImage : companyImage,
                            });

                    }
                }
                results[0][i].bookingInfo = jsArr;
                    

            }    
         }
        res.json({status: "success",message: 'ok','data': results[0],'businessType':authData.businessType,'serviceCount':serviceCount,userData:userData,bookingRequestCount:bookingRequestCount});
        return;
        
    });


}


exports.bookingDetail = function(req,res){

    let bookingId = req.body.bookingId;    
    if(bookingId == ''){
       res.json({status: "fail",message: commonLang.REQ_BOOKING_ID});
       return;
    }
   searchData = {};
   sortData = {};  
   searchData['_id'] = Number(bookingId);
   sortData["timeCount"] = 1;
   
   var baseUrl =  req.protocol + '://'+req.headers['host'];
    async.parallel([

        function(callback) {
            var query = booking.aggregate([
                 
              
                { $sort:sortData},
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "userDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "artistId",
                       "foreignField": "_id",
                       "as": "artistDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "clients",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "client"
                    }
                },
                {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                {
                    $match: searchData
                },
                {
                    "$project": {
                        "_id": 1,
                        "bookingDate": 1,
                        "artistId": 1,
                        "userId": 1,
                        "bookingTime": 1,
                        "totalPrice": 1,
                        "paymentType": 1,
                        "transjectionId": 1,
                        "paymentStatus": 1,
                        "location": 1,
                        "bookStatus": 1,
                        "bookingType": 1,
                        "timeCount":1,
                        "voucher":1,
                        "discountPrice":1,
                        "customerType":1,
                        "paymentTime":1,
                        "latitude":1,
                        "longitude":1,
                        "reviewByUser":1,
                        "reviewByArtist":1,
                        "userRating":1,
                        "artistRating":1,
                        "userRatingCrd":1,
                        "artistRatingCrd":1,
                        "adminCommision":1,
                        "adminAmount":1,
                        "artistRatingCrd":1,
                        "staffInfo.profileImage":1,
                        "userDetail._id": 1,
                        "userDetail.userName": 1,
                        "userDetail.profileImage":1,
                        "userDetail.ratingCount":1,
                        "userDetail.countryCode":1,
                        "userDetail.contactNo":1,
                        "artistDetail._id": 1,
                        "artistDetail.userName": 1,
                        "artistDetail.profileImage":1,
                        "artistDetail.ratingCount":1,
                        "artistDetail.countryCode":1,
                        "artistDetail.contactNo":1,
                        "client._id": 1,
                        "client.firstName": 1,
                        "client.lastName":1,
                        "client.phone":1
                       
                    }
                }

            ]);
            query.exec(function(err, g) {

                if (err) {
                    callback(err);
                }

                callback(null, g);
            });
        },

        function(callback) {
            var query = booking.aggregate([
               { $sort:sortData},
               {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                  { $unwind : "$bookingData" },
                       {
                            "$lookup": {
                                "from": "artistservices",
                                "localField": "bookingData.artistServiceId",
                                "foreignField": "_id",
                                "as": "artistService"
                            }
                        },
                        {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.staff",
                               "foreignField": "_id",
                               "as": "staffInfo"
                            }
                        },
                        {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.artistId",
                               "foreignField": "_id",
                               "as": "artistInfo"
                            }
                        },
                        {
                            "$lookup": {
                               "from": "bookingreports",
                               "localField": "bookingData._id",
                               "foreignField": "bookingInfoId",
                               "as": "bookingReport"
                            }
                        },
               { $match:searchData},

                {
                    "$project": {
                        "_id":1,
                        "bookingData._id":1,
                        "bookingData.bookingPrice":1,
                        "bookingData.serviceId":1,
                        "bookingData.subServiceId":1,
                        "bookingData.artistServiceId":1,
                        "bookingData.bookingDate":1,
                        "bookingData.startTime":1,
                        "bookingData.endTime":1,
                        "bookingData.endTimeBiz":1,
                        "bookingData.staff":1,
                        "bookingData.status":1,
                        "artistService._id":1,
                        "artistService.title":1,
                        "staffInfo._id": 1,
                        "staffInfo.userName": 1,
                        "staffInfo.profileImage":1,
                        "staffInfo.trackingLatitude":1,
                        "staffInfo.trackingLongitude":1,
                        "staffInfo.trackingAddress":1,
                        "artistInfo._id": 1,
                        "artistInfo.userName": 1,
                        "artistInfo.profileImage":1,
                        "artistInfo.businessName":1,
                        "artistInfo.trackingLatitude":1,
                        "artistInfo.trackingLongitude":1,
                        "artistInfo.trackingAddress":1,
                        "bookingReport._id": 1,
                        "bookingReport.title": 1,
                        "bookingReport.description":1,
                        "bookingReport.crd":1,
                        "bookingReport.bookingInfoId":1,
                        "bookingReport.bookingId":1,
                        "bookingReport.businessId":1,
                        "bookingReport.reportByUser":1,
                        "bookingReport.reportForUser":1,
                        "bookingReport.status":1,
                        "bookingReport.adminReason":1,
                        "bookingReport.favored":1,
                       
                    }
                }

            ]);
            query.exec(function(err, s) {

                if (err) {
                    callback(err);
                }

                callback(null, s);
            });
        },

    ],

    function(err, results) {
         

        if (results[0]) {
           for (var i = 0; i < results[0].length; i++) {


                results[0][i].voucher =  (results[0][i].voucher!='' && typeof results[0][i].voucher != 'undefined' && results[0][i].voucher != null) ? results[0][i].voucher : {};
                
                if(results[0][i].customerType=="online"){
                    if(results[0][i].userDetail[0].profileImage){

                       // results[0][i].userDetail[0].profileImage =  baseUrl+"/uploads/profile/"+results[0][i].userDetail[0].profileImage;
                       results[0][i].userDetail[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].userDetail[0].profileImage;
 
                    }
                }
                
                if(results[0][i].artistDetail[0].profileImage){

                    //results[0][i].artistDetail[0].profileImage =  baseUrl+"/uploads/profile/"+results[0][i].artistDetail[0].profileImage;
                    results[0][i].artistDetail[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].artistDetail[0].profileImage;
 
                }

                jsArr = [];
                for (var j = 0; j < results[1].length; j++) {
                   
                    if (results[0][i]._id == results[1][j]._id) {

                           
                            
                            if(results[1][j].staffInfo ==0){
                                                              
                               staffId       =  results[1][j].artistInfo[0]._id;
                               staffName     = results[1][j].artistInfo[0].userName;
                               trackingLatitude     = results[1][j].artistInfo[0].trackingLatitude;
                               trackingLongitude     = results[1][j].artistInfo[0].trackingLongitude;
                               trackingAddress     = results[1][j].artistInfo[0].trackingAddress;
                               staffImage    = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                               companyId     = results[1][j].artistInfo[0]._id;
                               companyName   = results[1][j].artistInfo[0].businessName;
                               companyImage  =commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                              
                            }else{
                                
                                if(results[1][j].staffInfo[0].profileImage){
                                   results[1][j].staffInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].staffInfo[0].profileImage; 
                                }

                                staffId       =  results[1][j].bookingData.staff;
                                staffName     =  results[1][j].staffInfo[0].userName;
                                staffImage    =  results[1][j].staffInfo[0].profileImage;
                                trackingLatitude    =  results[1][j].staffInfo[0].trackingLatitude;
                                trackingLongitude    =  results[1][j].staffInfo[0].trackingLongitude;
                                trackingAddress    =  results[1][j].staffInfo[0].trackingAddress;
                                companyId     =  results[1][j].artistInfo[0]._id;
                                companyName   =  results[1][j].artistInfo[0].businessName;
                                companyImage  =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                                    
                            }

                            if(results[1][j].bookingData.status!=0 &&  results[0][i].bookStatus == 1){
                                results[0][i].bookStatus =  5;
                            }

                            jsArr.push({
                                _id: results[1][j].bookingData._id,
                                bookingPrice: results[1][j].bookingData.bookingPrice,
                                serviceId: results[1][j].bookingData.serviceId,
                                subServiceId: results[1][j].bookingData.subServiceId,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                bookingDate: results[1][j].bookingData.bookingDate,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                startTime: results[1][j].bookingData.startTime,
                                endTime: results[1][j].bookingData.endTime,
                                endTimeBiz: results[1][j].bookingData.endTimeBiz,
                                status: results[1][j].bookingData.status,
                                artistServiceName: results[1][j].artistService[0].title,
                                staffId : staffId,
                                staffName : staffName,
                                staffImage : staffImage,
                                trackingLatitude : trackingLatitude,
                                trackingLongitude : trackingLongitude,
                                trackingAddress : trackingAddress,
                                companyId : companyId,
                                companyName : companyName,
                                companyImage : companyImage,
                                bookingReport : results[1][j].bookingReport

                            });

                    }
                }


                results[0][i].bookingInfo = jsArr;
                    

            }    
        }
        res.json({status: "success",message: 'ok',data: results[0][0]});
        return;
        
    });


}


exports.bookServices = function(req,res){

    searchData = {};
    sortData = {};
    let artistId = req.body.artistId;
    let bookingType = req.body.bookingType ? req.body.bookingType : 'online';
    if(artistId==""){
       res.json({status: "fail",message: commonLang.REQ_ARTIST_ID});
       return;
    }  
    searchData['artistId']= Number(artistId);
    searchData['userId']= req.body.userId ? Number(req.body.userId) : authData._id;
    searchData['bookingStatus']= '0';  
    searchData['bookingId']= 0;  
    searchData['bookingType']= bookingType; 

   // sortData["timeCount"] = 1;
    sortData["_id"] = 1;
   var baseUrl =  req.protocol + '://'+req.headers['host'];

    User.findOne({'_id':searchData['userId']}).exec( async function(err, userData) {

         if(userData.location){
             userData.location = userData.location.coordinates;
         }
        
        var query = bookingService.aggregate([
               { 
                    $sort:sortData
               },
               {
                    "$lookup": {
                        "from": "artistservices",
                        "localField": "artistServiceId",
                        "foreignField": "_id",
                        "as": "artistService"
                    }
                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "staff",
                       "foreignField": "_id",
                       "as": "staffInfo"
                    }
                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "artistId",
                       "foreignField": "_id",
                       "as": "artistInfo"
                    }
                },
                {
                    $match:searchData
                },
                {
                    "$project": {
                        "_id":1,
                        "bookingPrice":1,
                        "serviceId":1,
                        "subServiceId":1,
                        "artistServiceId":1,
                        "bookingDate":1,
                        "startTime":1,
                        "endTime":1,
                        "staff":1,
                        "upd":1,
                        "artistService._id":1,
                        "artistService.title":1,
                        "staffInfo._id": 1,
                        "staffInfo.userName": 1,
                        "staffInfo.profileImage":1,
                        "artistInfo._id": 1,
                        "artistInfo.userName": 1,
                        "artistInfo.profileImage":1,
                        "artistInfo.businessName":1,
                        "artistInfo.businessType":1,
                        "artistInfo.address":1,
                        "artistInfo.bookingSetting":1,
                       
                    }
                }

        ]);

        query.exec(function(err, s) {


            if (s.length) {
                jsArr = [];

                for (var j = 0; j < s.length; j++) {
                   
                            
                    if(s[j].staffInfo.length==0){
                                        
                       staffId       =  s[j].artistInfo[0]._id;
                       staffName     = s[j].artistInfo[0].userName;
                       staffImage    = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+s[j].artistInfo[0].profileImage;
                      
                    }else{
                        
                        if(s[j].staffInfo[0].profileImage){
                           s[j].staffInfo[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+s[j].staffInfo[0].profileImage; 
                        }

                        staffId       =  s[j].staff;
                        staffName     =  s[j].staffInfo[0].userName;
                        staffImage    =  s[j].staffInfo[0].profileImage;
                           
                    }

                     jsArr.push({
                        _id: s[j]._id,
                        bookingPrice: s[j].bookingPrice,
                        serviceId: s[j].serviceId,
                        subServiceId: s[j].subServiceId,
                        artistServiceId: s[j].artistServiceId,
                        bookingDate: s[j].bookingDate,
                        artistServiceId: s[j].artistServiceId,
                        startTime: s[j].startTime,
                        endTime: s[j].endTime,
                        artistServiceName: s[j].artistService[0].title,
                        staffId : staffId,
                        staffName : staffName,
                        staffImage : staffImage,
                        companyId : s[j].artistInfo[0]._id,
                        companyName : s[j].artistInfo[0].businessName,
                        artistName : s[j].artistInfo[0].userName,
                        companyAddress : s[j].artistInfo[0].address,
                        bookingSetting : s[j].artistInfo[0].bookingSetting,
                        companyImage :  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+s[j].artistInfo[0].profileImage,
                    });

                    
                }
            }
            
            res.json({status: "success",message: 'ok',data: jsArr,userData:userData});
            return;
        });

    });
        
}


exports.bookingAction = function(req, res) {
    console.log('request parameter',req.body);

    var msg ='';
    type            =  req.body.type;
    bookingId       =  req.body.bookingId;
    serviceId       =  req.body.serviceId;
    subserviceId    =  req.body.subserviceId;
    userId          =  req.body.userId;
    artistId        =  authData._id;
    artistServiceId =  req.body.artistServiceId;
    paymentType     =  req.body.paymentType;
    customerType     =  req.body.customerType ? req.body.customerType : 'online';
    paymentType     =  req.body.paymentType ? Number(req.body.paymentType) : 2;

    if(type == ''){
       res.json({status: "fail",message: commonLang.REQ_TYPE});
       return;
    }

    if(bookingId == ''){
       res.json({status: "fail",message: commonLang.REQ_BOOKING_ID});
       return;
    }

    if(serviceId == ''){
       res.json({status: "fail",message: commonLang.REQ_SERVICE_ID});
       return;
    }

    if(subserviceId == ''){
       res.json({status: "fail",message: commonLang.REQ_SUB_SERVICE_ID});
       return;
    }

    if(artistServiceId == ''){
       res.json({status: "fail",message: commonLang.REQ_ARTIST_SERVICE_ID});
       return;
    }

    if(userId == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }
     data = {};
     data['artistId']       =  artistId;
     data['userId']         =  userId;
     data['_id']            =  bookingId;
     serviceId              =  serviceId;
     data['subserviceId']   =  subserviceId;
     artistServiceId        =  artistServiceId;
    
     var ser = serviceId.split(",");
     var serId = ser.map(function(n) {
     return n;
     });

    var ar = artistServiceId.split(",");
    var artSerId = ar.map(function(n) {
     return n;
    });
     
    search ={};
    search['serviceId'] = {$in:serId};
    search['artistId'] = Number(data['artistId']);
     
    ar_search ={};
    ar_search['_id'] = {$in:artSerId};
    ar_search['artistId'] = Number(data['artistId']);

   
     updateData ={};
     bookStatus = 1;
    updateData['paymentTime'] ="";
    // code for block use not accept booking
    if(type =='accept' && authData.walkingBlock ==1){
        res.json({'status':'fail','message': 'You are currently blocked.please contact by admin'});
        return;                          
    }

    switch (type) {

        case 'accept':
          
             updateData['bookStatus'] =1;
             updateData['paymentTime'] = dateTime.localdate;
             msg = commonLang.BOOKING_ACCEPTED;
             notifyType = '2';
             break;    
       
        case 'reject':
        
            updateData['bookStatus'] =2; 
            bookStatus = 2
            msg = commonLang.BOOKING_REJECTED;
            notifyType = '3';
            break;
       
        case 'cancel':
          
            updateData['bookStatus'] =2; 
            bookStatus = 2
            msg = commonLang.BOOKING_REJECTED;
            notifyType = '4';
            break;
       
        case 'complete':

            updateData['bookStatus'] =3;
            updateData['paymentStatus'] = 1;
            msg = commonLang.BOOKING_COMPLETED_PAY;
            if(paymentType ==1){
               msg = commonLang.BOOKING_COMPLETED;
            }
            notifyType = '5';

             /*here code for main service count*/
             artistMainService.find(search,{'_id': 1,'bookingCount':1}).exec(function(err, tgdata) {
                if (tgdata.length > 0) {
                    count = 0;
                    tgdata.forEach(function(rs) {
                    count = Number(rs.bookingCount) + 1;
                    artistMainService.updateOne({_id:rs._id},{$set:{'bookingCount':count}},function(err, upd) {});
                    });
                }
            });
   
             /* here code for artist service count*/
            artistservices.find(ar_search,{'_id': 1,'bookingCount':1}).exec(function(err, tgdata) {
                if (tgdata.length > 0) {
                    count = 0;
                    tgdata.forEach(function(rs) {
                    count = Number(rs.bookingCount) + 1;
                    artistservices.updateOne({_id:rs._id},{$set:{'bookingCount':count}},function(err, upd) {});
                    });
                }
            });

            // code for new flow when admin set artist 0 commission 
            if(req.body.paymentType==2){
                User.findOne({'_id':artistId}).exec(function(err, arInfo) {
                      booking.findOne({'_id':data._id}).exec(function(err, bookCom) {
                    if(arInfo){
                        if(arInfo.commission=='0'|| arInfo.commission==0){
                            if(bookCom.adminCommision =='0'|| bookCom.adminCommision =='0.0' || bookCom.adminCommision == 0 ){
                             booking.updateOne({ _id:bookingId},{ $set:{'commissionStatus':1}},function(err, rs) {});
                            }
                        }                        
                    }
                });
                   
                })
            }
            //end

          break;

        case 'paid':
          
            updateData['paymentStatus'] = 1; 
            msg = commonLang.SUCCESS_CHANGE_PAYMENT_STATUS;
            notifyType = '27';
            break;

        case 'end':
          
            updateData['bookStatus'] =3;
            updateData['paymentStatus'] = 1;
            msg = commonLang.BOOKING_COMPLETED;
            notifyType = '5';
            break;     
    } 
    
    booking.updateMany({'_id':data._id},{$set:updateData}, function(err, docs){

     bookingService.updateMany({'bookingId':data._id},{$set: {'bookingStatus':bookStatus}}, function(err, docs){}); 
     
      
        booking.findOne({'_id':data._id}).exec(function(err, bookingData) {

            User.findOne({'_id':data['userId']}).exec(function(err, userData) {


                if(customerType=="online"){

                    /*code for notification*/              
                    var userId    = data['userId'];
                    var artistId  = data['artistId'];
                    var notifyId   = data['_id'];
                    var notifyTy = 'booking';

                    /*end notification code*/   
                     
                    //var stripe = require('stripe')('sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4');
                    //var stripe = require('stripe')('sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt');

                    if(type=="accept" && bookingData.paymentStatus==0 && bookingData.paymentType==1){

                        if(typeof bookingData.voucher != 'undefined' && bookingData.voucher){

                            var p = Number(bookingData.discountPrice)*100;
                        }else{
                            var p = Number(bookingData.totalPrice)*100;
                        }

                        customerId = userData.customerId;
                        token = userData.cardId;
                        price = Number(p).toFixed(0);
                            data = {
                            amount: price,
                            currency: "gbp",
                            source: token, // obtained with Stripe.js
                            description: "Charge for service booking"
                        };
                        data.customer = customerId;


                        stripe.charges.create(data, function(err, charge) {

                            if(err){

                                booking.updateMany({'_id':bookingData._id},{$set: {bookStatus:2}}, function(err, docs){
                        
                                    bookingService.updateMany({'bookingId':bookingData._id},{$set: {'bookingStatus':2}}, function(err, docs){

                                        res.json({'status':'fail','message': commonLang.BOOKING_NOT_ACCEPT});
                                        return;
                                    });
                                });
                            }

                            if(charge){


                                tranId = charge.balance_transaction;
                                booking.updateMany({'_id':bookingData._id},{$set: {transjectionId:tranId,paymentStatus:1}}, function(err, docs){

                                    paymentDetail.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err,userdata) { 


                                        var id = userdata ? userdata._id+1 : 1;
                                        jsArr = {
                                                _id: id,
                                                firstAdminPay: charge,
                                                bookingId: bookingData._id
                                            };
                                        paymentDetail.insertMany(jsArr,function(err,my) {});

                                        notify.notificationUser(artistId,userId,notifyType,notifyId,notifyTy,'',paymentType); 


                                        res.json({'status':'success','message':msg});  
                                        return;

                                    }); 

                                });
                            }
                        });
                    }else if(bookStatus==2 && bookingData.paymentStatus==1 && bookingData.paymentType==1){

                        paymentDetail.findOne({'bookingId':data._id}).exec(function(err, pData) {

                            if(pData){

                                if(pData.firstAdminPay.id){

                                    data = {
                                        charge: pData.firstAdminPay.id
                                    };

                                    stripe.refunds.create(data, function(err, charge) {

                                        if(err){

                                            res.json({'status':'fail','message': err});
                                            return;
                                        }

                                        if(charge){


                                            tranId = charge.id;

                                            booking.updateMany({'_id':bookingData._id},{$set: {transjectionId:tranId,paymentStatus:3}}, function(err, docs){
                                            
                                                paymentDetail.updateMany({'_id':pData._id},{$set: {refundPay:charge}}, function(err, docs){

                                                    notify.notificationUser(userId,artistId,notifyType,notifyId,notifyTy,'',paymentType); 

                                                    res.json({'status':'success','message':msg});  
                                                    return;

                                                }); 

                                            });
                                        }
                                    });

                                }
                            }

                           
                        });

                    }else{
                          if(bookingData .userId ==  authData._id && type =='reject' ){
                            }else{
                             notify.notificationUser(artistId,userId,notifyType,notifyId,notifyTy,'',paymentType); 
                             }
                       res.json({'status':'success','message':msg});  
                       return;

                    }
                
                }else{

                    res.json({'status':'success','message':msg});  
                    return;
                }
            });
        });
    }); 


     
}


exports.deleteBookService = function(req,res){

    if(req.body.bookingId){
        bookingService.deleteOne({'_id':req.body.bookingId}, function(err, results){
        if(err) throw err;

        res.json({status:"success",message: commonLang.SUCCESS_DELETE_BOOKING_SERVICE});
        });
    }else{
        res.json({status:"fail",message: commonLang.REQ_BOOKING_ID});
    }

}


exports.deleteUserBookService = function(req,res){

    if(req.body.userId){

        let bookingType = (req.body.bookingType) ? req.body.bookingType : 'online';

        bookingService.deleteMany({'userId':req.body.userId,'bookingStatus':0,'bookingType':bookingType}, function(err, results){
         if(err) throw err;
          res.json({status:"success",message: commonLang.SUCCESS_DELETE_BOOKING_SERVICE});
         });
      
    }else{
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
    }

}


exports.deleteClientBookService = function(req,res){

    if(req.body.artistId){

        let bookingType = (req.body.bookingType) ? req.body.bookingType : 'online';

        bookingService.deleteMany({'artistId':req.body.artistId,'bookingStatus':0,'bookingType':bookingType}, function(err, results){
         if(err) throw err;
          res.json({status:"success",message: commonLang.SUCCESS_DELETE_BOOKING_SERVICE});
         });
      
    }else{
        res.json({status:"fail",message: commonLang.REQ_USER_ID});
    }

}


exports.bookingcheck = function(req,res,next){

   loginId  =  authData._id;

   checkData = {}; 
   checkData["bookingDate"] = { $lte: dateTime.curDate};
   checkData['bookStatus']= '0';
   if(authData.appType=="social"){
        checkData['userId'] = loginId;
    }else{
        checkData['artistId'] = loginId;
    }
   serverTime = common.parseTime(common.timeConvert(dateTime.curTime));
   booking.aggregate([{$match:checkData}],function(err,userData){
     if(userData.length>0){
                       
               var i = 0;
              async.each(userData, function(element, callback){                
                  
                    if (common.parseTime(common.timeConvert(userData[i].bookingTime)) <= serverTime) {
                        
                         
                           booking.deleteMany({'_id':userData[i]._id},function(err, results) {});
                           addNotification.deleteMany({'notifyId':userData[i]._id,'type':'booking'},function(err, results) {});
                           bookingService.deleteMany({'bookingId':userData[i]._id},function(err, results) {});
                            
                    }              
                           
             
                callback();
                    
            i++;
            },function(){
            
            });

    }
    next()   
   
  });            
  
}


exports.bookingCreateCheck = function(req,res,next){

   checkData = {}; 
   checkData['bookStatus']= '0';

   serverTime = common.parseTime(common.timeConvert(dateTime.curTime));
   booking.aggregate([{$match:checkData}],function(err,userData){

     if(userData.length>0){
                       
               var i = 0;
              async.each(userData, function(element, callback){  

                  
                        var newDateObj = moment(userData[i].upd).add(30, 'm').toDate();
                        var crdTime = common.parseTime(moment(newDateObj).format('HH:mm'));
                      
                        if (serverTime >= crdTime) {
                            
                               booking.deleteMany({'_id':userData[i]._id},function(err, results) {});
                               addNotification.deleteMany({'notifyId':userData[i]._id,'type':'booking'},function(err, results) {});
                               bookingService.deleteMany({'bookingId':userData[i]._id},function(err, results) {});
                                
                        }  

                               
                           
             
                callback();
                    
            i++;
            },function(){
            
            });

    }
    next()   
   
  });            
  
}


exports.bookingRequestCount = function(req,res,next){

   checkData = {}; 
   checkData['bookStatus']= '0';
   checkData['artistId']= Number(artistId);

   bookingRequestCount = 0;

   serverTime = common.parseTime(common.timeConvert(dateTime.curTime));
   booking.aggregate([{$match:checkData}],function(err,bData){

        bookingRequestCount = bData.length;
        next()   
   
  });            
  
}


exports.paymentCheck = function(req,res,next){

   loginId  =  authData._id;

   checkData = {}; 
   checkData['paymentStatus']= 0;
   checkData['paymentType']= 1;
   checkData['bookStatus']= '1';
   checkData['paymentTime']= {'$ne':''};
   serverTime = common.parseTime(common.timeConvert(dateTime.curTime));

   booking.aggregate([{$match:checkData}],function(err,userData){

     if(userData.length>0){
                       
               var i = 0;
              async.each(userData, function(element, callback){   

                    if(userData[i].paymentTime){

                        var paymentTime = new Date(userData[i].paymentTime);                    
                        var newDateObj = moment(paymentTime).add(30, 'm').toDate();
                        var payTime = common.parseTime(moment(newDateObj).format('HH:mm'));
                        var payDate = moment(paymentTime).format('YYYY-MM-DD');
                        var date    = moment().format('YYYY-MM-DD');
                     

                        if (payDate<date || (payTime <= serverTime && payDate==date)) {


                            booking.updateMany({'_id':userData[i]._id},{$set:{"bookStatus" : '2','paymentTime':''}}, function(err, docs){ });
                           bookingService.updateMany({'bookingId':Number(userData[i]._id)},{$set: {'bookingStatus':'2'}}, function(err, docs){});

                        } 
                             
                           
                    }
                callback();
                    
            i++;
            },function(){
            
            });

    }
    next()   
   
  });            
  
}

exports.userBookingHistory = function(req,res){

    let type        = req.body.type;
    let paymentType = req.body.paymentType;
    let search      = req.body.search;
    let staffId      = req.body.staffId;
    let userId      = authData._id;
    let baseUrl     =  req.protocol + '://'+req.headers['host'];

    sortData = {};
    searchData = {};
    data = {};
    searchuName = {};
   
    sortData["bookingDate"] = 1;
    if(authData.appType=="social"){
        searchData['customerType'] = 'online';// test for walking and user same id                               
        searchData['userId'] = userId;
        if(search){
            searchuName['artistDetail.userName'] = {$regex:req.body.search,$options:'i'};
        }

    }else{

        searchData['artistId'] = userId;

        if(search){

            var searchuName = {

                "$or": [{
                    "userDetail.userName": {$regex:req.body.search,$options:'i'}
                }, {
                    "client.firstName": {$regex:req.body.search,$options:'i'}
                }]
            };
        }

    }
    if(type){

        // searchData['paymentStatus'] = 1; 
        searchData['bookStatus'] = '3';
       
        sortData["timeCount"] = 1; 
        if(type=="Cancelled Booking"){

            searchData['bookStatus'] = '2';            
        }
    }else{

      //  searchData['paymentStatus'] = {$ne : 1};
       // searchData['bookStatus'] = '1';
        var data = {
            "$or": [{
                "bookStatus": '0'
            }, {
                "bookStatus": '1'
            }]
        };

    }

    if(paymentType){

        if(paymentType=="paid"){

            searchData['paymentStatus'] = 1;
            searchData['deleteStatus'] = 1;
            data = {};

        
        }else if(paymentType=="unPaid"){

            searchData['paymentStatus'] = 0; 
            data = {};   
        
        }

    }


    if(staffId){

        searchData = {};
        data = {};
        searchuName = {};
        data['bookingData.staff'] = Number(staffId);
        data['artistId']          = Number(userId);       
        data['bookStatus']        = '3';
        searchData['artistId']    = Number(userId);       
        sortData["bookingDate"]         = 1;
        sortData["timeCount"]           = 1;       

    }


    async.parallel([

        function(callback) {
            var query = booking.aggregate([
                 
              
                { $sort:sortData},
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "userDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "artistId",
                       "foreignField": "_id",
                       "as": "artistDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "clients",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "client"
                    }
                },
                {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                {
                    $match: searchData
                },
                {
                    $match: data
                },
                {
                    $match: searchuName
                },
                {
                    "$project": {
                        "_id": 1,
                        "bookingDate": 1,
                        "artistId": 1,
                        "userId": 1,
                        "bookingTime": 1,
                        "totalPrice": 1,
                        "paymentType": 1,
                        "paymentStatus": 1,
                        "customerType": 1,
                        "location": 1,
                        "bookStatus": 1,
                        "reviewByUser": 1,
                        "reviewByArtist": 1,
                        "userRating": 1,
                        "artistRating": 1,
                        "timeCount":1,
                        "bookingType":1,
                        "paymentTime":1,
                        "staffInfo.profileImage":1,
                        "userDetail._id": 1,
                        "userDetail.userName": 1,
                        "userDetail.profileImage":1,
                        "userDetail.ratingCount":1,
                        "artistDetail._id": 1,
                        "artistDetail.userName": 1,
                        "artistDetail.profileImage":1,
                        "artistDetail.ratingCount":1,
                        "client._id": 1,
                        "client.firstName": 1,
                        "client.lastName":1,
                        "client.phone":1
                       
                    }
                }

            ]);
            query.exec(function(err, g) {

                if (err) {
                    callback(err);
                }

                callback(null, g);
            });
        },

        function(callback) {
            var query = booking.aggregate([
               { $sort:sortData},
               {
                    "$lookup": {
                        "from": "bookingservices",
                        "localField": "_id",
                        "foreignField": "bookingId",
                        "as": "bookingData"
                    }
                },
                  { $unwind : "$bookingData" },
                       {
                            "$lookup": {
                                "from": "artistservices",
                                "localField": "bookingData.artistServiceId",
                                "foreignField": "_id",
                                "as": "artistService"
                            }
                        },
                        {
                            "$lookup": {
                                "from": "subservices",
                                "localField": "bookingData.subServiceId",
                                "foreignField": "_id",
                                "as": "subService"
                            }
                        },
                        {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.staff",
                               "foreignField": "_id",
                               "as": "staffInfo"
                            }
                        },
                         {
                            "$lookup": {
                               "from": "users",
                               "localField": "bookingData.artistId",
                               "foreignField": "_id",
                               "as": "artistInfo"
                            }
                        },
               { $match:searchData},
                {
                    "$project": {
                        "_id":1,
                        "bookingData._id":1,
                        "bookingData.bookingPrice":1,
                        "bookingData.serviceId":1,
                        "bookingData.subServiceId":1,
                        "bookingData.artistServiceId":1,
                        "bookingData.bookingDate":1,
                        "bookingData.startTime":1,
                        "bookingData.endTime":1,
                        "bookingData.endTimeBiz":1,
                        "bookingData.staff":1,
                        "bookingData.status":1,
                        "artistService._id":1,
                        "artistService.title":1,
                        "subService.title":1,
                        "staffInfo._id": 1,
                        "staffInfo.userName": 1,
                        "staffInfo.profileImage":1,
                        "artistInfo._id": 1,
                        "artistInfo.userName": 1,
                        "artistInfo.profileImage":1,
                        "artistInfo.businessName":1
                       
                    }
                }

            ]);
            query.exec(function(err, s) {

                if (err) {
                    callback(err);
                }

                callback(null, s);
            });
        },

    ],

    function(err, results) {
     

        if (results[0]) {
           for (var i = 0; i < results[0].length; i++) {
             
                if(results[0][i].customerType=="online"){

                    if(results[0][i].userDetail[0].profileImage){

                        results[0][i].userDetail[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].userDetail[0].profileImage; 
                    }

                }
                if(results[0][i].artistDetail[0].profileImage){

                    results[0][i].artistDetail[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].artistDetail[0].profileImage; 
                }
                jsArr = [];
                for (var j = 0; j < results[1].length; j++) {
                   
                    if (results[0][i]._id == results[1][j]._id) {

                           
                            if(results[1][j].staffInfo ==0){
                                
                             
                               staffId       =  results[1][j].artistInfo[0]._id;
                               staffName     = results[1][j].artistInfo[0].userName;
                               staffImage    =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                               companyId     = results[1][j].artistInfo[0]._id;
                               companyName   = results[1][j].artistInfo[0].businessName;
                               companyImage  =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                              
                            }else{
                                
                                if(results[1][j].staffInfo[0].profileImage){
                                   results[1][j].staffInfo[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].staffInfo[0].profileImage; 
                                }

                                staffId       =  results[1][j].bookingData.staff;
                                staffName     =  results[1][j].staffInfo[0].userName;
                                staffImage    =  results[1][j].staffInfo[0].profileImage;
                                companyId     =  results[1][j].artistInfo[0]._id;
                                companyName   =  results[1][j].artistInfo[0].businessName;
                                companyImage  =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[1][j].artistInfo[0].profileImage;
                                    
                            }
                            if(results[1][j].bookingData.status!=0 && results[0][i].bookStatus == 1){
                                results[0][i].bookStatus =  5;

                            }

                            jsArr.push({
                                _id: results[1][j].bookingData._id,
                                bookingPrice: results[1][j].bookingData.bookingPrice,
                                serviceId: results[1][j].bookingData.serviceId,
                                subServiceId: results[1][j].bookingData.subServiceId,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                bookingDate: results[1][j].bookingData.bookingDate,
                                artistServiceId: results[1][j].bookingData.artistServiceId,
                                startTime: results[1][j].bookingData.startTime,
                                endTime: results[1][j].bookingData.endTime,
                                endTimeBiz: results[1][j].bookingData.endTimeBiz,
                                status: results[1][j].bookingData.status,
                                artistServiceName: results[1][j].artistService[0].title,
                                subServiceName: results[1][j].subService[0].title,
                                staffId : staffId,
                                staffName : staffName,
                                staffImage : staffImage,
                                companyId : companyId,
                                companyName : companyName,
                                companyImage : companyImage,
                            });

                    }
                }
                results[0][i].bookingInfo = jsArr;
                    

            }    
         }
        
        res.json({status: "success",message: 'ok',data: results[0]});
        return;
        
    });


}


exports.bookingupdate = function(req, res) {
    
    var msg ='';
    type            =  req.body.type;
    bookingId       =  req.body.bookingId;
    serviceId       =  req.body.serviceId;
    userId          =  req.body.userId;
    id              =  req.body.id;
    price           =  req.body.price;
    serviceName     =  req.body.serviceName;
    paymentType     =  req.body.paymentType;
    userType        =  req.body.userType;
    artistId        =  req.body.artistId;
    customerType     =  req.body.customerType ? req.body.customerType : 'online';


    if(type == ''){
       res.json({status: "fail",message: commonLang.REQ_TYPE});
       return;
    }

    if(bookingId == ''){
       res.json({status: "fail",message: commonLang.REQ_BOOKING_ID});
       return;
    }


    if(id == ''){
       res.json({status: "fail",message: commonLang.REQ_ID});
       return;
    }

    if(userId == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }


    data = {};
    data['artistId']       =  artistId;
    data['userId']         =  userId;
    data['_id']            =  id;
   
    updateData ={};     
    switch (type) {

        case 'on the way':
          
             updateData['status'] =1;
             msg = commonLang.BOOKING_ON_WAY;
             notifyType = '19';
             break;    
       
        case 'start':
        
            updateData['status'] =2; 
            msg = commonLang.BOOKING_STARTED;
            notifyType = '20';
            break;
       
        case 'end':
          
            updateData['status'] =3; 
            msg = commonLang.BOOKING_COMPLETED;
            notifyType = '21';
            break;
       
    } 

    if(userType=="staff"){
        bookData = {
            'staff' : Number(req.body.artistId),
            'status' : 1
        };
    }else{

        bookData = {
            'artistId' : Number(req.body.artistId),
            'status' : 1
        };
    }

    bookingService.findOne(bookData).exec(function(err, bdata) {


        if(bdata && type=='on the way'){

            res.json({'status':'success','message': commonLang.NOT_ON_WAY_MULTIPLE_SERVICES});  
            return;
        }
   
        bookingService.updateMany({'_id':data._id},{$set:updateData}, function(err, docs){

            if(type =='start'){
                booking.updateOne({'_id':bookingId},{$set:{inProgress:1}}, function(err, inStatus){

                });
            }

            if(customerType=="online"){
         
               /*code for notification*/              
                var userId    = data['userId'];
                var artistId  = data['artistId'];
                var notifyId   = bookingId;
                var notifyTy = 'booking';
                notify.notificationUser(artistId,userId,notifyType,notifyId,notifyTy,serviceName); 

            }
            /*end notification code*/   

            res.json({'status':'success','message':msg});  
            return;
        }); 
    }); 


     
}

exports.notificationClear = function(req, res) {
    
    userId          =  req.body.userId;
    userType        =  req.body.userType;

    if(userId == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }

    if(userType == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_TYPE});
       return;
    }

    data = {};
    if(userType=="user"){

        data['userId']         =  userId;

    }else{

        data['userId']         =  userId;

    }
    data['bookStatus']     =  3;

    var query = booking.find(data)
    query.exec(function(err, data) {

        if(data){

            a= data.map(a => a._id);
            addNotification.deleteMany({'notifyId':{$in:a},'type':'booking'},function(err, results) {

                res.json({'status':'success','message': commonLang.SUCCESS_DELETE});  
                return;
            });

        }

    });     
}


exports.bookingNotification = function(req, res) {
    
    userId          =  req.body.userId;
    id              =  req.body.id;
    artistId        =  req.body.artistId;
    bookingId        =  req.body.bookingId;


    if(bookingId == ''){
       res.json({status: "fail",message: commonLang.REQ_BOOKING_ID});
       return;
    }


    if(id == ''){
       res.json({status: "fail",message: commonLang.REQ_ID});
       return;
    }

    if(userId == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }

    if(artistId == ''){
       res.json({status: "fail",message: commonLang.REQ_ARTIST_ID});
       return;
    }

    data = {};
    updateData = {};
    data['artistId']       =  artistId;
    data['userId']         =  userId;
    data['_id']            =  id;
   
    updateData['status'] = 4;
    msg = 'Artist';
    notifyType = '22';
     
    bookingService.updateMany({'_id':data._id},{$set:updateData}, function(err, docs){
     
       /*code for notification*/              
        var userId    = data['userId'];
        var artistId  = data['artistId'];
        var notifyId   = data['bookingId'];
        var notifyTy = 'booking';
        notify.notificationUser(artistId,userId,notifyType,notifyId,notifyTy,''); 

        
        /*end notification code*/   

        res.json({'status':'success','message':'ok'});  
        return;
    }); 


     
}


exports.ratingReview = function(req,res){

    userType     =  req.query.userType;
    userId        =  req.query.userId;

    if(userId == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_ID});
       return;
    }

    if(userType == ''){
       res.json({status: "fail",message: commonLang.REQ_USER_TYPE});
       return;
    }

    let baseUrl   =  req.protocol + '://'+req.headers['host'];

    searchData = {};
   
    if(userType=="user"){
      //  searchData['userId'] = Number(userId);
       // searchData['artistRating'] = {'$ne':0};
        searchData['ratedUsrTyp']= 'social';
    }else{
     //   searchData['artistId'] = Number(userId);
      //  searchData['userRating'] = {'$ne':0};
       searchData['ratedUsrTyp'] = 'biz';

    }
    searchData['artistId'] = Number(userId);
   
    async.parallel([

        function(callback) {
            var query = ratingReview.aggregate([

               { $sort:{'_id':-1,'ratingDate':-1}},

                {
                    "$lookup": {
                       "from": "users",
                       "localField": "userId",
                       "foreignField": "_id",
                       "as": "userDetail"
                    }
                },
                {
                    "$lookup": {
                       "from": "users",
                       "localField": "artistId",
                       "foreignField": "_id",
                       "as": "artistDetail"
                    }
                },
                {
                    $match: searchData
                },
                
                {
                    "$project": {
                        "_id": 1,
                        "artistId": 1,
                        "review": 1,
                        "rating": 1,
                        "ratingDate":1,
                        "userDetail._id": 1,
                        "userDetail.userName": 1,
                        "userDetail.profileImage":1,
                        "artistDetail._id": 1,
                        "artistDetail.userName": 1,
                        "artistDetail.profileImage":1
                       
                    }
                }

            ]);
            query.exec(function(err, g) {

                if (err) {
                    callback(err);
                }

                callback(null, g);
            });
        }
    ],

    function(err, results) {
     

        if (results[0]) {

            data = [];

           for (var i = 0; i < results[0].length; i++) {
             
               
                if(results[0][i].userDetail[0].profileImage){

                    results[0][i].userDetail[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].userDetail[0].profileImage; 
                }
                if(results[0][i].artistDetail[0].profileImage){

                    results[0][i].artistDetail[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+results[0][i].artistDetail[0].profileImage; 
                } 

                userDetail = results[0][i].userDetail;


                data.push({
                    _id: results[0][i]._id,
                    userDetail: userDetail,
                    review: results[0][i].review,
                    rating: results[0][i].rating,
                    crd: results[0][i].ratingDate
                });               

            }    
         }
        
        res.json({status: "success",message: 'ok',data: data});
        return;
        
    });


}


exports.bookingReviewRating = function(req,res){

    bookingId      =  Number(req.body.bookingId);
    reviewByUser   =  req.body.reviewByUser;
    reviewByArtist =  req.body.reviewByArtist;
    rating         =  req.body.rating; 
    ratingDate     =  req.body.ratingDate; 
    userId         =  (req.body.userId) ? Number(req.body.userId) : Number(authData._id); 
    artistId       =  Number(req.body.artistId); 
    msg            =  '';  
    type           =  req.body.type ? req.body.type : 'insert'; 
   
    updateData = {};
    myData = {};
   
    if(bookingId ==''){
        res.json({status:"fail",message: commonLang.REQ_BOOKING_ID});
    }
    if(ratingDate ==''){
        res.json({status:"fail",message: commonLang.REQ_RATING_DATE});
    }

    if(authData.appType =='social'){


        updateData['reviewByUser']    = reviewByUser;
        updateData['userRating']      = rating;
        updateData['userRatingCrd']   = ratingDate;

        myData['review']      = reviewByUser;
        myData['rating']      = rating;
        myData['ratingDate']  = ratingDate;
        myData['bookingId']   = bookingId;
        myData['artistId']    = artistId;
        myData['userId']      = userId;
        myData['ratedUsrTyp'] = 'biz';

        msg = commonLang.SUCCESS_SUBMIT_REVIEW;
        rs = {'artistId' :artistId,'userRating':{$ne:0}};
        where     = {'_id':artistId};
        senderId  =  userId;
        reciverId = artistId;
        msgType = 2 // send by user
        
    }

    if(authData.appType=='biz'){

        updateData['reviewByArtist']    = reviewByArtist;
        updateData['artistRating']      = rating;
        updateData['artistRatingCrd']   = ratingDate;


        myData['review']      = reviewByArtist;
        myData['rating']      = rating;
        myData['ratingDate']  = ratingDate;
        myData['bookingId']   = bookingId;
        myData['artistId']    = userId;
        myData['userId']      = artistId;
        myData['ratedUsrTyp'] = 'social';

        msg = commonLang.SUCCESS_SUBMIT_REVIEW;
        
        rs = {'userId' : userId,'artistRating':{$ne:0}}; 
        where = {'_id':userId};

        senderId =   artistId;
        reciverId =  userId;
        msgType = 3 // send artist
    }

    booking.findOne({'_id':bookingId}).exec(function(err,data){
        if(data){
            
                if((reviewByUser!='' && data.artistRating!='')||(reviewByArtist!='' && data.userRating!='')){
                     updateData.reviewStatus = 1;
                }

                if(type=="edit"){

                    ratingReview.updateMany({'bookingId':Number(data._id),'artistId':Number(myData['artistId']),'userId':Number(myData['userId'])},{$set:myData}, function(err, docs){  

                        booking.updateMany({'_id':data._id},{$set:updateData}, function(err, docs){  

                            booking.find(rs, function(err, userData) {
                                count = 1;
                                if(userData){
                                    count = userData.length;
                                    total = 0;
                                    userData.forEach(function(rd) {
                                        
                                        if(authData.appType=='biz'){

                                            total = rd.artistRating+total;

                                        }else{

                                           total = rd.userRating+total;

                                        }

                                    });

                          
                                    r = total/count;
                                    rating = r; 
                                     //code change 28-07-2020
                                    if(authData.appType=='biz'){
                                       User.updateMany(where,{$set:{socialRatingCount:rating,socialReviewCount:count}}, function(err, docs){  });
                                    }else{
                                       User.updateMany(where,{$set:{ratingCount:rating,reviewCount:count}}, function(err, docs){  });
                                    }
                                    //end 
 
                                }

                            });
                            
                        });
                         /*code for notification*/
                       
                          var type = '6';
                          var notifyId   = bookingId;
                          var notifyType = 'booking'; 
                          notify.notificationUser(senderId,reciverId,type,notifyId,notifyType,msgType);
                        
                         /*end code*/     
                        res.json({status:"success",message:msg});
                    });

                }else{

                ratingReview.findOne().sort([['_id', 'descending']]).limit(1).exec(function(err,rdata) { 
                        
                    var b = 1;
                    if(rdata){

                        b = rdata._id+1;
                    }
                    myData['_id'] = b;


                    ratingReview.insertMany(myData,function(err,my) {
                   


                        booking.updateMany({'_id':data._id},{$set:updateData}, function(err, docs){  

                            booking.find(rs, function(err, userData) {
                                count = 1;
                                if(userData){
                                    count = userData.length;
                                    total = 0;
                                    userData.forEach(function(rd) {
                                        
                                        if(authData.appType=='biz'){

                                            total = rd.artistRating+total;

                                        }else{

                                           total = rd.userRating+total;

                                        }

                                    });

                          
                                    r = total/count;
                                    rating = r;  
                                    //code change 28-07-2020
                                    if(authData.appType=='biz'){
                                       User.updateMany(where,{$set:{socialRatingCount:rating,socialReviewCount:count}}, function(err, docs){  });
                                    }else{
                                       User.updateMany(where,{$set:{ratingCount:rating,reviewCount:count}}, function(err, docs){  });
                                    }
                                    //end
                                }

                            });
                            
                        });
                         /*code for notification*/
                       
                          var type = '6';
                          var notifyId   = bookingId;
                          var notifyType = 'booking'; 
                          notify.notificationUser(senderId,reciverId,type,notifyId,notifyType,msgType);
                        
                         /*end code*/     
                        res.json({status:"success",message:msg});
                    }); 
                });
                }

        }
    
   
    });

   
}


exports.bookingCheck = function(req,res,){

   type     = req.body.type;
   id       = Number(req.body.id);
   artistId =  Number(req.body.artistId);

    if(type==""){

        res.json({status:"fail",message: commonLang.REQ_TYPE});
        return;
    }

    if(artistId==""){

        res.json({status:"fail",message: commonLang.REQ_ARTIST_ID});
        return;
    }
    if(id==""){

        res.json({status:"fail",message: commonLang.REQ_ID});
        return;
    }

    checkData = {}; 
    checkData1 = {

        $and: [{
                'bookStatus': {'$ne':'3'}
    
            }, {
                'bookStatus': {'$ne':'2'}
            }]
    };

   checkData['artistId']= artistId;

   if(type=="businessType"){
        checkData['bookingData.serviceId']= id;
   }else if(type=="category"){
        checkData['bookingData.subServiceId']= id;
   }else if(type=="service"){
        checkData['bookingData.artistServiceId']= id;
   }


    var query = booking.aggregate([
         
        {
            "$lookup": {
                "from": "bookingservices",
                "localField": "_id",
                "foreignField": "bookingId",
                "as": "bookingData"
            }
        },
        {
            $match: checkData
        },
        {
            $match: checkData1
        },
        {
            "$project": {
                "_id": 1,               
            }
        }

    ]);
    query.exec(function(err, g) {

        if(g.length>0){

            res.json({status:"success",message: commonLang.EXIST_BOOKING});
            return;

        }else{
            res.json({status:"success",message: commonLang.NOT_EXIST_BOOKING});
            return;
        }
    });           
  
}


exports.checkTrackingBooking = function(req,res){

    let artistId = req.body.artistId;
    sortData = {};

    if(artistId == ''){
       res.json({status: "fail",message: commonLang.REQ_ARTIST_ID});
       return;
    }

    searchData = {

        $or: [{
                'bookingInfo.status': 1
    
            }, {
                'bookingInfo.status': 4
            }]
    };
    //searchData['artistId'] = Number(artistId);
   // searchData['bookingInfo.status'] = 1;
    sortData["timeCount"] = 1;
    d = {

        $or: [{
                    'bookingInfo.artistId': Number(artistId)
    
                }, {
                    'bookingInfo.staff': Number(artistId)
                }]
        };

    var query = booking.aggregate([
                         
        { $sort:sortData},
        
        {
            "$lookup": {
                "from": "bookingservices",
                "localField": "_id",
                "foreignField": "bookingId",
                "as": "bookingInfo"
            }
        },
        {
            $match: searchData
        },
        {
            $match: d
        },
        {
            "$project": {
                "_id": 1,
                "latitude":1,
                "longitude":1,
                "userId":1,
                "bookingInfo._id":1,
                "bookingInfo.status":1,
               
            }
        }

    ]);
    query.exec(function(err, g) {

        if(g.length){    
            res.json({status: "success",message: 'ok',data: g[0]});
            return;
        }else{
           res.json({status: "fail",message: 'ok'});
            return; 
        }
    });
    

}


exports.reportSubmit = function(req, res) {

    let bookingInfoId   = req.body.bookingInfoId; 
    let bookingId       = req.body.bookingId; 
    let title           = req.body.title; 
    let description     = req.body.description; 
    let businessId      = req.body.businessId; 
    let staffId         = req.body.staffId; 
    let reportByUser    = req.body.reportByUser; 
    let reportForUser   = req.body.reportForUser; 
    let serviceId       = req.body.serviceId; 
    let reportDate      = req.body.reportDate; 
    let serviceName     = req.body.serviceName ? req.body.serviceName : ''; 

    if(bookingId==""){

        res.json({'status':'fail','message': commonLang.REQ_BOOKING_ID});
        return;
    }

    if(bookingInfoId==""){

        res.json({'status':'fail','message': commonLang.REQ_BOOKING_INFO_ID});
        return;
    }

    if(businessId==""){

        res.json({'status':'fail','message': commonLang.REQ_BUSINESS_ID});
        return;
    }

    if(title==""){
        
        res.json({'status':'fail','message': commonLang.REQ_TITLE});
        return;
    }

    if(description==""){
        
        res.json({'status':'fail','message': commonLang.REQ_DESCRIPTION});
        return;
    }

    if(reportByUser==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_BY_USER});
            return;
    }

    if(reportForUser==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_FOR_USER});
            return;
    }

    if(serviceId==""){
            
            res.json({'status':'fail','message': commonLang.REQ_SERVICE_ID});
            return;
    }

    if(reportDate==""){
            
            res.json({'status':'fail','message': commonLang.REQ_REPORT_DATE});
            return;
    }

    report.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

        var data1 = {
            'bookingInfoId'  : bookingInfoId,
            'bookingId'      : bookingId,
            'businessId'     : businessId,
            'serviceId'      : serviceId,
            'title'          : title,
            'description'    : description,
            'reportByUser'   : reportByUser,
            'reportForUser'  : reportForUser,
             '_id'           : (aat) ? Number(aat._id)+Number(1) : 1,
            'crd'            : reportDate,
            'upd'            : reportDate
        };

        report.insertMany(data1);

     /*code for notification*/
   
          var type = '26';
          var notifyId   = bookingId;
          var notifyType = 'booking';
          if(businessId==authData._id){

               senderId = reportByUser;
                reciverId = reportForUser; 

                notify.notificationUser(reportByUser,reportForUser,type,notifyId,notifyType,serviceName);

                if(staffId){
                    if(staffId!=authData._id){
                        notify.notificationUser(reportByUser,staffId,type,notifyId,notifyType,serviceName);
                    }
                }

          }else{

                notify.notificationUser(reportByUser,reportForUser,type,notifyId,notifyType,serviceName);

                if(staffId){
                    if(staffId!=businessId){
                        notify.notificationUser(reportByUser,businessId,type,notifyId,notifyType,serviceName);
                    }
                }


          }
    
        res.json({'status': "success","message": commonLang.SUCCESS_REPORT});
        return;
    });

 
}


exports.bookingAnalytics = function(req,res){

    let startDate = req.body.startDate;
    let endDate = req.body.endDate;
    let previewsStartDate = req.body.previewsStartDate;
    let previewsEndDate = req.body.previewsEndDate;

    if(startDate==""){

        res.json({'status':'fail','message': commonLang.REQ_START_DATE});
        return;
    }

    if(endDate==""){

        res.json({'status':'fail','message': commonLang.REQ_END_DATE});
        return;
    }
    var query = booking.find({'bookingDate':{'$gte':startDate,'$lte':endDate},'artistId':authData._id});
    query.exec(function(err, g) {

        var query1 = booking.find({'bookingDate':{'$gte':previewsStartDate,'$lte':previewsEndDate},'artistId':authData._id});
        query1.exec(function(err, p) {

            res.json({'status': "success","message": 'ok','data':g,'previousData':p});

            return;
        });

    });

}


exports.paymentHistoryDelete = function(req, res) {

    bookingId       =  req.body.bookingId;

    if(bookingId == ''){
       res.json({status: "fail",message: commonLang.REQ_BOOKING_ID});
       return;
    }

    booking.updateMany({'_id':bookingId},{$set:{'deleteStatus':0}}, function(err, docs){
        
        res.json({'status':'success','message': commonLang.SUCCESS_DELETE_PAYMENT_HISTORY});  
        return;
    }); 


     
}


exports.adminCommision = function(req,res){

    let artistId = req.body.artistId;
    if(artistId==""){

        res.json({'status':'fail','message': commonLang.REQ_ARTIST_ID});
        return;
    }
    var query = admin.findOne({'_id':1});
    query.exec(function(err, g) {

        var query1 = User.findOne({'_id':artistId});
        query1.exec(function(err, p) {
            artistCommision ='';
            cash_commission = g.commission;
            card_commission = g.commissionCard;

            if(p.commission){
                cash_commission = p.commission;
            }
            if(p.commissionCard){
                card_commission = p.commissionCard;
            }
/*             if(p.commission!='' || p.commission!=0 || p.commission!='0.0' || p.commission!='0'){
                cash_commission = p.commission;
            }
            if(p.commissionCard!='' || p.commissionCard!=0 || p.commissionCard!='0.0' || p.commissionCard!='0'){
                card_commission = p.commissionCard;
            }*/
            res.json({'status': "success","message": 'ok','cashCommision':cash_commission,'cardCommision':card_commission});
            return;

        });

    });

}


exports.bookingCount = function(req,res){

   businessId =  Number(req.body.businessId);

    if(businessId==""){

        res.json({status:"fail",message: commonLang.REQ_BUSINESS_ID});
        return;
    }

    checkData = {}; 
    checkData1 = {

        $and: [{
                'bookStatus': {'$ne':'3'}
    
            }, {
                'bookStatus': {'$ne':'2'}
            }]
    };

    checkData['artistId']= artistId;
    checkData['customerType']= "online";
    checkData['paymentStatus']= {'$ne':1};
    booking.aggregate([{$match:checkData},{$match:checkData1}],function(err,bData){

        res.json({'status': "success","message": 'ok','bData':bData.length});
        return;  

    });            
  
}


exports.bookingAdminCommsion = function(req,res){
    /*changes addcording to new req 08-08-19*/
    // 1. walking booking is closed only 2 type of payment mode online and case 'customerType':'walking' remove add 'paymentType':2
     var now         = new Date();
    var currentDate =  moment().format('YYYY-MM-DD');
    var prevMonthLastDate = new Date(now.getFullYear(), now.getMonth(), 0);
    var prevMonthFirstDate = new Date(now.getFullYear() - (now.getMonth() > 0 ? 0 : 1), (now.getMonth() - 1 + 12) % 12, 1);
    var curentMonthFirstDate = new Date(now.getFullYear(), now.getMonth(), 1);

    previewsStartDate = formatDate(prevMonthFirstDate);
    previewsEndDate   = formatDate(prevMonthLastDate);
    startDate         = formatDate(curentMonthFirstDate);
    endDate           = currentDate;

    if(startDate==""){

        res.json({'status':'fail','message': commonLang.REQ_START_DATE});
        return;
    }

    if(endDate==""){

        res.json({'status':'fail','message':commonLang.REQ_END_DATE});
        return;
    }

    var query = booking.find({'bookingDate':{'$gte':(startDate),'$lte':(endDate)},'artistId':authData._id,'paymentType':2,'bookStatus':'3','commissionStatus':0});
    query.exec(function(err, g) {

        gtotal = 0;

        g.forEach(function(rd) {
                
           gtotal = Number(rd.adminAmount)+gtotal;


        });



        var query1 = booking.find({'bookingDate':{'$gte':(previewsStartDate),'$lte':(previewsEndDate)},'artistId':authData._id,'paymentType':2,'bookStatus':'3','commissionStatus':0});
        query1.exec(function(err, p) {

            ptotal = 0;
            p.forEach(function(rd) {
                    
               ptotal = Number(rd.adminAmount)+ptotal;

            });

            res.json({'status': "success","message": 'ok','data':gtotal,'previousData':ptotal,'previewsStartDate':previewsStartDate,'previewsEndDate':previewsEndDate,'startDate':startDate,'endDate':currentDate});

            return;
        });

    });

}


exports.adminReminder = function(req,res){

   var query1 = adminReminder.find({'artistId':authData._id});
    query1.exec(function(err, p) {

        res.json({'status': "success","message": 'ok','data':p});
        return;
    });

}

var formatDateComponent = function(dateComponent) {
  return (dateComponent < 10 ? '0' : '') + dateComponent;
};

var formatDate = function(date) {
  return  date.getFullYear() + '-' + formatDateComponent(date.getMonth() + 1) + '-' + formatDateComponent(date.getDate());
};

exports.notificationAdminCommsion = function(req,res,next){
        /*changes addcording to new req 08-08-19*/
    // 1. walking booking is closed only 2 type of payment mode online and case 'customerType':'walking' remove add 'paymentType':2

    var now         = new Date();
    var currentDate =  moment().format('YYYY-MM-DD');
    var prevMonthLastDate = new Date(now.getFullYear(), now.getMonth(), 0);
    var prevMonthFirstDate = new Date(now.getFullYear() - (now.getMonth() > 0 ? 0 : 1), (now.getMonth() - 1 + 12) % 12, 1);

    var curentMonthFirstDate = new Date(now.getFullYear(), now.getMonth(), 1);


    previewsStartDate = formatDate(prevMonthFirstDate);
    previewsEndDate   = formatDate(prevMonthLastDate);
    notifincationType = 0;
    startDate         = formatDate(curentMonthFirstDate);
    endDate           = currentDate;


   artistId = req.body.userId ? Number(req.body.userId) : authData._id;

   var query = User.findOne({'_id':artistId});
    query.exec(function(err, userData) {

       var query = admin.findOne({'_id':1});
        query.exec(function(err, g) {

            g.firstReminder = moment().format('YYYY-MM')+'-'+moment(g.firstReminder).format('DD');
            g.secondReminder = moment().format('YYYY-MM')+'-'+moment(g.secondReminder).format('DD');
            g.thirdReminder = moment().format('YYYY-MM')+'-'+moment(g.thirdReminder).format('DD');
            message = g.firstMessage;
            title = 'First Reminder';

            var query1 = booking.find({'bookingDate':{'$gte':(previewsStartDate),'$lte':(previewsEndDate)},'paymentType':2,'bookStatus':'3','commissionStatus':0,'artistId':artistId});
            query1.exec(function(err, p) {

                var query1 = booking.find({'bookingDate':{'$gte':(startDate),'$lte':(endDate)},'paymentType':2,'bookStatus':'3','commissionStatus':0,'artistId':artistId});
                query1.exec(function(err, c) {

                    if(c.length){

                        message = g.firstMessage;
                        title = 'First Reminder';


                        if(g.firstReminder<=currentDate){

                            var query1 = adminReminder.findOne({'artistId':artistId,'notifincationType':1});
                            query1.exec(function(err, s) {

                               ;
                                if(s=="" || s==null){

                                    notifincationType = 1;

                                    adminReminder.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

                                        var data1 = {
                                            'notifincationType'  : notifincationType,
                                            'artistId'       : artistId,
                                            '_id'            : (aat) ? Number(aat._id)+Number(1) : 1,
                                            'currentDate'    : currentDate,
                                            'message'         : message
                                        };
                                        adminReminder.insertMany(data1);

                                            if(notifincationType){

                                                if(userData.appType=="biz"){

                                                    body = message;
                                                    type = 30;   
                                                    token = userData.firebaseToken;
                                                    notification = { title:title,body: body,notifincationType:type,sound: "default"};                                   
                                                    data = {title:title,body: body,notifincationType:type };
                                                    notify.bedgeCount(artistId,'commissionCount','socialBookingBadgeCount',type);
                                    
                                                    notify.sendNotification(token,notification,data,artistId,'businessBookingBadgeCount');

                                                }

                                                
                                            }

                                        
                                    });


                                }

                            });

                        }

                    }
                    
                    if(p.length){

                        if(g.secondReminder<=currentDate){

                            message = g.secondMessage;
                            title = 'Second Reminder';


                            var query1 = adminReminder.findOne({'artistId':artistId,'notifincationType':2});
                            query1.exec(function(err, s) {

                                if(s=="" || s==null){

                                    notifincationType = 2;

                                    adminReminder.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

                                        var data1 = {
                                            'notifincationType'  : notifincationType,
                                            'artistId'           : artistId,
                                            '_id'                : (aat) ? Number(aat._id)+Number(1) : 1,
                                            'currentDate'        : currentDate,
                                            'message'            : g.secondMessage 
                                        };

                                        adminReminder.insertMany(data1);
                                        if(notifincationType){

                                            if(userData.appType=="biz"){

                                                body = g.secondMessage;
                                                type = 30;   
                                                token = userData.firebaseToken;
                                                notification = { title:title,body: body,notifincationType:type,sound: "default"};                                   
                                                data = {title:title,body: body,notifincationType:type };                                    
                                                notify.sendNotification(token,notification,data,artistId,'businessBookingBadgeCount');

                                            }

                                                
                                        }

                                    });

                                }

                            });

                        }

                        var query1 = adminReminder.findOne({'artistId':artistId,'notifincationType':3});
                        query1.exec(function(err, s) {

                


                            if(g.thirdReminder<=currentDate){

                                message = g.thirdMessage;
                                title = ' Third Reminder & Final Reminder';

                                    if(s=="" || s==null){

                                       notifincationType = 3;

                                        adminReminder.findOne().sort([['_id', 'descending']]).exec(function(err, aat){

                                            var data1 = {
                                                'notifincationType'  : notifincationType,
                                                'artistId'           : artistId,
                                                '_id'                : (aat) ? Number(aat._id)+Number(1) : 1,
                                                'currentDate'        : currentDate,
                                                'message'            : message,
                                                'crd'                : now
                                            };

                                            adminReminder.insertMany(data1);
                                            if(notifincationType){

                                                if(userData.appType=="biz"){

                                                    body = message;
                                                    type = 30;   
                                                    token = userData.firebaseToken;
                                                    notification = { title:title,body: body,notifincationType:type,sound: "default"};                                   
                                                    data = {title:title,body: body,notifincationType:type };                                    
                                                    notify.sendNotification(token,notification,data,artistId,'businessBookingBadgeCount');

                                                }

                                                    
                                            }

                                        });
                                    }

                                
                            }

                            if(g.blockDuration){


                                if(s){

                                    var blockDate = new Date(s.crd);
                                    blockDate.setHours(blockDate.getHours() + Number(g.blockDuration));

                                    if(now>blockDate){

                                        User.updateOne({ _id:artistId},{ $set:{'walkingBlock':1}},function(err, result) { });


                                    }

                                }
                            }
                        });


                      
                    }                  

                    next();
                    
                   

                });
               

            });

        });
    
    });

}
exports.reminderCron = function(req,res){

    getReminderSetting().then(async (setting)=>{
        
    var blockDuration = setting.blockDuration;

    var now = new Date();
    var currentDate          = moment().format('YYYY-MM-DD');
    var prevMonthLastDate    = new Date(now.getFullYear(), now.getMonth(), 0);
    var prevMonthFirstDate   = new Date(now.getFullYear() - (now.getMonth() > 0 ? 0 : 1), (now.getMonth() - 1 + 12) % 12, 1);
    var curentMonthFirstDate = new Date(now.getFullYear(), now.getMonth(), 1);

    previewsStartDate = formatDate(prevMonthFirstDate);
    previewsEndDate   = formatDate(prevMonthLastDate);
    notifincationType = 0;
    startDate = formatDate(curentMonthFirstDate);
    endDate   = currentDate;

   // console.log('currentDate ',currentDate);
     setting.firstReminder  = moment().format('YYYY-MM') + '-' + moment(setting.firstReminder).format('DD');
     setting.secondReminder = moment().format('YYYY-MM') + '-' + moment(setting.secondReminder).format('DD');
     setting.thirdReminder  = moment().format('YYYY-MM') + '-' + moment(setting.thirdReminder).format('DD');
  /*  console.log('previewsStartDate',previewsStartDate);
    console.log('previewsEndDate',previewsEndDate);
    console.log('startDate',startDate);
    console.log('endDate',endDate);

    console.log('g.firstReminder',setting.firstReminder);
    console.log('g.secondReminder',setting.secondReminder);
    console.log('g.thirdReminder',setting.thirdReminder);*/
   
    let results = await getAllArtist(req);
    
        var promises = [];
        var lastRequest =  await getRequestLatID();
        var reqLastID = 0;
        if(lastRequest){
            reqLastID = lastRequest._id; 
        }
       
        // code for check old month pending commission booking
         promisesBefore = results.map(async function(artistData){
            return await checkBeforePendingCommission(previewsStartDate,previewsEndDate, artistData,blockDuration,setting).then( async(p) => {
               
               if(p){ 
                   reqLastID = reqLastID + 1;
                   if(setting.thirdReminder <= currentDate) {  
                     message = setting.thirdMessage;
                     title = 'Third Reminder & Final Reminder';
                     notifincationType = 3;
                     
                  return await checkArtistReminderInfo(artistData,notifincationType,reqLastID,currentDate,message,now,title,blockDuration).then( s => {
                  
                        return s;
                        }).catch(function(error) {return error;});
                   }
                }

              return p;
            }).catch(function(error) {return error;});
 
        });
         
        promisesCurrent = results.map(async function(artistData){
            return await checkBeforePendingCommission(startDate,endDate, artistData,blockDuration,setting).then( async(p) => {
                 
               if(p){ 

                   reqLastID = reqLastID + 1;
                   if(setting.firstReminder <= currentDate) {
                  
                     message = setting.firstMessage;
                     title = 'First Reminder';
                     notifincationType = 1;
                      return await checkArtistReminderInfo(artistData,notifincationType,reqLastID,currentDate,message,now,title.blockDuration).then( s => {
                            return s;
                      }).catch(function(error) {return error;});
                   }

                   if(setting.secondReminder <= currentDate) {
                   
                       message = setting.secondMessage;
                       title = 'Second Reminder';
                       notifincationType = 2;
                        return await checkArtistReminderInfo(artistData,notifincationType,reqLastID,currentDate,message,now,title.blockDuration).then( s => {
                            return s;
                        }).catch(function(error) {return error;});
                   }
                      
                }
            
              return p;
            }).catch(function(error) {return error;});
 
        });
         let allPromise = promisesBefore.concat(promisesCurrent);
        Promise.all(allPromise).then(function(values) {
            res.json({status:"success",message:'ok',data:values});
        });
    });

}
async function getReminderSetting(){
    return await admin.findOne({'_id': 1}).exec()
}
async function getRequestLatID(){
    return await adminReminder.findOne().sort({ _id: -1 }).exec();
}
function getAllArtist(req){
    var query = booking.distinct("artistId",{'paymentType': 2,'bookStatus': '3','commissionStatus': 0});
     
    return query.exec();
}
async function checkBeforePendingCommission(previewsStartDate, previewsEndDate, artistData,blockDuration,setting){
    return await booking.findOne({
        'bookingDate':{'$gte':(previewsStartDate),'$lte': (previewsEndDate)},
        'paymentType': 2,
        'bookStatus': '3',
        'commissionStatus': 0,
        'artistId': artistData}).exec()
}
async function checkPendingCommission(startDate,endDate, artistData,blockDuration,setting){
    return await booking.findOne({
        'bookingDate':{'$gte':(startDate),'$lte': (endDate)},
        'paymentType': 2,
        'bookStatus': '3',
        'commissionStatus': 0,
        'artistId': artistData}).exec()
}
async function artistReminderInfo(artistId,notifincationType){
    return await adminReminder.findOne({'artistId': artistId,'notifincationType':notifincationType}).exec()
    
}
async function artistInfoData(artistId){
    return await User.findOne({'_id': artistId}).exec()
    
}
async function saveNewRequest11(data,msg,title){
    
    let newReqData = new adminReminder(data);
    return await artistInfoData(data.artistId).then( async (userData) => {
  
    return await newReqData.save().then(async (record) => {
       
           if (data.notifincationType) {
               
                if (userData.appType == "biz") {

                    body = msg;
                    type = 30;
                    token = userData.firebaseToken;
                    notification = {
                     title: title,
                     body: body,
                     notifincationType: type,
                     sound: "default"
                    };
                    data1 = {
                     title: title,
                     body: body,
                     notifincationType: type
                    };
                  await notify.sendNotification(token, notification, data1, data.artistId, 'businessBookingBadgeCount');
               }

            }        
             return record;         
        }).catch(function(error) {return error;})


      //  console.log('save data new');
     return userData;

    }).catch(function(error) {console.log('Crone save error', error);});
}
async function checkArtistReminderInfo(artistId,notifincationType,reqLastID,currentDate,msg,now,title,blockDuration){
    return new Promise(async (resolve, reject) => {
    var finalPromise = {};
   
           
            let s = await artistReminderInfo(artistId, notifincationType);

               /*  console.log('artistId id info',artistId);
                 console.log('notifincationType id info',notifincationType);*/
               
                if (s == "" || s == null) {
                      
                   // return new Promise(async (resolve, reject) => {   
                       
                          var data1 = {
                                       'notifincationType': notifincationType,
                                       'artistId': artistId,
                                       '_id': reqLastID,
                                       'currentDate':currentDate,
                                       'message':msg, 
                                       'crd': now
                                      };

                              let saveRequest = await saveNewRequest11(data1,msg,title);
                            //console.log('saveRequest',saveRequest);
                        resolve(saveRequest);
                  //  }) 
                }else{

                        if (notifincationType ==3) {
                            if (s) {
                               var blockDate = new Date(s.crd);
                               
                               blockDate.setHours(blockDate.getHours() + Number(blockDuration));
                                if (now > blockDate) {
                                    User.updateOne({_id: artistId},{$set:{'walkingBlock':1}},function(err, result) {});
                                }
                            }
                        }
                   
                    reject('user not found.');
                   
                }
        
    });
}

exports.bookingReminderCron = function(req,res){
       var allPromise = [];
       var currentDate          = moment().format('YYYY-MM-DD');
       var convert = new Date();
       var utc = convert.getTime() + (convert.getTimezoneOffset() * 60000);
       var d = new Date(utc + (3600000*'+1'));
       console.log('curentTime', d.getHours() * 60 + d.getMinutes());
       var minutes1H = d.getHours() * 60 + d.getMinutes()+55;
       var minutes2H = d.getHours() * 60 + d.getMinutes()+70;

       var minutes1F = d.getHours() * 60 + d.getMinutes()+25;
       var minutes2F = d.getHours() * 60 + d.getMinutes()+40;
  
       console.log('minutes1F',minutes1F);   
       console.log('minutes2F',minutes2F);   
       console.log('minutes1H',minutes1H);   
       console.log('minutes2H',minutes2H);   
       //timeCount':{'$gte':minutes1,'$lte':minutes2} (requestData.timeCount>=minutes1H && requestData.timeCount<=minutes2H)
       //bookingReminder: 1:not send,2:half hour send 3:one hour sen
        getBookingReminder().then(async (bookingF)=>{
           
            bookingF.map(async function(requestData){

                if(requestData.timeCount>=minutes1F && requestData.timeCount<=minutes2F && requestData.bookingReminder!=2){
                        

                    return await artistInfoData(requestData.userId).then( async (userData) => {
                  
                               insertData={};
                                addNotification.findOne().sort([
                                      ['_id', 'descending']
                                     ]).exec(function(err, aat) {
                                      console.log('id ididididi',aat._id);
                                    insertData['senderId']   = requestData.artistId;
                                    insertData['receiverId'] = requestData.userId;
                                    insertData['crd'] =  moment().format();;
                                    insertData['upd'] =  moment().format();;
                                    insertData['notifyId'] = requestData._id;
                                    insertData['notifincationType'] = 31;
                                    insertData['type'] = 'booking';
                                    insertData['serviceName'] = requestData.bookingTime;
                                    insertData['_id'] = (aat) ? Number(aat._id) + Number(1) : 1
                     
                                    addNotification.insertMany(insertData);
                                    title='Booking Reminder'
                                    body = 'Hi '+userData.userName+' '+commonLang.BOOKING_REMINDER_TODAY+' '+requestData.bookingTime;
                                    type = 31;
                                    token = userData.firebaseToken;
                                    notification = {
                                    title: title,
                                    body: body,
                                    notifincationType: type,
                                    sound: "default",
                                    'notifyId' : requestData._id,
                                     click_action:"ChatActivity"
                                    };
                                    data = {
                                    title: title,
                                    body: body,
                                    notifincationType: type,
                                    notifyId : requestData._id,
                                    click_action:"ChatActivity"
                                    };
                                    booking.updateOne({ _id:requestData._id},{ $set:{'bookingReminder':2}},function(err, rs) {});
                                    notify.sendNotification(token, notification, data, requestData.artistId, 'businessBookingBadgeCount');
                           
                                    });
               
                     }).catch(function(error) {return error;});
                } // reqLastID = reqLastID + 1;

                if(requestData.timeCount>=minutes1H && requestData.timeCount<=minutes2H && requestData.bookingReminder==1){
                        
                   
                    
                    return await artistInfoData(requestData.userId).then( async (userData) => {
                  
                               insertData={};
                                addNotification.findOne().sort([
                                      ['_id', 'descending']
                                     ]).exec(function(err, aat) {
                                      console.log('id ididididi',aat._id);
                                    insertData['senderId']   = requestData.artistId;
                                    insertData['receiverId'] = requestData.userId;
                                    insertData['crd'] =  moment().format();;
                                    insertData['upd'] =  moment().format();;
                                    insertData['notifyId'] = requestData._id;
                                    insertData['notifincationType'] = 31;
                                    insertData['type'] = 'booking';
                                    insertData['serviceName'] = requestData.bookingTime;
                                    insertData['_id'] = (aat) ? Number(aat._id) + Number(1) : 1
                     
                                    addNotification.insertMany(insertData);
                                    title='Booking Reminder'
                                    body = 'Hi '+userData.userName+' '+commonLang.BOOKING_REMINDER_TODAY+' '+requestData.bookingTime;
                                    type = 31;
                                    token = userData.firebaseToken;
                                    notification = {
                                    title: title,
                                    body: body,
                                    notifincationType: type,
                                    sound: "default",
                                    'notifyId' : requestData._id,
                                     click_action:"ChatActivity"
                                    };
                                    data = {
                                    title: title,
                                    body: body,
                                    notifincationType: type,
                                    notifyId : requestData._id,
                                    click_action:"ChatActivity"
                                    };
                                     booking.updateOne({ _id:requestData._id},{ $set:{'bookingReminder':3}},function(err, rs) {});
                                    notify.sendNotification(token, notification, data, requestData.artistId, 'businessBookingBadgeCount');
                           
                                    });
               
                     }).catch(function(error) {return error;});
                }

            });
       res.json({status: "success", data:bookingF});
     });


}
async function getBookingReminder(){
    var currentDate          = moment().format('YYYY-MM-DD');
       var d = new Date();
       console.log('curentTime', d.getHours() * 60 + d.getMinutes());
       var minutes1 = d.getHours() * 60 + d.getMinutes()+55;
       var minutes2 = d.getHours() * 60 + d.getMinutes()+70;
     
    return await booking.find({'bookStatus':'1','bookingDate':currentDate}).exec()
}
async function getBookingFirstReminder(){
    var currentDate          = moment().format('YYYY-MM-DD');
       var d = new Date();
       console.log('curentTime', d.getHours() * 60 + d.getMinutes());
       var minutes1 = d.getHours() * 60 + d.getMinutes()+55;
       var minutes2 = d.getHours() * 60 + d.getMinutes()+70;
     
    return await booking.find({'bookStatus':'1','bookingDate':currentDate,'timeCount':{'$gte':minutes1,'$lte':minutes2}}).exec()
}

async function getBookingSecondReminder(){
    var currentDate          = moment().format('YYYY-MM-DD');
           var d = new Date();
       var minutes1 = d.getHours() * 60 + d.getMinutes()+25;
       var minutes2 = d.getHours() * 60 + d.getMinutes()+40;
       // console.log('half houre minutes1',minutes1);
    
    return await booking.find({'bookStatus':'1','bookingDate':currentDate,'timeCount':{'$gte':minutes1,'$lte':minutes2}}).exec()
}
exports.checkBusySlot = function(req, res,next) {

     var bookData = {};
         bookData['artistId'] = req.body.artistId;
         bookData['bookingDate'] =  req.body.bookingDate;
         bookData['bookingStatus'] = {
            '$ne': 2
         }
         if (req.body.staff) {
             bookData['staff'] = Number(req.body.staff);
         }
    var bookingSTime = Array();
    var bookingETime = Array();
    var interval = 10;
    var staffId = req.body.staff;
    bookingService.find(bookData).sort([['_id', 'ascending']]).exec(function(err, bdata) {
        var t = 0;
        if (bdata) {
            bdata.forEach(function(rs) {
                bookingSTime.push(common.parseTime(common.timeConvert(rs.startTime)) + t);
                bookingETime.push(common.parseTime(common.timeConvert(rs.endTime)));
            });
        }
    var bussy_slot = common.bookingTime(bookingSTime, bookingETime, interval);
        if(bussy_slot.indexOf(req.body.startTime) !== -1){
            res.json({status: "fail",message: commonLang.TIME_SLOT_NOT_AVAIL});
            return;
        } 
        next(); 
       
    });
}
