var User = require('../../models/front/home.js');//it user for table and coulamn information
var folder = require('../../models/front/folder_model.js');
var files = require('../../models/front/files_model.js');
var feed = require('../../models/front/feed.js');
var moment = require('moment');
var dateFormat = require('dateformat');
var lodash = require('lodash');

var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');

exports.createFolder = function(req, res){

    var folderName =  req.body.folderName;
    if(folderName==''  || typeof folderName == "undefined"){

        res.json({'status': "fail","message":commonLang.REQ_FOLDER_NAME});
        return;

    }
    var jsArr = {};

    folder.findOne({'folderName':folderName,'userId':Number(authData._id)}).exec(function(err, data) {

        if(data){
           res.json({'status': "fail","message":commonLang.EXIST_FOLDER});
           return;
        }

        folder.findOne().sort([['_id', 'descending']]).exec(function(err, s) {
            if (err) {
                res.json({err});
            }
            var autoId = 1;
            if (s) {
                autoId = s._id + 1;

            }
            crd  =  dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
            jsArr = {
                _id: autoId,
                userId: Number(authData._id),
                folderName: folderName,
                crd : crd,
                upd : crd
            };
            folder.insertMany(jsArr);
            res.json({'status': "success","message":commonLang.SUCCESS_CREATE_FOLDER});
            return;
            
        });
    });
}

exports.getFolder = function(req, res){

    folder.find({'userId':Number(authData._id)}).exec(function(err, data) {

        if(data.length>0){
            res.json({'status': "success","message": 'successfully',data:data});
            return;
        }
        res.json({status:"fail",message:commonLang.RECORD_NOT_FOUND});
        return;

    });
}

exports.editFolder = function(req, res){

    var folderName =  req.body.folderName;
    var folderId =  Number(req.body.folderId);
    if(folderId=='' || typeof folderId == "undefined"){

        res.json({'status': "fail","message": commonLang.REQ_FOLDER_ID});
        return;

    }
    if(folderName=='' || typeof folderName == "undefined"){

        res.json({'status': "fail","message":commonLang.REQ_FOLDER_NAME});
        return;

    }

    var jsArr = {};

    folder.findOne({'folderName':folderName,'userId':Number(authData._id),'_id':{$ne:folderId}}).exec(function(err, data) {

        if(data){
           res.json({'status': "fail","message":commonLang.EXIST_FOLDER});
           return;
        }

        jsArr = {
                folderName: folderName
            };

        folder.update({ _id:folderId},{ $set:jsArr},function(err, docs) {
            res.json({'status': "success","message":commonLang.SUCCESS_UPDATE_FOLDER});
            return;
        });
        
    });
}


exports.deleteFolder = function(req, res){
    
    var folderId =  Number(req.body.folderId);

    if(folderId=='' || typeof folderId == "undefined"){

        res.json({'status': "fail","message":commonLang.REQ_FOLDER_ID});
        return;

    }

    folder.remove({ _id:folderId},function(err, docs) {

        files.remove({ folderId:folderId},function(err, docs) {});
        res.json({'status': "success","message": commonLang.SUCCESS_DELETE_FOLDER});
        return;
    });
}


exports.saveToFolder = function(req, res){

    var feedId =  Number(req.body.feedId);
    var folderId =  Number(req.body.folderId);
    if(folderId=='' || typeof folderId == "undefined"){

        res.json({'status': "fail","message":commonLang.REQ_FOLDER_ID});
        return;

    }
    if(feedId=='' || typeof feedId == "undefined"){

        res.json({'status': "fail","message": commonLang.REQ_FEED_ID});
        return;

    }

    var jsArr = {};
    var crd  =     dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");

    files.findOne({'feedId':feedId,'userId':Number(authData._id)}).exec(function(err, data) {

        if(data){
           res.json({'status': "fail","message":commonLang.EXIST_FEED });
           return;
        }

        files.findOne().sort([['_id', 'descending']]).exec(function(err, s) {
            if (err) {
                res.json({err});
            }
            var autoId = 1;
            if (s) {
                autoId = s._id + 1;

            }

            jsArr = {
                 _id: autoId,
                userId: Number(authData._id),
                feedId: feedId,
                folderId: folderId,
                crd: crd,
                upd: crd
            };
            files.insertMany(jsArr);            
            res.json({'status': "success","message": commonLang.SUCCESS_SAVE_FEED});
            return;
            
        });
    });
}



exports.removeToFolder = function(req, res){

    var feedId =  Number(req.body.feedId);
    if(feedId=='' || typeof feedId == "undefined"){

        res.json({'status': "fail","message": commonLang.REQ_FEED_ID});
        return;

    }

    var jsArr = {};
    jsArr = {
        userId: Number(authData._id),
        feedId: feedId,
    };
    files.remove(jsArr,function(err, docs) {});            
    res.json({'status': "success","message":commonLang.SUCCESS_REMOVE_FEED});
    return;
}

exports.getFolderData = function(req, res, next){

    folderData = [];
    loginUserId = (req.body.loginUserId) ? req.body.loginUserId : (req.body.userId) ? req.body.userId : authData._id;


    files.find({'userId':Number(loginUserId)}).sort([['_id', 'descending']]).exec(function(err, data) {

        if(data.length>0){

            folderData = data;           
        
        }
        next();

    });
}

exports.fileGet = function(req, res, next){

    var folderId =  req.body.folderId;
    var jsArr = [];
    var crd  =   Date.now();
    searchData = [];

    files.find({'userId':Number(loginUserId),'folderId':folderId}).exec(function(err, data) {
        
        if(data.length>0){

            searchData = data.map(function (rd) {
                return rd.feedId;
            });
            next();


        }else {

            res.json({status: "fail",message:commonLang.RECORD_NOT_FOUND,AllFeeds: []});
            return;
        }

    });
}



exports.folderfeedList = function(req, res) {

    var baseUrl = req.protocol + '://' + req.headers['host'];

    if (req.body.page) {
        page = ((Number(req.body.page)*Number(req.body.limit)) - Number(req.body.limit));
    } else {
        page=0;
    }

    if (req.body.limit) {
        limit = Number(req.body.limit);
    } else {
        limit=10;
    }
    var feedSearch = {};
    if(searchData && searchData!=''){
        feedSearch['_id'] = {$in:searchData};
    }
    var Value_match = {$regex:req.body.feedType};
    feedSearch['feedType'] =Value_match;
    aggregateArray = [
                                         
                {
                    $match:feedSearch
                },
                {
                    "$lookup": {
                        from: "users",
                        localField: "userId",
                        foreignField: "_id",
                        as: "userInfo"
                    }
                },
                { "$unwind": {
                        path:"$serviceTag", 
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.artistServiseTagId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.staffServiseTagId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.staffId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$unwind":{
                        path:"$serviceTag.tagDetails.artistId",
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$lookup": {
                        from: "artistservices",
                        localField: "serviceTag.tagDetails.artistServiseTagId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail"
                    }
                }, 
                {
                    "$lookup": {
                        from: "staffservices",
                        localField: "serviceTag.tagDetails.staffServiseTagId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail"
                    }
                },  
                {
                    "$lookup": {
                        from: "users",
                        localField: "serviceTag.tagDetails.staffId",
                        foreignField: "_id",
                        as: "staffDetail"
                    }
                }, 
                {
                    "$lookup": {
                        from: "users",
                        localField: "serviceTag.tagDetails.artistId",
                        foreignField: "_id",
                        as: "artistDetail"
                    }
                }, 
                { "$unwind": {
                        path:"$arstistServiceTagDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$staffServiceTagDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$staffDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                }, 
                { "$unwind": {
                        path:"$artistDetail", 
                        preserveNullAndEmptyArrays:true
                    }
                },
                {
                    "$lookup": {
                        from: "services",
                        localField: "arstistServiceTagDetail.serviceId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail.serviceId"
                    }
                },
                
                {
                    "$lookup": {
                        from: "subservices",
                        localField: "arstistServiceTagDetail.subserviceId",
                        foreignField: "_id",
                        as: "arstistServiceTagDetail.subserviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "services",
                        localField: "staffServiceTagDetail.serviceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.serviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "subservices",
                        localField: "staffServiceTagDetail.subserviceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.subserviceId"
                    }
                },
                {
                    "$lookup": {
                        from: "artistservices",
                        localField: "staffServiceTagDetail.artistServiceId",
                        foreignField: "_id",
                        as: "staffServiceTagDetail.artistServiceName"
                    }
                },
                { "$group": {
                        "_id":  "$_id",
                        "feedData": {"$first": "$feedData"},
                        "feedType": {"$first": "$feedType"},
                        "caption": {"$first": "$caption"},
                        "city": {"$first": "$city"},
                        "country": {"$first": "$country"},
                        "location": {"$first": "$location"},
                        "likeCount": {"$first": "$likeCount"},
                        "crd": {"$first": "$crd"},
                        "feedImageRatio": {"$first": "$feedImageRatio"},
                        "commentCount": {"$first": "$commentCount"},
                        "peopleTag": {"$first": "$peopleTag"},
                        "serviceTag": {"$first": "$serviceTag"},
                        "serviceTagId": {"$first": "$serviceTagId"},
                        "latitude": {"$first": "$latitude"},
                        "longitude": {"$first": "$longitude"},
                        "deleteStatus": {"$first": "$deleteStatus"},
                        "userId": {"$first": "$userId"},
                        "userInfo": {"$first": "$userInfo"},
                        "serviceTag": {
                            "$push": {
                                tag:"$serviceTag", 
                                arstistServiceTagDetail: {
                                    artistServiceID:"$arstistServiceTagDetail._id",
                                    title:"$arstistServiceTagDetail.title",
                                    description:"$arstistServiceTagDetail.description",
                                    inCallPrice:"$arstistServiceTagDetail.inCallPrice",
                                    outCallPrice:"$arstistServiceTagDetail.outCallPrice",
                                    completionTime:"$arstistServiceTagDetail.completionTime",
                                    buisnessTypeID:"$arstistServiceTagDetail.serviceId._id",
                                    buisnessTypeName:"$arstistServiceTagDetail.serviceId.title",
                                    categoryID:"$arstistServiceTagDetail.subserviceId._id",
                                    categoryName:"$arstistServiceTagDetail.subserviceId.title"
                                    
                                },
                                staffServiceTagDetail: {
                                    staffServiceID:"$staffServiceTagDetail.artistServiceId",
                                    title:"$staffServiceTagDetail.artistServiceName.title",
                                    description:"$staffServiceTagDetail.artistServiceName.description",
                                    inCallPrice:"$staffServiceTagDetail.inCallPrice",
                                    outCallPrice:"$staffServiceTagDetail.outCallPrice",
                                    completionTime:"$staffServiceTagDetail.completionTime",
                                    buisnessTypeID:"$staffServiceTagDetail.serviceId._id",
                                    buisnessTypeName:"$staffServiceTagDetail.serviceId.title",
                                    categoryID:"$staffServiceTagDetail.subserviceId._id",
                                    categoryName:"$staffServiceTagDetail.subserviceId.title"
                                },
                                staffDetail: {
                                    firstName:"$staffDetail.firstName",
                                    lastName:"$staffDetail.lastName"
                                },
                                artistDetail:{
                                    firstName:"$artistDetail.firstName",
                                    lastName:"$artistDetail.lastName"
                                }
                            }
                        }
                        
                    }
                },
                {
                $sort: {_id: -1}
                },  
                {
                    "$project": {
                        "_id":1,
                        "feedData":1,
                        "feedType":1,
                        "caption":1,
                        "city":1,
                        "country":1,
                        "location":1,
                        "likeCount":1,
                        "crd":1,
                        "feedImageRatio":1,
                        "commentCount":1,
                        "peopleTag":1,
                        "serviceTag":1,
                        "serviceTagId":1,
                        "latitude":1,
                        "longitude":1,
                        "deleteStatus":1,
                        "userId":1,
                        "userInfo._id":1,
                        "userInfo.userName":1,
                        "userInfo.firstName":1,
                        "userInfo.lastName":1,
                        "userInfo.profileImage":1,
                         "userInfo.userType":1,
                         "userInfo.ratingCount":1,
                         "staffDetail":1
                    }
                }
            ];
    aggregateArray.push({ $skip:page });
    aggregateArray.push({ $limit:limit });
    var query = feed.aggregate(aggregateArray);
    query.exec(function(err, jsArr) {
        if (err) {
            return console.log(err);
        }
        if (jsArr.length) {

            for (i = 0; i < jsArr.length; i++) {

                jsArr[i].timeElapsed = moment(jsArr[i].crd).fromNow();

                if(mData){
                        
                    var picked = lodash.filter(mData, { 'followerId': jsArr[i].userId} );

                    if(picked.length){

                       jsArr[i].followerStatus = 1;    
                    }else{

                       jsArr[i].followerStatus = 0;
                    }
                   /* var flowSt = mData.map(a => a.userId);
                    var b = flowSt.indexOf(jsArr[i].userId);

                    if(b >-1){
                        jsArr[i].followerStatus = 1
                    }else{
                       jsArr[i].followerStatus = 0
                    }*/

                }
                if(likeData){
                  
                    var picked1 = lodash.filter(likeData, { 'feedId': jsArr[i]._id} );
                      
                    if(picked1.length){

                       jsArr[i].isLike = 1;    
                    }else{

                       jsArr[i].isLike = 0;
                    }
                }

                if(folderData){
                  
                    var folderData1 = lodash.filter(folderData, { 'feedId': jsArr[i]._id} );
                      
                    if(folderData1.length){

                       jsArr[i].isSave = 1;    
                    }else{

                       jsArr[i].isSave = 0;
                    }
                }
                

                if (jsArr[i].feedData.length) {

                    for (j = 0; j < jsArr[i].feedData.length; j++) {

                        if (jsArr[i].feedData[j].feedPost) {

                            jsArr[i].feedData[j].feedPost = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR + jsArr[i].feedData[j].feedPost;

                        }
                        if (jsArr[i].feedData[j].videoThumb) {
                            jsArr[i].feedData[j].videoThumb = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.FEED_MEDIA_UPLOAD_FOLDEFR + jsArr[i].feedData[j].videoThumb;

                        }
                    }

                }

                if (jsArr[i].userInfo[0]) {

                    if (jsArr[i].userInfo[0].profileImage) {

                        jsArr[i].userInfo[0].profileImage = commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR + jsArr[i].userInfo[0].profileImage;

                    }
                } else {

                    jsArr[i].userInfo[0] = [];
                }
            }
            
            res.json({
                status: "success",
                message: 'successfully',
                AllFeeds: jsArr
            });
            return;

        } else {
            res.json({
                status: "fail",
                message: commonLang.RECORD_NOT_FOUND,
                AllFeeds: []
            });
            return;
        }

    });

}