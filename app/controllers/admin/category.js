var Category            = require('../../models/admin/category_model');
var subCategory         = require('../../models/admin/sub_category_model.js');
var artistCategory      = require('../../models/front/artistMainService');
var artistsubCategory   = require('../../models/front/artistSubService');
var artistService       = require('../../models/front/artistService.js');
var userCategory        = require('../../models/front/userCategory.js');
var addNotification     = require('../../models/front/notification.js');

var bcrypt = require('bcrypt-nodejs');
var dateFormat = require('dateformat');
var url = require('url');
var Cryptr = require('cryptr'),
 cryptr = new Cryptr('1234567890');
 var moment = require('moment');

exports.loggedIn = function(req, res, next)
{
	if (req.session.user) { // req.session.passport._id

		next();

	} else {

		res.redirect('/admin/login');

	}

}

exports.addCategory = function(req, res) {
	
	res.render('admin/addCategory.ejs', {
		error : req.flash("error"),
		success: req.flash("success"),
		session:req.session,
	
	 });
	 
}

function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}

exports.insertCategory = function(req, res) {

    var title = titleCase((req.body.title).trim());

	
	 Category.findOne({title:title,'deleteStatus':'1'}, function(err, category) {
        
        if(err){
            res.redirect('/addCategory/');
            req.flash('error', err);
        }
        
        if(category){
            req.flash('error', '');
            req.flash('error', 'Business type already exist');
            res.redirect('/addCategory');
        } else {
            
            
            Category.find().sort([['_id', 'descending']]).limit(1).exec(function(err, categorydata){
                
                // if there is no user with that email
                // create the user
                var newUser = new Category();

                // set the user's local credentials                
                var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
           	 
                var active_code=bcrypt.hashSync(Math.floor((Math.random() * 99999999) *54), null, null);
           	
                newUser.title    = title;
                newUser.created_date = day;
                newUser.updated_date = day;
                newUser.status = '1'; //inactive for email actiavators
                newUser.active_hash = active_code;
                newUser.role_id = 1;
                
                if(categorydata.length > 0){
                    newUser._id = categorydata[0]._id + 1;
                } 


                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    req.flash('success', ''); 
                    req.flash('success', 'Business type  Added Successfully'); 
                    res.redirect('/listCategory');
                });
                
            });
        }
          
    });
}

exports.listCategory = function(req, res) {

	res.render('admin/categoryList.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session
    });
	 
}


exports.categoryList = function(req, res) {

    let url_parts = url.parse(req.url, true);
    let query = url_parts.query;
    let search = req.query.search; 
    let creator = req.query.creator; 
    let status = req.query.status; 
    data = {};

    data['title'] = { $regex : search,'$options' : 'i' };
    data['deleteStatus'] = 1;

    if(creator){

         data['role_id'] = Number(creator);
    }

    if(status){

        status = (status==2) ? 0 : 1;
         data['type'] = status;
    }

    Category.find(data).sort([['title', 'ascending']]).exec(function(err, data) {
            if (err) throw err;

            var totalStudents = data.length,
             pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/category_list.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr
        });

});


     
}

exports.edit = function(req, res) {
    
    Category.findOne({_id:cryptr.decrypt(req.params.categoryId)}, function(err, categorydata) {
        res.render('admin/edit_category.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session,
            categorydata : categorydata,
         });
    });
   
}

exports.updateCategory = function(req, res){


    var catId = req.body.categoryid;
    Category.findOne({title:{'$regex' : req.body.title, '$options' : 'i'},_id: {'$ne':catId},'deleteStatus':'1'}, function(err, category) {
        
       
        if(category){

            req.flash('error', '');
            req.flash('error', 'Business type already exist');
            res.redirect('/categoryEdit/'+catId);

        } else {

            Category.updateOne({_id:catId}, 
                {$set: {title:req.body.title}},
                function(err, docs){
                    if(err) res.json(err);
                    else    {

                       artistCategory.updateMany({serviceId:catId},{$set: {serviceName:req.body.title}},function(err, docs){ });
                        req.flash("success","Business type updated successfully");
                        res.redirect('/listCategory');
                    };
            });
        }
     
    });        


}


exports.categoryStatus = function(req, res){

    let categoryId = cryptr.decrypt(req.params.categoryId);
    

    artistsubCategory.find({'serviceId' : categoryId}, function(err, data) {

        if(data){

            req.flash('error', "Can't inactive this business type, it is used by artist");
            res.redirect('/listCategory');
            return;  

        } 


        Category.updateOne({_id:categoryId},{$set: {status:cryptr.decrypt(req.params.status)}}, function(err, docs){
            if(err) res.json(err);
            else    {


                subCategory.updateMany({serviceId:categoryId},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });
                artistCategory.updateMany({serviceId:categoryId},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });
                artistsubCategory.updateMany({serviceId:categoryId},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });
                artistService.updateMany({serviceId:categoryId},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });
                req.flash("success","");

                if(cryptr.decrypt(req.params.status)=="1"){

                    req.flash("success","Business type active successfully");

                }else{

                   req.flash("success","Business type inactive successfully"); 
                }
                res.redirect('/listCategory');
            };
        });


    });

}

exports.categoryDelete = function(req, res){

    let categoryId = cryptr.decrypt(req.params.categoryId);

    artistCategory.find({'serviceId' : categoryId,'deleteStatus':1}, function(err, data) {

        if(data.length!=0){

            req.flash('error', "Can't delete this business type, it is used by artist");
            res.redirect('/listCategory');
            return;
        }


        Category.updateOne({_id:categoryId},{$set: {'deleteStatus':0}},function(err, docs){
            
            if(err)
                res.json(err);

            subCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
            artistCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
            artistsubCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
            artistService.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
            req.flash("success","");
            req.flash("success","Service deleted successfully");
            res.redirect('/listCategory');
            
        });
        
    });
  
}


exports.listUserCategory = function(req, res) {

    res.render('admin/userCategoryList.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session
    });
     
}


exports.userCategoryList = function(req, res) {

    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var search = req.query.search; 

    artistCategory.find({'serviceName' : { $regex : search,'$options' : 'i' },'deleteStatus':'1','status':0}, function(err, data) {
            if (err) throw err;

            var totalStudents = data.length,
             pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/user_category_list.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr
        });

    });
}


exports.categoryApprove = function(req, res){

    let query =  Category.aggregate([
        {  
            $lookup:{
                from: "users", 
                localField: "active_hash", 
                foreignField: "userName",
                as: "userInfo"
            }
        },
        { 
           $match:
            {

                '_id' : Number(cryptr.decrypt(req.params.categoryId))
            }, 
        },
      
        { 
            "$project": {
                "_id": 1,
                "userInfo.userName": 1,
                "userInfo.deviceType": 1,
                "userInfo.firebaseToken": 1,
                "userInfo._id": 1
            }
        }

    ]);

    query.exec(function(err, bdata) {

       Category.updateOne({_id:cryptr.decrypt(req.params.categoryId)}, {$set: {type:0}},function(err, docs){
                if(err) res.json(err);
                else    {
                    subCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'type':0}},function(err, docs){ });
                    artistCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'status':1}},function(err, docs){ });
                    artistsubCategory.updateMany({serviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'status':1}},function(err, docs){ });
                    userCategory.deleteMany({'categoryId':cryptr.decrypt(req.params.categoryId),'businessTypeId':0}, function(err, results){ });
                    userCategory.deleteMany({'businessTypeId':cryptr.decrypt(req.params.categoryId)}, function(err, results){ });

                    deviceType  = bdata[0].userInfo[0].deviceType;
                    token       = bdata[0].userInfo[0].firebaseToken;
                    receiverId  = bdata[0].userInfo[0]._id;
                    console.log(bdata[0].userInfo[0]);
                    querySecound = addNotification.findOne().sort([['_id', 'descending']]).limit(1);

                    body = 'Business type has been approved by admin.';
                    title = 'Admin Verification';
                    type = 28;

                    notification = { title:title,body: body,notifincationType:type,sound: "default"};                                   
                    data = {title:title,body: body,notifincationType:type }; 

                    insertData = {

                        'senderId' : 1,
                        'receiverId' : receiverId,
                        'notifincationType' : type,
                        'crd' : moment().format(),
                        'upd' : moment().format(),
                        'notifyId' : receiverId,
                        'type' : 'booking'
                    };                                     
                    querySecound.exec(function(err, result) {

                        if (result) {
                            insertData._id = result._id + 1;
                        }else{
                            insertData._id = 1;
                        }


                        addNotification(insertData).save(function(err, notifyData) {
                            
                            if(deviceType =='3'){
                            
                               // notify.sendWebNotification(receiverId,webData);
                            
                            }else{
                                notify.bedgeCount(receiverId,'booking','businessBookingBadgeCount');
                                notify.sendNotification(token,notification,data,receiverId,'businessBookingBadgeCount');
                            }

                            req.flash("success","Business type approved successfully");
                            res.redirect('/listCategory');
                        });
                  
                    }); 
                    
                };
       }); 

    });

}

function jsUcfirst(string) 

{

    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

}