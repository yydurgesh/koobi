var Category            = require('../../models/admin/category_model');
var subCategory         = require('../../models/admin/sub_category_model.js');
var artistsubCategory   = require('../../models/front/artistSubService');
var artistService       = require('../../models/front/artistService.js');
var artistCategory      = require('../../models/front/artistMainService');
var userCategory        = require('../../models/front/userCategory.js');
var User                = require('../../models/front/home');
var addNotification     = require('../../models/front/notification.js');

var bcrypt = require('bcrypt-nodejs');
var dateFormat = require('dateformat');
var url = require('url');
var formidable = require('formidable');
var fs = require('fs');
var Cryptr = require('cryptr'),
 cryptr = new Cryptr('1234567890');
 var lodash = require('lodash');
var moment = require('moment');

exports.addSubCategory = function(req, res) {

    Category.find({status:1,deleteStatus:1,'type':0}).sort([['title', 'ascending']]).exec(function(err, categorydata) {
        res.render('admin/addsubCategory.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session,
            categorydata : categorydata,
        });
    }); 

     
}

function titleCase(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}

exports.insertSubCategory = function(req, res) {


    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {

        var image = '';  
        var title = titleCase((fields.title).trim());
  
        subCategory.findOne({title:title,'deleteStatus' : 1,'serviceId':fields.serviceId}, function(err, category) {

            
            if(category){
                req.flash('error', '');
                req.flash('error', 'Category already exist');
                res.redirect('/addSubCategory');
            } else {
                
                 getSubCategoryId().then(async ( idData )=>{
                    var categoryId = (idData) ? Number(idData._id)+Number(1) : 1; 
                    // if there is no user with that email
                    // create the user
                    var newUser = new subCategory();
                    // set the user's local credentials                
                    var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
                    var active_code=bcrypt.hashSync(Math.floor((Math.random() * 99999999) *54), null, null);
                
                    newUser.title    = title;
                    newUser.serviceId    = fields.serviceId;
                    newUser.created_date = day;
                    newUser.updated_date = day;
                    newUser.status = '1'; //inactive for email actiavators
                    newUser.active_hash = active_code;
                    newUser.role_id = 1;
                    newUser._id = categoryId;
                       
                    //if(categorydata.length > 0){
                    // newUser._id = categorydata[0]._id + 1;
                    //} 
                    // save the user
                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        req.flash('success', ''); 
                        req.flash('success', 'Category  Added Successfully'); 
                        res.redirect('/listSubCategory');

                    });
                })
/*                //subCategory.find().sort([['_id', 'descending']]).limit(1).exec(function(err, categorydata){
                 subCategory.findOne().sort([['_id', 'descending']]).exec(function(err, categorydata){  
                 var categoryId = (categorydata) ? Number(categorydata._id)+Number(1) : 1;  
                    // if there is no user with that email
                    // create the user
                    var newUser = new subCategory();
                    // set the user's local credentials                
                    var day = dateFormat(Date.now(), "yyyy-mm-dd HH:MM:ss");
                    var active_code=bcrypt.hashSync(Math.floor((Math.random() * 99999999) *54), null, null);
                
                    newUser.title    = title;
                    newUser.serviceId    = fields.serviceId;
                    newUser.created_date = day;
                    newUser.updated_date = day;
                    newUser.status = '1'; //inactive for email actiavators
                    newUser.active_hash = active_code;
                    newUser.role_id = 1;
                    newUser._id = categoryId;
                       
                    //if(categorydata.length > 0){
                    // newUser._id = categorydata[0]._id + 1;
                    //} 
                    // save the user
                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        req.flash('success', ''); 
                        req.flash('success', 'Category  Added Successfully'); 
                        res.redirect('/listSubCategory');
                    });
                    
                });*/
            }
              
        });
    });
}
async function getSubCategoryId(){
   return await subCategory.findOne().sort({ _id: -1 }).exec();
}

exports.listsubCategory = function(req, res) {
    Category.find({status:1,deleteStatus:1}).sort([['title', 'ascending']]).exec(function(err, categorydata) {

        res.render('admin/subcatList.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata : categorydata,
        });

    });
     
}


exports.subcategoryList = function(req, res) {

    let url_parts = url.parse(req.url, true);
    let query = url_parts.query;
    let search = req.query.search; 
    let creator = req.query.creator; 
    let status = req.query.status; 
    let business_type = req.query.business_type; 

    data = {};

    data['title'] = { $regex : search,'$options' : 'i' };
    data['deleteStatus'] = 1;

    if(creator){

        data['role_id'] = Number(creator);
    }

     if(business_type){

        data['serviceId'] = Number(business_type);
    }


    if(status){

       status = (status==2) ? 0 : 1;
       data['type'] = status;
    }


    subCategory.aggregate([

        { 

            $sort:{'title' : 1}

        },
        { 
            $lookup: {

                from: 'services',
                localField: 'serviceId',
                foreignField: '_id',
                as: 'category'
            }
         
        },
        {
            $match: data
        }

      ]).exec(function(err, data) {
            if (err) throw err;

            var totalStudents = data.length,
            pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/subcat_List.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr
        });

});


     
}


exports.edit = function(req, res) {

    Category.find({status:1}, function(err, categorydata) {
    
        subCategory.findOne({_id:cryptr.decrypt(req.params.categoryId)}, function(err, subcategorydata) {
            res.render('admin/edit_subcategory.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata : subcategorydata,
                row : categorydata
             });
        });

    });
   
}

exports.subServicesStatus = function(req, res){

    let categoryId = cryptr.decrypt(req.params.categoryId);

    console.log(categoryId);
    console.log(cryptr.decrypt(req.params.status));

    artistCategory.find({'serviceId' : categoryId}, function(err, data) {

        if(data.length!=0){

            req.flash('error', "Can't inactive this category, it is used by artist");
            res.redirect('/listSubCategory');
            return;

        }else{

            
            subCategory.updateOne({_id:Number(categoryId)}, {$set: {status:cryptr.decrypt(req.params.status)}}, function(err, docs){
                
                if(err) res.json(err);

                console.log(err);
                console.log(docs);


                artistsubCategory.updateMany({subServiceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });
                artistService.updateMany({subserviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':cryptr.decrypt(req.params.status)}},function(err, docs){ });

                 req.flash("success","");
                if(cryptr.decrypt(req.params.status)=="1"){

                    req.flash("success","Category active successfully");

                }else{

                   req.flash("success","Category inactive successfully"); 
                }
                res.redirect('/listSubCategory');
                   
            });

        }
    });

}


exports.updateSubCategory = function(req, res){

    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {

        subCategory.findOne({title:{'$regex' : req.body.title, '$options' : 'i'},'deleteStatus':1}, function(err, category) {
                       
            if(category){

                req.flash('error', '');
                req.flash('error', 'Category already exist');
                res.redirect('/categorySubEdit/'+catId);

            } else {



                    subCategory.updateOne({_id:fields.id}, 
                        {$set: {title:fields.title}},
                        function(err, docs){
                            if(err) res.json(err);
                            else    {
                                artistsubCategory.updateMany({subServiceId:fields.id},{$set: {subServiceName:fields.title}},function(err, docs){ });
                                req.flash("success","");
                                req.flash("success","Category updated successfully");
                                res.redirect('/listSubCategory');
                            };
                    });
            } 

        });      

    });
}

exports.categoryDelete = function(req, res){

    let categoryId = cryptr.decrypt(req.params.categoryId);

    artistsubCategory.find({'subServiceId' : categoryId,'deleteStatus':1}, function(err, data) {

        if(data.length!=0){

            req.flash('error', "Can't delete this category, it is used by artist");
            res.redirect('/listSubCategory');
            return;

        }

        subCategory.updateOne({_id:categoryId},{$set: {'deleteStatus':0}}, function(err, docs){
            if(err) res.json(err);
            else    {
                artistsubCategory.updateMany({subServiceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
                artistService.updateMany({subserviceId:cryptr.decrypt(req.params.categoryId)},{$set: {'deleteStatus':0}},function(err, docs){ });
               req.flash('success', '');
               req.flash('success', 'Category deleted successfully');
                res.redirect('/listSubCategory');
            };
        });
    });
  
   
}

exports.total_services = function(req, res) {

    User.find({status:'1','userType':'artist'}).sort([['userName', 'ascending']]).exec(function(err, udata) {

        Category.find({status:1,deleteStatus:1}).sort([['title', 'ascending']]).exec(function(err, categorydata) {

            res.render('admin/total_services.ejs', {
                    error : req.flash("error"),
                    success : req.flash("success"),
                    session : req.session,
                    userData : udata,
                    categorydata : categorydata,
            });

        });

    });
     
}


exports.total_services_list = function(req, res) {

    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var search = req.query.search; 
    let business_type = Number(req.query.business_type); 
    let artistId = Number(req.query.artist); 

    data =  {
      'title': {$regex : search,'$options' : 'i'},
      'deleteStatus' : 1,
    };

    if(business_type){
        data.serviceId = business_type;
    }

    if(artistId){
        data.artistId = artistId;
    }
    
    artistService.aggregate([
        { $lookup:
          {
            from: 'services',
            localField: 'serviceId',
            foreignField: '_id',
            as: 'category'
          }
         
        },
        { $lookup:
          {
            from: 'subservices',
            localField: 'subserviceId',
            foreignField: '_id',
            as: 'subcategory'
          }
         
        },
         { $lookup:
          {
            from: 'users',
            localField: 'artistId',
            foreignField: '_id',
            as: 'artistDetail'
          }
         
        },
        {
         $match: data
        }

      ]).exec(function(err, data) {

            if (err) throw err;

            var totalStudents = data.length,
            pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/total_services_List.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr,
                lodash : lodash
        });

});


     
}


exports.listUserSubCategory = function(req, res) {

    res.render('admin/userSubcatList.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session
    });
     
}


exports.userSubcategoryList = function(req, res) {

    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var search = req.query.search; 

    artistsubCategory.aggregate([
        { $lookup:
          {
            from: 'services',
            localField: 'serviceId',
            foreignField: '_id',
            as: 'category'
          }
         
        },
        {
            $match: {

              'subServiceName'  : {$regex : search,'$options' : 'i'},
              'deleteStatus'    : 1,
              'status'          : 0

            }
        }

      ]).exec(function(err, data) {
            if (err) throw err;

            var totalStudents = data.length,
            pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/user_subcat_List.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr
        });

});


     
}

exports.subCategoryApprove = function(req, res){

    let query =  subCategory.aggregate([
        { 
           $match:
            {

                '_id' : Number(cryptr.decrypt(req.params.categoryId))
            }, 
        },

        {  
            $lookup:{
                from: "users", 
                localField: "active_hash", 
                foreignField: "userName",
                as: "userInfo"
            }
        },
      
        { 
            "$project": {
                "_id": 1,
                "userInfo.userName": 1,
                "userInfo.deviceType": 1,
                "userInfo.firebaseToken": 1,
                "userInfo._id": 1
            }
        }

    ]);

   query.exec(function(err, bdata) {

        subCategory.updateOne({_id:cryptr.decrypt(req.params.categoryId)},  {$set: {type:0}},function(err, docs){
                if(err) res.json(err);
                else    {
                    Category.updateMany({_id:cryptr.decrypt(req.params.businessTypeId)},{$set: {'type':0}},function(err, docs){ });
                    artistCategory.updateMany({serviceId:cryptr.decrypt(req.params.businessTypeId)},{$set: {'status':1}},function(err, docs){ });
                    artistsubCategory.updateMany({subServiceId:cryptr.decrypt(req.params.categoryId)},{$set: {'status':1}},function(err, docs){ });
                    userCategory.deleteMany({'categoryId':cryptr.decrypt(req.params.categoryId),'businessTypeId':{'$ne':0}}, function(err, results){ });
                    userCategory.deleteMany({'categoryId':cryptr.decrypt(req.params.businessTypeId),'businessTypeId':0}, function(err, results){ });

                   deviceType  = bdata[0].userInfo[0].deviceType;
                   token       = bdata[0].userInfo[0].firebaseToken;
                   receiverId  = bdata[0].userInfo[0]._id;

                    querySecound = addNotification.findOne().sort([['_id', 'descending']]).limit(1);

                    body = 'Category has been approved by admin.';
                    title = 'Admin Verification';
                    type = 29;

                    notification = { title:title,body: body,notifincationType:type,sound: "default"};                                   
                    data = {title:title,body: body,notifincationType:type }; 

                    insertData = {

                        'senderId' : 1,
                        'receiverId' : receiverId,
                        'notifincationType' : type,
                        'crd' : moment().format(),
                        'upd' : moment().format(),
                        'notifyId' : receiverId,
                        'type' : 'booking'
                    };                                     
                    querySecound.exec(function(err, result) {

                        if (result) {
                            insertData._id = result._id + 1;
                        }else{
                            insertData._id = 1;
                        }


                        addNotification(insertData).save(function(err, notifyData) {
                            
                            if(deviceType =='3'){
                            
                               // notify.sendWebNotification(receiverId,webData);
                            
                            }else{
                                notify.bedgeCount(receiverId,'booking','businessBookingBadgeCount');
                                notify.sendNotification(token,notification,data,receiverId,'businessBookingBadgeCount');
                            }

                            req.flash("success","Category approved successfully");
                            res.redirect('/listSubCategory');
                        });
                  
                    }); 
                    
                };
        });

    });


}

function jsUcfirst(string) 

{

    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

}