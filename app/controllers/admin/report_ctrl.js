var constanPath = require('../../../config/envConstants.js');
var commonConstants =   require('../../../config/commanConstants');
var report  = require('../../models/front/report_model');
var userReport  = require('../../models/front/userReport_model.js');
var booking             =   require('../../models/front/booking.js');
var paymentDetail       = require('../../models/front/paymentDetail.js');
//var stripe              =   require('../front/stripe');
var stripe             =    require('stripe')(constanPath.STRIPE_KEY);
var url     = require('url');
var moment  =   require('moment');
var cryptr  = require('cryptr');
cryptr      = new cryptr('1234567890');



exports.reportList = function(req, res) {

	res.render('admin/reportList.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session,
            cryptr   : cryptr
    });
	 
}


exports.listReport = function(req, res) {


    var query = report.aggregate([

        { 

            $sort:{'_id' : -1}

        },
                                 
        {
            "$lookup": {
                "from": "users",
                "localField": "reportByUser",
                "foreignField": "_id",
                "as": "byUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "reportForUser",
                "foreignField": "_id",
                "as": "forUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "businessId",
                "foreignField": "_id",
                "as": "businessInfo"
            }
        },
        {
            "$lookup": {
                "from": "artistservices",
                "localField": "serviceId",
                "foreignField": "_id",
                "as": "artistService"
            }
        },
        {
            "$project": {
                "_id": 1,
                "title":1,
                "description":1,
                "crd":1,
                "serviceId":1,
                "reportByUser":1,
                "reportForUser":1,
                "bookingId":1,
                "businessId":1,
                "status":1,
                "favored":1,
                "adminReason":1,
                "artistService._id":1,
                "artistService.title":1,
                "byUser._id": 1,
                "byUser.userName": 1,
                "byUser.profileImage":1,
                "forUser._id": 1,
                "forUser.userName": 1,
                "forUser.profileImage":1,
                "businessInfo._id": 1,
                "businessInfo.userName": 1,
                "businessInfo.businessName": 1,
                "businessInfo.profileImage":1,
               
            }
        }

    ]);

    query.exec(function(err, data) {
        
            if (err) throw err;
            if(data.length){
                i=0;
                 data.forEach(function(rs) { 

                    if(rs.forUser[0].profileImage){
                        rs.forUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.forUser[0].profileImage; 
                    }else{
                        rs.forUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }
                    if(rs.byUser[0].profileImage){
                        rs.byUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.byUser[0].profileImage; 
                    }else{
                        rs.byUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }
                    if(rs.businessInfo.length!=0){

                        if(rs.businessInfo[0].profileImage){
                            rs.businessInfo[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.businessInfo[0].profileImage; 
                        }else{
                            rs.businessInfo[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                        }

                    }else{

                        rs.businessInfo = [{
                            'id' : '',
                            'userName' : 'Test',
                            'profileImage' : 'http://www.cubaselecttravel.com/Content/images/default_user.png'
                        }];
                    }

            });

        }
            var totalStudents = data.length,
            pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/report_list.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr,
                moment : moment
        });

});
     
}
exports.userStatus = function(req, res){

    User.update({_id:cryptr.decrypt(req.params.categoryId)}, 
        {$set: {status:cryptr.decrypt(req.params.status),deviceToken:'',firebaseToken:''}},
        function(err, docs){
            if(err) res.json(err);
            else    {
                
                req.flash("success","");

                if(cryptr.decrypt(req.params.status)=="1"){

                    req.flash("success","User active successfully");

                }else{

                   req.flash("success","User inactive successfully"); 
                }
                res.redirect('/customer');
            };
    });


}

exports.bookingReport = function(req, res, next) {

    var id = req.params.id ? cryptr.decrypt(req.params.id) : req.query.id ? cryptr.decrypt(req.query.id) : req.body.BookingId;
    var query = report.aggregate([

        { 

            $sort:{'_id' : -1}

        },
                                 
        {
            "$lookup": {
                "from": "users",
                "localField": "reportByUser",
                "foreignField": "_id",
                "as": "byUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "reportForUser",
                "foreignField": "_id",
                "as": "forUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "businessId",
                "foreignField": "_id",
                "as": "businessInfo"
            }
        },
        {
            "$lookup": {
                "from": "artistservices",
                "localField": "serviceId",
                "foreignField": "_id",
                "as": "artistService"
            }
        },
        {
            $match : {'bookingId':Number(id)}
        },
        {
            "$project": {
                "_id": 1,
                "title":1,
                "description":1,
                "crd":1,
                "serviceId":1,
                "reportByUser":1,
                "reportForUser":1,
                "bookingId":1,
                "businessId":1,
                "status":1,
                "favored":1,
                "adminReason":1,
                "artistService._id":1,
                "artistService.title":1,
                "byUser._id": 1,
                "byUser.userName": 1,
                "byUser.profileImage":1,
                "forUser._id": 1,
                "forUser.userName": 1,
                "forUser.profileImage":1,
                "businessInfo._id": 1,
                "businessInfo.userName": 1,
                "businessInfo.businessName": 1,
                "businessInfo.profileImage":1,
               
            }
        }

    ]);

    query.exec(function(err, data) {
        
            if (err) throw err;
            if(data.length){
                i=0;
                 data.forEach(function(rs) { 

                    if(rs.forUser[0].profileImage){
                        rs.forUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.forUser[0].profileImage; 
                    }else{
                        rs.forUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }
                    if(rs.byUser[0].profileImage){
                        rs.byUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.byUser[0].profileImage; 
                    }else{
                        rs.byUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }

                    if(rs.businessInfo[0].profileImage){
                        rs.businessInfo[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.businessInfo[0].profileImage; 
                    }else{
                        rs.businessInfo[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }

            });
        }

        rData = data;
        next();           

    });
     
}

exports.reportDetail = function(req, res, next) {

    var id = req.body.id;

    var query = report.aggregate([

        { 

            $sort:{'_id' : -1}

        },
                                 
        {
            "$lookup": {
                "from": "users",
                "localField": "reportByUser",
                "foreignField": "_id",
                "as": "byUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "reportForUser",
                "foreignField": "_id",
                "as": "forUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "businessId",
                "foreignField": "_id",
                "as": "businessInfo"
            }
        },
        {
            "$lookup": {
                "from": "artistservices",
                "localField": "serviceId",
                "foreignField": "_id",
                "as": "artistService"
            }
        },
        {
            "$lookup": {
                "from": "bookings",
                "localField": "bookingId",
                "foreignField": "_id",
                "as": "booking"
            }
        },
        {
            $match : {'_id':Number(id)}
        },
        {
            "$project": {
                "_id": 1,
                "title":1,
                "description":1,
                "crd":1,
                "serviceId":1,
                "reportByUser":1,
                "reportForUser":1,
                "bookingId":1,
                "businessId":1,
                "status":1,
                "artistService._id":1,
                "artistService.title":1,
                "byUser._id": 1,
                "byUser.userName": 1,
                "byUser.profileImage":1,
                "byUser.firebaseToken":1,
                "byUser.appType":1,
                "forUser._id": 1,
                "forUser.appType": 1,
                "forUser.userName": 1,
                "forUser.profileImage":1,
                "forUser.firebaseToken":1,
                "businessInfo._id": 1,
                "businessInfo.userName": 1,
                "businessInfo.firebaseToken": 1,
                "businessInfo.businessName": 1,
                "businessInfo.profileImage":1,
                "businessInfo.firebaseToken":1,
                "businessInfo.appType":1,
                "booking.userId":1,
                "booking.artistId":1,
                "booking.paymentStatus":1,
                "booking.paymentType":1,
               
            }
        }

    ]);

    query.exec(function(err, data) {
        rData = data[0];

        next();           

    });
     
}


exports.createReportOld = function(req, res){

    let id = req.body.id;
    let explanation = req.body.explanation;
    let favored = req.body.favored;
    

    report.update({_id:id},{$set: {status:'2','adminReason':explanation,'favored':favored}}, function(err, docs){

        if(err) res.json(err);
        else    {

            if(rData.byUser[0]._id==rData.booking[0].userId){

                let = cName = rData.byUser[0].userName;

            }else{

                 let = cName = rData.forUser[0].userName;
            }

            if(rData.byUser[0]._id==rData.booking[0].artistId){

               let = aName = rData.byUser[0].userName;

            }else{

                let = aName = rData.forUser[0].userName;

            }
            name = (favored=="Customer") ? cName : aName;


            body = 'Admin has reviewed the report for booking & favored to '+name;
            title = 'Thank you for reporting';
            type = 25;

         
            notification = { 
                title:title,
                body: body,
                notifincationType:type,
                notifyId : rData.bookingId,
                sound: "default",
                click_action:"ChatActivity"
 
            };                                   
            data = {
                title:title,
                body: body,
                notifincationType:type,
                notifyId : rData.bookingId,
                click_action:"ChatActivity"

            }; 

            if(rData.byUser[0]._id != rData.businessInfo[0]._id && rData.forUser[0]._id != rData.businessInfo[0]._id){

                token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken,rData.businessInfo[0].firebaseToken];
              
                notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                notify.sendNotification(rData.byUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                notify.bedgeCount(rData.businessInfo[0]._id,'booking','businessBookingBadgeCount');
                notify.sendNotification(rData.businessInfo[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

            }else{

                token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken];

                notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                notify.sendNotification(rData.byUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

            }            
         
         //   notify.sendNotificationMultiple(token,notification,data);

           //req.flash("success","Booking report updated successfully");
            res.redirect('/bookingview/'+cryptr.encrypt(rData.bookingId));
        };
    });

}
exports.createReport = function(req, res){
    let receiverId = 1; 
    let id = req.body.id;
    let explanation = req.body.explanation;
    let favored = req.body.favored;

   // console.log(rData);return;

    report.update({_id:id},{$set: {status:'2','adminReason':explanation,'favored':favored}}, function(err, docs){

        if(err) res.json(err);
        else    {

                    if(favored=="Customer" && rData.booking[0].paymentStatus==1 && rData.booking[0].paymentType==1){
                      // var stripe          = require('stripe')('sk_test_0RIcHEnw2dna6r9qEk1yLWDU006s0c6hT4');
                     //  var stripe           = require('stripe')('sk_live_yIqrIhWPMHynsvs4W23NJbaB00WgrhEygt');
                        paymentDetail.findOne({'bookingId':rData.bookingId}).exec(function(err, pData) {

                            if(pData){

                                if(pData.firstAdminPay.id){

                                    data = {
                                        charge: pData.firstAdminPay.id
                                    };

                                    stripe.refunds.create(data, function(err, charge) {

                                        if(err){

                                            res.json({'status':'fail','message': err});
                                            return;
                                        }

                                        if(charge){


                                            tranId = charge.id;

                                            booking.updateMany({'_id':rData.bookingId},{$set: {transjectionId:tranId,paymentStatus:3}}, function(err, docs){
                                            
                                                paymentDetail.updateMany({'_id':pData._id},{$set: {refundPay:charge}}, function(err, docs){

                                                   if(rData.byUser[0]._id==rData.booking[0].userId){

                                                        let = cName = rData.byUser[0].userName;

                                                    }else{

                                                         let = cName = rData.forUser[0].userName;
                                                    }

                                                    if(rData.byUser[0]._id==rData.booking[0].artistId){

                                                       let = aName = rData.byUser[0].userName;

                                                    }else{

                                                        let = aName = rData.forUser[0].userName;

                                                    }
                                                    name = (favored=="Customer") ? cName : aName;

                                                    body = 'Admin has reviewed the report for booking & favored to '+name;
                                                    bodyBy = 'Admin has reviewed the report for booking & favored to '+name+'. Refund will be transferred to your original payment mode within 5-10 business days';
                                                    title = 'Thank you for reporting';
                                                    type = 25;
                                                    console.log(body);
                                                 
                                                    notification = { 
                                                        title:title,
                                                        body: body,
                                                        notifincationType:type,
                                                        notifyId : rData.bookingId,
                                                        sound: "default",
                                                        click_action:"ChatActivity"
                                         
                                                    };                                   
                                                    data = {
                                                        title:title,
                                                        body: body,
                                                        notifincationType:type,
                                                        notifyId : rData.bookingId,
                                                        click_action:"ChatActivity"

                                                    };

                                                    notificationBy = { 
                                                        title:title,
                                                        body: bodyBy,
                                                        notifincationType:type,
                                                        notifyId : rData.bookingId,
                                                        sound: "default",
                                                        click_action:"ChatActivity"
                                         
                                                    };                                   
                                                    dataBy = {
                                                        title:title,
                                                        body: bodyBy,
                                                        notifincationType:type,
                                                        notifyId : rData.bookingId,
                                                        click_action:"ChatActivity"

                                                    };  



                                                    if(rData.byUser[0]._id != rData.businessInfo[0]._id && rData.forUser[0]._id != rData.businessInfo[0]._id){

                                                        token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken,rData.businessInfo[0].firebaseToken];
                                                      
                                                        notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                                                        notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                                        notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                                                        notify.sendNotification(rData.byUser[0].firebaseToken,notificationBy,dataBy,receiverId,'businessBookingBadgeCount');

                                                        notify.bedgeCount(rData.businessInfo[0]._id,'booking','businessBookingBadgeCount');
                                                        notify.sendNotification(rData.businessInfo[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                                    }else{

                                                        token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken];

                                                        notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                                                        notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                                        notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                                                        notify.sendNotification(rData.byUser[0].firebaseToken,notificationBy,dataBy,receiverId,'businessBookingBadgeCount');

                                                    }
                                                     res.redirect('/bookingview/'+cryptr.encrypt(rData.bookingId));   

                                                }); 

                                            });
                                        }
                                    });

                                }
                            }

                        });

                    }else{

                            if(rData.byUser[0]._id==rData.booking[0].userId){

                                let = cName = rData.byUser[0].userName;

                            }else{

                                 let = cName = rData.forUser[0].userName;
                            }

                            if(rData.byUser[0]._id==rData.booking[0].artistId){

                               let = aName = rData.byUser[0].userName;

                            }else{

                                let = aName = rData.forUser[0].userName;

                            }
                            name = (favored=="Customer") ? cName : aName;
                            console.log("old"); 
                            body = 'Admin has reviewed the report for booking & favored to '+name;
                            title = 'Thank you for reporting';
                            type = 25;

                         
                            notification = { 
                                title:title,
                                body: body,
                                notifincationType:type,
                                notifyId : rData.bookingId,
                                sound: "default",
                                click_action:"ChatActivity"
                 
                            };                                   
                            data = {
                                title:title,
                                body: body,
                                notifincationType:type,
                                notifyId : rData.bookingId,
                                click_action:"ChatActivity"

                            }; 



                            if(rData.byUser[0]._id != rData.businessInfo[0]._id && rData.forUser[0]._id != rData.businessInfo[0]._id){

                                token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken,rData.businessInfo[0].firebaseToken];
                              
                                notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                                notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                                notify.sendNotification(rData.byUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                notify.bedgeCount(rData.businessInfo[0]._id,'booking','businessBookingBadgeCount');
                                notify.sendNotification(rData.businessInfo[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                            }else{

                                token = [rData.forUser[0].firebaseToken,rData.byUser[0].firebaseToken];

                                notify.bedgeCount(rData.forUser[0]._id,'booking','businessBookingBadgeCount');
                                notify.sendNotification(rData.forUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                                notify.bedgeCount(rData.byUser[0]._id,'booking','businessBookingBadgeCount');
                                notify.sendNotification(rData.byUser[0].firebaseToken,notification,data,receiverId,'businessBookingBadgeCount');

                            }            
                         
                            res.redirect('/bookingview/'+cryptr.encrypt(rData.bookingId));

                    }

        };
    });

}


exports.admin_user_report = function(req, res) {

    res.render('admin/userReportList.ejs', {
            error : req.flash("error"),
            success : req.flash("success"),
            session : req.session,
            cryptr   : cryptr
    });
     
}


exports.listUserReport = function(req, res) {


    var query = userReport.aggregate([

        { 

            $sort:{'_id' : -1}

        },
                                 
        {
            "$lookup": {
                "from": "users",
                "localField": "reportByUser",
                "foreignField": "_id",
                "as": "byUser"
            }
        },
        {
            "$lookup": {
                "from": "users",
                "localField": "reportForUser",
                "foreignField": "_id",
                "as": "forUser"
            }
        },
        {
            "$project": {
                "_id": 1,
                "title":1,
                "description":1,
                "crd":1,
                "serviceId":1,
                "reportByUser":1,
                "reportForUser":1,
                "bookingId":1,
                "businessId":1,
                "status":1,
                "favored":1,
                "adminReason":1,
                "byUser._id": 1,
                "byUser.userName": 1,
                "byUser.profileImage":1,
                "forUser._id": 1,
                "forUser.userName": 1,
                "forUser.profileImage":1,
               
            }
        }

    ]);

    query.exec(function(err, data) {
        
            if (err) throw err;
            if(data.length){
                i=0;
                 data.forEach(function(rs) { 

                    if(rs.forUser[0].profileImage){
                        rs.forUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.forUser[0].profileImage; 
                    }else{
                        rs.forUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }
                    if(rs.byUser[0].profileImage){
                        rs.byUser[0].profileImage =  commonConstants.S3_BASE_PATH+constanPath.AWS_S3_BUCKET_NAME+'/'+commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR+rs.byUser[0].profileImage; 
                    }else{
                        rs.byUser[0].profileImage = 'http://www.cubaselecttravel.com/Content/images/default_user.png';
                    }

            });

        }
            var totalStudents = data.length,
            pageSize = 10,
            olddata = data.length/pageSize;
            newdata =  Math.round(data.length/pageSize);
            if(newdata<olddata)
            {
                newdata = (newdata)+1;

            }
            pageCount = newdata,
            currentPage = 1,
            students = [],
            studentsArrays = [], 
            studentsList = []; 
   
            //split list into groups
            while (data.length > 0) {
                studentsArrays.push(data.splice(0, pageSize));
            }

            //set current page if specifed as get variable (eg: /?page=2)
            if (typeof req.query.page !== 'undefined') {
                currentPage = +req.query.page;
            }

            //show list of students from group
            studentsList = studentsArrays[+currentPage - 1];

            
            res.render('admin/userReport_list.ejs', {
                error : req.flash("error"),
                success : req.flash("success"),
                session : req.session,
                categorydata: studentsList,
                pageSize: pageSize,
                totalStudents: totalStudents,
                pageCount: pageCount,
                currentPage: currentPage,
                cryptr : cryptr,
                moment : moment
        });

});
     
}