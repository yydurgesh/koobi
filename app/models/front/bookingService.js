var mongoose = require('mongoose');
var bookServiceSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	userId: Number,
	artistId: Number,
	staff: { type: Number, default: 0 },
	serviceType:{ type: Number}, // 1:Incall 2: outCall
	bookingId:{ type: Number, default: 0 },
	bookingPrice:{ type: String, default: '' },
	serviceId: Number,
	subServiceId: Number,
	artistServiceId: Number,
	startTime:{ type: String},
	endTime:{ type: String},
	endTimeBiz:{ type: String},
	bookingDate:{ type: String}, //yyyy-mm-dd
	bookingStatus:{ type: String, default: 0},
	bookingType:{ type: String, default: 'online'},
	status:{ type: Number, default: 0},//0:pending,1:accept,2:start,3:complete
	timeCount:{ type: Number, default: 0},
	crd: { type: Date, default: new Date() },
	upd: { type: Date, default: new Date() }

});
module.exports = mongoose.model('bookingService',bookServiceSchema);
