var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagSchema = new Schema ({
	_id:{ type: Number,default: 1 },
	voucherCode: { type: String,default:"" },
	discountType:{ type: String,default:"" },
	amount:{ type: String,default:"" },
	startDate:{ type: String,default:"" },
	endDate:{ type: String,default:"" },
	artistId:{ type: Number,default:1 },
	status:{ type: Number,default:1 },
	deleteStatus:{ type: Number,default:1 },
	
});

module.exports = mongoose.model("voucher",tagSchema);