var mongoose = require('mongoose');
//mongoose.set('debug', true);

var bcrypt   = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var FeedSchema = new Schema ({
	_id:{ type: Number,default: 1 },
	userId: Number,
	feedType: { type: String,default:"" },
	feedData:{ type: Array,default:"" },
	caption: { type: String,default:"" },
	city:{ type: String,default:"" },
	country:{ type: String,default:"" },
	location:{ type: String,default:"" },
	view:{ type:Number,default:0 },
	likeCount:{type:Number,default:0 },
	sharelikeCount:{type:Number,default:0 },
	commentCount:{ type:Number,default:0},
	impressionCount:{type:Number,default:0},
	tagId:{type: Object,default:{}},
	peopleTag:{type: Object,default:{}},
	serviceTag:{type: Object,default:{}},
	serviceTagId:{type: Object,default:{}},
	businessTypeServiceTagId:{type: Object,default:{}},
	artistServiceId:{type: Object,default:{}},
	oucallOrIncall:{type: Object,default:{}},
	engagement:{type:Number,default:0},
	status:{type:Number,default:1},
	deleteStatus:{type:Number,default:1},
	feedImageRatio:{type:Number,default:1},
	crd: { type: String, default:'' },
	upd: { type: String, default:'' },
	latitude: { type: String, default:'' },
	longitude: { type: String, default:'' },
	geoLocation:{type: Object, index: "2dsphere", default:{}}  //we are using this lat long for radius search
	
});

module.exports = mongoose.model("feed",FeedSchema);