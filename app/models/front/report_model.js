var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	reportByUser: Number,
	reportForUser: Number,
	bookingId: Number,
	bookingInfoId: Number,
	businessId: Number,
	serviceId: Number,
	title:{ type: String,default:"" },
	description:{ type: String,default:"" },
	adminReason:{ type: String,default:"" },
	favored:{ type: String,default:"" },
	status:{ type: Number,default:1 },
	crd: { type: Date, default: new Date() },
	upd: { type: Date, default: new Date() }
});
module.exports = mongoose.model('bookingReport',userSchema);
