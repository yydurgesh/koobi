var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagSchema = new Schema ({
	_id:{ type: Number,default: 1 },
	userId: { type: Number,default:"" },
	folderName:{ type: String,default:"" },
	status:{ type: Number,default:1 },
	crd: { type: String, default:"" },
	upd: { type: String, default:"" }
	
});

module.exports = mongoose.model("folder",tagSchema);