var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = new Schema ({
	_id:{ type: Number,default: 1 },
	artistId:{ type: Number,default:"" },
	notifincationType:{ type: Number,default:""}, //1:firstReminder 2: secoundReminder 3: :third reminder
   	message:  {type: String, default:""},
   	currentDate:  {type: String, default:""},
   	crd:  {type: String, default:new Date()},
	upd:  {type: String, default:new Date()}
	
});

module.exports = mongoose.model("adminReminder",notificationSchema);