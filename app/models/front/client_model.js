var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var commentSchema = new Schema ({
	_id		 	:{ type: Number, default: 1 },
	firstName 	:  {type:String,default:''},
	lastName  	:  {type: String,default:''},
	userName  	:  {type: String,default:''},
	email	  	:  {type: String,default:''},
	phone	  	:  {type: String,default:''},
	artistId    :  {type:Number,default:0},
	crd			:  { type: String, default:'' },
    upd			:  { type: String, default:'' }
});

module.exports = mongoose.model("client", commentSchema);