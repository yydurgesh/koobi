var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	businessId: Number,
	staffId: Number,
	holidayGiven: Number,
	startDate:{ type: String,default:''},
	endDate:{ type: String,default:''},
	holiday:{ type: String,default:''},
	reason:{ type: String,default:1},
	status:{type: Number,default:0},
	staffMainId:{type: Number,default:0},
	crd: { type: Date, default: new Date() },
	upd: { type: Date, default: new Date() }

});

module.exports = mongoose.model('holiday',userSchema);