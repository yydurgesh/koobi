var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	artistId: Number,
	userId: Number,
	bookingId: Number,
	rating:{ type: Number,default:"" },
	review:{ type: String,default:"" },
    ratedUsrTyp:{ type: String,default:"" },// artist id  type when rating give  social or biz
	ratingDate:{ type: String,default:"" }
});
module.exports = mongoose.model('ratingReview',userSchema);
