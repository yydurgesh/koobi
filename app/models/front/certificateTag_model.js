var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	artistId: Number,
	title:{ type: String,default:"" },
});
module.exports = mongoose.model('certificateTag',userSchema);
