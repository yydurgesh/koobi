var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagSchema = new Schema ({
	_id:{ type: Number,default: 1 },
	userId: { type: Number,default:"" },
	feedId:{ type: Number,default:"" },
	folderId:{ type: Number,default:"" },
	status:{ type: Number,default:1 },
	crd: { type: String, default:"" },
	upd: { type: String, default:"" }
	
});

module.exports = mongoose.model("file",tagSchema);