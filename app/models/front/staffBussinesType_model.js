var mongoose = require('mongoose');
var userSchema = mongoose.Schema({
	_id:{ type: Number, default: 1 },
	staffId:{type: Number,default:0},
	businessId:{type: Number,default:0},
	artistId:{type: Number,default:0},
	serviceId:{type: Number,default:0},
	serviceName:{ type: String,default:''},
});
module.exports = mongoose.model('staffbussinessType',userSchema);