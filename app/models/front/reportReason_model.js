
//app/models/user.js
//load the things we need
var mongoose = require('mongoose');

//define the schema for our user model
var userSchema = mongoose.Schema({	
	_id:{ type: Number, default: 1 },
	title: { type: String,default:"" }
});


//methods ======================


//create the model for userCategory and expose it to our app
module.exports = mongoose.model('reportReason', userSchema);